// next.config.js 
// const withCSS = require('@zeit/next-css')
const withPWA = require('next-pwa');

// module.exports =
//   withPWA(
//     withCSS({

//       module: {
//         loaders: [
//           {
//             test: /\.(sass|less|css)$/,
//             loaders: ['style-loader', 'css-loader', 'less-loader']
//           }
//         ],
//         cssModules: true,
//         cssLoaderOptions: {
//           importLoaders: 1,
//           localIdentName: "[local]___[hash:base64:5]",
//         }

//       },
//       devIndicators: {
//         autoPrerender: false,
//       },
//       env: {
//         GET_API_URL: 'https://www.niftytrader.in/niftyaccessapi/api/',
//         POST_API_URL: 'https://www.niftytrader.in/niftyaccessapi/api/',
//       },
//       apiUrl: {
//         NIFTY_OI_LIST: 'FinNiftyOI/niftyoilistData',
//       },
//       pwa: {
//         dest: "public"
//       }
//     })
//   );

module.exports =
  withPWA(
    {
      env: {
        GET_API_URL: 'https://api.niftytrader.in/api/',
        POST_API_URL: 'https://api.niftytrader.in/api/',
        OLD_SITE_URL: '/',
        POST_SIGNALR_API_URL: 'https://signalr.niftytrader.in/NiftySignalRTest/api/',
        GOOGLE_LOGIN_CLIENT_ID: '954001959236-0ql471rf6ghochvuq3h1n0cnfif7t4sb.apps.googleusercontent.com',
        FACEBOOK_LOGIN_CLIENT_ID: '1133718293349113',
        TOP_HEADER_MSG_DATA: 'AnalyticsStock/NiftyAlertGet',
        USER_PLAN_DATA: 'NForgotPass/user_order_membership_details',
        ADD_SYMBOL_FOR_SIGNALR: 'NiftySignalR/AddSymbolConnection',
        UPDATE_WATCHLIST_FOR_SIGNALR: 'NiftySignalR/UpdateWatchlist',
        UPDATE_WATCHLIST_CONNECTION_FOR_SIGNALR: 'NiftyPostAPI/update_watchlist_connection',
        META_DATA: 'OtherNifty/fetchSeoData',
        REGISTER_USER_DATA: 'NTLAuth/registration',
        LOGIN_USER_DATA: 'NTLAuth/user_login',
        CURRENT_USER_DATA: 'FinNiftyOI/user_data',
        FORGOT_PASSWORD_USER_DATA: 'NTLAuth/resetpassword',
        SYMBOL_LIST_DATA: 'SA/PSymbolsList',
        WL_SYMBOL_LIST_DATA: 'SA/WLstocks_list_stockSearch',
        STOCK_LIST_DATA: 'NIndex/stocks_list_api',
        STOCK_LIST_SEARCH_DATA: 'AnalyticsStock/stocks_list_searchBox',
        WL_SYMBOL_NFO_LIST_DATA: 'SA/WL_kite_instrument_nfo_list',
        SYMBOL_SPOT_DATA: 'FinNiftyOI/getsymboldata',
        HOME_STOCK_DATA: 'NIndex/IndexStocksData',
        HOME_BROKERAGE_SUBMIT_DATA: 'Resources/freeTipsService',
        HOME_FOREX_RATES_DATA: 'NIndex/getForexRate',
        NIFTY_OI_DATA: 'FinNiftyOI/niftyoilistData',
        NIFTY_CHANGE_OI_DATA: 'FinNiftyOI/niftyoichange',
        NIFTY_PCR_DATA: 'FinNiftyOI/niftypcrData',
        NIFTY_PCR_VOLUME_DATA: 'FinNiftyOI/finniftyVolumepcrData',
        OPTION_STOCK_CHART_DATA: 'SA/symboloidata',
        LIVE_MAX_PAIN_DATA: 'SA/PSymbolsDetailList',
        SGX_NIFTY_DATA: 'NIndex/sgxvalue',
        LIVE_MARKET_SCREENER_DATA: 'Screener/Get_Web_Screener_Tech_Financial',
        LIVE_MARKET_FILTER_LIST_DATA: 'Screener/view_Filter',
        SAVE_LIVE_MARKET_FILTER_DATA: 'Screener/create_Filter',
        UPDATE_LIVE_MARKET_FILTER_DATA: 'Screener/update_Filter',
        DELETE_LIVE_MARKET_FILTER_DATA: 'Screener/delete_user_screener',
        OPTIONS_SCREENER_DATA: "Screener/getoptionscreenerdata",
        OPTIONS_SCREENER_FILTER_LIST_DATA: 'Screener/getoptionSaveFilterData',
        SAVE_OPTIONS_SCREENER_FILTER_DATA: 'Screener/createoption_Filter',
        UPDATE_OPTIONS_SCREENER_FILTER_DATA: 'Screener/updateoption_Filter',
        DELETE_OPTIONS_SCREENER_FILTER_DATA: 'Screener/deleteoptionStockScreenerFilter',
        Live_Analytics_DATA: 'FinNiftyOI/GetLiveAnalysisNiftyandBank',
        Live_Analytics_CHART_DATA: 'FinNiftyOI/LivechartsBySymbol',
        RSS_FEED_DATA: 'Resources/GetRssFeedDataHome',
        PLANS_LIST_DATA: 'Plan/plans_list',
        WATCHLIST_LIST_DATA: 'WatchList/view_user_watchlists',
        WATCHLIST_DETAILS_DATA: 'WatchList/view_watchlist_details_nfo',
        ADD_WATCHLIST_DATA: 'WatchList/add_user_watchlist_auth',
        EDIT_WATCHLIST_DATA: 'WatchList/edit_user_watchlist_auth',
        DELETE_WATCHLIST_DATA: 'WatchList/delete_user_watchlist_auth',
        SET_DEFAULT_WATCHLIST_DATA: 'WatchList/SetWLasDefaultAuth',
        SYMBOL_ADD_TO_WATCHLIST_DATA: 'WatchList/edit_user_symbol_watchlist_auth',
        STOCK_ALERTS_DATA: 'SA/GetAlertListDataAuth',
        SAVE_STOCK_ALERTS_DATA: 'SA/SaveAlertAuth',
        DELETE_STOCK_ALERTS_DATA: 'SA/DeleteStockAlertAuth',
        GET_STOCK_ALERT_DATA: 'SA/GetAlertDataAuth',
        ADD_NEWSLATTER_DATA: 'NTLAuth/newsLetter',
        PARTICEPENT_WISE_OI_TABLE_DATA: 'Resources/pwOiTableViewData',
        PARTICEPENT_WISE_OI_CHART_DATA: 'Resources/pwOiChartData',
        NEWS_DATA: 'https://www.reddit.com/.rss',
        NIFTY50_STOCK_DATA: 'Resources/nifty50Stock',
        WATCHLIST_STOCK_CHART_DATA: 'IndexSgx/all_stocks_Intraday_Data',
        TOP_BROKERS: 'Resources/top_index_Broker',
        GAP_DATE_LIST_DATA: 'SA/gapupdates',
        GAP_UP_GAP_DOWNS_DATA: 'SA/gapAnalysis',
        NSE_OPTION_STOCK_DATA: 'SA/stocks_NseOption',
        NSE_OPTION_DATA: 'SA/fetchNseOptionData',
        FUTURES_CONTRACTS_DATA: 'SA/kite_instrument_nfo_list',
        SYMBOL_SPOT_DATA: 'FinNiftyOI/getsymbolSpotdata'
      },
      apiUrl: {
        NIFTY_OI_DATA: 'FinNiftyOI/niftyoilistData',
      },
      pwa: {
        dest: "public"
      }
    }
  )