var newdataarr = [[1598931120000, 210.55], [1598931240000, 210.55], [1598931360000, 210.55], [1598931480000, 211], [1598931480000, 211], [1598931660000, 211], [1598931840000, 211], [1598931960000, 209.95], [1598932080000, 209.75], [1598932200000, 210.4], [1598932320000, 210.9], [1598932440000, 210.25], [1598932560000, 210], [1598932680000, 209.95], [1598932800000, 210.55], [1598932920000, 210.75], [1598933040000, 211.4], [1598933160000, 211.55], [1598933280000, 211.95], [1598933400000, 211.95], [1598933520000, 212.05], [1598933640000, 211.8], [1598933760000, 210.9], [1598933880000, 210.15], [1598934000000, 209.6], [1598934120000, 208.85], [1598934240000, 210.05], [1598934360000, 210.1], [1598934480000, 210.45], [1598934600000, 210.9], [1598934720000, 210], [1598934840000, 210.15], [1598934960000, 210.4], [1598935080000, 210.5], [1598935200000, 210.4], [1598935320000, 210.3], [1598935440000, 211.1], [1598935560000, 210.9], [1598935680000, 210.9], [1598935800000, 209.05], [1598935920000, 208.95], [1598936040000, 208.9], [1598936160000, 209.25], [1598936280000, 209.25], [1598936400000, 209.05], [1598936520000, 209.1], [1598936640000, 208.95], [1598936760000, 208.9], [1598936880000, 209.3], [1598937000000, 209.35], [1598937120000, 209.35], [1598937240000, 209.45], [1598937360000, 209.4], [1598937480000, 209.5], [1598937600000, 209.35], [1598937720000, 209.15], [1598937840000, 209.15], [1598937960000, 209.2], [1598938080000, 209.5], [1598938200000, 209.3], [1598938320000, 209.3], [1598938440000, 209.3], [1598938560000, 209.05], [1598938680000, 209.05], [1598938800000, 209.45], [1598938920000, 208.6], [1598939040000, 208.75], [1598939160000, 209.1], [1598939280000, 209.1], [1598939400000, 208.55], [1598939520000, 208.7], [1598939640000, 209.15], [1598939760000, 209.6], [1598939880000, 209.9], [1598940000000, 210], [1598940120000, 209.7], [1598940240000, 209.95], [1598940360000, 209.95], [1598940480000, 210.2], [1598940600000, 210.05], [1598940720000, 209.35], [1598940840000, 209.4], [1598940960000, 209.3], [1598941080000, 209.25], [1598941200000, 208.85]]
Highcharts.stockChart('sparkline_chart3', {
    chart: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        type: 'area',
        margin: [2, 0, 2, 0],
        width: 120,
        height: 30,
        style: {
            overflow: 'visible'
        },
        skipClone: true
    },
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    xAxis: {
        labels: {
            enabled: false
        },
        title: {
            text: null
        },
        startOnTick: false,
        endOnTick: false,
        tickPositions: []
    },
    yAxis: {
        endOnTick: false,
        startOnTick: false,
        labels: {
            enabled: false
        },
        title: {
            text: null
        },
        tickPositions: []
    },
    legend: {
        enabled: false
    },
    tooltip: {
        hideDelay: 0,
        outside: true,
        shared: true,
        split: false,
        crosshairs: true,
        backgroundColor: 'rgb(220, 233, 247)',
        boxShadow: '0px 1px 2px 0px rgba(29, 30, 38, 0.2)',
        borderWidth: 0,
        style: {
            fontFamily: 'Montserrat',
            fontColor: 'rgba(29, 30, 38, 0.6)',
            fontWeight: 500,
        },
        formatter: function () {
            //debugger
            const points = this.points;
            // Get content for each tooltip
            let tooltips = points.map(v => {
                return `<span style="font-size:8px;"><b>${v.series.name}: ${v.y.toFixed(2)}</b></span>`
            });
            tooltips = tooltips.concat('');
            // Return array of tooltip content html strings
            return tooltips;
        }
    },
    rangeSelector: {
        enabled: false
    },
    plotOptions: {
        series: {
            animation: true,
            lineWidth: 1,
            shadow: false,

            states: {
                hover: {
                    lineWidth: 1
                }
            },
            marker: {
                radius: 1,
                states: {
                    hover: {
                        radius: 2
                    }
                }
            },
            fillOpacity: 0.25
        },
        column: {
            negativeColor: '#910000',
            borderColor: 'silver'
        }
    },
    navigator: {
        enabled: false
    },
    scrollbar: {
        enabled: false
    },
    series: [{
        // name: item,
        type: 'area',
        data: newdataarr,
        // gapSize: 5,
        tooltip: {
            valueDecimals: 2
        },
        lineWidth: 2,
        lineColor: 'rgb(83, 178, 110)',
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, 'rgb(83, 178, 110,0)'],
                [1, 'rgb(83, 178, 110,1)']
            ]
        },
        threshold: null
    }]
});
