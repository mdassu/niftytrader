// Header fixed on Scroll
var id;
if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
    id = "body";
} else {
    id = window;
}
$(id).bind("scroll", function () {
    if ($(id).scrollTop() > 400) {
        $("button.back-to-top").addClass("show");
        $(".feedback-box").css("right", "-2px");
        $(".socialSharingBtn").css("right", "15px");
        $(".socialShring").css("right", "12px");
        $(".socialSharingBtn").css("visibility", "visible");

        $(".h_header-top").addClass("fixScroll");
        $(".mobile-android").css("right", "12px");
    } else {
        $("button.back-to-top").removeClass("show");
        $(".feedback-box").css("right", "-50px");
        $(".socialShring").css("right", "-50px");
        $(".socialSharingBtn").css("right", "-50px");
        $(".socialSharingBtn").css("visibility", "hidden");
        $(".mobile-android").css("right", "-50px");
        $(".h_header-top").removeClass("fixScroll");
    }
});

$(".wsmenu .wsmenu-list li a").click(function () {
    $(".wsactive").removeClass();
});

$("#mobile-menu li").on("click", function () {
    $("#mobile-menu li").removeClass("mobile_active");
    $(this).addClass("mobile_active");
});

$('#userProfile').addClass('user-profile-nifty');
