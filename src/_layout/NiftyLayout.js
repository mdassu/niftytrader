import React, { Component } from "react";
import { connect } from "react-redux";
import HeadSection from "components/HeadSection";
import OtherSiteLinks from "components/OtherSiteLinks";
import MarketUpdates from "components/MarketUpdates";
import Header from "components/Header";
import SideMenu from "components/SideMenu";
import Footer from "components/Footer";
import Router from "next/router";
import Link from "next/link";
import { CALL_API } from "_services/CALL_API";
import { decrypt, encrypt } from "_helper/EncrDecrypt";
import Cookies from "js-cookie";
import { NewsService } from "_services/NewsService";
import { ToastContainer } from "react-toastify";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";
import { AuthenticationService } from "_services/AuthenticationService";
import { userData, userLoggedIn } from "_redux/actions";
// import Loader from "../Loader";

const mapStateToProps = (state) => ({
  currentUser: state.userData.userData,
});

const mapDispatchToProps = (dispatch) => ({
  setUserData: (data) => {
    dispatch(userData(data));
  },
  setUserLoggedIn: (data) => {
    dispatch(userLoggedIn(data));
  },
});

class NiftyLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currPath: "/",
      pinEnable: false,
      newsMarket: [],
      metaData: {},
      viewPlanStatus: false,
      primeMember: false,
      childrenLoaded: false,
    };
  }

  componentDidMount() {
    Cookies.remove("_currentUser");
    this.setState({
      currPath: Router.pathname,
      sideBar: false,
      sideBarOverlay: false,
    });
    this.getRSSFeed();

    if (!Cookies.get("_viewPlan")) {
      this.setState({
        viewPlanStatus: true,
      });
    }

    AuthenticationService.currentUser.subscribe((currentUser) => {
      if (currentUser != null) {
        if (currentUser["membership_flag"] == 1) {
          this.setState({
            primeMember: true,
          });
        } else {
          this.setState({
            primeMember: false,
          });
        }
      }
    });
    this.getCurrentUserData();
  }

  sidebarCollapse = () => {
    var sideBarOverlay;
    if (this.state.pinEnable) {
      sideBarOverlay = false;
    } else {
      sideBarOverlay = !this.state.sideBarOverlay;
    }
    this.setState({
      sideBar: !this.state.sideBar,
      sideBarOverlay: sideBarOverlay,
    });
  };

  changePin = (event) => {
    if (event.target.checked) {
      $("#sidebar").addClass("pin-active");
      $(".pin-menu small").hide(1000);
      $(".panel-heading a small").hide();
      $(".collapse").find("small").hide();
      $("#sidebar ul ul .sidebar-menu-link").addClass("pl-3");
      $("#sidebar").addClass("pl-1");
    } else {
      $("#sidebar").removeClass("pin-active");
      $(".pin-menu small").show(1000);
      // $('.overlay').show();
      // $('.overlay').removeClass('overlaybg');
      $(".panel-heading a small").show();
      $(".collapse").find("small").show();
      $("#sidebar ul ul .sidebar-menu-link").removeClass("pl-3");
      $("#sidebar").removeClass("pl-1");
    }
    this.setState({
      sideBarOverlay: !this.state.sideBarOverlay,
      pinEnable: !this.state.pinEnable,
    });
  };

  getRSSFeed() {
    CALL_API("GET", process.env.RSS_FEED_DATA, {}, (res) => {
      if (res.status) {
        var latestNews = [];
        res["data"]
          .sort((a, b) => {
            return new Date(b.publishDate) - new Date(a.publishDate);
          })
          .map((item, key) => {
            if (key <= 19) {
              latestNews.push({
                source: item.source,
                title: item.title,
                url: item.url,
              });
            }
          });
        NewsService.setLatestNews({ news: encrypt(latestNews) });
        var newsMarket = res["data"].filter((item) => {
          return item["source"] == "Business Standard";
        });
        this.setState({
          newsMarket: newsMarket,
        });
      }
    });
  }

  viewPlan = () => {
    Cookies.set("_viewPlan", encrypt({ status: true }), { expires: 1 });
    this.setState({
      viewPlanStatus: false,
    });
  };

  getCurrentUserData = () => {
    if (
      Object.keys(this.props.currentUser).length == 0 &&
      Cookies.get("_accessToken")
    ) {
      CALL_API("POST", process.env.CURRENT_USER_DATA, {}, (res) => {
        if (res.status) {
          let data = res.data;
          let userData = data.user_details;
          let planData = data.plan_data;
          let user = {
            name: userData["name"],
            city: userData["city"],
            country: userData["country"],
            date_of_birth: userData["date_of_birth"],
            email: userData["email"],
            gender: userData["gender"],
            industry: userData["industry"],
            membership_flag: data["membership_flag"],
            occupation: userData["occupation"],
            phone_no: userData["phone_no"],
            pincode: userData["pincode"],
            plan_type: planData["type"],
            state: userData["state"],
            user_id: userData["id"],
          };
          this.props.setUserData(user);
          this.props.setUserLoggedIn(true);
          this.setState({
            childrenLoaded: true,
          });
        }
      });
    } else {
      this.setState({
        childrenLoaded: true,
      });
    }
  };

  render() {
    const { metaData } = this.props;
    const {
      currPath,
      sideBar,
      sideBarOverlay,
      pinEnable,
      newsMarket,
      viewPlanStatus,
      primeMember,
      childrenLoaded,
    } = this.state;
    // this.getMetaData();
    var title = "";
    var keywords = "";
    var description = "";
    var canonical = "";
    if (metaData && metaData != "") {
      title = metaData["page_title"];
      keywords = metaData["page_keyword"];
      description = metaData["page_description"];
      canonical = metaData["page_canonical"];
    }
    return (
      <React.Fragment>
        {/* <div id='closebtn_Desktop_Only' className='stickyads_Desktop_Only'>
          <div id='div-DStickyAds'>
            <script dangerouslySetInnerHTML={
              {
                __html: `
                    googletag.cmd.push(function() {
                      googletag.display('div-DStickyAds')
                    });
                    `
              }
            } />
          </div><button className='btn_Desktop_Only' onclick='change_css_Desktop_Only()'>x </button>
        </div>
        <script dangerouslySetInnerHTML={
          {
            __html: `
                function change_css_Desktop_Only() {
                  document.getElementById('closebtn_Desktop_Only').style.cssText = 'display:none;'
                }
                    `
          }
        } />
        <div id='closebtn_Left' className='stickyads_Left'><div id='div-LDWebStickyAds'>
          <script dangerouslySetInnerHTML={
            {
              __html: `
                googletag.cmd.push(function() {
                  googletag.display('div-LDWebStickyAds')
                });
                    `
            }
          } />
        </div>
          <button className='btn_left' onclick='change_css_Left()'>x</button>
        </div>
        <script dangerouslySetInnerHTML={
          {
            __html: `
                function change_css_Left() {
                  document.getElementById('closebtn_Left').style.cssText = 'display:none;';
                }
                    `
          }
        } /> */}
        <ToastContainer />
        <HeadSection
          title={title}
          metaKeywords={keywords}
          metaDesc={description}
          metaCanonical={canonical}
        />
        <div className="wrapper">
          {/* <app id="right_btns">
            <div id="feedback_icon" className="feedback-btn">
              <button type="button" className="btn" title="Feedback"><img src="/images/chat-icon-2.png" alt="" /></button>
            </div>
            <div id="share_icon" className="feedback-btn share-icon-btn">
              <button type="button" className="btn" id="share_btn" title="Share"><img src="/images/share-icon-2.png" alt="" /></button>
              <div className="footer-social-icons share-social-links p-0">
                <ul>
                  <li><a
                    href="https://www.facebook.com/sharer/sharer.php?u=https://www.niftytrader.in&amp;amp;src=sdkpreparse"
                    target="_blank" title="Facebook" className="social-fackbook">
                    <img src="/images/facebook-app-symbol.svg" alt="" /></a></li>
                  <li><a href="https://twitter.com/share?url=https://www.niftytrader.in&amp;via=niftytraderin" target="_blank"
                    title="Twitter" className="social-twitter">
                    <img src="/images/twitter.svg" alt="" /></a></li>
                  <li><a
                    href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.niftytrader.in&amp;title=&amp;summary=&amp;source=niftytrader"
                    target="_blank" title="Linkedin" className="social-linkedin">
                    <img src="/images/linkedin.svg" alt="" /></a>
                  </li>
                  <li><a href="https://www.youtube.com/channel/UC6JvXGzemVCW9FMUjkLBLrA" target="_blank" title="Youtube"
                    className="social-youtube">
                    <img src="/images/youtube.svg" alt="" />
                  </a></li>
                </ul>
              </div>
            </div>
          </app> */}
          {sideBar ? (
            <SideMenu changePin={() => this.changePin} pinEnable={pinEnable} />
          ) : (
            ""
          )}
          <div id="content">
            <Header />

            <section className="section1">
              <div
                className={
                  currPath != "/" && currPath != "/primeplans"
                    ? "container p-0"
                    : ""
                }
              >
                {currPath != "/" &&
                currPath != "/watchlist" &&
                currPath != "/primeplans" ? (
                  <OtherSiteLinks />
                ) : (
                  ""
                )}
                {childrenLoaded}
                {this.props.children}
              </div>
            </section>
            <section className="footer_full_width">
              <Footer />
            </section>
          </div>
          {/* Scroll to top */}
          <div className="scroll-top-home" title="Go to Top">
            <ScrollUpButton />
          </div>
        </div>
        {viewPlanStatus && !primeMember ? (
          <div className="plans_sticky">
            <div className="container-fluid">
              <div className="row">
                <div className="col-12">
                  <div className="plans_sticky_box">
                    <h5>
                      <img src="/images/favicon.png" alt="Logo" />
                      NiftyTrader Prime
                    </h5>
                    <p>
                      Get access to the best of NiftyTrader (website/app)
                      features by becoming a prime member.
                    </p>
                    <h6>
                      <p className="prime_disc_label">
                        Get minimum 50% discount on all plans
                      </p>
                      <Link href="/primeplans">
                        <a title="View Plans" onClick={this.viewPlan}>
                          View Plans
                        </a>
                      </Link>
                    </h6>
                  </div>
                </div>
              </div>
            </div>
            <button className="close_plan_sticky" onClick={this.viewPlan}>
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="times"
                className="svg-inline--fa fa-times fa-w-11 "
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 352 512"
                width="9"
                height="14"
              >
                <path
                  fill="currentColor"
                  d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
                ></path>
              </svg>
            </button>
          </div>
        ) : (
          ""
        )}
        <div
          className={`overlay ${sideBarOverlay ? "active" : ""}`}
          id="overlay"
          onClick={this.sidebarCollapse}
        ></div>
        <script
          type="text/javascript"
          src="/js/jquery-3.5.1.slim.min.js"
        ></script>
        <script type="text/javascript" src="/js/popper.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/webslidemenu.js"></script>
        <script type="text/javascript" src="/js/custome.js"></script>
        <script
          type="text/javascript"
          src="/js/typeahead.jquery.min.js"
        ></script>
        {/* <!-- End Global Scripts -->
      <!-- Start Page Scripts --> */}
        <script
          type="text/javascript"
          src="/js/bootstrapValidator.min.js"
        ></script>
        <script type="text/javascript" src="/js/intlTelInput.min.js"></script>
        <script
          type="text/javascript"
          src="/js/intlTelInput-jquery.min.js"
        ></script>
        <script type="text/javascript" src="/js/owl.carousel.min.js"></script>

        <script>(adsbygoogle = window.adsbygoogle || []).push({})</script>
        {/* <!-- advertising tjeMooi0sMuBdSJT2CppbaqxzLM3hL9L9V6xILi8KR_IpUpfbOEWF8Db0ITdUFy-lT5bdCglmkNQcJAdlm-A2w==--> */}
        <script
          id="clevernt"
          dangerouslySetInnerHTML={{
            __html: `
          (function (document, window) {
            var c = document.createElement("script");
            c.type = "text/javascript"; c.async = !0; c.id = "CleverNTLoader49811";  c.setAttribute("data-target",window.name); c.setAttribute("data-callback","put-your-callback-macro-here");
            c.src = "//clevernt.com/scripts/9468947f5851b486c1080db9d9723606.min.js?20210409=" + Math.floor((new Date).getTime());
            var a = !1;
            try {
                a = parent.document.getElementsByTagName("script")[0] || document.getElementsByTagName("script")[0];
            } catch (e) {
                a = !1;
            }
            a || ( a = document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]);
            a.parentNode.insertBefore(c, a);
          })(document, window);
                    `,
          }}
        />
        {/* <!-- end advertising --> */}
      </React.Fragment>
    );
  }
}

// export default Layout;
export default connect(mapStateToProps, mapDispatchToProps)(NiftyLayout);
