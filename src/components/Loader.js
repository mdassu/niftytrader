export default function Loader() {
  return (
    <div id="preloader">
      <div className="lds-ripple">
        <div></div>
        <div></div>
      </div>
    </div>
  )
}