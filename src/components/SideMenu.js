import Link from 'next/link';

export default function SideMenu(props) {
  return (
    <nav id="sidebar" className={`active ${props.pinEnable ? 'pin-active pl-1' : ''}`}>
      <div className="menu-scrollbar">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <ul className="panel panel-default">
            <li className="panel-heading" role="tab" id="live_market">
              <a title="Live Market Screener" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                <img src="/images/live-market.png" alt="" /> <small style={{ display: props.pinEnable ? 'none' : '' }}>Live Market Screener</small>
              </a>
            </li>
          </ul>


          <ul className="panel panel-default">
            <li className="panel-heading" role="tab" id="stock_analysis">
              <a title="Stock Analysis" className={`collapsed sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <img src="/images/stock-analysis.png" alt="" /> <small style={{ display: props.pinEnable ? 'none' : '' }}>Stock Analysis</small>
              </a>
            </li>
          </ul>


          <ul className="panel panel-default ">
            <li className="panel-heading hovermenu">
              <a title="Nifty Live Analytics" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <img src="/images/nifty-live-analytics.png" alt="" /> <small style={{ display: props.pinEnable ? 'none' : '' }}>Nifty Live Analytics</small>
              </a>
              <ul className="panel-collapse main-submenu">
                <li className="panel-heading submenu-inside">
                  <a href="#" title="Options" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`}>
                    <img src="/images/nifty-options.png" alt="" />
                    <small style={{ display: props.pinEnable ? 'none' : '' }}>Options</small></a>
                  <ul className="inner-menu">
                    {/* <!-- Start Row --> */}
                    <div className="row">
                      <div className="col-12 col-lg-6">
                        <h5 className="inner-menu-title">Nifty OI</h5>
                        <div className="inner-menu-link">
                          <Link href="/live-nifty-open-interest">
                            <a title="Nifty Open Interest Tracker">
                              Nifty Open Interest Tracker
											      </a>
                          </Link>
                          <Link href="/nifty-live-change-in-oi">
                            <a title="Change in Open Interest">
                              Change in Open Interest
                            </a>
                          </Link>
                          <Link href="/nifty-put-call-ratio">
                            <a title="Put Call Ratio">
                              Put Call Ratio
											      </a>
                          </Link>
                          <Link href="/nifty-put-call-ratio-volume">
                            <a title="Volume PCR">Volume PCR</a>
                          </Link>
                        </div>
                      </div>
                      <div className="col-12 col-lg-6">
                        <h5 className="inner-menu-title">Option Strategy</h5>
                        <div className="inner-menu-link">
                          <a href="#" title="Stock Options Chart">Stock
                          Options Chart
											</a>
                          <a href="#" title="Stock Option Chain">Stock Option
                          Chain
											</a>
                          <a href="#" title="Live Max Pain Chart For Derivative Stocks">Live
                          Max Pain
                          Chart For Derivative Stocks
											</a>
                          <a href="#" title="Option Strategies to Mint Money">Option
                          Strategies to
                          Mint Money
											</a>
                          <a href="#" title="Option Strategy Optimizer"> <img src="/images/optimizer.png"
                            alt="" />
                          Option Strategy
                          Optimizer
											</a>
                        </div>
                      </div>
                      <div className="col-12 col-lg-6">
                        <h5 className="inner-menu-title">Bank Nifty OI</h5>
                        <div className="inner-menu-link">
                          <Link href="/banknifty-live-oi-tracker">
                            <a title="Open Interest Live Chart">
                              Open Interest Live Chart
											      </a>
                          </Link>
                          <Link href="/bank-nifty-live-oi-change">
                            <a title="Change In Open Interest">
                              Change In Open Interest
											      </a>
                          </Link>
                          <Link href="/banknifty-intra-pcr-trend">
                            <a title="PCR Live Chart">
                              PCR Live Chart
                            </a>
                          </Link>
                          <Link href="/banknifty-intra-volume-pcr-trend">
                            <a title="Volume PCR Live Chart">
                              Volume PCR Live Chart
											      </a>
                          </Link>
                        </div>
                      </div>
                      <div className="col-12 col-lg-6">
                        <h5 className="inner-menu-title">USD INR OI</h5>
                        <div className="inner-menu-link">
                          <Link href="/usd-inr-options-live">
                            <a title="Options Live">
                              <b className="label-new">New</b> Options Live
											      </a>
                          </Link>
                          <Link href="/usd-inr-change-open-interest-live">
                            <a title="Change in Open Interest Live">
                              <b className="label-new">New</b> Change in Open Interest Live
											      </a>
                          </Link>
                          <Link href="/usdinr-put-call-ratio">
                            <a title="Put Call Ratio">
                              <b className="label-new">New</b> Put Call Ratio
											      </a>
                          </Link>
                        </div>
                      </div>
                      <div className="col-12 col-lg-6">
                        <h5 className="inner-menu-title">Fin Nifty OI</h5>
                        <div className="inner-menu-link">
                          <Link href="/live-finnifty-open-interest">
                            <a title="Fin Nifty Open Interest Tracker">
                              Fin Nifty Open Interest Tracker
											      </a>
                          </Link>
                          <Link href="/finnifty-live-change-in-oi">
                            <a title="Fin Nifty Change in Open Interest">
                              Fin Nifty Change in Open Interest
											      </a>
                          </Link>
                          <Link href="/finnifty-put-call-ratio">
                            <a title="Fin Nifty Option Chain Put Call Ratio">
                              Fin Nifty Option Chain Put Call Ratio
                            </a>
                          </Link>
                          <Link href="/finnifty-put-call-ratio-volume">
                            <a title="Fin Nifty Option Chain Put Call Ratio Volume">
                              Fin Nifty Option Chain Put Call Ratio Volume
											      </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Row --> */}
                  </ul>
                </li>
                <li><a href="" title="Analytics" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`}> <img src="/images/nifty-anlytics.png" alt="" />
                  <small style={{ display: props.pinEnable ? 'none' : '' }}>Analytics</small></a></li>
                <li>
                  <a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} title="Resources"> <img src="/images/nifty-resources.png" alt="" />
                    <small style={{ display: props.pinEnable ? 'none' : '' }}>Resources</small></a>
                </li>
              </ul>
            </li>
          </ul>
          <ul className="panel panel-default">
            <li className="panel-heading" role="tab" id="sgx_nifty">
              <a title="Sgx Nifty" className={`collapsed sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                <img src="/images/sgx-nifty.png" alt="" /> <small style={{ display: props.pinEnable ? 'none' : '' }}>Sgx Nifty</small>
              </a>
            </li>
            {/* <!-- <ul id="collapseFive" className="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="sgx_nifty">
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                        </ul> --> */}
          </ul>
          <ul className="panel panel-default">
            <li className="panel-heading" role="tab" id="nifty_news">
              <a title="News" className={`collapsed sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                <img src="/images/nifty-news.png" alt="" /> <small style={{ display: props.pinEnable ? 'none' : '' }}>News</small>
              </a>
            </li>
            {/* <!-- <ul id="collapseSix" className="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="nifty_news">
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                        </ul> --> */}
          </ul>
          <ul className="panel panel-default">
            <li className="panel-heading" role="tab" id="nifty_news">
              <a title="Money" className={`collapsed sidebar-menu-link ${props.pinEnable ? 'pl-3' : ''}`} role="button" data-toggle="collapse"
                data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                <img src="/images/money.png" alt="" /> <small className="money-menu" style={{ display: props.pinEnable ? 'none' : '' }}>Money</small>
              </a>
            </li>
            {/* <!-- <ul id="collapseSeven" className="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="nifty_news">
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                            <li><a href="" className={`sidebar-menu-link ${props.pinEnable ? 'pl-3': ''}`}> <small style={{ display: props.pinEnable ? 'none' : '' }}>Home1</small></a></li>
                        </ul> --> */}
          </ul>
        </div>
      </div>
      <div className="pin-menu">
        <div className="custom-control custom-switch">
          <input type="checkbox" className="custom-control-input" id="pin_menu" onClick={props.changePin()} checked={props.pinEnable} />
          <label className="custom-control-label" htmlFor="pin_menu"><small style={{ display: props.pinEnable ? 'none' : '' }}>Pin Menu</small></label>
        </div>
      </div>
    </nav>
  )
}

