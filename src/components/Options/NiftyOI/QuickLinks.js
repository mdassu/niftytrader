import Link from 'next/link';

export function QuickLinks(props) {
  return (
    <div className="col-md-12 col-xl-8 col-lg-8 col-sm-12 col-12 p-0">
      {
        props.page == 'usdINR' ? (

          <ul className="nifty-chart-ul">
            <li><Link href='/usd-inr-options-live'><a className={props.index == 1 ? 'nifty-oi-active' : ''}>USD INR Options Live</a></Link></li>
            <li><Link href='/usd-inr-change-open-interest-live'><a className={props.index == 2 ? 'nifty-oi-active' : ''}>USD INR Change In Open Interest Live </a></Link></li>
            <li><Link href='/usdinr-put-call-ratio'><a className={props.index == 3 ? 'nifty-oi-active' : ''}>USD INR Put Call Ratio</a></Link></li>
          </ul>
        ) : (
            <ul className="nifty-chart-ul">
              <li><Link href={props.page == 'niftyOI' ? '/live-nifty-open-interest' : props.page == 'finNiftyOI' ? '/live-finnifty-open-interest' : '/banknifty-live-oi-tracker'}><a className={props.index == 1 ? 'nifty-oi-active' : ''} title="Open Interest">Open Interest</a></Link></li >
              <li><Link href={props.page == 'niftyOI' ? '/nifty-live-change-in-oi' : props.page == 'finNiftyOI' ? '/finnifty-live-change-in-oi' : '/bank-nifty-live-oi-change'}><a className={props.index == 2 ? 'nifty-oi-active' : ''} title="Change in OI">Change in OI </a></Link></li>
              <li><Link href={props.page == 'niftyOI' ? '/nifty-put-call-ratio' : props.page == 'finNiftyOI' ? '/finnifty-put-call-ratio' : '/banknifty-intra-pcr-trend'}><a className={props.index == 3 ? 'nifty-oi-active' : ''} title="Put Call Ratio">Put Call Ratio </a></Link></li>
              <li><Link href={props.page == 'niftyOI' ? '/nifty-put-call-ratio-volume' : props.page == 'finNiftyOI' ? '/finnifty-put-call-ratio-volume' : '/banknifty-intra-volume-pcr-trend'}><a className={props.index == 4 ? 'nifty-oi-active' : ''} title="Volume PCR">Volume PCR </a></Link></li>
              {
                props.index != 0 ? (
                  // <li><Link href={props.page == 'niftyOI' ? '/options-max-pain-chart-live/nifty' : props.page == 'finNiftyOI' ? '/options-max-pain-chart-live/finnifty' : '/options-max-pain-chart-live/banknifty'}><a className={props.index == 5 ? 'nifty-oi-active' : ''}>Live Max Pain</a></Link></li>
                  <li><a href={props.page == 'niftyOI' ? '/options-max-pain-chart-live/nifty' : props.page == 'finNiftyOI' ? '/options-max-pain-chart-live/finnifty' : '/options-max-pain-chart-live/banknifty'} className={props.index == 5 ? 'nifty-oi-active' : ''} title="Live Max Pain">Live Max Pain</a></li>
                ) : ''
              }
            </ul >
          )
      }
    </div >
  )
}