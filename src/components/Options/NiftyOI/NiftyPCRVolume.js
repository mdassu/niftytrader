import React, { Component } from "react";
import Head from "next/head";
import RightSection from "components/RightSection";
import { CALL_API, CALL_SIGNALR_API } from "_services/CALL_API";
import moment from "moment";
import Loader from "components/Loader";
import Link from "next/link";
import GroupLineChart from "components/Charts/GroupLineChart";
import { QuickLinks } from "./QuickLinks";
import NiftyPCRVolumeContent from "./NiftyOIContents/NiftyPCRVolumeContent";
import BankNiftyPCRVolumeContent from "./BankNiftyOIContents/BankNiftyPCRVolumeContent";
import FinNiftyPCRVolumeContent from "./FinNiftyOIContents/FinNiftyPCRVolumeContent";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import * as signalR from "@microsoft/signalr";
import SEOPageContent from "components/SEOPageContent";

class NiftyPCRVolume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      symbolSpotData: {},
      chartData: [],
      pcrValue: null,
      isChartLoaded: false,
      isLoading: true,
    };
    this.connection = "";
  }

  componentDidMount() {
    if (this.props.symbol) {
      var day = moment().format("dddd");
      var currentDate = moment().format("MM/DD/YYYY");
      var currentDateTime = moment().format();
      var checkDateTime = moment(currentDate + " 16:00:00").format();
      var checkStartDateTime = moment(currentDate + " 09:00:00").format();
      if (
        day != "Saturday" &&
        day != "Sunday" &&
        checkDateTime > currentDateTime &&
        checkStartDateTime < currentDateTime
      ) {
        this.connection = new signalR.HubConnectionBuilder()
          .withUrl(
            "https://signalr.niftytrader.in/NiftySignalRTest/niftySignalRHub"
          )
          .build();

        this.connection.on(
          "sendTickDataToAndroidAtSA",
          (InstrumentToken, SymbolName, TickData) => {
            var spotData = {
              high: TickData["high"],
              low: TickData["low"],
              nifty_value: TickData["lastPrice"],
              open: TickData["open"],
              previous_close: TickData["close"],
            };
            this.setState({
              symbolSpotData: spotData,
            });
          }
        );

        this.connection.start().then(() => {
          this.addSymbolForSignalR(this.connection.connectionId);
        });
      }
      this.getSymbolSpotData();
    }

    this.getPCRData();
  }

  getSymbolSpotData() {
    CALL_API(
      "POST",
      process.env.SYMBOL_SPOT_DATA,
      { symbol: this.props.symbol },
      (res) => {
        this.setState({
          symbolSpotData: res["data"],
        });
      }
    );
  }

  addSymbolForSignalR = (connectionId) => {
    CALL_SIGNALR_API(
      "GET",
      process.env.ADD_SYMBOL_FOR_SIGNALR,
      { Symbol: this.props.symbol, ConnectionId: connectionId },
      (res) => {}
    );
  };

  getPCRData() {
    var params = {
      reqType: this.props.requestType,
    };
    CALL_API("GET", process.env.NIFTY_PCR_VOLUME_DATA, params, (res) => {
      if (res.status) {
        var response = res["data"];

        this.setState({
          isChartLoaded: true,
          chartData: response,
          pcrValue: response[response.length - 1]["pcr"],
          isLoading: false,
        });
      } else {
        this.setState({
          isChartLoaded: true,
          chartData: [],
          pcrValue: 0,
          isLoading: false,
        });
      }
    });
  }

  componentWillUnmount = () => {
    if (this.props.symbol) {
      if (this.connection != "") {
        this.connection.stop();
      }
    }
  };

  render() {
    const { symbolSpotData, chartData, pcrValue, isChartLoaded, isLoading } =
      this.state;
    const { title, requestType, symbol, pageContent } = this.props;

    var columnName = ["PCR", "Nifty"];
    var columnKey = ["pcr", "index_close"];
    var chartTitle = "";
    var columnColor = ["#2196f3", "#f96c92"];

    if (symbol == "NIFTY 50") {
      chartTitle = `NIFTY DAILY PUT CALL RATIO(VOLUME PCR) TREND: ${
        pcrValue ? pcrValue : ""
      }`;
    } else if (symbol == "NIFTY BANK") {
      chartTitle = `BANK NIFTY DAILY PUT CALL RATIO(VOLUME PCR) TREND: ${
        pcrValue ? pcrValue : ""
      }`;
    } else {
      chartTitle = `FIN NIFTY DAILY PUT CALL RATIO(VOLUME PCR) TREND: ${
        pcrValue ? pcrValue : ""
      }`;
    }

    var niftyValue = symbolSpotData["nifty_value"];
    var niftyValueDiff = Number(
      symbolSpotData["nifty_value"] - symbolSpotData["previous_close"]
    ).toFixed(2);
    var changePer = Number(
      (niftyValueDiff / symbolSpotData["nifty_value"]) * 100
    ).toFixed(2);

    return (
      <React.Fragment>
        {/* {
          !isLoading ? "" : <Loader />
        } */}
        <div className="row">
          <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
            <div className="sidebar-search searchbar-responsive">
              <div className="form-group">
                <form action="">
                  <div className="ui-widget">
                    <input
                      placeholder="Search"
                      className="stocksearching autoComplete-ui ui-autoComplete-input"
                      autoComplete="off"
                    />
                    <span className="fa fa-search"></span>
                  </div>
                  <a
                    id="HideStockNavigator"
                    style={{ display: "none" }}
                    href="stocks-analysis/acc"
                    className="d-none"
                  ></a>
                </form>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-9 col-xl-9 col-12">
                <h1 className="main-page-heading">
                  {pageContent["page_Content_Title"]}
                </h1>
              </div>
              <div className="col-12 col-lg-3 col-xl-3">
                <div className="text-right">
                  <span
                    id="StockLTP"
                    style={{ fontSize: "15px" }}
                    className={
                      Math.sign(niftyValueDiff) == -1
                        ? "newstockLTPDown"
                        : "newstockLTPUP"
                    }
                  >
                    {niftyValue}
                  </span>{" "}
                  <FontAwesomeIcon
                    icon={
                      Math.sign(niftyValueDiff) == -1 ? faCaretUp : faCaretUp
                    }
                    className={
                      Math.sign(niftyValueDiff) == -1
                        ? "percent-red"
                        : "percent-green"
                    }
                    width="10"
                    height="16"
                  />{" "}
                  <b
                    className={
                      Math.sign(niftyValueDiff) == -1
                        ? "color-text-red"
                        : "color-text"
                    }
                    id="StockPriceChange"
                  >
                    {niftyValueDiff} ({changePer}%)
                  </b>
                </div>
              </div>
            </div>

            <div className="nifty-chart-panel">
              <div className="row m-0">
                <QuickLinks
                  page={
                    requestType == "niftyvolumepcr"
                      ? "niftyOI"
                      : requestType == "finniftyvolumepcr"
                      ? "finNiftyOI"
                      : "bankNiftyOI"
                  }
                  index={4}
                />
                <div className="col-md-12 col-sm-12 col-xl-4 col-lg-4 col-12 p-0">
                  <div className="select-dropdown nifty-chart-dropdown">
                    <div className="form-group">
                      {/* <label htmlFor="exampleFormControlSelect1 mr-1" className="mr-1">Date</label> */}
                      {/* <select id="exampleFormControlSelect1" onChange={this.changeDataByDate}>
                            <option value="Current">Current</option>
                            {
                              dateList && dateList.length > 0 ? dateListData : ''
                            }
                          </select> */}
                      <a
                        href={process.env.OLD_SITE_URL + "options-trading"}
                        className="nifty-table-icon float-right"
                      >
                        <span className="table-tooltip-box">Table View</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ height: "400px" }} id="container">
                {/* <img src="/images/chart.jpg" alt="" className="img-fluid" /> */}
                {isChartLoaded && pcrValue != null ? (
                  <GroupLineChart
                    chartData={chartData}
                    columnName={columnName}
                    columnKey={columnKey}
                    chartTitle={chartTitle}
                    columnColor={columnColor}
                    yAxisInterval={
                      requestType == "finniftyvolumepcr" ? 0.5 : 0.2
                    }
                  />
                ) : (
                  ""
                )}
              </div>
            </div>
            {requestType == "finniftyvolumepcr" ? (
              <FinNiftyPCRVolumeContent />
            ) : requestType == "niftyvolumepcr" ? (
              <NiftyPCRVolumeContent />
            ) : (
              <BankNiftyPCRVolumeContent />
            )}
            {pageContent && pageContent["page_Content"] != "" ? (
              <SEOPageContent pageContent={pageContent["page_Content"]} />
            ) : (
              ""
            )}
          </div>
          <RightSection />
        </div>
      </React.Fragment>
    );
  }
}

export default NiftyPCRVolume;
