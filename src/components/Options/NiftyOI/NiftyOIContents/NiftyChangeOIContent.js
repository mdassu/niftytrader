import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function NiftyChangeOIContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>
        Nifty Change in Open Interest – Explained Meaning of “Change in Open
        Interest”:
      </h2>
      <p>
        Change in Open Interest is equal to net increase or decrease in Open
        Interest for a particular option.
      </p>
      <h3>How to interpret “Change in Open Interest”?</h3>
      <p>
        Nifty Open Interest” or “Nifty Change in Open Interest” are two very
        reliable indicators to identify the ST direction of the market. When the
        smart money is bullish, they usually start writing Puts. And when the
        smart money is bearish, they prefer writing calls. Change in OI” can
        also be used to identify approximate support and resistance levels. If
        the OI has increased for 7500PE the most then it will imply that the
        option writers consider 7500 level as a strong support. On the other
        hand, if the highest increase in OI is for 7500CE, then it implies that
        option writers consider the 7500 level as a strong resistance level.{" "}
        <br />
        The Option writers are generally the market participants with deeper
        pockets compared to option buyers.
        <br />
        For more accuracy in predicting the ST direction, this chart should be
        read with “Nifty Options Traded Volume”
        {'\u00A0'} and {'\u00A0'}
        <Link href="/live-nifty-open-interest">
          <a
            title="Nifty Open Interest Chart Live"
          >
            “Nifty Open Interest Chart Live”
        </a>
        </Link>
        {'\u00A0'} If the OI increase is low but the volumes are heavily traded for a
        particular option then it will imply some underlying uncertainty in the
        camp. e.g. if the 7500PE has highest traded volume but a slight increase
        in OI then 7500 will not act as a good support.
      </p>
      <h4>Also, check out:</h4>
      <p>
        <Link href="/live-nifty-open-interest">
          <a
            title="Nifty Open Interest Live Chart: Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Nifty Open Interest Live Chart: Nifty Option Chain | NiftyTrader
                    </a>
        </Link>
      </p>
      <p>
        <Link href="/nifty-put-call-ratio">
          <a
            title="Nifty Put Call Ratio | Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Nifty Put Call Ratio | Nifty Option Chain | NiftyTrader
                    </a>
        </Link>
      </p>
      <p>
        <Link href="/bank-nifty-live-oi-change">
          <a
            title="Bank Nifty Change in Open Interest: Bank Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Bank Nifty Change in Open Interest: Bank Nifty Option Chain | NiftyTrader
                    </a>
        </Link>
      </p>
    </div>
  );
}
