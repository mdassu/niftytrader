import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";
export default function NiftyPCRVolumeContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>Nifty Volume Put Call Ratio (Volume PCR)</h2>
      <p>
        While OI PCR is calculated by dividing put OI by call OI, Nifty volume
        PCR is calculated by dividing put trading volume by call trading volume.
        It is easy to get confused between them, so l lets differentiate.
      </p>

      <p>
        To differentiate between volume and open interest, volume shows total
        number of transactions for a specific period, whereas, open interest
        shows the total number of unsettled contracts for a specific period of
        time.
      </p>
      <p>
        Also, volume increases every time a transaction takes place(i.e. trade
        position is opened or closed), whereas open interest increases when new
        contracts are opened and decreases when the old ones are settled.
      </p>
      <h3>Nifty PCR (Volume) = put trading volume / call trading volume</h3>
      <p>
        Again, a high PCR would suggest bearish sentiments in the market whereas
        a low PCR indicates bullish sentiments in the market. Nifty Volume PCR
        can be calculated manually or you can get the ratio at NiftyTrader.
      </p>
      <h4>Also, check out:</h4>
      <p>
        <Link href="/nifty-put-call-ratio">
          <a title="Nifty OI PCR" className="active">
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Nifty OI PCR
        </a></Link>
      </p>
    </div>
  );
}
