import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function NiftyPCRContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>PUT CALL RATIO of NIFTY OPTION CHAIN – THE KEYS TO HIGH PROBABILITY TRADE:</h2>
      <p>
        As a trader, what would you give to be able to know what the rest of the market participants are doing at
        any given point in time? Here is this secret report card, the put call ratio of Nifty Option Chain is as
        close to actually having this information as a trader is ever going to find..
                  </p>
      <p>
        The Put Call Ratio measures how many put options contract s are open versus call options contrancts in
        the Option Chain. The formula remains the same whether it is the Option Chain of Nifty, Bank Nifty or any
        stock.
                  </p>
      <p>
        The formula is very simple to calculate – take the put options Open Interest (from the Option Chain
        table) and divide by the Open Interest of calls. This data is easily available in option chain Nifty. In
        the above chart we have automated the calculations for you so you can get live view of what market is
        thinking.
                  </p>
      <p>
        You can also see <Link href="/live-nifty-open-interest"><a title="Nifty Change in Open Interest"><FontAwesomeIcon
          icon={faCheck}
          width="13"
          height="13"
        /> Nifty Change in Open Interest
                        </a>
        </Link> &amp; <Link href="/nifty-live-change-in-oi"><a title="Open Interest in Nifty Options"><FontAwesomeIcon
          icon={faCheck}
          width="13"
          height="13"
        /> Open Interest in Nifty
                            Options
                        </a></Link> here.
                  </p>
      <p>
        No need to use excel to download Open Interest of Puts and Calls separately and then running the formula
        to calculate Put Call Ratio.
                  </p>
      <h3>How to combine Nifty Option Chain PCR in your trade plan?</h3>
      <p>
        Here is how I use this indicator. For the sake of simplicity, lets assume that everyone in the market is
        bearish at a given time and because of this prevalent feeling everyone is entering shorts in stocks,
        indices and even buying puts. With all market participants bearish and now sitting on short positions, a
        very interesting thing takes place – there is nobody left to sell. With nobody left to sell, there is no
        selling pressure on the market, and they start to drift higher. This drifting eventually hits the first
        set of stop loss orders. Now all these market participants will be using some sort of stop loss. Some will
        use a very tight SL, others may use medium and remaining using wide stop loss.
                  </p>
      <p>
        The group of tight stops get hit first, which generates a buying pressure taking market higher. Then the
        medium and finally the wide SL until all the stops are taken out.
                  </p>
      <p>
        By the time all stops are taken out the market participants turn bullish and want to remain on the long
        side of the market. Once everyone has bought then there is no one left to buy and the market starts to
        drift lower.
                  </p>
      <p>Here is my Golden Rule:</p>
      <p className="rules">
        “If PCR ratio stays below the previous day closing PCR, then market is likely to be bearish and I look for
        short trade setups. If the PCR ratio stays above the previous day closing PCR then I search for long trade
        patterns only”
                  </p>
      <h3>Nifty Option Chain: Put Call Ratio:</h3>
      <p>
        See the correlation between ‘Nifty Put Call Ratio Live Chart’ and the Nifty Spot Price. This is very
        useful indicator for day-trading.
                  </p>
      <p>For intraday trading – live PCR trend can be extremely reliable indicator.</p>
      <h3>4 ways to interpret ‘Nifty Put Call Ratio Live Chart’ and Nifty Spot Correlation:</h3>
      <p>
        If the PCR (Put Call Ratio) is increasing during correction in the up trending market – this is very
        bullish indication. It means, the Put writers are aggressively writing at dips. Look for retracement
        percentage of last rise during correction while keeping an eye on this chart.
                  </p>
      <p>If the PCR is steadily rising during the day along with Nifty spot – also considered bullish</p>
      <p>
        If PCR is declining while the Nifty spot is near resistance level – bearish indication. This implies that
        bulls are fearful of bears.
                  </p>
      <p>
        If PCR declines during correction in the down trending market – this is very bearish indication. It
        means, either call writers are aggressively writing at every rise or Put writers are closing there
        positions cutting losses. Look for retracement percentage of last fall during correction while keeping an
        eye on this chart.
                  </p>
      <p>
        Here is what <a href="https://www.investopedia.com/trading/forecasting-market-direction-with-put-call-ratios/" title="Investopedia">Investopedia</a> has to say about using Put Call Ratio in
                        your trading:
                  </p>
      <p>
        It is widely known that options traders, especially option buyers, are not the most successful traders.
        On balance, option buyers lose about 90% of the time. Although there are certainly some traders who do
        well, would it not make sense to trade against the positions of option traders since most of them have
        such a bleak record? The contrarian sentiment put/call ratio demonstrates it pays to go against the
        options-trading crowd. After all, the options crowd is usually wrong.
                  </p>
    </div>
  );
}
