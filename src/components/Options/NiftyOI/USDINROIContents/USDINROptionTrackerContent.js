export default function USDINROptionTrackerContent() {
  return (
    <div className="yesbank-box pricetable-box open-int-box illustration-box">
      <h2>What are Currency Options?</h2>
      <p>
        Currency Options are contracts that grant the buyer of the option the
        right, but not the obligation, to buy or sell underlying currency at a
        specified exchange rate during a specified period of time. For this
        right, the buyer pays premium to the seller of the option.
      </p>
      <h3>What is the need for Exchange traded Currency Options?</h3>
      <p>
        The need for Exchange traded currency options arises on account of the
        following reasons:
      </p>
      <ul>
        <li>
          Options have the comparative advantage of maintaining a certain degree
          of flexibility in hedging, as, while protecting against a downside
          risk, they allow the investor from profiting from favorable movements
          of the foreign exchange rates by simply not exercising the option.
        </li>
        <li>
          The exchange platform brings in all attendant benefits of
          transparency, finer spreads, access, safety, central counter party
          etc.
        </li>
      </ul>
      <h4>What is the underlying for USD-INR options?</h4>
      <p>Underlying is US Dollar – Indian Rupee (US$-INR) spot rate.</p>
      <h4>What is the type of options?</h4>
      <p>
        USD-INR option contracts are Premium styled European Call and Put
        Options.
      </p>
      <h4>What is the trading hour and size of USD-INR options contract?</h4>
      <p>
        The trading hours are from 9 a.m. to 5.00 p.m. on all working days from
        Monday to Friday and the contract Size is US$ 1,000.
      </p>
      <h4>What is the quotation of USD-INR options?</h4>
      <p>
        The premium is quoted in rupee terms. However, the outstanding position
        is in USD terms.
      </p>
      <h4>What is the contract cycle for USD-INR options?</h4>
      <p>
        The contract cycle consists of three serial monthly contracts followed
        by three quarterly contracts of the cycle March/June/September/December.
      </p>
      <h4>What is the settlement mechanism for USD-INR options?</h4>
      <p>USD-INR options contracts are cash settled in Indian Rupee.</p>
      <h4>Which day is the expiry/last trading day?</h4>
      <p>
        The expiry / last trading day for the options contract is two working
        days prior to the last working day of the expiry month.
      </p>
      <h4>How settlement price is derived?</h4>
      <p>
        The final settlement price is the Reserve Bank of India USD-INR
        Reference Rate on the date of expiry of the contracts.
      </p>
      <h4>Which day is the final settlement day?</h4>
      <p>
        The options contract would expire on the last working day (excluding
        Saturdays) of the contract month. The last working day would be taken to
        be the same as that for Interbank Settlements in Mumbai. The rules for
        Inter-bank Settlements, including those for ‘known holidays’ and
        ‘subsequently declared holiday’ would be those as laid down by FEDAI.
      </p>
      <h4>How would contracts be settled at expiry?</h4>
      <p>
        On expiry date, all open long in-the-money contracts, on a particular
        strike of a series, at the close of trading hours would be automatically
        exercised at the final settlement price and assigned on a random basis
        to the open short positions of the same strike and series.
      </p>
      <h4>What is the Initial Margin levied in USD-INR Options?</h4>
      <p>
        The Initial Margin is based on a worst scenario loss of a portfolio of
        an individual client comprising his positions in options and futures
        contracts on the same underlying across different maturities and across
        various scenarios of price and volatility changes. In order to achieve
        this, the price range for generating the scenarios is 3.5 standard
        deviation and volatility range for generating the scenarios is 3%. The
        initial margin is deducted from the liquid net-worth of the clearing
        member on an online, real time basis.
      </p>
      <p>
        The sigma is calculated using the methodology specified for currency
        futures in SEBI circular no. SEBI/DNPD/Cir-38/2008 dated August 06, 2008
        and is the standard deviation of daily returns of USD-INR futures price.
      </p>
      <h4>What is the Extreme Loss margin?</h4>
      <p>
        Extreme loss margin of 1.5% of the notional value of the open short
        option position is deducted from the liquid assets of the clearing
        member on an on line, real time basis.
      </p>
      <p>
        Notional Value is calculated on the basis of the latest available
        Reserve Bank Reference Rate for USD-INR.
      </p>
      <h4>What is Net Option Value?</h4>
      <p>
        The Net Option Value is the current market value of the option times the
        number of options (positive for long options and negative for short
        options) in the portfolio. The Net Option Value is added to the Liquid
        Net Worth of the clearing member. Thus, mark to market gains and losses
        is not settled in cash for options positions.
      </p>
      <h4>What is the Calendar Spread Margin levied on USD-INR Options?</h4>
      <p>
        A long currency option position at one maturity and a short option
        position at a different maturity in the same series, both having the
        same strike price is treated as a calendar spread. The margin for
        options calendar spread is same as specified for USD-INR currency
        futures calendar spread. The calendar spread margin is calculated on the
        basis of delta of the portfolio in each month. A portfolio consisting of
        a near month option with a delta of 100 and a far month option with a
        delta of –100 would bear a spread charge equal to the spread charge for
        a portfolio which is long 100 near month currency futures and short 100
        far month currency futures.
      </p>
      <h4>How premium paid by the buyer is settled?</h4>
      <p>
        Premium is paid by the buyer in cash and paid out to the seller in cash
        on T+1 day. Until the buyer pays in the premium, the premium due is
        deducted from the available Liquid Net Worth on a real time basis.
      </p>
      <h4>What is the Position Limit at Client level?</h4>
      <p>
        The gross open positions of the client across all contracts (both
        futures and options contracts) not to exceed 6% of the total open
        interest or USD 10 million whichever is higher. The Exchange
        disseminates alerts whenever the gross open position of the client
        exceeds 3% of the total open interest at the end of the previous day’s
        trade.
      </p>
      <h4>What is the Position Limit at Trading Member level?</h4>
      <p>
        The gross open positions of the trading member across all contracts
        (both futures and options contracts) not to exceed 15% of the total open
        interest or USD 50 million whichever is higher.
      </p>
      <h4>What is the Position Limit for Banks?</h4>
      <p>
        The gross open positions of the bank across all contracts (both futures
        and options contracts) not to exceed 15% of the total open interest or
        USD 100 million whichever is higher.
      </p>
      <h4>What is the Position Limit at Clearing Member level?</h4>
      <p>
        No separate position limit is prescribed at the level of clearing
        member. However, the clearing member ensures that his own trading
        position and the positions of each trading member clearing through him
        is within the limits specified above.
      </p>
    </div>
  );
}
