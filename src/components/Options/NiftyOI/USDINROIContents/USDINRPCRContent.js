export default function USDINRPCRContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>USD INR Put Call Ratio (PCR)</h2>
      <p>
        USD INR Put call ratio or PCR is often used as an indicator to gauge the
        overall sentiment of the USD INR options market. The ratio calculation
        is based on option contracts or option trading volume on a given day.
        You can either calculate PCR manually or get the values from Nifty
        Trader. To manually calculate PCR, use the formulas given below.
      </p>
      <p>
        USD INR PCR (OI) can be calculated by dividing USD INR Put OI by USD INR
        Call OI on the same day.
      </p>
      <h3>PCR (OI) = USD INR Put OI / USD INR Call OI</h3>
      <p>
        To calculate USD INR PCR (Volume), divide USD INR Put trading volume by
        USD INR Call trading volume.
      </p>
      <p>
        <strong>
          PCR (Volume) = USD INR put trading volume / USD INR call trading
          volume
        </strong>
      </p>
      <h4>What does PCR value suggest?</h4>
      <p>
        If PCR value is less than 1, then traders might be buying more call
        option contracts than put option contracts. Market sentiment is deemed
        as bullish when PCR is low.
      </p>
      <p>
        If PCR value is more than 1, then traders might be buying more put
        option contracts than call option contracts. Market sentiment is deemed
        as bearish when PCR is high.
      </p>
      <h4>Also check:</h4>
      <p>
        <a href="/usd-inr-options-live" title="USD INR Options Live">
          USD INR Options Live
        </a>
      </p>
      <p>
        <a
          href="/usd-inr-change-open-interest-live"
          title="USD INR Change In Open Interest Live"
        >
          USD INR Change In Open Interest Live
        </a>
      </p>
      <div className="nifty-datatable table-responsive  mb-3">
        <table className="table">
          <thead>
            <tr>
              <th scope="col" width="">
                Nifty OI
              </th>
              <th scope="col" width="">
                BankNifty OI
              </th>
              <th scope="col" width="">
                USD INR OI
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <a
                  href="/live-nifty-open-interest"
                  title="Nifty Open Interest Live Chart"
                >
                  Nifty Open Interest Live Chart
                </a>
              </td>
              <td>
                <a
                  href="/banknifty-live-oi-tracker"
                  title="Bank Nifty Open Interest Live Chart"
                >
                  Bank Nifty Open Interest Live Chart
                </a>
              </td>
              <td>
                <a href="/usd-inr-options-live" title="USD INR Options Live">
                  USD INR Options Live
                </a>
              </td>
            </tr>

            <tr>
              <td>
                <a
                  href="/nifty-live-change-in-oi"
                  title="Nifty Change in Open Interest Live Chart"
                >
                  Nifty Change in Open Interest Live Chart
                </a>
              </td>
              <td>
                <a
                  href="/bank-nifty-live-oi-change"
                  title="Bank Nifty Change In Open Interest"
                >
                  Bank Nifty Change In Open Interest
                </a>
              </td>
              <td>
                <a
                  href="/usd-inr-change-open-interest-live"
                  title="USD INR Change In Open Interest Live"
                >
                  USD INR Change In Open Interest Live
                </a>
              </td>
            </tr>

            <tr>
              <td>
                <a href="/nifty-put-call-ratio" title="Nifty Put Call Ratio">
                  Nifty Put Call Ratio
                </a>
              </td>
              <td>
                <a
                  href="/banknifty-intra-pcr-trend"
                  title="Bank Nifty PCR Live Chart"
                >
                  Bank Nifty PCR Live Chart
                </a>
              </td>
              <td>
                <a
                  href="#"
                  title="USD INR Put Call Ratio"
                  className="nifty-oi-active"
                >
                  USD INR Put Call Ratio
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
