export default function USDINRChangeOIContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>USD INR Options: Change In Open Interest Live</h2>
      <p>
        Open interests reflect the value of the total number of open contracts
        on a given day. USD INR change in open interest happens when a contract
        is created of closed by two people, buyer, and seller.
      </p>
      <h3>USD INR Options Facts:</h3>
      <p>
        <strong>USD INR Option Expiry Style:</strong> European, Expires 2 days
        before the last working day of the month.
      </p>
      <p>
        <strong>Contract Cycle:</strong> USD INR Option Contracts are available
        for only the next 3 months
      </p>
      <p>
        <strong>Settlements:</strong> The USD INR options are settled in Indian
        Rupee based on the settlement price.
      </p>
      <p>
        <strong>Lot Size:</strong> $1000
      </p>
      <p>
        Low open interest means a lot of contracts have been closed whereas high
        open interest suggests that a lot of positions are openly implying
        additional money entering the market.
      </p>
      <p>
        Apart from Volume and price, Open Interest can help you bring a new
        dimension to your analysis. Calculating the total number of open
        contracts may provide an insight into the liquidity of options and
        futures price movements.
      </p>
      <h4>USD INR Open Interest vs Volume</h4>
      <p>
        Difference between Open Interest and Volume: Open Interest and volume
        may confuse some new traders, to differentiate between them:
      </p>
      <div className="nifty-datatable table-responsive mb-3">
        <table className="table">
          <thead>
            <tr>
              <th scope="col" width="">
                Open Interest
              </th>
              <th scope="col" width="">
                Volume
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>A total of all open USD INR contracts on a given day.</td>
              <td>A total of all USD INR transactions on a given day.</td>
            </tr>

            <tr>
              <td>
                Increases when traders open USD INR trade contracts and decrease
                when traders close USD INR trade contracts.
              </td>
              <td>Increases every time a USD INR trade is opened or closed.</td>
            </tr>
          </tbody>
        </table>
      </div>
      <h4>Also check:</h4>
      <p>
        <a href="/usd-inr-options-live" title="USD INR Options Live">
          USD INR Options Live
        </a>
      </p>
      <p>
        <a href="/usdinr-put-call-ratio" title="USD INR Put Call Ratio">
          USD INR Put Call Ratio
        </a>
      </p>
      <div className="nifty-datatable table-responsive mb-3">
        <table className="table">
          <thead>
            <tr>
              <th scope="col" width="">
                Nifty OI
              </th>
              <th scope="col" width="">
                BankNifty OI
              </th>
              <th scope="col" width="">
                USD INR OI
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <a
                  href="/live-nifty-open-interest"
                  title="Nifty Open Interest Live Chart"
                >
                  Nifty Open Interest Live Chart
                </a>
              </td>
              <td>
                <a
                  href="/banknifty-live-oi-tracker"
                  title="Bank Nifty Open Interest Live Chart"
                >
                  Bank Nifty Open Interest Live Chart
                </a>
              </td>
              <td>
                <a href="/usd-inr-options-live" title="USD INR Options Live">
                  USD INR Options Live
                </a>
              </td>
            </tr>

            <tr>
              <td>
                <a
                  href="/nifty-live-change-in-oi"
                  title="Nifty Change in Open Interest Live Chart"
                >
                  Nifty Change in Open Interest Live Chart
                </a>
              </td>
              <td>
                <a
                  href="/bank-nifty-live-oi-change"
                  title="Bank Nifty Change In Open Interest"
                >
                  Bank Nifty Change In Open Interest
                </a>
              </td>
              <td>
                <a
                  href="#"
                  title="USD INR Change In Open Interest Live"
                  className="nifty-oi-active"
                >
                  USD INR Change In Open Interest Live
                </a>
              </td>
            </tr>

            <tr>
              <td>
                <a href="/nifty-put-call-ratio" title="Nifty Put Call Ratio">
                  Nifty Put Call Ratio
                </a>
              </td>
              <td>
                <a
                  href="/banknifty-intra-pcr-trend"
                  title="Bank Nifty PCR Live Chart"
                >
                  Bank Nifty PCR Live Chart
                </a>
              </td>
              <td>
                <a href="/usdinr-put-call-ratio" title="USD INR Put Call Ratio">
                  USD INR Put Call Ratio
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
