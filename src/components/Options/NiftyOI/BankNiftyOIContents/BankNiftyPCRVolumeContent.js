import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function BankNiftyPCRVolumeContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>Bank Nifty Volume Put Call Ratio (Volume PCR)</h2>
      <h3>
        How to interpret “Nifty Option Chain: Open Interest | Put Call Ratio
        Tracker”?
      </h3>
      <p>
        Bank Nifty volume PCR is calculated by dividing the trading volume of
        all the put option contracts on a given day by the trading volume of all
        the call option contracts..
      </p>

      <p>
        Bank Nifty volume PCR is different from Bank Nifty OI PCR as OI PCR is
        calculated by dividing OI of put option contracts by OI of call option
        contracts.
      </p>
      <p>
        Remember, unlike OI, which increases when more contracts are open and
        decreases when the contracts are settled, Volume increases with every
        transaction.
      </p>
      <p>
        From the formula, we can derive that if the ratio is greater than 1,
        more bank nifty put contracts are open and if the ratio is less than 1,
        more bank nifty call option contracts remain unsettled.
      </p>
      <p>
        This further helps us in gauging the direction of the market. If more
        put contracts are open, the market has an overall bearish sentiment. On
        the other hand, if more call option contracts are open, the market has
        an overall bullish sentiment.
      </p>
      <h4>Also, check out:</h4>
      <p>
        <Link href="/banknifty-intra-pcr-trend">
          <a title="Bank Nifty OI PCR">
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> {" "} Bank Nifty OI PCR
        </a>
        </Link>
      </p>
    </div>
  );
}
