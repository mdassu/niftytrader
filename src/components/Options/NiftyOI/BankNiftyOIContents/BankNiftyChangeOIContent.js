export default function BankNiftyChangeOIContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>Bank Nifty Change in Open Interest Live</h2>
      <p>
        Bank Nifty Live OI Change Chart can give very useful clues for intraday
        support and resistance levels for Bank Nifty. If there is more addition
        in Open Interest at 12200 Calls, that would mean most market players are
        comfortable writing call options at this level because they believe it
        to be a strong resistance. That would be bearish indication for
        BankNifty.
      </p>
      <p>
        Similarly is there is highest writing in 12000 Puts that would indicate
        strong intraday support at that level.
      </p>
      <p>
        Third Scenario: There is good amount of Open Interest increase in 12000
        PE and 12200 CE –&gt; this means we should expect a range bound session
        for the day, as both bears and bulls are comfortable holding the 12200
        and 12000 levels respectively.
      </p>
      <p>
        Following factors could improve reliability of BankNifty OI Change
        analysis:
      </p>
      <p>
        1) Put Call Ratio: Higher PCR means bullishness. If there is more
        writing at 12000PE and PCR is high and increasing during the day that
        would add to bullish scenario
      </p>
      <p>
        If the PCR is declining for the day and more writing happening at
        12200CE then this adds to bearishness.
      </p>
      <p>
        2) Close to expiry: The closer to expiry we are, the more reliable the
        ‘Open Interest’ analysis. Early in the series, the OI analysis is less
        reliable.
      </p>
      <p>
        3) More Players: As the number of players increase, the OI analysis
        become more reliable.
      </p>
      <p>
        4) Bid-ask Spread: The lower the bid-ask spread the more reliable the OI
        analysis.
      </p>
      <p>
        5) Technical Indicators: The best trades are found by combining OI
        analysis with other technical indicators. MACD, RSI, Channel lines and
        EW count give best results with Open Interest Analysis.
      </p>
    </div>
  );
}
