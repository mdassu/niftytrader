export default function BankNiftyOptionTrackerContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>
        Interpretation of Bank Nifty Open Interest charts to understand Market
        Trend:
      </h2>
      <p>
        Open Interest analysis can provide very useful insights pertaining
        market trend and support / resistance. It is very important for option
        traders to understand the relation between open interest and market
        direction. Combining interpretations from Open Interest (OI) and change
        in OI can give meaning results. Intelligent traders understand that even
        though OI is a very crucial ‘market indicator’, it should be combined
        with other technical indicators to get good results.
      </p>
      <h3>Open Interest to determine Support and Resistance for Bank Nifty:</h3>
      <p>
        Open Interest data can give very useful clues to determine Support and
        Resistance. e.g. if 11000PE has highest open interest, traders perceive
        it as important support for the current expiry. Keeping in view that
        most institutional investors write options rather than buy, the data
        helps to understand mood of ‘intelligent money’. Similarly if huge open
        interest is build for 11400 calls it will be seen as major resistance
        zone. If the expiry is near than the market may stay range bound between
        these two levels.
      </p>
      <h3>OI &amp; Price Change Interpretation</h3>
      <div className="nifty-datatable table-responsive  mb-3">
        <table className="table">
          <thead>
            <tr>
              <th scope="col" width="">
                PRICE
              </th>
              <th scope="col" width="">
                OPEN INTEREST
              </th>
              <th scope="col" width="">
                INTERPRETATION
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Increase in Price</td>
              <td>Increase in Open Interest</td>
              <td>
                Indication of new money coming and indicates further continuance
                of uptrend
              </td>
            </tr>

            <tr>
              <td>Increase in Price</td>
              <td>Decrease in Open Interest</td>
              <td>Increase in price is due to short covering of positions</td>
            </tr>

            <tr>
              <td>Decrease in Price</td>
              <td>Increase in Open Interest</td>
              <td>
                Decrease in price is due to newly build short positions and
                further weakness is predicted
              </td>
            </tr>
            <tr>
              <td>Decrease in Price</td>
              <td>Decrease in Open Interest</td>
              <td>
                Traders unwinding their long positions by selling existing
                contracts
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <p>
        While first and third scenarios of interpretation of open interest
        charts indicate direction of future market trend (Bullish in first case
        and Bearish in third Case), other scenarios does not indicate a clear
        trend. Traders can wait for clarity in open interest data or use other
        indicators to initiate positions.
      </p>
    </div>
  );
}