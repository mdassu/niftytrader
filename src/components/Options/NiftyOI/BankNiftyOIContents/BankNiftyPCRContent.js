import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function BankNiftyPCRContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>Bank Nifty Options Analysis:</h2>
      <p>
        Put Call Ratio trend can provide very useful clues for effective ‘Bank
        Nifty Options’ analysis. If the Put Call Ratio (PCR) is rising that will
        imply more bullishness.
      </p>
      <p>
        The Bank Nifty Options PCR chart can be used in the same way as we use
        the Intraday{" "}
        <Link href="/nifty-put-call-ratio">
          <a title="Nifty PCR Chart">
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Nifty PCR Chart
        </a>
        </Link>
      </p>
      <p>
        This chart should be read along with{" "}
        <Link href="/bank-nifty-live-oi-change">
          <a
            title="Bank Nifty Options OI change"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> Bank Nifty Options OI change
        </a>
        </Link>{" "}
        and{" "}
        <Link href="/banknifty-live-oi-tracker">
          <a
            title="Bank Nifty Options Open Interest"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            />  Bank Nifty Options Open Interest{" "}
          </a>
        </Link>
        Charts
      </p>
    </div>
  );
}
