import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function FinNiftyOptionTrackerContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>OPTION CHAIN FIN NIFTY – Interpretation of Open Interest:</h2>
      <p>
        Change in Open Interest is equal to net increase or decrease in Open
        Interest for a particular option.
      </p>
      <h3>How to interpret “Change in Open Interest”?</h3>

      <p></p>
      <h4>Also, check out:</h4>
      <p>
        <Link href="/live-finnifty-open-interest">
          <a
            title="Fin Nifty Open Interest Live Chart: Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> {" "} Fin Nifty Open Interest Live Chart: Fin
          Nifty Option Chain | NiftyTrader
        </a>
        </Link>
      </p>
      <p>
        <Link href="/finnifty-put-call-ratio">
          <a
            title="Fin Nifty Put Call Ratio | Fin Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> {" "} Fin Nifty Put Call Ratio | Fin Nifty
          Option Chain | NiftyTrader
        </a>
        </Link>
      </p>
      <p>
        <Link href="/bank-nifty-live-oi-change">
          <a
            title="Bank Nifty Change in Open Interest: Bank Nifty Option Chain | NiftyTrader"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> {" "} Bank Nifty Change in Open Interest: Bank
          Nifty Option Chain | NiftyTrader
        </a>
        </Link>
      </p>
    </div>
  );
}
