import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck
} from "@fortawesome/free-solid-svg-icons";

export default function FinNiftyPCRVolumeContent() {
  return (
    <div className="yesbank-box open-int-box pricetable-box">
      <h2>Fin Nifty Volume Put Call Ratio (Volume PCR)</h2>

      <h4>Also, check out:</h4>
      <p>
        <Link href="/finnifty-put-call-ratio">
          <a
            title="Fin Nifty OI PCR"
            className="active"
          >
            <FontAwesomeIcon
              icon={faCheck}
              width="13"
              height="13"
            /> {" "} Fin Nifty OI PCR
        </a>
        </Link>
      </p>
    </div>
  );
}
