import React from 'react';
export default function StockOptionChainContent() {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-12 option_chain_content">
          <h2 className="mt-4">NSE Option Chain:</h2>
          <p>
            NSE option chain displays “Open Interest”, “Change in OI”,
            “Volume”, “Implied Volatility (IV)”, for all strike price for a
            particular underlying stock. The tool also provides the
            functionality to see the trend of how these values have changed
            over the month.
              </p>
          <h3>How to use NSE Option Chain table?</h3>
          <p>
            The option chain table can be very useful for traders in many
            different ways, few of those are:
              </p>
          <ul>
            <li>
              Identifying Support and resistance for stocks for the month
                </li>
            <li>
              Identify support and resistance for intraday perspective
                </li>
            <li>
              Identify market expectation of volatility using IV(Implied
              Volatility)
                </li>
          </ul>
          <p>
            For “Options Trading”, this is extremely useful as it shows
            where the rest of the market participants are seeing support and
            resistance. The “Option Writer” and “Option Buyer” both can use
            this table to improve their odds in trading.
              </p>
          <p>
            To explain it with a very rudimentary example: if SBI has
            highest Open Interest at 150PE and highest addition in 150PE as
            well, this will imply that the market participants are expecting
            150 to be a good support level for SBI.
              </p>
          <p>
            With practice and experience, traders can develop a good gut
            feeling for identifying support and resistance using the table.
            Some traders also use IV and Volume to make sense of these
            numbers, however for a beginner OI and Change in OI are
            sufficient.
              </p>
          <h4>Also, Check:</h4>
          <p>
            <a
              href="https://www.niftytrader.in/live-nifty-open-interest/"
              title="Nifty Open Interest Live Chart: Nifty Option Chain"
            >
              Nifty Open Interest Live Chart: Nifty Option Chain
                </a>
          </p>
          <p>
            <a
              href="https://www.niftytrader.in/nifty-live-change-in-oi/"
              title="Nifty Change in Open Interest Live: Nifty Option Chain"
            >
              Nifty Change in Open Interest Live: Nifty Option Chain
                </a>
          </p>
          <p>
            <a
              href="https://www.niftytrader.in/nifty-put-call-ratio/"
              title="Nifty Put Call Ratio | Nifty Option Chain"
            >
              Nifty Put Call Ratio | Nifty Option Chain
                </a>
          </p>
          <p>
            <a
              href="https://www.niftytrader.in/banknifty-live-oi-tracker/"
              title="Bank Nifty Open Interest Live Chart: Bank Nifty Option Chain"
            >
              Bank Nifty Open Interest Live Chart: Bank Nifty Option Chain
                </a>
          </p>
          <p>
            <a
              href="https://www.niftytrader.in/bank-nifty-live-oi-change/"
              title="Bank Nifty Change in Open Interest: Bank Nifty Option Chain | Nifty Trader"
            >
              Bank Nifty Change in Open Interest: Bank Nifty Option Chain |
              Nifty Trader
                </a>
          </p>
          <p>
            <a
              href="https://www.niftytrader.in/banknifty-intra-pcr-trend/"
              title="Bank Nifty PCR Live Chart | Bank Nifty Option Chain"
            >
              Bank Nifty PCR Live Chart | Bank Nifty Option Chain
                </a>
          </p>
        </div>
      </div>
      <div className="container p-0">
        <div className="row">
          <div className="col-lg-12 p-0 mb-5">
            <div className="stock-analysis-profile-div mt-4 option_chain_content">
              <p></p>
              <h1>NSE Option Chain</h1>

              <p>
                <strong>NSE Stock&nbsp;Options Chain</strong> shows a list
                    of all the
                    <span className="callputoption">
                  {" "}
                      call put options
                    </span>{" "}
                    strike prices with their premiums for a given expiry. It is
                    one of the most important leading indicators as it depends
                    on so many variables like Open Interest (OI), Change in OI,
                    Implied Volatility (IV), Premium, Premium Decay for each
                    strike price closer to the current price of underlying.
                  </p>

              <p>
                We are trying to simplify the Option Chain analysis for
                traders by making it easy to interpret. The option chain on
                niftytrader shows the position of option writers and option
                buyers - this will show the points of strength and weakness
                for the market and the position of option writers.
                  </p>

              <p>&nbsp;</p>

              <h2>
                <u>FAQs:</u>
              </h2>

              <h4>
                <strong>Who is an option writer?&nbsp;</strong>
              </h4>

              <h4>
                The option writer is a trading member who is permitted by
                the F&amp;O segment of the exchange to write options
                contracts. Option Writer has obligation to honor the
                contract and they receive a premium for that.
                  </h4>

              <h4>
                <br />
                <strong>Who is an option Buyer?&nbsp;</strong>
              </h4>

              <h4>
                Option Buyer is a person who has the right but no
                obligation.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is “in the money” strike price?&nbsp;</strong>
              </h4>

              <h4>
                For put options - it is the strike price of the option
                contract which is above “at the money” or “near the money”
                strike price.&nbsp;
                    <br />
                    For call options - it is the strike price of the option
                    contract which is below “at the money” or “near the money”
                    strike price.
                  </h4>

              <h4>
                <br />
                <strong>
                  What are the “at the money” or “near the money” strike
                  prices?&nbsp;
                    </strong>
              </h4>

              <h4>
                "At the Money" or "Near the Money" strike price are those
                options contracts that have the strike price very close to
                the underlying price (spot price or the last traded price of
                the Index or security).
                  </h4>

              <h4>
                <br />
                <strong>
                  What is an “out of the money” strike price?&nbsp;
                    </strong>
              </h4>

              <h4>
                "Out of the Money," strike price are those options contracts
                that have the strike price far from the underlying price
                (spot price or the last traded price of the Index or
                security).
                  </h4>

              <h4>
                <br />
                <strong>What is a premium?&nbsp;</strong>
              </h4>

              <h4>
                Premium is the price that the buyer of the option pays to
                the seller of the option for the rights conveyed by the
                option contract.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is a long position?&nbsp;</strong>
              </h4>

              <h4>
                A long position in a derivatives contract means outstanding
                purchase obligations in respect of a permitted derivatives
                contract at any point of time.
                  </h4>

              <h4>
                <br />
                <strong>What is a short position?&nbsp;</strong>
              </h4>

              <h4>
                A short position in a derivatives contract means outstanding
                sell obligations in respect of a permitted derivatives
                contract at any point of time.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is open interest?&nbsp;</strong>
              </h4>

              <h4>
                It is the total number of derivatives contracts of an
                underlying security that have not yet been offset and closed
                by an opposite derivatives transaction nor fulfilled by
                delivery of the cash or underlying security or option
                exercise.&nbsp;
                  </h4>

              <h4>
                For the calculation of open interest, only one side of the
                derivatives contract is counted.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is strike price interval?&nbsp;</strong>
              </h4>

              <h4>
                It is the gap between any two successive strike prices which
                the relevant authority may prescribe from time to time.
                  </h4>

              <h4>
                <br />
                <strong>What is a contract month or contract week?</strong>
              </h4>

              <h4>
                It is the month or week in which a contract needs to be
                finally settled, as decided by the stock exchange.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is contract value?</strong>
              </h4>

              <h4>
                It is the value arrived at by multiplying the strike price
                of the options contract with the regular/market lot
                size.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>What is the settlement date?&nbsp;</strong>
              </h4>

              <h4>
                It is the date on which the settlement of outstanding
                obligations is required to be settled.
                  </h4>

              <h4>
                <br />
                <strong>What is F&amp;O lot/market lot size?&nbsp;</strong>
              </h4>

              <h4>
                It is the number of units that can be bought or sold in a
                specified derivatives contract.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>
                  What is the last trading day/expiry day?&nbsp;
                    </strong>
              </h4>

              <h4>
                It is the day up to and on which a derivatives contract is
                available for trading. It is normally the last Thursday of
                the month. If the last Thursday is a holiday, the expiry
                will happen on Wednesday.
                    <br />
                    &nbsp;
                  </h4>

              <h4>
                <strong>How to read option Chain data in NSE?</strong>
              </h4>

              <p>
                NSE Option chain is closely monitored by day traders to
                identify support and resistance levels, which they confirm
                with other indicators before placing a trade.
                  </p>

              <h4>
                <strong>
                  How to read the options chain (equity derivatives)?
                    </strong>
              </h4>

              <p>
                On the option chain page, you will find 2 columns - one on
                the extreme right and the other on the extreme left. These 2
                columns try to analyze&nbsp;the behavior of&nbsp;option
                buyers&nbsp;&amp; sellers on all strike prices. For the sake
                of simplicity, it has been color-coded with GREEN and RED
                background so traders can easily read it.
                  </p>

              <h4>
                <strong>
                  What is an option Chain? What does Option Chain mean?
                    </strong>
              </h4>

              <p>
                An Option chain is a tabular representation of all available
                option contracts for an underlying (stock, index, currency
                or&nbsp;commodity) and provides a quick picture of all
                available put options and calls options of the underlying
                along with their pricing, volume, open interest details,
                which could be advantageous for a trader to analyze the
                market and take appropriate and immediate actions. Option
                chain allows you to see all options (Put &amp; call) for a
                particular strike in a single page view so you can analyze
                it.
                  </p>

              <h4>
                <strong>What is option chain analysis?</strong>
              </h4>

              <p>
                Traders use the Option chain as a complementary tool along
                with the study of market technicals or chart patterns. It
                can offer very useful clues to traders especially the day
                traders.
                  </p>

              <h4>
                <strong>What is IV in Option Chain?</strong>
              </h4>

              <p>IV stands for Implied Volatility.</p>

              <h4>
                <strong>How to download option chain data from NSE?</strong>
              </h4>

              <p>
                You can directly download the option chain data from the NSE
                website in an excel sheet. There are various excel tools
                available on the internet to freely download the NSE Option
                chain data and analyze it. This data can be delayed by a few
                minutes.&nbsp;
                  </p>

              <p>
                We recommend traders use the niftytrader website and app for
                option chain analysis. You can also get in touch (email:
                care@niftytrader.in) with us to incorporate any specific
                analysis on the option chain - our technical team will
                review the suggestion and try to incorporate it for the
                benefit of all traders.
                  </p>

              <p>
                Check out Live Put &amp; Call Options Quote: Find all
                details related to selected options here. You can use the
                filter to change the Expiration date, Option type i.e. Call
                &amp; Put option &amp; Strike Price. It will show detailed
                data related to your selection for analysis.
                  </p>

              <p>
                <strong>What is exercise style?&nbsp;</strong>
                <br />
                    The exercise style of an option refers to the price at which
                    and/or time as to when the option is exercisable by the
                    holder. It may either be an American style option or a
                    European style option or such other exercise style of option
                    as the relevant authority (stock exchange) may prescribe
                    from time to time.
                  </p>

              <h4>
                <br />
                <strong>
                  What is a European-style option contract?&nbsp;
                    </strong>
                <br />A European-style option contract is an option contract
                    that may be exercised on the expiration day on or before the
                    expiration time.&nbsp;
                  </h4>

              <h4>
                <br />
                <strong>
                  What is an American-style options contract?&nbsp;
                    </strong>
                <br />
                    American Style option contract is an option contract that
                    may be exercised any day before the expiration day or before
                    the expiration time.
                  </h4>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}