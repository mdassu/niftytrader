

import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { CALL_API } from '_services/CALL_API';
export default function ChartModal(props) {

  return (
    <div
      className="modal myCharModal "
      id="large-Modal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="large-Modal"
      aria-hidden="true"
    >
      <div
        className="modal-dialog modal-dialog-centered modal-xl"
        role="document"
      >
        <div className="modal-content">
          <div className="modal-header pt-3 pb-3">
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
            <h5 className="modal-title mb-0">
              NIFTY Chart
            <a
                href="https://www.niftytrader.in/live-analytics"
                title="Nifty 50"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-box-arrow-up-right"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill-rule="evenodd"
                    d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"
                  />
                  <path
                    fill-rule="evenodd"
                    d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"
                  />
                </svg>
              </a>
            </h5>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="col-lg-3 border-right">
                <h4>Select Options</h4>
                <select
                  className="custom-select custom-select-md mb-3"
                  id="firstOptData"
                >
                  <option value="Options..." selected="">
                    Options...
                </option>

                  <option value="Stock_Price">Stock Price</option>

                  <option value="Call_OI">Calls OI</option>

                  <option value="Put_OI">Puts OI</option>

                  <option value="Call_LTP">Calls LTP</option>

                  <option value="Put_LTP">Puts LTP</option>

                  <option value="Call_Volume">Calls Volume</option>

                  <option value="Put_Volume">Puts Volume</option>
                </select>
                <select
                  className="custom-select custom-select-md mb-3"
                  id="secOptData"
                >
                  <option value="Options..." selected="">
                    Options...
                </option>

                  <option value="Stock_Price">Stock Price</option>

                  <option value="Call_OI">Calls OI</option>

                  <option value="Put_OI">Puts OI</option>

                  <option value="Call_LTP">Calls LTP</option>

                  <option value="Put_LTP">Puts LTP</option>

                  <option value="Call_Volume">Calls Volume</option>

                  <option value="Put_Volume">Puts Volume</option>
                </select>
                <select
                  className="custom-select custom-select-md"
                  id="trdOptData"
                >
                  <option value="Options..." selected="">
                    Options...
                </option>

                  <option value="Stock_Price">Stock Price</option>

                  <option value="Call_OI">Calls OI</option>

                  <option value="Put_OI">Puts OI</option>

                  <option value="Call_LTP">Calls LTP</option>

                  <option value="Put_LTP">Puts LTP</option>

                  <option value="Call_Volume">Calls Volume</option>

                  <option value="Put_Volume">Puts Volume</option>
                </select>

                <h4 className="mt-4">Chart View</h4>
                <div className="footerBtn text-center">
                  <button
                    type="button"
                    id="IntradayBTN"
                    className="btn btn-secondary enquiry-btn active"
                    title="Intraday"
                    disabled=""
                  >
                    Intraday
                </button>
                  <button
                    type="button"
                    id="EodBTN"
                    className="btn btn-secondary enquiry-btn"
                    title="EOD"
                  >
                    EOD
                </button>
                </div>
              </div>

              <div className="col-lg-9">
                <div className="rightChart nifty-page-chart">
                  {/* <!-- Chart Here --> */}
                  <img src="images/newchart.png" className="img-fluid" />
                </div>
              </div>
            </div>
          </div>

          <div className="modal-footer text-center"></div>
        </div>
      </div>
    </div>
  )
}
