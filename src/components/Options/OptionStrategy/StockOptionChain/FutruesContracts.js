import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { CALL_API } from '_services/CALL_API';
export default function FutruesContracts(props) {
  const { symbol, spotData, closeModal } = props;
  const [futuresContracts, setFuturesContracts] = useState([]);
  const [rolloverData, setRolloverData] = useState({});

  useEffect(() => {
    getFutureContracts()
  }, []);

  function getFutureContracts() {
    const params = {
      symbol: symbol.toLowerCase()
    };
    CALL_API('POST', process.env.FUTURES_CONTRACTS_DATA, params, (res => {
      if (res.status) {
        var response = res['data'];
        setFuturesContracts(response);
        const data = {
          rolloverCost: 0,
          rolloverCostPer: 0,
          rolloverPer: 0,
        };
        data['rolloverCost'] = response[1].last_price - response[0].last_price;
        data['rolloverCostPer'] = (data['rolloverCost'] / response[0].last_price) * 100;
        data['rolloverPer'] = ((response[1].oi + response[2].oi) / (response[2].oi + response[1].oi + response[0].oi)) * 100;
        setRolloverData(data);
      }
    }));
  }

  function renderFutureContracts() {
    if (futuresContracts.length > 0) {
      const data = futuresContracts.map(item => {
        return (
          <tr>
            <td className="text-right">
              <span>{moment(item['expiry']).format('YYYY-MM-DD')}</span>
            </td>

            <td className="text-right">
              <span>{Number(item['oi']).toFixed(2)}</span>
            </td>

            <td className="text-right">
              <span>{Number(item['valume']).toFixed(2)}</span>
            </td>

            <td className="text-right">
              <span>{Number(item['close']).toFixed(2)}</span>
            </td>

            <td className="text-right">
              <span>{Number(item['lot_size']).toFixed(2)}</span>
            </td>
          </tr>
        )
      });
      return data;
    }
  }
  return (
    <div
      className="bd-example-modal-lg modal filter_modal future_modal d-block"
      id="features-modal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="features-modal"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-lg modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <div className="float-left modal_heading">
              <h4 className="modal-title stock-analysis-heading">
                {symbol} : <span className="sp" style={{ background: spotData['change_value'] > 0 ? '' : '#e6191e' }}>{spotData['index_value']}</span>
                <sub>
                  <svg
                    id="StockCaret"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className={`bi ${spotData['change_value'] > 0 ? 'bi-caret-up-fill percent-green' : 'bi-caret-down-fill percent-red'}`}
                    viewBox="0 0 16 16"
                  >
                    <path d={spotData['change_value'] > 0 ? "m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" : "M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"} />
                  </svg>



                  <b className={spotData['change_value'] > 0 ? 'color-text' : 'color-text-red'} id="StockPriceChange">
                    {Number(spotData['change_value']).toFixed(2)} ({Number(spotData['percentage_value']).toFixed(2)}%)
                </b>
                </sub>
              </h4>
            </div>

            <div className="float-left modal_sp_price"></div>
            <button type="button" className="close" onClick={closeModal}>
              ×
          </button>
          </div>

          <div className="modal-body">
            <div className="table-responsive">
              <table className="table modal-future-table">
                <thead>
                  <tr>
                    <th>EXPIRY DATE</th>
                    <th>OI</th>
                    <th>VOLUME</th>
                    <th>LAST PRICE</th>
                    <th>LOT SIZE</th>
                  </tr>
                </thead>
                <tbody>
                  {renderFutureContracts()}
                  {/* <tr>
                    <td className="text-right">
                      <span>2021-05-27</span>
                    </td>

                    <td className="text-right">
                      <span>10796025.00</span>
                    </td>

                    <td className="text-right">
                      <span>7581450.00</span>
                    </td>

                    <td className="text-right">
                      <span>14938.00</span>
                    </td>

                    <td className="text-right">
                      <span>75.0</span>
                    </td>
                  </tr>

                  <tr>
                    <td className="text-right">
                      <span>2021-06-24</span>
                    </td>

                    <td className="text-right">
                      <span>1680225.00</span>
                    </td>

                    <td className="text-right">
                      <span>724350.00</span>
                    </td>

                    <td className="text-right">
                      <span>14969.00</span>
                    </td>

                    <td className="text-right">
                      <span>75.0</span>
                    </td>
                  </tr>

                  <tr>
                    <td className="text-right">
                      <span>2021-07-29</span>
                    </td>

                    <td className="text-right">
                      <span>168750.00</span>
                    </td>

                    <td className="text-right">
                      <span>92750.00</span>
                    </td>

                    <td className="text-right">
                      <span>15001.80</span>
                    </td>

                    <td className="text-right">
                      <span>50.0</span>
                    </td>
                  </tr> */}

                  <tr className="bottom-table-row">
                    <td colspan="3" className="text-center">
                      <div className="table-bottom">
                        ROLLOVER COST:
                      <span className={`${rolloverData['rolloverCost'] > 0 ? 'percent-green' : 'percent-red'}`}>
                          {Number(rolloverData['rolloverCostPer']).toFixed(2)}% (INR {Number(rolloverData['rolloverCost']).toFixed(2)})
                      </span>
                      </div>
                    </td>

                    <td colspan="3" className="text-center">
                      <div className="table-bottom">ROLLOVER%:{Number(rolloverData['rolloverPer']).toFixed(2)}%</div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}