import React, { Component } from "react";
import moment from 'moment';
import { CALL_API } from "_services/CALL_API";
import FutruesContracts from "./StockOptionChain/FutruesContracts";
import { roundToTwo } from "_utilities/common";
import Loader from "components/Loader";
import StockOptionChainContent from "./StockOptionChain/StockOptionChainContent";

export default class StockOptionChain extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stockList: [],
      selectedStock: 'NIFTY',
      selectedExpiryDate: '',
      allData: [],
      tableData: [],
      expiryDateList: [],
      futuresContracts: [],
      symbolSpotData: {},
      showFutureCOntractsModal: false,
      isLoaded: false
    };
  }

  componentDidMount() {
    this.getStockList();
    this.getTableData();
    // this.getFutureContracts();
    this.getSymbolSpotData();
  }

  getStockList() {
    CALL_API('GET', process.env.NSE_OPTION_STOCK_DATA, {}, (res => {
      if (res.status) {
        var response = res['data'];
        this.setState({
          stockList: response
        });
      }
    }));
  }

  getTableData() {
    const params = {
      symbol: this.state.selectedStock.toLowerCase(),
      strikePrice: 0
    };
    CALL_API('GET', process.env.NSE_OPTION_DATA, params, (res => {
      if (res.status) {
        var response = res['data'];
        var dateList = [...new Set(response.sort((a, b) => {
          return new Date(a.expiry_date) - new Date(b.expiry_date);
        }).map(x => moment(x.expiry_date).format('MM/DD/YYYY')))];

        this.setState({
          allData: response,
          expiryDateList: dateList,
          selectedExpiryDate: dateList[0]
        }, () => {
          this.getDataByExpiryDate();
        });
      }
    }));
  }

  getDataByExpiryDate = () => {
    this.setState({
      tableData: this.state.allData.filter(x => moment(x.expiry_date).format('MM/DD/YYYY') == this.state.selectedExpiryDate),
      isLoaded: true
    });
  }

  getSymbolSpotData() {
    let symbol = this.state.selectedStock;
    if (symbol == 'NIFTY') {
      symbol = 'NIFTY 50'
    } else if (symbol == 'BANKNIFTY') {
      symbol = 'NIFTY BANK'
    } else if (symbol == 'FINNIFTY') {
      symbol = 'NIFTY FIN SERVICE'
    }
    const params = {
      symbol: symbol
    };
    CALL_API('GET', process.env.SYMBOL_SPOT_DATA, params, (res => {
      if (res.status) {
        var response = res['data'];
        this.setState({
          symbolSpotData: response
        });
      }
    }));
  }

  changeStock = (symbol) => {
    this.setState({
      selectedStock: symbol
    }, () => {
      this.getTableData();
      this.getSymbolSpotData();
    })
  }

  changeExpiryDate = (date) => {
    this.setState({
      selectedExpiryDate: date
    }, () => {
      this.getDataByExpiryDate();
    })
  }

  niftyChart = () => {
    $("#large-Modal").modal("show");
  };

  futuresContracts = () => {
    this.setState({
      showFutureCOntractsModal: true
    })
  };

  render() {

    const { stockList, selectedStock, expiryDateList, allData, tableData, symbolSpotData, selectedExpiryDate, showFutureCOntractsModal, isLoaded } = this.state;
    let stockOptions = '';
    let expDateOptions = '';
    let tableDataOptions = '';
    let spotData = {};

    spotData['index_value'] = symbolSpotData['nifty_value'];
    spotData['change_value'] = symbolSpotData['nifty_value'] - symbolSpotData['previous_close'];
    spotData['percentage_value'] = (spotData['change_value'] / symbolSpotData['previous_close']) * 100;

    if (stockList && stockList.length > 0) {
      stockOptions = stockList.map((item, key) => {
        return <option key={key}>{item['symbol_name']}</option>
      })
    }

    if (expiryDateList && expiryDateList.length > 0) {
      expDateOptions = expiryDateList.map((item, key) => {
        return <option key={key}>{item}</option>
      })
    }

    if (tableData && tableData.length > 0) {
      let Min_Call_changeOI = 0;
      let Min_puts_changeOI = 0;
      let MaxOI_Calls = 0;
      let MaxcngOI_Calls = 0;
      let Maxvolume_Calls = 0;
      let MaxOI_puts = 0;
      let MaxcngOI_puts = 0;
      let Maxvolume_puts = 0;

      Min_Call_changeOI = tableData.sort((a, b) => { return a['calls_change_oi'] - b['calls_change_oi'] })[0]['calls_change_oi'];

      Min_puts_changeOI = tableData.sort((a, b) => { return a['puts_change_oi'] - b['puts_change_oi'] })[0]['puts_change_oi'];

      MaxOI_Calls = tableData.sort((a, b) => { return b['calls_oi'] - a['calls_oi'] })[0]['calls_oi'];
      MaxcngOI_Calls = tableData.sort((a, b) => { return b['calls_change_oi'] - a['calls_change_oi'] })[0]['calls_change_oi'];
      Maxvolume_Calls = tableData.sort((a, b) => { return b['calls_volume'] - a['calls_volume'] })[0]['calls_volume'];

      MaxOI_puts = tableData.sort((a, b) => { return b['puts_oi'] - a['puts_oi'] })[0]['puts_oi'];
      MaxcngOI_puts = tableData.sort((a, b) => { return b['puts_change_oi'] - a['puts_change_oi'] })[0]['puts_change_oi'];
      Maxvolume_puts = tableData.sort((a, b) => { return b['puts_volume'] - a['puts_volume'] })[0]['puts_volume'];

      tableDataOptions = tableData.sort((a, b) => {
        return a.strike_price - b.strike_price;
      }).map((item, key) => {
        let CallBuiltUpTxt = '';
        let CallBuiltUpClass = '';
        let PutBuiltUpTxt = '';
        let PutBuiltUpClass = '';
        let puts_Hig_volume = '';
        let classputs = '';
        let puts_Hig_OI = '';
        let calls_Hig_volume = '';
        let classcalls = '';
        let calls_Hig_OI = '';
        if (item['index_close'] > item['strike_price']) {
          //Calls BUILTUP
          if (item.calls_change_oi > 0 && item.calls_net_change > 0) {
            CallBuiltUpTxt = "Call Buying";
            CallBuiltUpClass = "high_value_Builtup";
          } else if (item.calls_change_oi > 0 && item.calls_net_change < 0) {
            CallBuiltUpTxt = "Call Writing";
            CallBuiltUpClass = "low_value_Builtup";
          } else if (item.calls_change_oi < 0 && item.calls_net_change > 0) {
            CallBuiltUpTxt = "Call Short Covering";
            CallBuiltUpClass = "high_value_Builtup";
          } else if (item.calls_change_oi < 0 && item.calls_net_change < 0) {
            CallBuiltUpTxt = "Call Long Covering";
            CallBuiltUpClass = "low_value_Builtup";
          } else if (item.calls_change_oi == 0 && item.calls_net_change == 0) {
            CallBuiltUpTxt = "No conclusion";
            CallBuiltUpClass = "";
          } else {
            CallBuiltUpTxt = "No conclusion";
            CallBuiltUpClass = "";
          }

          // Puts BUILTUP
          if (item.puts_change_oi > 0 && item.puts_net_change > 0) {
            PutBuiltUpTxt = "Put Buying";
            PutBuiltUpClass = "low_value_Builtup";
          } else if (item.puts_change_oi > 0 && item.puts_net_change < 0) {
            PutBuiltUpTxt = "Put Writing";
            PutBuiltUpClass = "high_value_Builtup";
          } else if (item.puts_change_oi < 0 && item.puts_net_change > 0) {
            PutBuiltUpTxt = "Put Short Covering";
            PutBuiltUpClass = "low_value_Builtup";
          } else if (item.puts_change_oi < 0 && item.puts_net_change < 0) {
            PutBuiltUpTxt = "Put Long Covering";
            PutBuiltUpClass = "high_value_Builtup";
          } else if (item.puts_change_oi == 0 && item.puts_net_change == 0) {
            PutBuiltUpTxt = "No conclusion";
            PutBuiltUpClass = "";
          } else {
            PutBuiltUpTxt = "No conclusion";
            PutBuiltUpClass = "";
          }

          if (item.puts_volume == Maxvolume_puts) {
            puts_Hig_volume = "high_value";
          } else {
            puts_Hig_volume = "";
          }

          if (item.puts_change_oi == MaxcngOI_puts) {
            classputs = "high_value";
          } else if (item.puts_change_oi == Min_puts_changeOI) {
            classputs = "low_value";
          } else {
            classputs = "";
          }

          if (item.puts_oi == MaxOI_puts) {
            puts_Hig_OI = "high_value";
          } else {
            puts_Hig_OI = "";
          }

          if (item.calls_volume == Maxvolume_Calls) {
            calls_Hig_volume = "high_value";
          } else {
            calls_Hig_volume = "calls-bg1";
          }

          if (item.calls_change_oi == MaxcngOI_Calls) {
            classcalls = "high_value";
          } else if (item.calls_change_oi == Min_Call_changeOI) {
            classcalls = "low_value";
          } else {
            classcalls = "calls-bg1";
          }

          if (item.calls_oi == MaxOI_Calls) {
            calls_Hig_OI = "high_value";
          } else {
            calls_Hig_OI = "calls-bg1";
          }

          return <tr>
            <td className="text-center">
              <span className="pt-0 pb-0">
                <button
                  type="button"
                  className="
                  glyphicon
                  show-features-icons show-features
                  dropdown-toggle
                "
                  title="Greeks"
                  id="dropdownMenuButton1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    src="images/greek-white.svg"
                    alt="Details"
                    title="Details"
                  />
                </button>

                <ul
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton1"
                >
                  <li className="dropdown-item">
                    Delta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Theta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Vega: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Gamma: <span></span>
                  </li>
                </ul>
              </span>
            </td>
            <td className={`text-left ${CallBuiltUpClass}`}>{CallBuiltUpTxt}</td>
            {
              item.calls_oi == 0 ? <td class="text-center calls-bg1">-</td> : (
                <td class={`${calls_Hig_OI} calls-bg1`} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.calls_oi)}</a></td>
              )
            }
            {
              item.calls_change_oi > 0 ?
                <td class={`${classcalls} percent-green calls-bg1`}>{roundToTwo(item.calls_change_oi)}</td> :
                item.calls_change_oi < 0 ?
                  <td class={`${classcalls} percent-red calls-bg1`}>{roundToTwo(item.calls_change_oi)}</td> :
                  <td class="text-center calls-bg1">-</td>
            }
            {
              item.calls_volume == 0 ? <td class="text-center calls-bg1">-</td> :
                <td class="calls-bg1" style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.calls_volume)}</a></td>
            }
            {
              item.calls_iv == 0 || item.calls_iv == null || item.calls_iv == '0.00' ? <td class="text-center calls-bg1">-</td> :
                <td class="calls-bg1">{item.calls_iv}</td>
            }
            {
              item.calls_ltp == 0 ? <td class="text-center calls-bg1">-</td> :
                <td class="calls-bg1" style={{ cursor: 'pointer', textDecoration: 'underline' }}><a class="ltp-color">{roundToTwo(item.calls_ltp)}</a></td>
            }
            {
              item.calls_net_change > 0 ? <td class="calls-bg1"><span class="percent-green">{item.calls_net_change}</span></td> :
                item.calls_net_change < 0 ? <td class="calls-bg1"><span class="percent-red">{item.calls_net_change}</span></td> :
                  <td class="calls-bg1 text-center"><span class="">-</span></td>
            }
            <td class="calls-bg2 border text-center">{roundToTwo(item.strike_price)}</td>
            {
              item.puts_net_change > 0 ? <td class=""><span class="percent-green">{roundToTwo(item.puts_net_change)}</span></td> :
                item.puts_net_change < 0 ? <td class=""><span class="percent-red">{roundToTwo(item.puts_net_change)}</span></td> :
                  <td class="text-center"><span class="">-</span></td>
            }
            {
              item.puts_ltp == 0 ? <td class="text-center">-</td> :
                <td class="" style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.puts_ltp)}</a></td>
            }
            {
              item.puts_iv == 0 || item.puts_iv == null || item.puts_iv == 0.00 ? <td class="text-center">-</td> :
                <td class="">{item.puts_iv}</td>
            }
            {
              item.puts_volume == 0 ? <td class="text-center">-</td> :
                <td class={puts_Hig_volume} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.puts_volume)}</a></td>
            }
            {
              item.puts_change_oi > 0 ? <td class={`${classputs} percent-green`}>{roundToTwo(item.puts_change_oi)}</td> :
                item.puts_change_oi < 0 ? <td class={`${classputs} percent-red`}>{roundToTwo(item.puts_change_oi)}</td> :
                  <td class="text-center">-</td>
            }
            {
              item.puts_oi == 0 ? <td class="text-center">-</td> :
                <td class={puts_Hig_OI} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{item.puts_oi}</a></td>
            }
            <td className={PutBuiltUpClass}>{PutBuiltUpTxt}</td>
            <td className="text-center">
              <span className="pt-0 pb-0">
                <button
                  type="button"
                  className="
                glyphicon
                show-features-icons show-features
                dropdown-toggle
              "
                  title="Greeks"
                  id="dropdownMenuButton1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    src="images/greek-white.svg"
                    alt="Details"
                    title="Details"
                  />
                </button>

                <ul
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton1"
                >
                  <li className="dropdown-item">
                    Delta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Theta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Vega: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Gamma: <span></span>
                  </li>
                </ul>
              </span>
            </td>
          </tr>
        } else {
          if (item.calls_change_oi > 0 && item.calls_net_change > 0) {
            CallBuiltUpTxt = "Call Buying";
            CallBuiltUpClass = "high_value_Builtup";
          } else if (item.calls_change_oi > 0 && item.calls_net_change < 0) {
            CallBuiltUpTxt = "Call Writing";
            CallBuiltUpClass = "low_value_Builtup";
          } else if (item.calls_change_oi < 0 && item.calls_net_change > 0) {
            CallBuiltUpTxt = "Call Short Covering";
            CallBuiltUpClass = "high_value_Builtup";
          } else if (item.calls_change_oi < 0 && item.calls_net_change < 0) {
            CallBuiltUpTxt = "Call Long Covering";
            CallBuiltUpClass = "low_value_Builtup";
          } else if (item.calls_change_oi == 0 && item.calls_net_change == 0) {
            CallBuiltUpTxt = "No conclusion";
            CallBuiltUpClass = "";
          } else {
            CallBuiltUpTxt = "No conclusion";
            CallBuiltUpClass = "";
          }

          if (item.puts_change_oi > 0 && item.puts_net_change > 0) {
            PutBuiltUpTxt = "Put Buying";
            PutBuiltUpClass = "low_value_Builtup";
          } else if (item.puts_change_oi > 0 && item.puts_net_change < 0) {
            PutBuiltUpTxt = "Put Writing";
            PutBuiltUpClass = "high_value_Builtup";
          } else if (item.puts_change_oi < 0 && item.puts_net_change > 0) {
            PutBuiltUpTxt = "Put Short Covering";
            PutBuiltUpClass = "low_value_Builtup";
          } else if (item.puts_change_oi < 0 && item.puts_net_change < 0) {
            PutBuiltUpTxt = "Put Long Covering";
            PutBuiltUpClass = "high_value_Builtup";
          } else if (item.puts_change_oi == 0 && item.puts_net_change == 0) {
            PutBuiltUpTxt = "No conclusion";
            PutBuiltUpClass = "";
          } else {
            PutBuiltUpTxt = "No conclusion";
            PutBuiltUpClass = "";
          }

          if (item.puts_volume == Maxvolume_puts) {
            puts_Hig_volume = "high_value";
          } else {
            puts_Hig_volume = "calls-bg1";
          }

          if (item.puts_change_oi == MaxcngOI_puts) {
            classputs = "high_value";
          } else if (item.puts_change_oi == Min_puts_changeOI) {
            classputs = "low_value";
          } else {
            classputs = "calls-bg1";
          }

          if (item.puts_oi == MaxOI_puts) {
            puts_Hig_OI = "high_value";
          } else {
            puts_Hig_OI = "calls-bg1";
          }
          if (item.calls_volume == Maxvolume_Calls) {
            calls_Hig_volume = "high_value";
          } else {
            calls_Hig_volume = "";
          }

          if (item.calls_change_oi == MaxcngOI_Calls) {
            classcalls = "high_value";
          } else if (item.calls_change_oi == Min_Call_changeOI) {
            classcalls = "low_value";
          } else {
            classcalls = "";
          }

          if (item.calls_oi == MaxOI_Calls) {
            calls_Hig_OI = "high_value";
          } else {
            calls_Hig_OI = "";
          }

          return <tr>
            <td className="text-center">
              <span className="pt-0 pb-0">
                <button
                  type="button"
                  className="
                  glyphicon
                  show-features-icons show-features
                  dropdown-toggle
                "
                  title="Greeks"
                  id="dropdownMenuButton1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    src="images/greek-white.svg"
                    alt="Details"
                    title="Details"
                  />
                </button>

                <ul
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton1"
                >
                  <li className="dropdown-item">
                    Delta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Theta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Vega: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Gamma: <span></span>
                  </li>
                </ul>
              </span>
            </td>
            <td className={`text-left ${CallBuiltUpClass}`}>{CallBuiltUpTxt}</td>
            {
              item.calls_oi == 0 ? <td class="text-center">-</td> : (
                <td class={`${calls_Hig_OI}`} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.calls_oi)}</a></td>
              )
            }
            {
              item.calls_change_oi > 0 ?
                <td class={`${classcalls} percent-green`}>{roundToTwo(item.calls_change_oi)}</td> :
                item.calls_change_oi < 0 ?
                  <td class={`${classcalls} percent-red`}>{roundToTwo(item.calls_change_oi)}</td> :
                  <td class="text-center">-</td>
            }
            {
              item.calls_volume == 0 ? <td class="text-center">-</td> :
                <td class={calls_Hig_volume} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.calls_volume)}</a></td>
            }
            {
              item.calls_iv == 0 || item.calls_iv == null || item.calls_iv == '0.00' ? <td class="text-center">-</td> :
                <td>{item.calls_iv}</td>
            }
            {
              item.calls_ltp == 0 ? <td class="text-center">-</td> :
                <td style={{ cursor: 'pointer', textDecoration: 'underline' }}><a class="ltp-color">{item.calls_ltp}</a></td>
            }
            {
              item.calls_net_change > 0 ? <td><span class="percent-green">{item.calls_net_change}</span></td> :
                item.calls_net_change < 0 ? <td><span class="percent-red">{item.calls_net_change}</span></td> :
                  <td class="calls-bg1 text-center"><span class="">-</span></td>
            }
            <td class="calls-bg2 border text-center">{roundToTwo(item.strike_price)}</td>

            {
              item.puts_net_change > 0 ? <td class="calls-bg1"><span class="percent-green">{roundToTwo(item.puts_net_change)}</span></td> :
                item.puts_net_change < 0 ? <td class="calls-bg1"><span class="percent-red">{roundToTwo(item.puts_net_change)}</span></td> :
                  <td class="text-center calls-bg1"><span class="">-</span></td>
            }
            {
              item.puts_ltp == 0 ? <td class="calls-bg1 text-center">-</td> :
                <td class="calls-bg1" style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.puts_ltp)}</a></td>
            }
            {
              item.puts_iv == 0 || item.puts_iv == null || item.puts_iv == 0.00 ? <td class="calls-bg1 text-center">-</td> :
                <td class="calls-bg1">{item.puts_iv}</td>
            }
            {
              item.puts_volume == 0 ? <td class="calls-bg1 text-center">-</td> :
                <td class={`${puts_Hig_volume} calls-bg1`} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{roundToTwo(item.puts_volume)}</a></td>
            }
            {
              item.puts_change_oi > 0 ? <td class={`${classputs} percent-green calls-bg1`}>{roundToTwo(item.puts_change_oi)}</td> :
                item.puts_change_oi < 0 ? <td class={`${classputs} percent-red calls-bg1`}>{roundToTwo(item.puts_change_oi)}</td> :
                  <td class="text-center calls-bg1">-</td>
            }
            {
              item.puts_oi == 0 ? <td class="text-center calls-bg1">-</td> :
                <td class={`${puts_Hig_OI} calls-bg1`} style={{ cursor: 'pointer', textDecoration: 'underline' }}><a>{item.puts_oi}</a></td>
            }
            <td className={PutBuiltUpClass}>{PutBuiltUpTxt}</td>
            <td className="text-center">
              <span className="pt-0 pb-0">
                <button
                  type="button"
                  className="
                glyphicon
                show-features-icons show-features
                dropdown-toggle
              "
                  title="Greeks"
                  id="dropdownMenuButton1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    src="images/greek-white.svg"
                    alt="Details"
                    title="Details"
                  />
                </button>

                <ul
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton1"
                >
                  <li className="dropdown-item">
                    Delta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Theta: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Vega: <span></span>
                  </li>

                  <li className="dropdown-item">
                    Gamma: <span></span>
                  </li>
                </ul>
              </span>
            </td>
          </tr>
        }
      })
    }

    return (
      <React.Fragment>
        {
          isLoaded ? '' : <Loader />
        }
        {
          showFutureCOntractsModal ? <FutruesContracts symbol={selectedStock} spotData={spotData} closeModal={() => this.setState({ showFutureCOntractsModal: false })} /> : ''
        }
        <div className="row option-heading mt-2">
          <div className="col-lg-6 col-xl-6 col-md-6 col-12">
            <h1 className="d-none d-md-block">
              Option Chain (Equity Derivatives)
              </h1>
          </div>
          <div className="col-lg-6 col-xl-6 col-md-6 col-12 text-left text-lg-right">
            <span>Last data auto refreshed at {allData && allData.length > 0 ? moment(moment(allData[0]['created_at']).format('YYYY-MM-DD') + ' ' + allData[0]['time']).format('DD-MMM-YYYY hh:mm A') : ''}</span>
          </div>

          <div className="col-lg-4 col-xl-4 col-md-4 col-6">
            <div className="option_select option_select-align">
              <div className="form-group">
                <label for="exampleFormControlSelect1">
                  <b>Symbol:</b>
                </label>
                <select id="exampleFormControlSelect1" value={selectedStock} onChange={(e) => this.changeStock(e.target.value)}>
                  {
                    stockOptions != '' ? stockOptions : ''
                  }
                </select>

                <a
                  href={process.env.OLD_SITE_URL + 'stocks-analysis/' + selectedStock.toLowerCase()}
                  className="MshownavMy ml-3"
                  title={selectedStock}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-box-arrow-up-right"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"
                    />
                    <path
                      fill-rule="evenodd"
                      d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-5 col-xl-5 col-md-5 col-6">
            <div className="option_select text-left option_select-align">
              <div className="form-group">
                <label for="expiry_date" className="">
                  <b>Expiry Date:</b>
                </label>
                <select id="exampleFormControlSelect1" className="mt-0" value={selectedExpiryDate} onChange={(e) => this.changeExpiryDate(e.target.value)}>
                  {
                    expDateOptions != '' ? expDateOptions : ''
                  }
                </select>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-xl-3 col-md-3 col-6">
            <div className="option_select text-center option_select-align mb-3">
              <span
                title="Futures Contracts"
                className="future-contract-btn"
                onClick={this.futuresContracts}
              >
                Futures Contracts
                </span>

              <a
                href="https://www.niftytrader.in/live-nifty-open-interest"
                title="Chart View"
                className="
                    nifty-chart-icon nifty-chart-icon-2
                    float-right
                    d-none d-md-block
                  "
              >
                <span className="table-tooltip-box">Chart View</span>
              </a>
            </div>
          </div>

          <div className="col-lg-4 col-xl-4 col-md-4 col-6">
            <div className="option_select text-left option_select-align">
              <div className="form-group">
                <label for="PCR" className="">
                  <b>PCR:</b>
                </label>
                <label>0.7848</label>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-xl-4 col-md-4 col-12">
            <div className="option_select text-left option_select-align">
              <div className="form-group">
                <label for="ExpectedRange" className="mr-0">
                  <b>Expected Range:</b>
                  <button
                    type="button"
                    className="tooltip-top"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="For the day based on OI Data"
                    data-original-title="For the day based on OI Data"
                  >
                    {/* <i className="fal fa-info-circle"></i> */}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-info-circle"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                      <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                    </svg>
                  </button>
                </label>
                <label>14900.00 ~ 15000.00</label>
              </div>
            </div>
          </div>
          <div
            className="
              option_select
              text-right
              MspotRight
              col-lg-4 col-xl-4 col-md-4 col-12
            "
          >
            <label for="SpotPrice" className="">
              <b>Spot Price:</b>
            </label>
            <span className={`sp ${spotData['change_value'] > 0 ? 'newstockLTPUP' : 'newstockLTPDown'}`}>{spotData['index_value']}</span>

            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className={`bi ${spotData['change_value'] > 0 ? 'bi-caret-up-fill percent-green' : 'bi-caret-down-fill percent-red'}`}
              viewBox="0 0 16 16"
            >
              <path d={spotData['change_value'] > 0 ? "m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" : "M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"} />
            </svg>

            {/* <i id="StockCaret" className="fas fa-caret-down percent-red"></i> */}
            <b className={spotData['change_value'] > 0 ? 'color-text' : 'color-text-red'} id="StockPriceChange">
              {Number(spotData['change_value']).toFixed(2)} ({Number(spotData['percentage_value'].toFixed(2))}%)
              </b>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12 fixetable ">
            <table className="table optionchain_table">
              <thead>
                <tr id="call_put_head" className="red">
                  <th colspan="8">
                    <img src="images/call.png" alt="" className="mr-2" />{" "}
                      CALLS
                    </th>
                  <th></th>
                  <th colspan="8">
                    <img src="images/put.png" alt="" className="mr-2" /> PUTS
                    </th>
                </tr>
                <tr className="red">
                  <th id="call">
                    <img src="images/call.png" alt="" />
                  </th>
                  <th>BUILTUP</th>
                  <th>OI</th>
                  <th>CHNG IN OI</th>
                  <th>VOL</th>
                  <th>IV</th>
                  <th>LTP</th>
                  <th>NET CHNG</th>
                  <th>STRIKE PRICE</th>
                  <th>NET CHNG</th>
                  <th>LTP</th>
                  <th>IV</th>
                  <th>VOL</th>
                  <th>CHNG IN OI</th>
                  <th>OI</th>
                  <th>BUILTUP</th>
                  <th id="put">
                    <img src="images/put.png" alt="" />
                  </th>
                </tr>
              </thead>
              <tbody className="" id="tbody">
                {tableDataOptions != '' ? tableDataOptions : ''}
                <tr className="p-5">
                  <td>
                    <span>
                      <b>Total</b>
                    </span>
                  </td>
                  <td></td>
                  <td>
                    <b>30942825</b>
                  </td>

                  <td></td>
                  <td>
                    <b>248223525</b>
                  </td>

                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>
                    <b>17487600</b>
                  </td>

                  <td></td>
                  <td>
                    <b>251383725</b>
                  </td>

                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <StockOptionChainContent />
      </React.Fragment >
    );
  }
}
