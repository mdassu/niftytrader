import React from "react";
import { ChartComponent, SeriesCollectionDirective, SeriesDirective, AxesDirective, AxisDirective, Inject, ColumnSeries, Legend, Category, Tooltip, DataLabel, LineSeries } from '@syncfusion/ej2-react-charts';
class ColumnLineMixedChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true
    };
    this.primaryxAxis = { valueType: 'Category', majorGridLines: { width: 0 }, };
    this.primaryyAxis = { majorGridLines: { width: 0 } };
    this.tooltip = { enable: true };
    this.legendSettings = { visible: true, position: 'Bottom' };
  }

  componentDidMount() {
  }

  componentDidUpdate(prevProps) {
    if (this.props.chartTitle != prevProps['chartTitle']) {
      this.setState({
        isLoaded: false
      }, () => {
        this.setState({
          isLoaded: true
        })
      })

    }
  }

  render() {
    const { chartTitle, chartData, columnName, columnColor, columnKey, xName } = this.props;
    const { isLoaded } = this.state;
    return (
      <React.Fragment>
        {
          isLoaded ? (
            <ChartComponent id='charts' primaryXAxis={this.primaryxAxis} primaryYAxis={this.primaryyAxis} tooltip={this.tooltip} legendSettings={this.legendSettings} chartArea={{ border: { width: 0 } }} title={chartTitle} height="400px">
              <Inject services={[ColumnSeries, Legend, Tooltip, DataLabel, LineSeries, Category]} />
              <AxesDirective>
                <AxisDirective rowIndex={0} name='yAxis1' opposedPosition={true} majorGridLines={{ width: 0 }}>
                </AxisDirective>
                <AxisDirective rowIndex={0} name='yAxisLine1' opposedPosition={false} majorGridLines={{ width: 0 }}>
                </AxisDirective>
                <AxisDirective rowIndex={0} name='yAxisLine2' opposedPosition={true} majorGridLines={{ width: 0 }}>
                </AxisDirective>
              </AxesDirective>
              <SeriesCollectionDirective>
                {
                  columnName[0] ? (
                    <SeriesDirective dataSource={chartData} xName={xName} name={columnName[0]} yName={columnKey[0]} type='Column' fill={columnColor[0]}>
                    </SeriesDirective>
                  ) : ''
                }
                {
                  columnName[1] ? (
                    <SeriesDirective dataSource={chartData} xName={xName} name={columnName[1]} yName={columnKey[1]} type='Column' fill={columnColor[1]} yAxisName='yAxis1'>
                    </SeriesDirective>
                  ) : ''
                }
                {
                  columnName[2] ? (
                    <SeriesDirective dataSource={chartData} xName={xName} name={columnName[2]} yName={columnKey[2]} type='Line' fill={columnColor[2]} yAxisName='yAxisLine1' width="3px">
                    </SeriesDirective >
                  ) : ''
                }
                {
                  columnName[3] ? (
                    <SeriesDirective dataSource={chartData} xName={xName} name={columnName[3]} yName={columnKey[3]} type='Line' fill={columnColor[3]} yAxisName='yAxisLine2' width="3px">
                    </SeriesDirective >
                  ) : ''
                }

                {/* <SeriesDirective dataSource={this.state.data} xName='country' yName='bronze' type='Column'>
          </SeriesDirective> */}
              </SeriesCollectionDirective>
            </ChartComponent>
          ) : ''
        }

      </React.Fragment>
    );
  }
}

export default ColumnLineMixedChart;