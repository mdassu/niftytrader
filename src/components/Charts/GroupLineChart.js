import React from "react";
import { ChartComponent, SeriesCollectionDirective, SeriesDirective, AxesDirective, AxisDirective, Inject, ColumnSeries, Legend, Category, Tooltip, DataLabel, LineSeries, DateTime } from '@syncfusion/ej2-react-charts';
class GroupLineChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true
    };
    this.primaryxAxis = { valueType: 'DateTime', labelFormat: "h:mm a", majorGridLines: { width: 0 } };
    this.primaryyAxis = {
      // interval: this.props.yAxisInterval, 
      majorTickLines: { width: 0 }
    };
    this.tooltip = { enable: true };
    this.legendSettings = { visible: true, position: 'Bottom' };
  }

  componentDidMount() {
  }

  componentDidUpdate(prevProps) {
    if (this.props.chartTitle != prevProps['chartTitle']) {
      this.setState({
        isLoaded: false
      }, () => {
        this.setState({
          isLoaded: true
        })
      })
    }
  }

  render() {
    const { chartTitle, chartData, columnName, columnColor, columnKey } = this.props;
    const { isLoaded } = this.state;
    var seriesDirective = '';
    if (columnName && columnName.length > 0) {
      seriesDirective = columnName.map((item, key) => {
        if (key == 1) {
          return (<SeriesDirective key={key} dataSource={chartData} xName='time' name={item} yName={columnKey[key]} type='Line' fill={columnColor[key]} yAxisName='yAxis1' width="3px">
          </SeriesDirective >)
        } else {
          return (
            <SeriesDirective key={key} dataSource={chartData} xName='time' name={item} yName={columnKey[key]} type='Line' fill={columnColor[key]} width="3px" lineStyle={{ color: '#2b2b43', width: 0 }}>
            </SeriesDirective >
          )
        }
      });
    }
    if (columnName.length > 1) {
      this.primaryyAxis['majorGridLines'] = { width: 0 };
      this.primaryyAxis['lineStyle'] = {
        color: '#2b2b43', width: 0
      };
    }
    return (
      <React.Fragment>
        {
          isLoaded ? (
            <ChartComponent id='charts' primaryXAxis={this.primaryxAxis} primaryYAxis={this.primaryyAxis} tooltip={this.tooltip} legendSettings={this.legendSettings} chartArea={{ border: { width: 0 } }} title={chartTitle} height="400px" >
              <Inject services={[ColumnSeries, Legend, Tooltip, DataLabel, LineSeries, Category, DateTime]} />
              <AxesDirective>
                <AxisDirective rowIndex={0} name='yAxis1' opposedPosition={true}>
                </AxisDirective>
              </AxesDirective>
              <SeriesCollectionDirective>
                {
                  seriesDirective && seriesDirective != '' ? seriesDirective : ''
                }
              </SeriesCollectionDirective>
            </ChartComponent >
          ) : ''
        }
      </React.Fragment>
    );
  }
}

export default GroupLineChart;