import * as React from "react";
import { StockChartComponent, StockChartSeriesCollectionDirective, StockChartSeriesDirective, Inject, Crosshair, DateTime, Tooltip, RangeTooltip, ColumnSeries, LineSeries, SplineSeries, CandleSeries, HiloOpenCloseSeries, HiloSeries, RangeAreaSeries, Trendlines, StockChartRowsDirective, StockChartRowDirective, StockChartAxesDirective, StockChartAxisDirective } from '@syncfusion/ej2-react-charts';
import { EmaIndicator, RsiIndicator, BollingerBands, TmaIndicator, MomentumIndicator, SmaIndicator, AtrIndicator, AccumulationDistributionIndicator, MacdIndicator, StochasticIndicator } from '@syncfusion/ej2-react-charts';
const SAMPLE_CSS = `
    .control-fluid {
        padding: 0px !important;
    }
        .charts {
            align :center
        }`;
export let tooltipRender = (args) => {
  if (args.text.split('<br/>')[4]) {
    let target = args.text.split('<br/>');
    args.text = target[0] + '<br/>' + target[1] + '<br/>' + target[2] + '<br/>' + target[3]
  }
};
export default class CandleStickChart extends React.Component {

  constructor(props) {
    super(props);
  }



  render() {
    return (<div className='control-pane'>
      <style>
        {SAMPLE_CSS}
      </style>
      <div className='control-section'>
        <StockChartComponent
          height="400px"
          border={{ width: 0 }}
          id='stockchartpane'
          primaryYAxis={{
            lineStyle: { color: '#2b2b43', width: 0 },
            majorTickLines: { color: '#2b2b43', width: 1 },
            majorGridLines: { color: '#e8e9eb', width: .8 },
            labelPosition: 'outside',
            opposedPosition: true,
            labelStyle: {
              color: 'red',
              size: 15
            },
            title: 'Volume',
          }}
          primaryXAxis={{
            lineStyle: { color: '#2b2b43', width: 0 },
            majorTickLines: { color: '#2b2b43', width: 1 },
            crosshairTooltip: { enable: true },
            majorGridLines: { width: 0 },
            valueType: 'DateTime',
            labelFormat: "MMM, dd"
          }}
          chartArea={{ border: { width: 0 } }}
          tooltip={{ enable: true }}
          tooltipRender={tooltipRender}
          // axisLabelRender={this.axisLabelRender.bind(this)}
          crosshair={{ enable: true }}
          // load={this.load.bind(this)}
          indicatorType={[]}
          trendlineType={[]}
          seriesType={[]}
          enableCustomRange={false}
          enableSelector={false}
          periods={[
            { text: '1M', interval: 1, intervalType: 'Months' },
            { text: '3M', interval: 3, intervalType: 'Months', selected: true },
            { text: '6M', interval: 6, intervalType: 'Months' },
            { text: '1Y', interval: 1, intervalType: 'Years' },
          ]}
        >
          <Inject services={[DateTime, Crosshair, Tooltip, RangeTooltip, ColumnSeries, LineSeries, CandleSeries]} />
          {/* <StockChartRowsDirective>
            <StockChartRowDirective height={'25%'}>
            </StockChartRowDirective>
            <StockChartRowDirective height={'10%'}>
            </StockChartRowDirective>
            <StockChartRowDirective height={'65%'}>
            </StockChartRowDirective>
          </StockChartRowsDirective> */}
          <StockChartAxesDirective>
            <StockChartAxisDirective name='yAxis1' rowIndex={2} title="OHLC" opposedPosition={true} lineStyle={{ color: '#2b2b43', width: 0 }} majorGridLines={{ color: '#e8e9eb', width: .8 }} majorTickLines={{ color: '#2b2b43', width: 1 }}>
            </StockChartAxisDirective>
          </StockChartAxesDirective>
          <StockChartSeriesCollectionDirective>
            {/* <StockChartSeriesDirective dataSource={this.props.chartData} xName='created_at' yName='volume' type='Column' fill="#BED3FD">
            </StockChartSeriesDirective> */}
            <StockChartSeriesDirective dataSource={this.props.chartData} xName='created_at' type='Candle' yAxisName='yAxis1' fill="#645DF9">
            </StockChartSeriesDirective>
          </StockChartSeriesCollectionDirective>
        </StockChartComponent>
      </div>
    </div >);
  }
  axisLabelRender(args) {
    let text = parseInt(args.text);
    if (args.axis.name === "primaryYAxis") {
      args.text = text / 100000000 + 'B';
    }
  }
  load(args) {
    let selectedTheme = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.stockChart.theme = (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).
      replace(/-dark/i, "Dark");
  }
}