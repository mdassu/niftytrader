import {
  SparklineComponent,
  Inject,
  SparklineTooltip,
} from "@syncfusion/ej2-react-charts";
import React, { Component } from "react";
import * as ReactDOM from "react-dom";

class WatchListChart extends Component {
  constructor(props) {
    super(props);
    this.data = [
      { month: "Jan", sales: 35 },
      { month: "Feb", sales: 28 },
      { month: "Mar", sales: 34 },
      { month: "Apr", sales: 32 },
      { month: "May", sales: 40 },
      { month: "Jun", sales: 32 },
      { month: "Jul", sales: 35 },
      { month: "Aug", sales: 55 },
      { month: "Sep", sales: 38 },
      { month: "Oct", sales: 30 },
      { month: "Nov", sales: 25 },
      { month: "Dec", sales: 32 },
    ];
    this.tooltip = { enable: true, shared: false };
    this.primaryyAxis = {};
    this.primaryxAxis = {
      valueType: "DateTime",
      labelFormat: "h:mm a",
      majorGridLines: { width: 0 },
    };
    this.legendSettings = { visible: true };
    this.marker = { dataLabel: { visible: true } };
    this.state = {
      chartData: [],
    };
  }

  componentDidMount() {


  }

   componentDidUpdate(prevProps){
    let wtchGphDta=[] ;
    if (prevProps.graphWatchlistData.length !== this.props.graphWatchlistData.length) {
     let gphWatchData = [];
     let result =this.props.graphWatchlistData.filter((value) => {
         if (value["symbol"] === this.props.symbolName ) {
           let data = {
             symbol: value["symbol"],
             close: value["close"],
             created:value["created"]
           };
         return  gphWatchData.push(data);
         }
       }) ;
     
    this.setState({
        chartData:gphWatchData
    })
      }
  }

  render() {
    const { chartData } = this.state;
    

    //   return ( <ChartComponent id="charts" primaryXAxis={this.primaryxAxis} legendSettings={this.legendSettings} primaryYAxis={this.primaryyAxis} tooltip={this.tooltip}
    //   width='100%' height='50%'>
    //   <Inject services={[ColumnSeries, DataLabel, Tooltip, Legend, LineSeries, Category,DateTime]}/>
    //   <SeriesCollectionDirective>
    //     <SeriesDirective dataSource={chartData} xName='created' yName='close'  marker={this.marker}/>
    //   </SeriesCollectionDirective>
    // </ChartComponent> );

    return (
      <div>
        {
           this.props.topWatchList.map((item, key) => {
         
            if (item["symbol_name"] === this.props.symbolName) {
             
              return (
                <div>
                  {" "}
                  <SparklineComponent
                    id="sparkline"
                    theme="Highcontrast"
                    dataLabelSettings={{ visible: ["All"] }}
                    tooltipSettings={{
                      trackLineSettings: { visible: true },
                    }}
                    lineWidth={3}
                    type="Column"
                    dataSource={[
                      { x: 0, xval: "2005", yval: 20090440 },
                      { x: 1, xval: "2006", yval: 20264080 },
                      { x: 2, xval: "2007", yval: 20434180 },
                      { x: 3, xval: "2008", yval: 21007310 },
                      { x: 4, xval: "2009", yval: 21262640 },
                      { x: 5, xval: "2010", yval: 21515750 },
                      { x: 6, xval: "2011", yval: 21766710 },
                      { x: 7, xval: "2012", yval: 22015580 },
                      { x: 8, xval: "2013", yval: 22262500 },
                      { x: 9, xval: "2014", yval: 22507620 },
                    ]}
                    xName="xval"
                    yName="yval"
                  >
                    <Inject services={[SparklineTooltip]} />
                  </SparklineComponent>{" "}
                </div>
              );
            }
          })
        }
      </div>
       
    );
  }
}

export default WatchListChart;
