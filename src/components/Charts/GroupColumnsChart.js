import React from "react";
import {
  ChartComponent,
  SeriesCollectionDirective,
  SeriesDirective,
  Inject,
  ColumnSeries,
  Legend,
  Category,
  Tooltip,
  DataLabel,
  LineSeries,
} from "@syncfusion/ej2-react-charts";
class GroupColumnsChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true,
    };
    this.primaryxAxis = { valueType: "Category", majorGridLines: { width: 0 } };
    this.primaryyAxis = {};
    this.tooltip = { enable: true };
    this.legendSettings = { visible: true, position: "Bottom" };
  }

 

  componentDidUpdate(prevProps) {
    if (this.props.chartTitle != prevProps["chartTitle"]) {
      this.setState(
        {
          isLoaded: false,
        },
        () => {
          this.setState({
            isLoaded: true,
          });
        }
      );
    }
  }

  render() {
    const {
      chartTitle,
      chartData,
      columnName,
      columnColor,
      columnKey,
      xName,
    } = this.props;
    const { isLoaded } = this.state;
    return (
      <React.Fragment>
        {isLoaded ? (
          <ChartComponent
            id="charts"
            primaryXAxis={this.primaryxAxis}
            primaryYAxis={this.primaryyAxis}
            tooltip={this.tooltip}
            legendSettings={this.legendSettings}
            chartArea={{ border: { width: 0 } }}
            title={chartTitle}
            height="400px"
          >
            <Inject
              services={[
                ColumnSeries,
                Legend,
                Tooltip,
                DataLabel,
                LineSeries,
                Category,
              ]}
            />
            <SeriesCollectionDirective>
              {columnName[0] ? (
                <SeriesDirective
                  dataSource={chartData}
                  xName={xName}
                  name={columnName[0]}
                  yName={columnKey[0]}
                  type="Column"
                  fill={columnColor[0]}
                ></SeriesDirective>
              ) : (
                ""
              )}
              {columnName[1] ? (
                <SeriesDirective
                  dataSource={chartData}
                  xName={xName}
                  name={columnName[1]}
                  yName={columnKey[1]}
                  type="Column"
                  fill={columnColor[1]}
                ></SeriesDirective>
              ) : (
                ""
              )}
              {columnName[2] ? (
                <SeriesDirective
                  dataSource={chartData}
                  xName={xName}
                  name={columnName[2]}
                  yName={columnKey[2]}
                  type="Column"
                  fill={columnColor[2]}
                ></SeriesDirective>
              ) : (
                ""
              )}
              {columnName[3] ? (
                <SeriesDirective
                  dataSource={chartData}
                  xName={xName}
                  name={columnName[3]}
                  yName={columnKey[3]}
                  type="Column"
                  fill={columnColor[3]}
                ></SeriesDirective>
              ) : (
                ""
              )}
              {columnName[4] ? (
                <SeriesDirective
                  dataSource={chartData}
                  xName={xName}
                  name={columnName[4]}
                  yName={columnKey[4]}
                  type="Column"
                  fill={columnColor[4]}
                ></SeriesDirective>
              ) : (
                ""
              )}
              {/* <SeriesDirective dataSource={this.state.data} xName='country' yName='bronze' type='Column'>
          </SeriesDirective> */}
            </SeriesCollectionDirective>
          </ChartComponent>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}

export default GroupColumnsChart;
