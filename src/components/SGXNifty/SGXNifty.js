import React, { Component } from "react";
import Head from "next/head";
import RightSection from "components/RightSection";
import { CALL_API } from "_services/CALL_API";
import GroupColumnsChart from "components/Charts/GroupColumnsChart";
import moment from "moment";
import Loader from "components/Loader";
import Link from "next/link";
import AdComponent from "components/AdComponent";
import SEOPageContent from "components/SEOPageContent";

class SGXNifty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sgxData: {},
      isLoading: true,
    };
    this.sgxDataInterval = 0;
  }

  componentDidMount() {
    this.getSGXNifty();
    this.sgxDataInterval = setInterval(() => {
      this.getSGXNifty();
    }, 1000 * 120);
  }

  getSGXNifty() {
    CALL_API("GET", process.env.SGX_NIFTY_DATA, {}, (res) => {
      if (res.status) {
        this.setState({
          sgxData: res["data"],
          isLoading: false,
        });
      } else {
        this.setState({
          sgxData: {},
          isLoading: false,
        });
      }
    });
  }

  componentWillUnmount() {
    clearInterval(this.sgxDataInterval);
  }

  render() {
    const { sgxData, isLoading } = this.state;
    const { title, pageContent } = this.props;
    return (
      <React.Fragment>
        {
          !isLoading ? "" : <Loader />
        }
        <div className="row">
          <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
            {/* <!-- pricetable-box start --> */}

            <h1 className="main-page-heading ">
              {pageContent["page_Content_Title"]}
            </h1>

            <div className="nifty-chart-panel">
              <div className="row m-0">
                <div className="col-lg-12 p-0">
                  <h4>
                    Last Data Refresh: {moment(sgxData.timeNew).format('dddd DD MMM YYYY hh:mm:ss A')} GMT+05:30
                      (India Standard Time)
                    </h4>
                  <div className="source-link">
                    Source:{" "}
                    <a
                      href={process.env.OLD_SITE_URL}
                      title="www.niftytrader.in"
                    >
                      www.niftytrader.in
                      </a>
                  </div>
                  <div className="nifty-datatable table-responsive">
                    <table className="table">
                      <thead>
                        <tr>
                          <th scope="col" width="">
                            LAST
                            </th>
                          <th scope="col" width="">
                            CHANGE
                            </th>
                          <th scope="col" width="">
                            CHANGE %
                            </th>
                          <th scope="col" width="">
                            HIGH
                            </th>
                          <th scope="col" width="">
                            LOW
                            </th>
                          <th scope="col" width="">
                            VOLUME
                            </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <span className={Math.sign(sgxData['change']) != -1 ? 'percent-green' : 'percent-red'}>{sgxData && Object.keys(sgxData).length > 0 ? Number(sgxData['close']).toFixed(2) : ''}</span>
                          </td>
                          <td>
                            <span className={Math.sign(sgxData['change']) != -1 ? 'percent-green' : 'percent-red'}>{sgxData && Object.keys(sgxData).length > 0 ? Number(sgxData['change']).toFixed(2) : ''}</span>
                          </td>
                          <td>
                            <span className={Math.sign(sgxData['change']) != -1 ? 'percent-green' : 'percent-red'}>{sgxData && Object.keys(sgxData).length > 0 ? Number(sgxData['change_percent']).toFixed(2) + '%' : ''}</span>
                          </td>
                          <td>{sgxData && Object.keys(sgxData).length > 0 ? Number(sgxData['high']).toFixed(2) : ''}</td>
                          <td>{sgxData && Object.keys(sgxData).length > 0 ? Number(sgxData['low']).toFixed(2) : ''}</td>
                          <td>{sgxData && Object.keys(sgxData).length > 0 ? sgxData['volumn'] ? Number(sgxData['volumn']).toFixed(2) : '0.00' : ''}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            {/* <!-- pricetable-box close --> */}

            <div className="yesbank-box open-int-box">
              <h2>SGX Nifty Importance:</h2>
              <p>
                SGX Nifty, also known as a Singapore Nifty, involves taking
                position in the Singapore Exchange on Futures contracts . The
                Futures contracts settlement is based on the NIFTY settlement
                price in the Indian stock exchange NSE. This gives
                international investors the flexibility of betting on Indian
                markets without having to setup or register the entity with
                the Indian authorities. Since the SGX allows for 24 hour
                trading via after market trades, investors can hedge their
                bets at any time. Plus, it is also considered a good indicator
                to where Indian market will open the next day, due to this a
                lot of traders follow this to check how SGX Nifty is doing.
                </p>
              <h3>SGX Nifty Futures Explained:</h3>
              <AdComponent adType="CM_NIFTYTRADER_WC1_RSPV" />
              <p>
                SGX Nifty is a derivative product of NIFTY 50 which is also
                known as Singapore Nifty. If we break the terminology in two
                parts.
                </p>
              <p>
                SGX stands for Singapore Stock Exchange. <br />
                  NIFTY is the benchmark index of National Stock Exchange of
                  India which represents the weighted average of top 50 Indian
                  companies’ stocks in 12 sectors.
                </p>
              <p>
                Singapore Stock Exchange SGX is considered as one of the
                leading stock exchange of Asia. SGX NIFTY is a derivative of
                NSE NIFTY Futures, which is quite popular in Singapore Stock
                Exchange. It is an attractive product for foreign investors,
                who want to take a position in the Indian stock market but do
                not want to register with India authorities. SGX NIFTY is also
                popular among Hedge funds who are exposed in India market and
                want to hedge their exposure. So in simple terms, SGX NIFTY is
                nothing but the Indian NIFTY traded in the Singapore Stock
                Exchange.
                </p>
              <p>
                SGX Stock Exchange also allows the investors to take the
                positions in different products including China A50 index,
                FTSE, MSCI Asia, MSCI Singapore, MSCI Hongkong, MSCI Taiwan,
                Nikkei 225, etc. so Foreign Institutional Investors (FIIs) can
                also take positions in all major indices while being in
                Singapore.
                </p>
              <h3>SGX – Singapore exchange:</h3>
              <AdComponent adType="CM_NIFTYTRADER_WC2_RSPV" />
              <p>
                Singapore exchange is the leading exchange of Asia allowing
                investors to take positions in different products based on the
                futures which are traded on the exchange. Apart from India the
                exchange also allows one to take positions in FTSE, China A50
                index, MSCI Asia, MSCI Hongkong, MSCI Singapore, MSCI Taiwan
                ,Nikkei 225, Strait Times etc. Thus, FIIs are able to take
                positions in all major indices while being in Singapore.
                </p>
              <p>
                We bring you SGX Nifty Live Chart & Quotes to know expected
                opening levels of NSE Nifty before the market opens.
                </p>
              <h4>
                How does SGX Nifty provides a good indication of Nifty opening
                price next day?
                </h4>
              <p>
                Indian stock market open at 9:15 AM and close at 3:30 PM while
                SGX NIFTY trades for 16 hours a day in Singapore Stock
                exchange from 6:30 AM to 11:30 PM IST. Because of the long
                trading hours, the impact of global events is more advanced on
                it. That is why it is considered as a good indicator to know
                where India market will open next day. Lots of traders follow
                it to predict the direction of Indian stock market. Intraday
                traders can take long or short positions in Indian stock
                market depending on the movement of SGX Nifty.
                </p>
              <h4>Technical Analysis of SGX Nifty</h4>
              <p>
                If you are a trader and want to know about the price movement
                and trends of SGX NIFTY, you can follow historic charts of SGX
                NIFTY. You can use weekly or monthly charts to get a detailed
                idea about the trends of SGX Nifty. Monthly and weekly charts
                give a clear idea about the major trading levels which are
                even followed by Foreign Institutional Investors (FIIs) and
                fund managers for entry and profit booking.
                </p>
              <h4>For Positional Traders:</h4>
              <p>
                For long-term investment, you can use Moving Average
                Convergence/Divergence (MACD) Charts, Candle Stick Charts, and
                Relative Strength Index (RSI) to get the idea about the future
                prediction of SGX Nifty.
                </p>
              <h4>For Intraday Traders:</h4>
              <p>
                For day traders{" "}
                <Link href="/nifty-put-call-ratio">
                  <a
                    title="PCR (Put Call Ratio)"
                  >
                    PCR (Put Call Ratio)
                  </a>
                </Link>
                  trends and Nifty SpotPrice are considered extremely reliable
                  indicators. You can follow live PCR trends for intraday
                  trading.Like{" "}
                <Link href="/banknifty-live-oi-tracker">
                  <a title="Bank Nifty Open Interest">
                    Bank Nifty Open Interest
                  </a>
                </Link>{" "}
                  charts you can also get SGX{" "}
                <Link href="/live-nifty-open-interest">
                  <a
                    title="Nifty Open Interest"
                  >
                    Nifty Open Interest
                  </a></Link>{" "}
                  live charts for intradaytrading. Open Interest charts give
                  clues about intraday support and resistance levels of SGX
                  Nifty.
                </p>
              <h4>Contract Specifications:</h4>
              <p>
                The derivative contract size of each SGX Nifty contract is:{" "}
                <br />
                  Contract Size = $2 (USD) * Current Price of NIFTY Index
                  futures price
                </p>
              <p>
                AS per the norms of SGX Exchange, the minimum price movement
                of the SGX Nifty futures contract is US$1, which means it is
                equal to 0.5 in index price movement.
                </p>
              <p>
                The contracts, which are available for trading can be divided
                into 2 kinds:
                </p>
              <p>Monthly contracts and Quarterly contracts.</p>
              <p>
                Most of the volumes is concentrated in the monthly contracts.
                Normally, the monthly contracts are available for the 2
                consecutive months e.g. in the month of November, trading can
                happen in November Contracts as well as December contracts.
                </p>
              <p>
                The quarterly contracts of SGX Nifty are March, June,
                September and December. The contracts are cash settled based
                on the closing price of S&P NIFTY index as on 6:00pm Singapore
                time or 3:30 pm Indian Standard Time (IST). The closing price
                of Nifty index is based on the value weighted average price
                (or VWAP) of the last 30 mins of trading on Nifty Futures
                contract and is not the final value also known as LTP (or Last
                Traded Price) of the NSE Nifty index.
                </p>
              <h4>Contract Settlement</h4>
              <AdComponent adType="CM_NIFTYTRADER_WC3_RSPV" />
              <p>
                The settlement price of SGX Nifty contract depends on NIFTY
                price in Indian Stock Exchange NSE. Singapore Stock Exchange
                allows 24-hour trading so investors can hedge their investment
                any time via after market trades. Effect of long trading hours
                </p>
            </div>
            {
              pageContent && pageContent['page_Content'] != '' ? (
                <SEOPageContent pageContent={pageContent['page_Content']} />
              ) : ''
            }
          </div>
          <RightSection />
        </div>
      </React.Fragment>
    );
  }
}

export default SGXNifty;
