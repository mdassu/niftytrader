import React from "react";
// let Parser = require("rss-parser");
import Masonry from "react-masonry-component";
import Parser from "rss-parser";

const masonryOptions = {
  transitionDuration: 0,
};

const imagesLoadedOptions = { background: "white" };

class RssNews extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      selectedSource: "All",
      selectedSourceId: "all",
      viewSource: true,
      newsBS: [],
      newsBLOOM: [],
      newsCNBC: [],
      newsMINT: [],
      newsECO: [],
      newsNYT: [],
      newsUJALA: [],
      newsBHASKAR: [],
      newsZEE: [],
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.getLatestNews();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getLatestNews = () => {
    var fn = this;
    var previousDate = new Date();
    previousDate.setHours(previousDate.getHours() - 24);

    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";
    let parser = new Parser();

    // //Feeds Data
    // var BSFeedUrl = [
    //   "https://www.business-standard.com/rss/home_page_top_stories.rss"
    // ];
    // var finalBSData = [];
    // BSFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter(item => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalBSData = finalBSData.concat(filterData);
    //     if (BSFeedUrl.length - 1 == key) {
    //       await finalBSData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       fn.setState({
    //         newsBS: finalBSData
    //       });
    //     }
    //   });

    // });

    var BloomFeedUrl = ["https://zeenews.india.com/hindi/business.xml"];
    var finalBLOOMData = [];

    BloomFeedUrl.map((url, key) => {
      parser.parseURL(CORS_PROXY + url, (err, feed) => {
        try {
          if (feed !== undefined) {
            var filterData = feed.items.filter((item) => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalBLOOMData = finalBLOOMData.concat(filterData);
            if (BloomFeedUrl.length - 1 == key) {
              finalBLOOMData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              fn.setState({
                newsBLOOM: finalBLOOMData,
              });
            }
          }
        } catch (error) {
              error
        }
      });
    });

    // var CNBCFeedUrl = ["https://www.cnbc.com/id/10000664/device/rss/rss.html"];
    // var finalCNBCData = [];
    // CNBCFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalCNBCData = finalCNBCData.concat(filterData);
    //     if (CNBCFeedUrl.length - 1 == key) {
    //       await finalCNBCData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       fn.setState({
    //         newsCNBC: finalCNBCData,
    //       });
    //     }
    //   });
    // });

    // var MintFeedUrl = ["https://www.livemint.com/rss/markets"];
    // var finalMINTData = [];
    // MintFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalMINTData = finalMINTData.concat(filterData);
    //     if (MintFeedUrl.length - 1 == key) {
    //       await finalMINTData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       fn.setState({
    //         newsMINT: finalMINTData,
    //       });
    //     }
    //   });
    // });

    // var NYTFeedUrl = [
    //   // 'https://www.economist.com/business/rss.xml',
    //   "https://rss.nytimes.com/services/xml/rss/nyt/Economy.xml",
    //   "https://rss.nytimes.com/services/xml/rss/nyt/SmallBusiness.xml",
    //   "https://rss.nytimes.com/services/xml/rss/nyt/Business.xml",
    // ];
    // var finalNYTData = [];
    // NYTFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalNYTData = finalNYTData.concat(filterData);
    //     if (NYTFeedUrl.length - 1 == key) {
    //       await finalNYTData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       fn.setState({
    //         newsNYT: finalNYTData,
    //       });
    //     }
    //   });
    // });

    // var UjalaFeedUrl = ["https://www.amarujala.com/rss/business.xml"];
    // var finalUjalaData = [];
    // UjalaFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalUjalaData = finalUjalaData.concat(filterData);
    //     if (UjalaFeedUrl.length - 1 == key) {
    //       await finalUjalaData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       $("#english").addClass("active show");
    //       fn.setState({
    //         newsUJALA: finalUjalaData,
    //       });
    //     }
    //   });
    // });

    // var BhaksarFeedUrl = ["https://www.bhaskar.com/rss-feed/1051/"];
    // var finalBhaskarData = [];
    // BhaksarFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate.trim()) >= previousDate;
    //     });
    //     finalBhaskarData = finalBhaskarData.concat(filterData);
    //     if (BhaksarFeedUrl.length - 1 == key) {
    //       await finalBhaskarData.sort((a, b) => {
    //         return new Date(b.pubDate.trim()) - new Date(a.pubDate.trim());
    //       });
    //       $("#english").addClass("active show");
    //       fn.setState({
    //         newsBHASKAR: finalBhaskarData,
    //       });
    //     }
    //   });
    // });

    // var ZeeFeedUrl = ["https://zeenews.india.com/hindi/business.xml"];
    // var finalZeeData = [];
    // ZeeFeedUrl.map((url, key) => {
    //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
    //     if (err) throw err;
    //     var filterData = feed.items.filter((item) => {
    //       return new Date(item.pubDate) >= previousDate;
    //     });
    //     finalZeeData = finalZeeData.concat(filterData);
    //     if (ZeeFeedUrl.length - 1 == key) {
    //       await finalZeeData.sort((a, b) => {
    //         return new Date(b.pubDate) - new Date(a.pubDate);
    //       });
    //       $("#english").addClass("active show");
    //       fn.setState({
    //         newsZEE: finalZeeData,
    //       });
    //     }
    //   });
    // });
  };

  changeTitle(title, id) {
    $(".dropdown-menu a").click(function () {
      $(".dropdown-menu .active").removeClass("active");
      // $('#' + id).addClass('active');
    });
    var sourceId;
    if (title == "Business Standard") {
      sourceId = "bs";
    } else if (title == "Bloomberg Quint") {
      sourceId = "bloom";
    } else if (title == "CNBC") {
      sourceId = "cnbc";
    } else if (title == "Live Mint") {
      sourceId = "mint";
    } else if (title == "The Economic Times") {
      sourceId = "eco";
    } else if (title == "The New York Times") {
      sourceId = "nyt";
    } else if (title == "Amar Ujala") {
      sourceId = "ujala";
    } else if (title == "Bhaskar") {
      sourceId = "bhaskar";
    } else if (title == "Zee News") {
      sourceId = "zee";
    }
    this.setState({
      selectedSource: title,
      selectedSourceId: sourceId,
    });
  }

  changeView() {
    this.setState({
      viewSource: !this.state.viewSource,
      selectedSource: "All",
    });
  }

  render() {
    // //newsBs
    // const newsBs = this.state.newsBS.map((item, key) => {
    //   var time;
    //   var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
    //   if (hours < 1) {
    //     time = Math.round(hours * 60) + ' minutes ago';
    //   } else {
    //     var text = 'hour';
    //     if (Math.round(hours) > 1) {
    //       text = 'hours';
    //     }
    //     time = Math.round(hours) + ' ' + text + ' ago';
    //   }
    //   return (
    //     <div className="grid-item" key={key}>
    //       <div className="news-box">
    //         <a
    //           href={item.link}
    //           title={item.title}
    //         >
    //           {item.title}
    //         </a>
    //         <span>
    //           <a href="javascript:void(0)">
    //             <img
    //               src="/static/img/news/bs.png"
    //               title="Business Standard"
    //               alt="News logo"
    //             />
    //           </a>{time}
    //         </span>
    //         <p>
    //           {item.content}
    //         </p>
    //       </div>
    //     </div>
    //   );
    // });

    //newsBloom
    const newsBloom = this.state.newsBLOOM.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.bloombergquint.com" target="_blank">
                <img
                  src="/static/img/news/bloom.png"
                  title="Bloomberg Quint"
                  alt="Bloomberg Quint"
                />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    //newsCnbc
    const newsCnbc = this.state.newsCNBC.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.cnbc.com" target="_blank">
                <img src="/static/img/news/cnbc.png" title="CNBC" alt="CNBC" />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    //newsMint
    const newsMint = this.state.newsMINT.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.livemint.com" target="_blank">
                <img
                  src="/static/img/news/mint.png"
                  title="Live Mint"
                  alt="Live Mint"
                />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    // //newsEco
    // const newsEco = this.state.newsECO.map((item, key) => {
    //   var time;
    //   var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
    //   if (hours < 1) {
    //     time = Math.round(hours * 60) + ' minutes ago';
    //   } else {
    //     var text = 'hour';
    //     if (Math.round(hours) > 1) {
    //       text = 'hours';
    //     }
    //     time = Math.round(hours) + ' ' + text + ' ago';
    //   }
    //   return (
    //     <div className="grid-item" key={key}>
    //       <div className="news-box">
    //         <a
    //           href={item.link}
    //           title={item.title}
    //         >
    //           {item.title}
    //         </a>
    //         <span>
    //           <a href="javascript:void(0)">
    //             <img
    //               src="/static/img/news/eco.png"
    //               title="The Economic Times"
    //               alt="The Economic Times"
    //             />
    //           </a>{time}
    //         </span>
    //         <p>
    //           {item.content}
    //         </p>
    //       </div>
    //     </div>
    //   );
    // });

    //newsNyt
    const newsNyt = this.state.newsNYT.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.livemint.com/" target="_blank">
                <img
                  src="/static/img/news/nyt.png"
                  title="The New York Times"
                  alt="The New York Times"
                />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    //newsUjala
    const newsUjala = this.state.newsUJALA.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      var image = "amar-ujala.png";
      if (item.link.search("zeenews") != -1) {
        image = "zee-news.png";
      } else if (item.link.search("bhaskar") != -1) {
        image = "bhaskar.png";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.amarujala.com" target="_blank">
                <img
                  src={`/static/img/news/amar-ujala.png`}
                  title="Amar Ujala"
                  alt="Amar Ujala"
                />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    //newsBhaskar
    const newsBhaskar = this.state.newsBHASKAR.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }

      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://www.bhaskar.com" target="_blank">
                <img
                  src={`/static/img/news/bhaskar.png`}
                  title="Bhaskar"
                  alt="Bhaskar"
                />
              </a>
              {time}
            </span>
            <p>{item.content.replace(/(<([^>]+)>)/gi, "")} ...</p>
          </div>
        </div>
      );
    });

    //newsZee
    const newsZee = this.state.newsZEE.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href="https://zeenews.india.com" target="_blank">
                <img
                  src={`/static/img/news/zee-news.png`}
                  title="Zee News"
                  alt="Zee News"
                />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    var allFeeds = [];
    allFeeds = allFeeds.concat(this.state.newsBLOOM);
    allFeeds = allFeeds.concat(this.state.newsCNBC);
    allFeeds = allFeeds.concat(this.state.newsMINT);
    allFeeds = allFeeds.concat(this.state.newsNYT);
    allFeeds.sort((a, b) => {
      return new Date(b.pubDate) - new Date(a.pubDate);
    });
    //newsAll
    const newsAll = allFeeds.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      var image = "bloom.png";
      var title = "Bloomberg Quint";
      var url = "https://www.bloombergquint.com";
      if (item.link.search("bloom") != -1) {
        image = "bloom.png";
        title = "Bloomberg Quint";
        url = "https://www.bloombergquint.com";
      } else if (item.link.search("cnbc") != -1) {
        image = "cnbc.png";
        title = "CNBC";
        url = "https://www.cnbc.com";
      } else if (item.link.search("mint") != -1) {
        image = "mint.png";
        title = "Live Mint";
        url = "https://www.livemint.com";
      } else if (item.link.search("nyt") != -1) {
        image = "nyt.png";
        title = "The New York Times";
        url = "https://www.nytimes.com";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link} title={item.title}>
              {item.title}
            </a>
            <span>
              <a href={url} target="_blank">
                <img src={`/static/img/news/${image}`} title={title} alt="" />
              </a>
              {time}
            </span>
            <p>{item.content}</p>
          </div>
        </div>
      );
    });

    var allHindiFeeds = [];
    // const newsHindiAll = '';
    allHindiFeeds = allHindiFeeds.concat(this.state.newsUJALA);
    allHindiFeeds = allHindiFeeds.concat(this.state.newsBHASKAR);
    allHindiFeeds = allHindiFeeds.concat(this.state.newsZEE);
    allHindiFeeds.sort((a, b) => {
      return new Date(b.pubDate.trim()) - new Date(a.pubDate.trim());
    });
    //newsHindiAll
    const newsHindiAll = allHindiFeeds.map((item, key) => {
      var time;
      var hours = Math.abs(new Date() - new Date(item.pubDate.trim())) / 36e5;
      if (hours < 1) {
        time = Math.round(hours * 60) + " minutes ago";
      } else {
        var text = "hour";
        if (Math.round(hours) > 1) {
          text = "hours";
        }
        time = Math.round(hours) + " " + text + " ago";
      }
      var image = "amar-ujala.png";
      var title = "Amar Ujala";
      var url = "https://www.amarujala.com";
      if (item.link.trim().search("bhaskar") != -1) {
        image = "bhaskar.png";
        title = "Bhaskar";
        url = "https://www.bhaskar.com";
      } else if (item.link.trim().search("zee") != -1) {
        image = "zee-news.png";
        title = "Zee News";
        url = "https://zeenews.india.com/";
      }
      return (
        <div className="grid-item" key={key}>
          <div className="news-box">
            <a href={item.link.trim()} title={item.title.trim()}>
              {item.title}
            </a>
            <span>
              <a href={url} target="_blank">
                <img
                  src={`/static/img/news/${image}`}
                  title={title}
                  alt={title}
                />
              </a>
              {time}
            </span>
            <p>{item.content.replace(/(<([^>]+)>)/gi, "")}</p>
          </div>
        </div>
      );
    });
    return (
      <section className="news-section">
        <div className="container">
          <div className="row">
            <div className="col-12 news-block">
              <h1>News</h1>
              <div className="row">
                <div className="col-xl-6 col-xl-6 col-md-6 col-sm-6 col-5 news-tab-list">
                  {/* <!-- Tab List --> */}
                  <ul
                    className="nav nav-tabs news-tabs"
                    id="newsTab"
                    role="tablist"
                  >
                    <li className="nav-item">
                      <a
                        className="nav-link active"
                        title="English"
                        id="english-all-tab"
                        data-toggle="tab"
                        href="#english-all"
                        role="tab"
                        aria-controls="english-all"
                        aria-selected="true"
                        onClick={() => this.changeView()}
                      >
                        English
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className="nav-link"
                        title="Hindi"
                        id="hindi-all-tab"
                        data-toggle="tab"
                        href="#hindi-all"
                        role="tab"
                        aria-controls="hindi-all"
                        aria-selected="false"
                        onClick={() => this.changeView()}
                      >
                        Hindi
                      </a>
                    </li>
                  </ul>
                </div>
                {this.state.viewSource ? (
                  <div className="col-xl-6 col-xl-6 col-md-6 col-sm-6 col-7 news-source">
                    {/* <!-- View By Saurce --> */}
                    <label htmlFor="source">View by Source</label>
                    <div className="btn-group">
                      <button
                        type="button"
                        className="select dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        id="source"
                      >
                        <span>{this.state.selectedSource}</span>
                      </button>
                      <div className="dropdown-menu">
                        {/* <a
                          title="Business Standard"
                          id="english-bs-tab"
                          data-toggle="tab"
                          href="#english-bs"
                          role="tab"
                          aria-controls="english-bs"
                          aria-selected="true"
                          className="dropdown-item"
                          onClick={() => this.changeTitle('Business Standard', 'english-bs-tab')}
                        >Business Standard</a> */}
                        <a
                          title="All"
                          id="english-all-tab"
                          data-toggle="tab"
                          href="#english-all"
                          role="tab"
                          aria-controls="english-all"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("All", "english-all-tab")
                          }
                        >
                          All
                        </a>
                        <a
                          title="Bloomberg Quint"
                          id="english-bloom-tab"
                          data-toggle="tab"
                          href="#english-bloom"
                          role="tab"
                          aria-controls="english-bloom"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle(
                              "Bloomberg Quint",
                              "english-bloom-tab"
                            )
                          }
                        >
                          Bloomberg Quint
                        </a>
                        <a
                          title="CNBC"
                          id="english-cnbc-tab"
                          data-toggle="tab"
                          href="#english-cnbc"
                          role="tab"
                          aria-controls="english-cnbc"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("CNBC", "english-cnbc-tab")
                          }
                        >
                          CNBC
                        </a>
                        <a
                          title="Live Mint"
                          id="english-mint-tab"
                          data-toggle="tab"
                          href="#english-mint"
                          role="tab"
                          aria-controls="english-mint"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("Live Mint", "english-mint-tab")
                          }
                        >
                          Live Mint
                        </a>
                        {/* <a
                          title="The Economics Times"
                          id="english-eco-tab"
                          data-toggle="tab"
                          href="#english-eco"
                          role="tab"
                          aria-controls="english-eco"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() => this.changeTitle('The Economics Times', 'english-eco-tab')}
                        >The Economics Times</a> */}
                        <a
                          title="The New York Times"
                          id="english-nyt-tab"
                          data-toggle="tab"
                          href="#english-nyt"
                          role="tab"
                          aria-controls="english-nyt"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle(
                              "The New York Times",
                              "english-nyt-tab"
                            )
                          }
                        >
                          The New York Times
                        </a>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="col-xl-6 col-xl-6 col-md-6 col-sm-6 col-7 news-source">
                    {/* <!-- View By Saurce --> */}
                    <label htmlFor="source">View by Source</label>
                    <div className="btn-group">
                      <button
                        type="button"
                        className="select dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        id="source"
                      >
                        <span>{this.state.selectedSource}</span>
                      </button>
                      <div className="dropdown-menu">
                        <a
                          title="All"
                          id="hindi-all-tab"
                          data-toggle="tab"
                          href="#hindi-all"
                          role="tab"
                          aria-controls="hindi-all"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("All", "hindi-all-tab")
                          }
                        >
                          All
                        </a>
                        <a
                          title="Amar Ujala"
                          id="hindi-ujala-tab"
                          data-toggle="tab"
                          href="#hindi-ujala"
                          role="tab"
                          aria-controls="hindi-ujala"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("Amar Ujala", "hindi-ujala-tab")
                          }
                        >
                          Amar Ujala
                        </a>
                        <a
                          title="Bhaskar"
                          id="hindi-bhaskar-tab"
                          data-toggle="tab"
                          href="#hindi-bhaskar"
                          role="tab"
                          aria-controls="hindi-bhaskar"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("Bhaskar", "hindi-bhaskar-tab")
                          }
                        >
                          Bhaskar
                        </a>
                        <a
                          title="Zee News"
                          id="hindi-zee-tab"
                          data-toggle="tab"
                          href="#hindi-zee"
                          role="tab"
                          aria-controls="hindi-zee"
                          aria-selected="false"
                          className="dropdown-item"
                          onClick={() =>
                            this.changeTitle("Zee News", "hindi-zee-tab")
                          }
                        >
                          Zee News
                        </a>
                      </div>
                    </div>
                  </div>
                )}
              </div>

              <div className="tab-content" id="newsTabContent">
                {/* All */}
                {allFeeds.length > 0 ? (
                  <div
                    className="tab-pane fade show active"
                    id="english-all"
                    role="tabpanel"
                    aria-labelledby="english-all-tab"
                  >
                    <Masonry
                      className={"grid"} // default ''
                      elementType={"div"} // default 'div'
                      options={masonryOptions} // default {}
                      disableImagesLoaded={false} // default false
                      updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                      imagesLoadedOptions={imagesLoadedOptions} // default {}
                    >
                      {newsAll}
                    </Masonry>
                  </div>
                ) : (
                  <div
                    className="d-flex justify-content-center"
                    style={{ padding: "16%" }}
                  >
                    <div className="spinner-border" role="status">
                      <span className="sr-only">Loading...</span>
                    </div>
                  </div>
                )}
                {/* Business Standard */}
                {/* {this.state.newsBS.length > 0 ? (<div
                  className="tab-pane fade show active"
                  id="english-bs"
                  role="tabpanel"
                  aria-labelledby="english-bs-tab"
                >
                  <Masonry
                    className={'grid'} // default ''
                    elementType={'div'} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsBs}
                  </Masonry>
                </div>) : (<div className="d-flex justify-content-center" style={{ padding: '16%' }}>
                  <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>)} */}
                {/* Bloomberg Quint */}
                <div
                  className="tab-pane fade"
                  id="english-bloom"
                  role="tabpanel"
                  aria-labelledby="english-bloom-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsBloom}
                  </Masonry>
                </div>
                {/* CNBC */}
                <div
                  className="tab-pane fade"
                  id="english-cnbc"
                  role="tabpanel"
                  aria-labelledby="english-cnbc-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsCnbc}
                  </Masonry>
                </div>
                {/* Live Mint */}
                <div
                  className="tab-pane fade"
                  id="english-mint"
                  role="tabpanel"
                  aria-labelledby="english-mint-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsMint}
                  </Masonry>
                </div>
                {/* The Economic Times */}
                {/* <div
                  className="tab-pane fade"
                  id="english-eco"
                  role="tabpanel"
                  aria-labelledby="english-eco-tab"
                >
                  <Masonry
                    className={'grid'} // default ''
                    elementType={'div'} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsEco}
                  </Masonry>
                </div> */}
                {/* The New York Times */}
                <div
                  className="tab-pane fade"
                  id="english-nyt"
                  role="tabpanel"
                  aria-labelledby="english-nyt-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsNyt}
                  </Masonry>
                </div>
                {/* <!-- Hindi News Tab Start --> */}
                <div
                  className="tab-pane fade"
                  id="hindi-all"
                  role="tabpanel"
                  aria-labelledby="hindi-all-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsHindiAll}
                  </Masonry>
                </div>
                {/* Amar Ujala */}
                <div
                  className="tab-pane fade"
                  id="hindi-ujala"
                  role="tabpanel"
                  aria-labelledby="hindi-ujala-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsUjala}
                  </Masonry>
                </div>
                {/* Bhaskar */}
                <div
                  className="tab-pane fade"
                  id="hindi-bhaskar"
                  role="tabpanel"
                  aria-labelledby="hindi-bhaskar-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsBhaskar}
                  </Masonry>
                </div>
                {/* Zee News */}
                <div
                  className="tab-pane fade"
                  id="hindi-zee"
                  role="tabpanel"
                  aria-labelledby="hindi-zee-tab"
                >
                  <Masonry
                    className={"grid"} // default ''
                    elementType={"div"} // default 'div'
                    options={masonryOptions} // default {}
                    disableImagesLoaded={false} // default false
                    updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                    imagesLoadedOptions={imagesLoadedOptions} // default {}
                  >
                    {newsZee}
                  </Masonry>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default RssNews;
