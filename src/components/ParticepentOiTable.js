import React, { Component } from "react";
import { connect } from 'react-redux';
import CandleStickChart from "./Charts/CandleStickChart";
import ParticipantWiseOiTable from "./Tables/ParticepentWiseOiTable";
import { AuthenticationService } from "../_services/AuthenticationService";
import Router from "next/router";
import { CALL_API } from "../_services/CALL_API";
import * as moment from "moment";
import GroupColumnsChart from "./Charts/GroupColumnsChart";
import domtoimage from "dom-to-image";
import Cookies from "js-cookie";
import Loader from "./Loader";
import html2canvas from "html2canvas";
import { saveAs } from "file-saver";
import SEOPageContent from "../components/SEOPageContent";
import Link from "next/link";


var showData = false;

const mapStateToProps = state => ({
  userData: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn
});

const mapDispatchToProps = {
};

class PartipentOiTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showChart: false,
      chartData: [],
      pwOiTableData: [],
      pwOiTableDate: [],
      pwOiChartData: [],
      ptDate: "",
      changeDate: false,
      chartDataTypeCurrent: "future_index_net",
      chartDateList: [],
      prevDate: new Date().toISOString().slice(0, 10),
      isDownload: false,
      primeMember: false,
    };
  }

  componentDidMount() {
    const {userData} = this.props;
    this.getParticepentOiTableData(this.state.ptDate);

    var today = new Date();
    var yesterday = new Date(today);

    yesterday.setDate(today.getDate() - 1);

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    var month = today.getMonth();
    yesterday =
      yesterday.getDate() +
      " " +
      monthNames[month] +
      " " +
      yesterday.getFullYear();

    this.setState({
      prevDate: moment(yesterday).format("DD-MMM-YYYY"),
    });

    if (this.props.isLoggedIn) {
      this.getParticepentOiChartData();
      this.setState({
        showChart: true,
      });
    } else {
      this.setState({
        showChart: false,
      });
    }

    if (userData != null || userData !== undefined) {
      if (userData.membership_flag == 1) {
        this.getParticepentOiChartData();
        this.setState({
          primeMember: true,
          showChart: true,
        });
      } else {
        this.setState({
          primeMember: false,
          showChart: false,
        });
      }
    }

  }

  componentDidUpdate(prevProps, prevState) {
    const {userData} = this.props;

    if (prevProps.userData !== userData) {
      if (userData != null || userData !== undefined) {
        if (userData.membership_flag == 1) {
          this.getParticepentOiChartData();
          this.setState({
            primeMember: true,
            showChart: true,
          });
        } else {
          this.setState({
            primeMember: false,
            showChart: false,
          });
        }
      }
    }

    if (this.state.pwOiTableData !== prevState.pwOiTableData) {
      this.setState({
        pwOiTableData: this.state.pwOiTableData,
      });
    }

   
  }

  getParticepentOiChartData() {
    const { chartDataTypeCurrent } = this.state;
    CALL_API("POST", process.env.PARTICEPENT_WISE_OI_CHART_DATA, {}, (res) => {
      if (res.status) {
        var dateList = res["data"]["date"];
        var data = res["data"]["data"];

        if (dateList && dateList.length > 0) {
          this.setState({
            chartDateList: dateList,
            pwOiChartData: data,
          });

          // var dateData = [];
          this.chartDataType(chartDataTypeCurrent);
        }
      }
    });
  }

  getParticepentOiTableData = (ptDateIs) => {
    var params = {
      date: ptDateIs,
    };
    CALL_API(
      "POST",
      process.env.PARTICEPENT_WISE_OI_TABLE_DATA,
      params,
      (res) => {
        if (res.status) {
          var data = res["data"];
          showData = true;
          this.setState(
            {
              pwOiTableData: data["oiData"],
              pwOiTableDate: data["date"],
            },
            () => {
              const currDate = this.state.pwOiTableDate[0];
              this.setState({
                prevDate: moment(currDate).format("DD-MMM-YYYY"),
              });
            }
          );
        }
      }
    );
  };

  particepentDate = (e) => {
    const formatedDate = moment(e.target.value).format("YYYY-MM-DD");
    this.setState(
      {
        changeDate: true,
        ptDate: formatedDate,
      },
      () => {
        this.getParticepentOiTableData(this.state.ptDate);
      }
    );
  };

  chartDataType = (chartDataType) => {
    const { chartDateList, pwOiChartData } = this.state;
    var clientTypeData = [];
    var netChartType =
      chartDataType !== "total_long_net"
        ? chartDataType.replace("_net", "_long")
        : chartDataType.replace("_long_net", "_long_contracts");
    var currChartType = netChartType;
    var newChartType =
      chartDataType !== "total_long_net"
        ? currChartType.replace("_long", "_short")
        : currChartType.replace("_long_contracts", "_short_contracts");

    chartDateList.map((date) => {
      var dateData = pwOiChartData.filter((item) => {
        return item.date == date;
      });
      if (dateData && dateData.length > 0) {
        var clientTypeJson = {
          client:
            dateData.filter((res) => {
              return res["client_type"] == "Client";
            })[0][currChartType] -
            dateData.filter((res) => {
              return res["client_type"] == "Client";
            })[0][newChartType],

          dii:
            dateData.filter((res) => {
              return res["client_type"] == "DII";
            })[0][currChartType] -
            dateData.filter((res) => {
              return res["client_type"] == "DII";
            })[0][newChartType],

          fii:
            dateData.filter((res) => {
              return res["client_type"] == "FII";
            })[0][currChartType] -
            dateData.filter((res) => {
              return res["client_type"] == "FII";
            })[0][newChartType],

          pro:
            dateData.filter((res) => {
              return res["client_type"] == "Pro";
            })[0][currChartType] -
            dateData.filter((res) => {
              return res["client_type"] == "Pro";
            })[0][newChartType],

          total:
            dateData.filter((res) => {
              return res["client_type"] == "TOTAL";
            })[0][currChartType] -
            dateData.filter((res) => {
              return res["client_type"] == "TOTAL";
            })[0][newChartType],

          date: moment(date).format("DD-MMM"),
        };
        clientTypeData.push(clientTypeJson);
      }
    });
    this.setState({
      chartData: clientTypeData,
      chartDataTypeCurrent: chartDataType,
    });
  };

  downloadTable = (event) => {
    event.preventDefault();

    var options = {
      quality: 1.0,
      width: 2500,
      height: 1500,
      bgcolor: "white",
    };
    this.setState(
      {
        isDownload: true,
      },
      () => {
        domtoimage
          .toBlob(document.getElementById("my-node"), options)
          .then(function (blob) {
            saveAs(blob, "particepentwiseoitable.jpeg");
          });

        setTimeout(() => {
          this.setState({
            isDownload: false,
          });
        }, 400);
      }
    );
  };

  render() {
    const {
      showChart,
      chartData,
      pwOiTableData,
      pwOiTableDate,
      changeDate,
      pwOiChartData,
      chartDateList,
      isDownload,
      ptDate,
      prevDate,
    } = this.state;
    const { title, pageContent } = this.props;
    var columnName = ["Client", "DII", "FII", "Pro"];
    var columnKey = ["client", "dii", "fii", "pro"];
    var chartTitle = "";
    var columnColor = ["#1c9ff8", "#26e7a6", "#febc3b", "#e855a2", "#f627c0"];
    var xName = "date";

    return pwOiTableData.length > 0 ? (
      <div className="container p-0">
        <div className="row">
          <div className="col-md-6 p-0">
            <h1 className="main-page-heading">{pageContent["page_Content_Title"]}</h1>
          </div>
          <div className="col-md-6 p-0">
            <div className="select-dropdown nifty-chart-dropdown">
              <div className="form-group">
                <label
                  for="exampleFormControlSelect1"
                  style={{ marginRight: "5px !important;" }}
                >
                  Date
                </label>
                &nbsp;
                <select
                  id="exampleFormControlSelect1"
                  onChange={this.particepentDate}
                >
                  {pwOiTableDate.map((dropdown) => {
                    return (
                      <option dropdown={dropdown} value={dropdown}>
                        {moment(dropdown).format("YYYY-MM-DD")}
                      </option>
                    );
                  })}
                  {/* <option value="Current">Current</option>
                  <option value="01-Apr-2021">01-Apr-2021</option>
                  <option value="29-Apr-2021">29-Apr-2021</option>
                  <option value="24-Jun-2021">24-Jun-2021</option>
                  <option value="30-Sep-2021">30-Sep-2021</option> */}
                </select>
                <button
                  style={{ height: "30px !important;" }}
                  className="part_wise_download"
                  id="convert"
                  onClick={this.downloadTable}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="currentColor"
                    className="bi bi-cloud-arrow-down"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M7.646 10.854a.5.5 0 0 0 .708 0l2-2a.5.5 0 0 0-.708-.708L8.5 9.293V5.5a.5.5 0 0 0-1 0v3.793L6.354 8.146a.5.5 0 1 0-.708.708l2 2z"
                    />
                    <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                  </svg>{" "}
                  Download
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-12 p-0">
            <div className="part_wise_oi_box table-responsive" id="my-node">
              {isDownload && (
                <h2
                  style={{ textAlign: "center", fontSize: 22 }}
                >{`Participant wise OI (no. of content) in EQ. Der. as on ${ptDate === ""
                  ? prevDate
                  : moment(ptDate).format("DD-MMM-YYYY")
                  }`}</h2>
              )}
              {pwOiTableData && (
                <ParticipantWiseOiTable
                  pwOiTableData={pwOiTableData}
                  changeDate={changeDate}
                />
              )}

              {isDownload && (
                <div>
                  <h5 style={{ textTransform: "uppercase", paddingTop: 25 }}>
                    note : change is from previous day{" "}
                    <span
                      style={{ float: "right", textTransform: "lowercase" }}
                    >
                      <h5>www.niftytrader.in</h5>
                    </span>
                  </h5>
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.primeMember === true ? (
          <div className="row pt-5">
            <div className="col-md-6 p-0">
              <h1 className="main-page-heading">
                Participant wise OI Chart View
              </h1>
            </div>
            <div className="col-md-6 p-0">
              <div className="select-dropdown nifty-chart-dropdown">
                <div className="form-group">
                  <label
                    for="exampleFormControlSelect1"
                    style={{ marginRight: "5px !important;" }}
                  >
                    Select Data Type
                  </label>{" "}
                  &nbsp;
                  <select
                    id="exampleFormControlSelect1"
                    onChange={($event) =>
                      this.chartDataType($event.target.value)
                    }
                  >
                    <option value="future_index_net">Future Index Net</option>

                    <option value="option_index_call_net">
                      Option Index Call Net
                    </option>

                    <option value="option_index_put_net">
                      Option Index Put Net
                    </option>

                    <option value="future_stock_net">Future Stock Net</option>

                    <option value="option_stock_call_net">
                      Option Stock Call Net
                    </option>

                    <option value="option_stock_put_net">
                      Option Stock Put Net
                    </option>

                    <option value="total_long_net">
                      Total Long Contracts Net
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div className="col-md-12 p-0">
              <div className="part_wise_oi_box pt-4">
                <GroupColumnsChart
                  chartData={chartData}
                  columnName={columnName}
                  columnKey={columnKey}
                  chartTitle={chartTitle}
                  columnColor={columnColor}
                  xName={xName}
                />
              </div>
            </div>
          </div>
        ) : (
          <div className="row">
            <div className="col-md-12 mt-3 mb-4 p-0">
              <div className="part_wise_oi_box text-center">
                <h5 className="main-page-heading">
                  Participant wise OI chart view
                </h5>
                <div className="blur_premium_content">
                  <div className="m-auto">
                    <div className="part_data_box">
                      <span>
                        <img src="/images/part_premium.png" alt="Img" /> Premium
                        Feature
                      </span>
                      <p>
                      This feature available for <b>Prime Members</b>Prime Members
                      </p>
                     <Link href="/primeplans">
                     <a title="Explore Premium Plans">
                        Explore Premium Plans
                      </a>
                     </Link> 
                    </div>
                  </div>
                  
                </div>
                <img
                  src="/images/blurchart.jpg"
                  className="chart_blur_img img-fluid"
                />
              </div>
            </div>
          </div>
        )}
        {pageContent && pageContent["page_Content"] != "" ? (
          <div className="row">
            <SEOPageContent pageContent={pageContent["page_Content"]} />
          </div>
        ) : (
          ""
        )}
      </div>
    ) : (
      <Loader />
    );
  }
}

// export default PartipentOiTable;
export default connect(mapStateToProps, mapDispatchToProps)(PartipentOiTable);
