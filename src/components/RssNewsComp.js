import React from "react";
let Parser = require('rss-parser');
import Masonry from 'react-masonry-component';

const masonryOptions = {
    transitionDuration: 0
  };
  
  const imagesLoadedOptions = { background: 'white' };

class RssNews extends Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            selectedSource: 'All',
            selectedSourceId: 'all',
            viewSource: true,
            newsBS: [],
            newsBLOOM: [],
            newsCNBC: [],
            newsMINT: [],
            newsECO: [],
            newsNYT: [],
            newsUJALA: [],
            newsBHASKAR: [],
            newsZEE: []
          };
    }


    componentDidMount() {
        this._isMounted = true;
        this.getLatestNews();
    
      }
    
      componentWillUnmount() {
        this._isMounted = false;
      }

      getLatestNews() {

        var fn = this;
        var previousDate = new Date();
        previousDate.setHours(previousDate.getHours() - 24);
    
        const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";
        let parser = new Parser();
    
        // //Feeds Data
        // var BSFeedUrl = [
        //   "https://www.business-standard.com/rss/home_page_top_stories.rss"
        // ];
        // var finalBSData = [];
        // BSFeedUrl.map((url, key) => {
        //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
        //     if (err) throw err;
        //     var filterData = feed.items.filter(item => {
        //       return new Date(item.pubDate) >= previousDate;
        //     });
        //     finalBSData = finalBSData.concat(filterData);
        //     if (BSFeedUrl.length - 1 == key) {
        //       await finalBSData.sort((a, b) => {
        //         return new Date(b.pubDate) - new Date(a.pubDate);
        //       });
        //       fn.setState({
        //         newsBS: finalBSData
        //       });
        //     }
        //   });
        // });
    
        var BloomFeedUrl = [
          "https://www.bloombergquint.com/stories.rss"
        ];
        var finalBLOOMData = [];
        BloomFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalBLOOMData = finalBLOOMData.concat(filterData);
            if (BloomFeedUrl.length - 1 == key) {
              await finalBLOOMData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              fn.setState({
                newsBLOOM: finalBLOOMData
              });
            }
          });
        });
    
        var CNBCFeedUrl = [
          'https://www.cnbc.com/id/10000664/device/rss/rss.html',
        ];
        var finalCNBCData = [];
        CNBCFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalCNBCData = finalCNBCData.concat(filterData);
            if (CNBCFeedUrl.length - 1 == key) {
              await finalCNBCData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              fn.setState({
                newsCNBC: finalCNBCData
              });
            }
          });
        });
    
        var MintFeedUrl = [
          "https://www.livemint.com/rss/markets"
        ];
        var finalMINTData = [];
        MintFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalMINTData = finalMINTData.concat(filterData);
            if (MintFeedUrl.length - 1 == key) {
              await finalMINTData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              fn.setState({
                newsMINT: finalMINTData
              });
            }
          });
        });
    
        // var EcoFeedUrl = [
        //   "https://economictimes.indiatimes.com/rssfeedstopstories.cms"
        // ];
        // var finalECOData = [];
        // EcoFeedUrl.map((url, key) => {
        //   parser.parseURL(CORS_PROXY + url, async (err, feed) => {
        //     if (err) throw err;
        //     var filterData = feed.items.filter(item => {
        //       return new Date(item.pubDate) >= previousDate;
        //     });
        //     finalECOData = finalECOData.concat(filterData);
        //     if (EcoFeedUrl.length - 1 == key) {
        //       await finalECOData.sort((a, b) => {
        //         return new Date(b.pubDate) - new Date(a.pubDate);
        //       });
        //       $('#english').addClass('active show');
        //       fn.setState({
        //         newsECO: finalECOData
        //       });
        //     }
        //   });
        // });
    
        var NYTFeedUrl = [
          // 'https://www.economist.com/business/rss.xml',
          'https://rss.nytimes.com/services/xml/rss/nyt/Economy.xml',
          'https://rss.nytimes.com/services/xml/rss/nyt/SmallBusiness.xml',
          'https://rss.nytimes.com/services/xml/rss/nyt/Business.xml'
        ];
        var finalNYTData = [];
        NYTFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalNYTData = finalNYTData.concat(filterData);
            if (NYTFeedUrl.length - 1 == key) {
              await finalNYTData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              fn.setState({
                newsNYT: finalNYTData
              });
            }
          });
        });
    
        var UjalaFeedUrl = [
          'https://www.amarujala.com/rss/business.xml'
        ];
        var finalUjalaData = [];
        UjalaFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalUjalaData = finalUjalaData.concat(filterData);
            if (UjalaFeedUrl.length - 1 == key) {
              await finalUjalaData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              $('#english').addClass('active show');
              fn.setState({
                newsUJALA: finalUjalaData
              });
            }
          });
        });
    
        var BhaksarFeedUrl = [
          'https://www.bhaskar.com/rss-feed/1051/'
        ];
        var finalBhaskarData = [];
        BhaksarFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate.trim()) >= previousDate;
            });
            finalBhaskarData = finalBhaskarData.concat(filterData);
            if (BhaksarFeedUrl.length - 1 == key) {
              await finalBhaskarData.sort((a, b) => {
                return new Date((b.pubDate).trim()) - new Date((a.pubDate).trim());
              });
              $('#english').addClass('active show');
              fn.setState({
                newsBHASKAR: finalBhaskarData
              });
            }
          });
        });
    
        var ZeeFeedUrl = [
          'https://zeenews.india.com/hindi/business.xml'
        ];
        var finalZeeData = [];
        ZeeFeedUrl.map((url, key) => {
          parser.parseURL(CORS_PROXY + url, async (err, feed) => {
            if (err) throw err;
            var filterData = feed.items.filter(item => {
              return new Date(item.pubDate) >= previousDate;
            });
            finalZeeData = finalZeeData.concat(filterData);
            if (ZeeFeedUrl.length - 1 == key) {
              await finalZeeData.sort((a, b) => {
                return new Date(b.pubDate) - new Date(a.pubDate);
              });
              $('#english').addClass('active show');
              fn.setState({
                newsZEE: finalZeeData
              });
            }
          });
        });
    
      }
    
      changeTitle(title, id) {
        $('.dropdown-menu a').click(function () {
          $('.dropdown-menu .active').removeClass('active');
          // $('#' + id).addClass('active');
        });
        var sourceId;
        if (title == 'Business Standard') {
          sourceId = 'bs';
        } else if (title == 'Bloomberg Quint') {
          sourceId = 'bloom';
        } else if (title == 'CNBC') {
          sourceId = 'cnbc';
        } else if (title == 'Live Mint') {
          sourceId = 'mint';
        } else if (title == 'The Economic Times') {
          sourceId = 'eco';
        } else if (title == 'The New York Times') {
          sourceId = 'nyt';
        } else if (title == 'Amar Ujala') {
          sourceId = 'ujala';
        } else if (title == 'Bhaskar') {
          sourceId = 'bhaskar';
        } else if (title == 'Zee News') {
          sourceId = 'zee';
        }
        this.setState({
          selectedSource: title,
          selectedSourceId: sourceId
        });
      }
    
      changeView() {
        this.setState({
          viewSource: !this.state.viewSource,
          selectedSource: 'All'
        });
      }
    
    render() { 
        const newsBloom = this.state.newsBLOOM.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.bloombergquint.com" target='_blank'>
                      <img
                        src="/static/img/news/bloom.png"
                        title="Bloomberg Quint"
                        alt="Bloomberg Quint"
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          const newsCnbc = this.state.newsCNBC.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.cnbc.com" target='_blank' >
                      <img
                        src="/static/img/news/cnbc.png"
                        title="CNBC"
                        alt="CNBC"
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          const newsMint = this.state.newsMINT.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.livemint.com" target='_blank'>
                      <img
                        src="/static/img/news/mint.png"
                        title="Live Mint"
                        alt="Live Mint"
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          const newsNyt = this.state.newsNYT.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.livemint.com/" target='_blank'>
                      <img
                        src="/static/img/news/nyt.png"
                        title="The New York Times"
                        alt="The New York Times"
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          const newsUjala = this.state.newsUJALA.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            var image = 'amar-ujala.png';
            if (item.link.search("zeenews") != -1) {
              image = 'zee-news.png';
            } else if (item.link.search("bhaskar") != -1) {
              image = 'bhaskar.png';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.amarujala.com" target="_blank">
                      <img
                        src={`/static/img/news/amar-ujala.png`}
                        title='Amar Ujala'
                        alt='Amar Ujala'
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          const newsBhaskar = this.state.newsBHASKAR.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
      
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://www.bhaskar.com" target='_blank'>
                      <img
                        src={`/static/img/news/bhaskar.png`}
                        title='Bhaskar'
                        alt='Bhaskar'
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content.replace(/(<([^>]+)>)/ig, '')} ...
                  </p>
                </div>
              </div>
            );
          });

          const newsZee = this.state.newsZEE.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href="https://zeenews.india.com" target="_blank">
                      <img
                        src={`/static/img/news/zee-news.png`}
                        title='Zee News'
                        alt='Zee News'
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          var allFeeds = [];
          allFeeds = allFeeds.concat(this.state.newsBLOOM);
          allFeeds = allFeeds.concat(this.state.newsCNBC);
          allFeeds = allFeeds.concat(this.state.newsMINT);
          allFeeds = allFeeds.concat(this.state.newsNYT);
          allFeeds.sort((a, b) => {
            return new Date(b.pubDate) - new Date(a.pubDate);
          });

          const newsAll = allFeeds.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate)) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            var image = 'bloom.png';
            var title = 'Bloomberg Quint';
            var url = 'https://www.bloombergquint.com';
            if (item.link.search("bloom") != -1) {
              image = 'bloom.png';
              title = 'Bloomberg Quint';
              url = 'https://www.bloombergquint.com';
            } else if (item.link.search("cnbc") != -1) {
              image = 'cnbc.png';
              title = 'CNBC';
              url = 'https://www.cnbc.com';
            } else if (item.link.search("mint") != -1) {
              image = 'mint.png';
              title = 'Live Mint';
              url = 'https://www.livemint.com';
            } else if (item.link.search("nyt") != -1) {
              image = 'nyt.png';
              title = 'The New York Times';
              url = 'https://www.nytimes.com';
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link}
                    title={item.title}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href={url} target='_blank'>
                      <img
                        src={`/static/img/news/${image}`}
                        title={title}
                        alt=""
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content}
                  </p>
                </div>
              </div>
            );
          });

          var allHindiFeeds = [];
          // const newsHindiAll = '';
          allHindiFeeds = allHindiFeeds.concat(this.state.newsUJALA);
          allHindiFeeds = allHindiFeeds.concat(this.state.newsBHASKAR);
          allHindiFeeds = allHindiFeeds.concat(this.state.newsZEE);
          allHindiFeeds.sort((a, b) => {
            return new Date(b.pubDate.trim()) - new Date(a.pubDate.trim());
          });

          const newsHindiAll = allHindiFeeds.map((item, key) => {
            var time;
            var hours = Math.abs(new Date() - new Date(item.pubDate.trim())) / 36e5;
            if (hours < 1) {
              time = Math.round(hours * 60) + ' minutes ago';
            } else {
              var text = 'hour';
              if (Math.round(hours) > 1) {
                text = 'hours';
              }
              time = Math.round(hours) + ' ' + text + ' ago';
            }
            var image = 'amar-ujala.png';
            var title = 'Amar Ujala';
            var url = 'https://www.amarujala.com';
            if (item.link.trim().search("bhaskar") != -1) {
              image = 'bhaskar.png';
              title = 'Bhaskar';
              url = "https://www.bhaskar.com";
            } else if (item.link.trim().search("zee") != -1) {
              image = 'zee-news.png';
              title = 'Zee News';
              url = "https://zeenews.india.com/";
            }
            return (
              <div className="grid-item" key={key}>
                <div className="news-box">
                  <a
                    href={item.link.trim()}
                    title={item.title.trim()}
                  >
                    {item.title}
                  </a>
                  <span>
                    <a href={url} target='_blank'>
                      <img
                        src={`/static/img/news/${image}`}
                        title={title}
                        alt={title}
                      />
                    </a>{time}
                  </span>
                  <p>
                    {item.content.replace(/(<([^>]+)>)/ig, '')}
                  </p>
                </div>
              </div>
            );
          });

        return ( 
            <section className="news-section">
            <div className="container">
              <div className="row">
                <div className="col-12 news-block">
                  <h1>News</h1>
                  <div className="row">
                    <div
                      className="col-xl-6 col-xl-6 col-md-6 col-sm-6 col-5 news-tab-list"
                    >
                      {/* <!-- Tab List --> */}
                      <ul className="nav nav-tabs news-tabs" id="newsTab" role="tablist">
                          <li  className="nav-item">
                          <a
                            className="nav-link active"
                            title="English"
                            id="english-all-tab"
                            data-toggle="tab"
                            href="#english-all"
                            role="tab"
                            aria-controls="english-all"
                            aria-selected="true"
                            onClick={() => this.changeView()}
                          >English</a>

                          </li>
                          <li  className="nav-item">
                              
                              <a
                            className="nav-link"
                            title="Hindi"
                            id="hindi-all-tab"
                            data-toggle="tab"
                            href="#hindi-all"
                            role="tab"
                            aria-controls="hindi-all"
                            aria-selected="false"
                            onClick={() => this.changeView()}
                          >Hindi</a>

                              
                          </li>
                      </ul>
                    
                    </div>
                  </div>

                    </div>
                    </div>
                    </div>
                    </section>

         );
    }
}
 
export default RssNews;