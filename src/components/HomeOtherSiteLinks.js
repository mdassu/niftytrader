import Link from 'next/link';

export default function HomeOtherSiteLinks(props) {
  return (
    <div className=" all-traders">
      <div className="owl-carousel owl-theme logos-slider" id="logos_slider">
        <div className=" item logos d-flex align-items-center justify-content-center">
          <a href="https://zerodha.com/open-account?c=ZMPPKK" target="_blank" title="Zerodha">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/zerodha.png" alt="Zerodha" className="zerodha-logo" />
              </div>
              <p>Open Account (₹20/Trade)</p>
            </div>
          </a>
        </div>
        <div className=" item logos d-flex align-items-center justify-content-center">
          <a href="https://itrade.angelbroking.com" target="_blank" title="Angel Broking">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/angel-broking.png" alt="Angel Broking" className="angel-broking-logo" />
              </div>
              <p>Flat ₹20 per trade</p>
            </div>
          </a>
        </div>
        <div className=" item logos d-flex align-items-center justify-content-center ">
          <a href="https://alicebluepartner.com/open-myaccount/?P=PN73&nifty_trader" target="_blank" title="Aliceblue">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/aliceblue.png" alt="Aliceblue" className="aliceblue-logo" />
              </div>
              <p>Get 0% Commission</p>
            </div>
          </a>
        </div>
        <div className=" item logos d-flex align-items-center justify-content-center">
          <a href="https://www.5paisapartners.com/partners-elite?rcode=NTY3Mjc3OTg" target="_blank" title="5paisa">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/5paisa.png" alt="5paisa" className="paisa-logo" />
              </div>
              <p>₹0 Account Opening</p>
            </div>
          </a>
        </div>
        <div className=" item logos d-flex align-items-center justify-content-center">
          <a href="https://tradesmartonline.in/landing-page/online-trading/?landing_page=asli-trader&utm_refer=YRJS285&utm_source=AP"
            title="Trade Smart" target="_blank">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/trade-smart.png" alt="Trade Smart" className="tradesmart-logo" />
              </div>
              <p>₹15 per Trade</p>
            </div>
          </a>
        </div>
        <div className=" item logos d-flex align-items-center justify-content-center">
          <a href={process.env.OLD_SITE_URL + "broker-directory"} title="NiftyTrader">
            <div className="">
              <div className="inner-logo-div">
                <img src="/images/niftytrader-logo.png" alt="NiftyTrader" className="nifty-logo" />
              </div>
              <p>Compare All Top Brokers</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  )
}

