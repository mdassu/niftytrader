import React, { Component } from "react";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-regular-svg-icons";
import {
  faCaretDown,
  faCaretUp,
  faCheck,
  faEllipsisV,
  faListUl,
  faPlus,
  faTimes,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { AuthenticationService } from "_services/AuthenticationService";
import { CALL_API, CALL_SIGNALR_API } from "_services/CALL_API";
import Loader from "components/Loader";
// import Autocomplete from 'react-autocomplete';
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import { toast } from "react-toastify";
import * as signalR from "@microsoft/signalr";
import * as moment from 'moment';
import Router from "next/router";
import StockAlertModal from "components/Modals/StockAlertModal";
import SEOPageContent from "components/SEOPageContent";

const mapStateToProps = state => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn
});

const mapDispatchToProps = {
};

class WatchlistData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      watchList: [],
      watchListData: [],
      symbolsList: "",
      allSymbolList: [],
      selectedWatchList: {},
      selectedWatchListName: "",
      oldSelectedWatchListName: "",
      selectedWatchListId: 0,
      selectedStatus: true,
      addWatchlistType: false,
      isLoaded: false,
      toastMsg: '',
      stockAlertPopup: false,
      stockAlertData: {},
    };
    this.connection = "";
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { isLoggedIn } = this.props;
      if (isLoggedIn) {
        this.getWatchlistList();
        this.getSymbolList();
      } else {
        Router.push('/')
      }
    }, 1500);

    // this.getWatchlistDataSignalR();
  }

  getWatchlistList = () => {
    const { currentUser, isLoggedIn } = this.props;
    if (isLoggedIn) {
      CALL_API(
        "POST",
        process.env.WATCHLIST_LIST_DATA,
        { user_id: currentUser["user_id"] },
        (res) => {
          if (res.status) {
            this.setState(
              {
                watchList: res["data"],
                addWatchlistType: false,
              },
              () => {
                var defaultWatchlist = [];
                if (Object.keys(this.state.selectedWatchList).length > 0) {
                  this.getWatchlistData(
                    this.state.selectedWatchList["watchlist_id"]
                  );
                  this.getWatchlistDataSignalR();
                } else {
                  if (this.state.selectedWatchListId > 0) {
                    defaultWatchlist = this.state.watchList.filter((item) => {
                      return (
                        item["watchlist_id"] == this.state.selectedWatchListId
                      );
                    });
                  } else {
                    defaultWatchlist = this.state.watchList.filter((item) => {
                      return item["isDefault"] == true;
                    });
                  }
                  this.setState({
                    selectedWatchList: defaultWatchlist[0],
                    selectedWatchListName: defaultWatchlist[0]["watchlist_name"],
                    selectedWatchListId: defaultWatchlist[0]["watchlist_id"],
                    symbolsList: defaultWatchlist[0]["symbol_name_list"],
                  });
                  this.getWatchlistData(defaultWatchlist[0]["watchlist_id"]);
                  this.getWatchlistDataSignalR();
                }
              }
            );
          } else {
            this.setState({
              isLoaded: true,
            });
          }
        }
      );
    }
  };
  openStockAlertModal = (stockData) => {
    if (this.props.isLoggedIn) {
      this.setState({
        stockAlertPopup: true,
        stockAlertData: stockData
      }, () => {
        $('#stockAlertPopup').modal('show');
      });
    } else {
      $('#loginpopup').modal('show');
    }
  }
  closeStockAlertModal = () => {
    this.setState({
      stockAlertPopup: false,
      stockAlertData: {}
    }, () => {
      $('#stockAlertPopup').modal('hide');
    });
  }

  getWatchlistDataSignalR = () => {
    var day = moment().format('dddd');
    var currentDate = moment().format('MM/DD/YYYY');
    var currentDateTime = moment().format();
    var checkDateTime = moment(currentDate + ' 16:00:00').format();
    var checkStartDateTime = moment(currentDate + ' 09:00:00').format();
    if ((day != 'Saturday' && day != 'Sunday') && (checkDateTime > currentDateTime) && (checkStartDateTime < currentDateTime)) {
      this.connection = new signalR.HubConnectionBuilder()
        .withUrl(
          "https://signalr.niftytrader.in/NiftySignalRTest/niftySignalRHub"
        )
        .build();

      this.connection.on(
        "sendTickDataToAndroid",
        (InstrumentToken, SymbolName, TickData) => {
          var listData = this.state.watchListData;
          listData.map((item, key) => {
            if (item['symbol_name'] == SymbolName) {
              item['close'] = TickData['lastPrice']
              item['high'] = TickData['high']
              item['low'] = TickData['low']
              item['open'] = TickData['open']
              item['volume'] = TickData['volume']
            }
            if (key == listData.length - 1) {
              this.setState({
                watchListData: listData
              });
            }
          })
        }
      );

      this.connection.start().then(() => {
        this.updateWatchlistConnectionForSignalR(this.connection.connectionId);
        this.updateWatchlistForSignalR(this.connection.connectionId);
      });
    }

  };

  updateWatchlistConnectionForSignalR = (connectionId) => {
    CALL_API(
      "POST",
      process.env.UPDATE_WATCHLIST_CONNECTION_FOR_SIGNALR,
      { watchlist_id: this.state.selectedWatchListId, connection_id: connectionId },
      (res) => { }
    );
  };

  updateWatchlistForSignalR = (connectionId) => {
    CALL_SIGNALR_API(
      "GET",
      process.env.UPDATE_WATCHLIST_FOR_SIGNALR,
      { ConnectionId: connectionId },
      (res) => { }
    );
  };

  getWatchlistData = (watchlistId) => {
    if (this.props.isLoggedIn) {
      CALL_API(
        "POST",
        process.env.WATCHLIST_DETAILS_DATA,
        { watchlist_id: watchlistId },
        (res) => {
          if (res.status) {
            this.setState({
              watchListData: res["data"],
              isLoaded: true,
            });
          } else {
            this.setState({
              watchListData: [],
              isLoaded: true,
            });
          }
        }
      );
    }
  };

  selectWatchList = (watchlist) => {
    this.setState({
      selectedWatchList: watchlist,
      selectedWatchListName: watchlist["watchlist_name"],
      selectedWatchListId: watchlist["watchlist_id"],
      selectedWatchListId: watchlist["watchlist_id"],
      symbolsList: watchlist["symbol_name_list"],
      isLoaded: false,
    });
    this.getWatchlistData(watchlist["watchlist_id"]);
  };

  getSymbolList() {
    CALL_API("GET", process.env.WL_SYMBOL_LIST_DATA, {}, (res) => {
      if (res.status) {
        var symbols = [];
        res["data"].map((item) => {
          symbols.push({
            id: item["symbol_name"],
            name: item["symbol_name"],
          });
        });
        this.setState({
          allSymbolList: symbols,
        });
      } else {
        this.setState({
          allSymbolList: [],
        });
      }
    });
  }

  addNewWatchlist = () => {
    var watchListName = this.state.selectedWatchListName;
    if (watchListName != "") {
      if (this.state.selectedWatchListId == 0) {
        var params = {
          watchlist_id: 0,
          watchlist_name: watchListName,
          symbol_list: "",
          isDefault: false,
        };
        CALL_API("POST", process.env.ADD_WATCHLIST_DATA, params, (res) => {
          if (res.status) {
            this.setState(
              {
                selectedWatchListId: res["data"],
                selectedWatchListName: watchListName,
                selectedWatchList: {},
              },
              () => {
                this.getWatchlistList();
              }
            );
            this.toastConfig["type"] = "success";
            toast(res["message"], this.toastConfig);
          } else {
            this.toastConfig["type"] = "error";
            toast(res["message"], this.toastConfig);
          }
        });
      } else {
        var params = {
          watchlist_id: this.state.selectedWatchListId,
          watchlist_name: watchListName,
          symbol_list: this.state.symbolsList,
          isDefault: this.state.selectedWatchList["isDefault"],
        };
        CALL_API("POST", process.env.EDIT_WATCHLIST_DATA, params, (res) => {
          if (res.status) {
            this.setState(
              {
                addWatchlistType: !this.state.addWatchlistType,
              },
              () => {
                this.getWatchlistList();
              }
            );
            this.toastConfig["type"] = "success";
            toast(this.state.toastMsg, this.toastConfig);
            this.setState({
              toastMsg: ''
            });
          } else {
            this.toastConfig["type"] = "error";
            toast(res["message"], this.toastConfig);
          }
        });
      }
    } else {
      this.toastConfig["type"] = "error";
      ("Please enter watchlist name", this.toastConfig);
    }
  };

  handleChangeWatchlistName = (event) => {
    this.setState({
      selectedWatchListName: event.target.value,
    });
  };

  changeAddWatchlistType = (type) => {
    var oldName = this.state.selectedWatchListName;
    if (type == "add") {
      var existWatchlistName = '';
      var newWatchlistName = 'Watchlist 1'
      this.state.watchList.map(item => {
        if (item.watchlist_name.search('Watchlist ') != -1) {
          existWatchlistName = item.watchlist_name
        }
      });
      if (existWatchlistName != '') {
        newWatchlistName = existWatchlistName.split(' ')[0] + ' ' + (existWatchlistName.split(' ')[1] ? Number(existWatchlistName.split(' ')[1]) + 1 : '')
      }
      this.setState({
        watchListData: [],
        addWatchlistType: !this.state.addWatchlistType,
        selectedWatchListName: newWatchlistName,
        oldSelectedWatchListName: oldName,
        selectedWatchListId: 0,
      });
    } else {
      this.setState({
        addWatchlistType: !this.state.addWatchlistType,
        oldSelectedWatchListName: oldName,
        toastMsg: 'Watchilst name updated successfully.'
      });
    }
  };

  setDefaultWhatchlist = (watchlistId) => {
    var params = {
      watchlist_id: watchlistId,
    };

    CALL_API("POST", process.env.SET_DEFAULT_WATCHLIST_DATA, params, (res) => {
      if (res.status) {
        this.getWatchlistList();
        this.toastConfig["type"] = "success";
        toast('Default Watchlist Change Successfully', this.toastConfig);
      } else {
        this.toastConfig["type"] = "error";
        toast(res["message"], this.toastConfig);
      }
    });
  };

  handleSymbolSelect = (item) => {
    this.setState({
      selectedStatus: false
    }, () => {
      var symbol = item["name"];
      var symbolList = this.state.symbolsList;
      if (symbolList != '') {
        symbolList = symbolList + "," + symbol.split(" | ")[0];
      } else {
        symbolList = symbol.split(" | ")[0];
      }
      this.setState(
        {
          symbolsList: symbolList,
          selectedStatus: true
        },
        () => {
          this.setState({
            toastMsg: symbol + ' Added to your Watchlist'
          }, () => {
            this.addNewWatchlist();
          })
        }
      );
    });
  };

  deleteWatchlist = () => {
    var params = {
      watchlist_id: this.state.selectedWatchListId,
    };
    CALL_API("POST", process.env.DELETE_WATCHLIST_DATA, params, (res) => {
      if (res.status) {
        this.setState(
          {
            selectedWatchList: {},
            selectedWatchListName: "",
            selectedWatchListId: "",
          },
          () => {
            this.getWatchlistList();
          }
        );
        this.toastConfig["type"] = "success";
        toast(res["message"], this.toastConfig);
      } else {
        var msg = res['message'];
        if (this.state.selectedWatchList['isDefault']) {
          msg = 'Default Watchlist cannot be deleted';
        }
        this.toastConfig["type"] = "error";
        toast(msg, this.toastConfig);
      }
    });
    $('#removeWatchlist').modal('hide');
  };

  removeSymbolFromWatchlist = (symbolName) => {
    var symbolListArr = this.state.symbolsList.split(',');
    var index = symbolListArr.indexOf(symbolName);
    if (index != -1) {
      symbolListArr.splice(index, 1);
    }
    var symbolList = symbolListArr.join();
    this.setState(
      {
        symbolsList: symbolList,
      },
      () => {
        this.setState({
          toastMsg: 'Your Watchlist Symbol has been Removed!'
        }, () => {
          this.addNewWatchlist();
        })
      }
    );
  }

  searchStock = () => {
    $('#autoComplete input').focus();
  }

  showFeatures = (key) => {
    $('.show-feature-table-' + key).toggle();
  }

  cancelNewWatchlist = () => {
    this.setState({
      selectedWatchListName: this.state.oldSelectedWatchListName
    }, () => {
      this.getWatchlistList();
    });

  }

  componentWillUnmount = () => {
    if (this.connection != '') {
      this.connection.stop();
    }
  }

  render() {
    const {
      watchList,
      watchListData,
      selectedWatchListName,
      allSymbolList,
      addWatchlistType,
      selectedStatus,
      isLoaded,
      stockAlertPopup,
      stockAlertData,
    } = this.state;
    const { pageContent } = this.props;
    var watchListTbody = "";

    if (watchListData && watchListData.length > 0) {
      watchListTbody = watchListData.map((item, key) => {
        var changeValue = item["close"] - item["prev_close"];
        var changePer = (changeValue / item["prev_close"]) * 100;
        var todayLowPer =
          ((item["close"] - item["low"]) / (item["high"] - item["low"])) * 100;
        return (
          <React.Fragment>
            <tr key={key}>
              <td className="text-capitalize">
                <a href={process.env.OLD_SITE_URL + 'stocks-analysis/' + item["symbol_name"].toLowerCase()} title={item["symbol_name"]}>
                  {item["symbol_name"]}
                </a>
              </td>
              <td>{Number(item["close"]).toFixed(2)}</td>
              <td
                className={
                  Math.sign(changeValue) == -1 ? "percent-red" : "percent-green"
                }
              >
                <FontAwesomeIcon
                  icon={Math.sign(changeValue) == -1 ? faCaretDown : faCaretUp}
                  width="7.5"
                  height="12"
                />{" "}
                {Number(changeValue).toFixed(2)} ({Number(changePer).toFixed(2)}%)
            </td>
              <td> {item["prev_close"]} </td>
              <td>
                <div className="number-box percent-red">
                  {" "}
                  {Number(item["low"]).toFixed(2)}
                </div>
                <div className="watchlist-range-box">
                  <div
                    className="watchlist-high-range"
                    style={{ width: `${todayLowPer}%` }}
                  ></div>
                  <div
                    className="watchlist-low-range"
                    style={{ width: `${100 - todayLowPer}%` }}
                  ></div>
                  <img
                    src="/images/watchlist-range-control.png"
                    className="watchlist-range-control"
                    alt=""
                    style={{ left: `${todayLowPer}%` }}
                  />
                  <div className="clearfix"></div>
                </div>
                <div className="number-box percent-green text-right">
                  {" "}
                  {Number(item["high"]).toFixed(2)}
                </div>
              </td>
              <td> {item["volume"]}</td>
              <td className="action-btns">
                <div className="ellipsis-top">
                  <button className="ellipsis-btn">
                    <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
                  </button>
                  <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                    {/* <li>
                    <a href="" title="Add to Portfolio">
                     <FontAwesomeIcon icon={faPlus} width="14" height="14" /> Add to Portfolio
                    </a>
                  </li> */}
                    <li className="" onClick={() => this.openStockAlertModal(item)} >
                      <a title="Remove Symbol">
                        <img src="/images/createalert.png" alt="" className="alertBTn" /> Create Alert
                      </a>
                    </li>
                    <li className="" onClick={() => this.removeSymbolFromWatchlist(item['symbol_name'])}>
                      <a title="Remove Symbol">
                        <FontAwesomeIcon icon={faTrashAlt} width="14" height="14" style={{ color: 'red' }} /> Remove Symbol
                    </a>
                    </li>
                    {
                      item['nfo_list'].length > 0 ? (
                        <React.Fragment>
                          <li className="" onClick={() => this.showFeatures(key)}>
                            <a title="Show Futures">
                              <FontAwesomeIcon icon={faListUl} width="14" height="14" style={{ color: '#2196f3' }} /> Show Futures
                          </a>
                          </li>
                          <li className="">
                            <a href={process.env.OLD_SITE_URL + "options-trading/" +
                              item["symbol_name"].toLowerCase()} title="Stock Option Chain">
                              <FontAwesomeIcon icon={faListUl} width="14" height="14" /> Stock Option Chain
                        </a>
                          </li>
                        </React.Fragment>
                      ) : ''
                    }

                  </ul>
                </div>
              </td>
            </tr>
            {
              item['nfo_list'] && item['nfo_list'].length > 0 ? (
                item['nfo_list'].map((nfoData, index) => {
                  var changeValueChild = nfoData["close"] - nfoData["prev_close"];
                  var changePerChild = (changeValueChild / nfoData["prev_close"]) * 100;
                  var todayLowPerChild =
                    ((nfoData["close"] - nfoData["low"]) / (nfoData["high"] - nfoData["low"])) * 100;
                  return (
                    <tr key={index} style={{ display: 'none' }} className={"show-feature-ta old-feature-forHid show-feature-table-" + key} >
                      <td>{nfoData['trading_symbol']}</td>
                      <td>{Number(nfoData['close']).toFixed(2)}</td>
                      <td>
                        <span className={Math.sign(changeValueChild) != -1 ? 'percent-green' : 'percent-red'} id="CHANGE_OF_ACC21FEBFUT"><FontAwesomeIcon icon={Math.sign(changeValueChild) != -1 ? faCaretUp : faCaretDown} width="12" height="12" />{" "}{Number(changeValueChild).toFixed(2)} ({Number(changePerChild).toFixed(2)}%)</span>
                      </td>
                      <td>{Number(nfoData['prev_close']).toFixed(2)}</td>
                      <td>

                        <div className="number-box percent-red">{Number(nfoData['prev_close']).toFixed(2)}</div>
                        <div className="watchlist-range-box">
                          <div className="watchlist-high-range" style={{ width: todayLowPerChild + '%' }}>

                          </div>
                          <div className="watchlist-low-range" style={{ width: 100 - todayLowPerChild + '%' }}>

                          </div>
                          <img src="/images/watchlist-range-control.png" id="Range_OF_ACC21FEBFUT" className="watchlist-range-control" alt="" style={{ left: todayLowPerChild + '%' }} />
                        </div>
                        <div className="number-box percent-green text-right"> {Number(nfoData['high']).toFixed(2)}</div>
                      </td>
                      <td>{Number(nfoData['valume'])}</td>
                      <td></td>
                    </tr>
                  )
                })
              ) : ''
            }
          </React.Fragment >
        );
      });
    }

    return (
      <React.Fragment>
        {/* <Container /> */}
        {
          isLoaded ? "" : <Loader />
        }
        <React.Fragment>
          <div className="row">
            <div className="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 p-0">
              <div>
                <div className="gap-up-box watchlist-tab">
                  <div className="row m-0">
                    <div className="col-md-12 col-xl-12 col-lg-12 col-12 padding-r">
                      <ul
                        className="nav nav-pills flex-column"
                        id="pills-tab"
                        role="tablist"
                      >
                        <li className="nav-item">
                          <a
                            href="#pills-home"
                            className="nav-link active"
                            id="pills-home-tab"
                            data-toggle="pill"
                            role="tab"
                            aria-controls="pills-home"
                            aria-selected="true"
                            title="Watchlist "
                          >
                            {pageContent["page_Content_Title"]}
                          </a>
                        </li>
                        {/* <li className="nav-item">
                          <a
                            href="#pills-gap"
                            className="nav-link"
                            id="pills-gap-tab"
                            data-toggle="pill"
                            role="tab"
                            aria-controls="pills-gap"
                            aria-selected="true"
                            title="Portfolio"
                          >
                            Portfolio
                          </a>
                        </li> */}
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="gap-up-content pricetable-box illustration-box border-top-0 p-lg-0 m-0">
                  <div className="tab-content" id="pills-tabContent">
                    <div
                      className="tab-pane fade show active"
                      id="pills-home"
                      role="tabpanel"
                      aria-labelledby="pills-home-tab"
                    >
                      <div className="row">
                        <div className="col-12 watchlist-new-table">
                          {/* <!-- row start --> */}
                          <div className="row m-0">
                            <div className="col-md-6 col-xl-6 col-lg-6 col-sm-6 col-7 pr-0 pl-0">
                              {addWatchlistType ? (
                                <div className="d-flex">
                                  <input
                                    type="text"
                                    className="add-symbol"
                                    value={selectedWatchListName}
                                    onChange={this.handleChangeWatchlistName}
                                    autoFocus={true}
                                    placeholder="Watch List Name"
                                  />
                                  <div className="watchlist_action">
                                    <a
                                      className="edit-icon afterAdded"
                                      id="showinput"
                                      data-toggle="tooltip"
                                      data-placement="top"
                                      onClick={this.addNewWatchlist}
                                      title="Add"
                                    >
                                      <FontAwesomeIcon
                                        icon={faCheck}
                                        width="17"
                                        height="15"
                                        title="Add"
                                      />
                                    </a>
                                    {/* <a className="edit-icon addBTN d-none " title="Add" id="addicon">Add</a> */}
                                    <a
                                      className="edit-icon deleteBTN text-center"
                                      title="Cancel"
                                      data-toggle="tooltip"
                                      onClick={this.cancelNewWatchlist}
                                      data-placement="top"
                                    >
                                      <FontAwesomeIcon
                                        icon={faTimes}
                                        width="13"
                                        height="15"
                                        title="Cancel"
                                      />
                                    </a>
                                  </div>
                                </div>
                              ) : (
                                <div className="d-flex">
                                  <div className="dropdown watchlist-dropdown">
                                    <button
                                      className="btn dropdown-toggle"
                                      type="button"
                                      title="Watchlist"
                                      id="dropdownMenuButton"
                                      data-toggle="dropdown"
                                      aria-haspopup="true"
                                      aria-expanded="false"
                                    >
                                      {" "}
                                      {selectedWatchListName}{" "}
                                    </button>
                                    <div
                                      className="dropdown-menu"
                                      aria-labelledby="dropdownMenuButton"
                                    >
                                      {watchList && watchList.length > 0
                                        ? watchList.map((item, key) => {
                                          return (
                                            <div
                                              style={{ position: "relative" }}
                                            >
                                              <a
                                                className="dropdown-item"
                                                onClick={() =>
                                                  this.selectWatchList(item)
                                                }
                                                key={key}
                                                title={item["watchlist_name"]}
                                              >
                                                {item["watchlist_name"]}
                                              </a>
                                              {item["isDefault"] ? (
                                                <span className="badge badge-pill badge-success defualtwlBtn">
                                                  Default
                                                </span>
                                              ) : (
                                                <span
                                                  className="badge badge-pill badge-secondary defualtwlBtn"
                                                  onClick={() =>
                                                    this.setDefaultWhatchlist(
                                                      item["watchlist_id"]
                                                    )
                                                  }
                                                >
                                                  Set Default
                                                </span>
                                              )}
                                            </div>
                                          );
                                        })
                                        : ""}
                                      <a
                                        className="dropdown-item add-watchlist"
                                        onClick={() =>
                                          this.changeAddWatchlistType("add")
                                        }
                                        title="Watchlist"
                                      >
                                        <FontAwesomeIcon
                                          icon={faPlus}
                                          width="11"
                                          height="15"
                                        />{" "}
                                        Watchlist
                                      </a>
                                    </div>
                                  </div>
                                  <div className="watchlist_action">
                                    <a
                                      className="edit-icon afterAdded"
                                      id="showinput"
                                      data-toggle="tooltip"
                                      data-placement="top"
                                      title="Edit"
                                      onClick={() =>
                                        this.changeAddWatchlistType("edit")
                                      }
                                    >
                                      <FontAwesomeIcon
                                        icon={faEdit}
                                        width="17"
                                        height="15"
                                      />
                                    </a>
                                    <a
                                      className="edit-icon addBTN d-none "
                                      title="Edit"
                                      id="addicon"
                                    >
                                      Add
                                    </a>
                                    <a
                                      className="edit-icon deleteBTN text-center"
                                      title="Delete"
                                      data-toggle="modal"
                                      data-target="#removeWatchlist"
                                    >
                                      <FontAwesomeIcon
                                        icon={faTrashAlt}
                                        width="13"
                                        height="15"
                                      />
                                    </a>
                                  </div>
                                </div>
                              )}
                            </div>
                            <div className="col-md-6 col-xl-6 col-lg-6 col-sm-6 col-5 pr-0 pl-0" id="autoComplete">
                              {/* <input type="text" placeholder="Add Symbol" className="add-symbol" /> */}
                              {/* <Autocomplete
                                    // getItemValue={(item) => item.label}
                                    className="add-symbol"
                                    items={allSymbolList}
                                    renderItem={(item, isHighlighted) =>
                                      <div>
                                        {item.symbol_name}
                                      </div>
                                    }
                                  // value={value}
                                  // onChange={(e) => value = e.target.value}
                                  // onSelect={(val) => value = val}
                                  /> */}
                              {
                                selectedStatus && !addWatchlistType ? (
                                  <ReactSearchAutocomplete
                                    items={allSymbolList}
                                    placeholder="Add stock to watchlist"
                                    // onSearch={handleOnSearch}
                                    onSelect={this.handleSymbolSelect}
                                  />
                                ) : ''
                              }
                            </div>
                          </div>
                          {/* <!-- row close -->
                        <!-- row start --> */}
                          {watchListTbody && watchListTbody != "" ? (
                            <div className="nifty-chart-panel mt-3 mb-3">
                              <div className="row m-0">
                                <div className="col-12 p-0">
                                  <div className="nifty-datatable table-responsive watchlistTable">
                                    <table className="table">
                                      <thead>
                                        <tr>
                                          <th>symbol</th>
                                          <th>current price</th>
                                          <th>change(%)</th>
                                          <th>prev. close</th>
                                          <th className=" ">today's l/h</th>
                                          <th>volume</th>
                                          {/* <th>&nbsp;</th> */}
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>{watchListTbody}</tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ) : (
                            <div className="row mr-0 ml-0">
                              <div className="col-12 pr-0 pl-0 mt-3 text-center">
                                <img
                                  src="/images/empty-watchlist.jpg"
                                  alt="Watchlist Empty"
                                  title="Watchlist Empty"
                                  className="img-fluid empty-w-img mb-3"
                                />
                                <h6 className="mb-3">
                                  You are not watching anything yet
                                </h6>
                                <p className="">
                                  Add stocks to your watchlist to start tracking
                                  them here
                                </p>
                                {
                                  !addWatchlistType ? (
                                    <button
                                      onClick={this.searchStock}
                                      className="search-stock-btn mb-3"
                                      title="Add stock to watchlist"
                                    >
                                      Add stock to watchlist
                                    </button>
                                  ) : ''
                                }

                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    {/* <!-- portfolio tab --> */}
                    <div
                      className="tab-pane fade show"
                      id="pills-gap"
                      role="tabpanel"
                      aria-labelledby="pills-gap-tab"
                    >
                      {/* <!-- login alert tab --> */}
                      <div className="row m-0">
                        <div className="col-12 padding-r">
                          <div
                            className="alert alert-warning alert-dismissible fade show"
                            role="alert"
                          >
                            <div className="alert-box clearfix">
                              <div className="alert-icon">
                                <i className="fas fa-exclamation-triangle"></i>
                              </div>
                              <div className="alert-content">
                                <p>Login now to save & sync your watchlist</p>
                              </div>
                            </div>
                            <button
                              type="button"
                              className="close alert-close-btn alert-login-btn d-sm-block"
                              title="Login"
                            >
                              Login{" "}
                            </button>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End login Alert tab --> */}
                      <div className="nifty-chart-panel mt-1 mb-3">
                        <div className="row m-0">
                          <div className="col-12 p-0">
                            <div className="nifty-datatable table-responsive">
                              <table className="table" id="MyTable">
                                <thead>
                                  <tr>
                                    <th>STOCK</th>
                                    <th>CURRENT PRICE </th>
                                    <th>TODAY'S L/H</th>
                                    <th>QUANTITY</th>
                                    <th>Today's Gain/Loss</th>
                                    <th className=" ">TODAY'S GAIN/LOSS</th>
                                    <th>NET GAIN/LOSS</th>
                                    <th className="text-center">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {" "}
                                  <tr>
                                    <td>Nifty</td>
                                    <td>59988.00</td>
                                    <td>
                                      <div className="number-box percent-red">
                                        {" "}
                                        34.3 0{" "}
                                      </div>
                                      <div className="watchlist-range-box">
                                        <div
                                          className="watchlist-low-range"
                                          style={{ width: "30%" }}
                                        ></div>
                                        <div
                                          className="watchlist-high-range"
                                          style={{ width: "70%" }}
                                        ></div>
                                        <img
                                          src="/images/watchlist-range-control.png"
                                          className="watchlist-range-control"
                                          alt=""
                                          style={{ left: "30%" }}
                                        />
                                        <div className="clearfix"></div>
                                      </div>
                                      <div className="number-box percent-green text-right">
                                        {" "}
                                        3 0.80{" "}
                                      </div>
                                    </td>
                                    <td>34.60</td>
                                    <td className="percent-red"> -50.63 </td>
                                    <td className="percent-green"> 80.63 </td>
                                    <td>1501</td>
                                    <td className="table-action-btns text-center">
                                      <a
                                        href="#"
                                        data-toggle="modal"
                                        data-target="#addto-watchlist-portfolio"
                                        title="Edit"
                                      >
                                        <i className="fal fa-edit"></i>
                                      </a>
                                      <a title="Delete" id="delete-row">
                                        <i className="fal fa-trash-alt"></i>
                                      </a>
                                    </td>
                                  </tr>{" "}
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {
            pageContent && pageContent['page_Content'] != '' ?
              (
                <div className="row">
                  <div className="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 p-0">
                    <SEOPageContent pageContent={pageContent['page_Content']} />
                  </div>
                </div>
              ) : ''
          }
        </React.Fragment>
        <div className="modal filter_modal filter_content_modal confirmation-modal" id="removeWatchlist" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenter1Title" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">

              <div className="modal-body">

                <div className="row mr-0 ml-0 ">
                  <div className="col-12">
                    <div className="icon-box ml-auto mr-auto">
                      <FontAwesomeIcon icon={faTrashAlt} width="71" height="75" style={{ width: '31px', height: '35px', color: '#fc5345' }} />
                    </div>
                  </div>
                </div>


                <div className="row ml-0 mr-0">
                  <div className="col-12 text-center">
                    <h6>Are you sure?</h6>
                    <p className="pr-0 pl-0">Are you sure you want to delete the watchlist?</p>
                  </div>
                </div>


                <div className="row">
                  <div className="col-12 text-center">
                    <button className="feedback-submit-btn cancel-modal-btn" data-dismiss="modal">Cancel</button>
                    <button className="feedback-submit-btn delete-modal-btn" onClick={this.deleteWatchlist}>Delete</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div className="modal filter_modal filter_content_modal feedback-modal show createalert-for-banking"
          id="stockAlertPopup" tabIndex="-1" role="dialog" aria-labelledby="stockAlertPopup"
          aria-hidden="true">
          {
            stockAlertPopup ? (
              <StockAlertModal stockAlertData={stockAlertData} closeStockAlertModal={this.closeStockAlertModal} />
            ) : ''
          }
        </div>
      </React.Fragment>
    );
  }
}

// export default WatchlistData;
export default connect(mapStateToProps, mapDispatchToProps)(WatchlistData);
