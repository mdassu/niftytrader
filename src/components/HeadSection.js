import Layout from 'components/Layout';
import Head from 'next/head';
import NiftyOptionTracker from 'components/Options/NiftyOI/NiftyOptionTracker';

export default function HeadSection(props) {
  return (
    <Head>
      {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47841985-1"></script>
      <script dangerouslySetInnerHTML={
        {
          __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-47841985-1');
                    `
        }
      } />
      <meta charSet="utf-8" />
      <title>{props.title ? props.title : 'NiftyTrader'}</title>
      <link
        rel="icon"
        href="favicon.jpg"
        type="image/jpg"
        sizes="16x16" />
      <meta
        name="viewport"
        content="width=device-width,  initial-scale=1 maximum-scale=1.0, user-scalable=no" />
      {props.metaKeywords ? <meta name="keywords" content={props.metaKeywords} /> : ''}
      {props.metaDesc ? <meta name="description" content={props.metaDesc} /> : ''}
      {props.metaCanonical ? <link rel="canonical" href={props.metaCanonical} /> : ''}
      {/* <link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet" /> */}
      <link rel="stylesheet preload" as="style" href="/css/bootstrap/bootstrap.min.css" />
      {/* <link rel="stylesheet preload" as="style" href="css/style.css" />
      <link rel="stylesheet preload" as="style" href="css/responsive.css" />
      <link rel="stylesheet preload" as="style" href="css/nifty-style.css" />
      <link rel="stylesheet preload" as="style" href="css/nifty-responsive.css" />
      <link rel="stylesheet preload" as="style" href="css/style-home.css" />
      <link rel="stylesheet preload" as="style" href="css/customnifty.css" />
      <link rel="stylesheet preload" as="style" href="css/nifty-broker-profile.css" />
      <link rel="stylesheet preload" as="style" href="css/fade-down.css" />
      <link rel="stylesheet preload" as="style" href="css/home-webslidemenu.css" />
      <link rel="stylesheet preload" as="style" href="css/home-responsive.css" />
      <link rel="stylesheet preload" as="style" href=" css/bootstrap-select.min.css" /> */}
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,500i,700&display=swap" rel="stylesheet preload"
        as="style" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css" />

      {/* <script async src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script> */}


      <script async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag|| {
            cmd: []
          }
          
          ;
          googletag.cmd.push(function() {
            var REFRESH_KEY='refresh';
            var REFRESH_VALUE='true';
            googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_BOTTOM_STICKY', [[728, 90]], 'div-DStickyAds').setTargeting(REFRESH_KEY, REFRESH_VALUE).addService(googletag.pubads());
            var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;
            googletag.pubads().addEventListener('impressionViewable', function(event) {
              var slot=event.slot;
              if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1) {
                setTimeout(function() {
                  googletag.pubads().refresh([slot]);
                }
                , SECONDS_TO_WAIT_AFTER_VIEWABILITY*1000);
              }
            }
            );
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs(true);
            googletag.enableServices();
          }
          
          );
                    `
        }
      } /> */}


      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag|| {
            cmd: []
          }
          
          ;
          googletag.cmd.push(function() {
            var REFRESH_KEY='refresh';
            var REFRESH_VALUE='true';
            googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_LEFT_STICKY', [[160, 600]], 'div-LDWebStickyAds').setTargeting(REFRESH_KEY, REFRESH_VALUE).addService(googletag.pubads());
            var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;
            googletag.pubads().addEventListener('impressionViewable', function(event) {
              var slot=event.slot;
              if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1) {
                setTimeout(function() {
                  googletag.pubads().refresh([slot]);
                }
                , SECONDS_TO_WAIT_AFTER_VIEWABILITY*1000);
              }
            }
            );
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs(true);
            googletag.enableServices();
          }
          
          );
                    `
        }
      } /> */}

      {/* 1) DESKTOP STICKY TAG--> Please update this tag in <head> */}
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag||{cmd:[]};googletag.cmd.push(function(){var REFRESH_KEY='refresh';var REFRESH_VALUE='true';googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_BOTTOM_STICKY',[[728,90]],'div-DStickyAds').setTargeting(REFRESH_KEY,REFRESH_VALUE).addService(googletag.pubads());var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;googletag.pubads().addEventListener('impressionViewable',function(event){var slot=event.slot;if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1){setTimeout(function () { googletag.pubads().refresh([slot]); }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);}});googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs(true);googletag.enableServices();});</script><style>@media only screen and (min-width:480px){.stickyads_Desktop_Only{position:fixed;left:0;bottom:0;width:100%;text-align:center;z-index:999999;display:flex;justify-content:center;background-color:rgba(0,0,0,0.1)}}@media only screen and (max-width:480px){.stickyads_Desktop_Only{display:none}}.stickyads_Desktop_Only .btn_Desktop_Only{position:absolute;top:10px;left:10px;transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);background-color:#555;color:white;font-size:16px;border:0;cursor:pointer;border-radius:25px;text-align:center}.stickyads_Desktop_Only .btn_Desktop_Only:hover{background - color:red}.stickyads{display:none}</style><div id='closebtn_Desktop_Only' className='stickyads_Desktop_Only'><div id='div-DStickyAds'><script>googletag.cmd.push(function(){googletag.display('div-DStickyAds')});</script></div><button className='btn_Desktop_Only' onclick='change_css_Desktop_Only()'>x </button></div><script>function change_css_Desktop_Only(){document.getElementById('closebtn_Desktop_Only').style.cssText = 'display:none;'}
                    `
        }
      } /> */}
      {/* 2) MOBILE STICKY TAG--> Please update this tag in <head> */}
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag||{cmd:[]};googletag.cmd.push(function(){var REFRESH_KEY='refresh';var REFRESH_VALUE='true';googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_BOTTOM_STICKY_MOBILE',[[300,75]],'div-MWebStickyAds').setTargeting(REFRESH_KEY,REFRESH_VALUE).addService(googletag.pubads());var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;googletag.pubads().addEventListener('impressionViewable',function(event){var slot=event.slot;if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1){setTimeout(function () { googletag.pubads().refresh([slot]); }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);}});googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs(true);googletag.enableServices();});</script><style>@media only screen and (min-width: 480px){.stickyads_Mobile_Only{display:none}}@media only screen and (max-width: 480px){.stickyads_Mobile_Only{position:fixed;left:0;bottom:0;width:100%;text-align:center;z-index:999999;display:flex;justify-content:center;background-color:rgba(0,0,0,0.1)}}.stickyads_Mobile_Only .btn_Mobile_Only{position:absolute;top:10px;left:10px;transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);background-color:#555;color:white;font-size:16px;border:none;cursor:pointer;border-radius:25px;text-align:center}.stickyads_Mobile_Only .btn_Mobile_Only:hover{background - color:red}.stickyads{display:none}</style><div id='closebtn_Mobile_Only' className='stickyads_Mobile_Only'><div id='div-MWebStickyAds'><script>googletag.cmd.push(function(){googletag.display('div-MWebStickyAds')});</script></div><button className='btn_Mobile_Only' onclick='change_css_Mobile_Only()'>x</button></div> <script>function change_css_Mobile_Only(){document.getElementById('closebtn_Mobile_Only').style.cssText = 'display:none;';}
                    `
        }
      } /> */}
      {/* 3) LEFT STICKY TAG--> Please update this tag in <head> */}
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag||{cmd:[]};googletag.cmd.push(function(){var REFRESH_KEY='refresh';var REFRESH_VALUE='true';googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_LEFT_STICKY',[[160,600]],'div-LDWebStickyAds').setTargeting(REFRESH_KEY,REFRESH_VALUE).addService(googletag.pubads());var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;googletag.pubads().addEventListener('impressionViewable',function(event){var slot=event.slot;if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1){setTimeout(function(){googletag.pubads().refresh([slot]);},SECONDS_TO_WAIT_AFTER_VIEWABILITY*1000);}});googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs(true);googletag.enableServices();});</script><style>.stickyads_Left{position:fixed;left:0px;top:25px;text-align:center;z-index:999999;background-color:rgba(0,0,0,0.1)}.stickyads_Left .btn_left{position:absolute;top:-12px;left:80px;transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);background-color:#555;color:white;font-size:16px;border:none;cursor:pointer;border-radius:25px;text-align:center}.stickyads_Left .btn_left:hover{background-color:red}@media only screen and (max-width: 1440px){.stickyads_Left{display:none}}</style><div id='closebtn_Left' className='stickyads_Left'><div id='div-LDWebStickyAds'><script>googletag.cmd.push(function(){googletag.display('div-LDWebStickyAds')});</script></div><button className='btn_left' onclick='change_css_Left()'>x</button></div> <script>function change_css_Left(){document.getElementById('closebtn_Left').style.cssText='display:none;';}
          `
        }
      } /> */}
      {/* 4) RIGHT STICKY TAG--> Please update this tag in <head> */}
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag||{cmd:[]};googletag.cmd.push(function(){var REFRESH_KEY='refresh';var REFRESH_VALUE='true';googletag.defineSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_RIGHT_STICKY',[[160,600]],'div-RDWebStickyAds').setTargeting(REFRESH_KEY,REFRESH_VALUE).addService(googletag.pubads());var SECONDS_TO_WAIT_AFTER_VIEWABILITY=30;googletag.pubads().addEventListener('impressionViewable',function(event){var slot=event.slot;if(slot.getTargeting(REFRESH_KEY).indexOf(REFRESH_VALUE)>-1){setTimeout(function(){googletag.pubads().refresh([slot]);},SECONDS_TO_WAIT_AFTER_VIEWABILITY*1000);}});googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs(true);googletag.enableServices();});</script><style>.stickyads_Right{position:fixed;right:0px;top:25px;text-align:center;z-index:999999;background-color:rgba(0,0,0,0.1)}.stickyads_Right .btn_Right{position:absolute;top:-12px;right:80px;transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);background-color:#555;color:white;font-size:16px;border:none;cursor:pointer;border-radius:25px;text-align:center}.stickyads_Right .btn_Right:hover{background-color:red}@media only screen and (max-width: 1440px){.stickyads_Right{display:none}}</style><div id='closebtn_Right' className='stickyads_Right'><div id='div-RDWebStickyAds'><script>googletag.cmd.push(function(){googletag.display('div-RDWebStickyAds')});</script></div><button className='btn_Right' onclick='change_css_Right()'>x</button></div> <script>function change_css_Right(){document.getElementById('closebtn_Right').style.cssText='display:none;';}
                    `
        }
      } /> */}
      {/* 5) INTERSTITIAL TAG--> Please update this tag in <head> of the domain. */}
      {/* <script dangerouslySetInnerHTML={
        {
          __html: `
          window.googletag=window.googletag||{cmd:[]};var interstitialSlot,staticSlot;googletag.cmd.push(function(){interstitialSlot = googletag.defineOutOfPageSlot('/13406045/CM_NIFTYTRADER_WEB_TOP/CM_NIFTYTRADER_WEB_INTERSTITIAL', googletag.enums.OutOfPageFormat.INTERSTITIAL);if(interstitialSlot){interstitialSlot.addService(googletag.pubads());console.log(interstitialSlot);googletag.pubads().addEventListener('slotOnload',function(event){if(interstitialSlot===event.slot){console.log(interstitialSlot);}});}googletag.enableServices();});
                    `
        }
      } /> */}
    </Head>
  )
}
