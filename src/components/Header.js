import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCommentAlt,
  faShareAlt,
  faStream,
  faAngleRight,
  faBars,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import MobileMenu from "./MobileMenu";
import Login from "./Login";
import { AuthenticationService } from "_services/AuthenticationService";
import Router from "next/router";
import { slide as Menu } from "react-burger-menu";
import { CALL_API } from "_services/CALL_API";
import StockUpdates from "./StockUpdates";

const mapStateToProps = (state) => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn,
});

const mapDispatchToProps = {};

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerMsg: {},
      nifty50Data: [],
    };
  }

  componentDidMount() {
    this.getTopHeadMessage();
    this.getNifty50Data();
  }

  checkPermium = (url) => {
    const { isLoggedIn, currentUser } = this.props;
    if (
      url === "/options-screener" ||
      url === "/live-market-screener" ||
      url === "/live-analytics" ||
      url === "/banknifty-live-analysis"
    ) {
      Router.push(url);
    } else {
      if (isLoggedIn) {
        if (currentUser["membership_flag"] == 1) {
          Router.push(url);
        } else {
          Router.push("/primeplans");
        }
      } else {
        $("#loginpopup").modal("show");
      }
    }
  };

  // setUserLoagged = (val) => {
  //   this.setState({
  //     isLogged: val
  //   })
  // }

  getTopHeadMessage = () => {
    CALL_API("GET", process.env.TOP_HEADER_MSG_DATA, {}, (res) => {
      if (res.status) {
        this.setState({
          headerMsg: res.data,
        });
      }
    });
  };

  getNifty50Data() {
    CALL_API("GET", process.env.NIFTY50_STOCK_DATA, {}, (res) => {
      if (res.status) {
        this.setState({
          nifty50Data: res["data"],
        });
      }
    });
  }
  render() {
    const { headerMsg, nifty50Data } = this.state;
    const { isLoggedIn } = this.props;
    return (
      <React.Fragment>
        {nifty50Data && nifty50Data.length > 0 ? (
          <StockUpdates nifty50Data={nifty50Data} />
        ) : (
          ""
        )}

        {Object.keys(headerMsg).length > 0 ? (
          <div
            className="alert nifty-announcement-bar show"
            style={{
              color: `${headerMsg.content_color}`,
              backgroundImage: `-webkit-linear-gradient( 0deg, ${headerMsg.background_color1} 0%, ${headerMsg.background_color2} 100%)`,
            }}
            role="alert"
          >
            {headerMsg.content}
            <a
              href={headerMsg.url}
              target="_blank"
              title="Open FREE Demat A/C"
              style={{
                color: `${headerMsg.button_text_color}`,
                background: `${headerMsg.button_color}`,
              }}
            >
              {headerMsg.urlcontent}
            </a>
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">
                <FontAwesomeIcon icon={faTimes} width="9" height="14" />
              </span>
            </button>
          </div>
        ) : (
          ""
        )}
        <div className="nifty_inner_header">
          <section className="h_header-top">
            <div className="container p-0">
              <div className="row">
                <div className="wsmobileheader clearfix">
                  <a id="wsnavtoggle" className="wsanimated-arrow">
                    {/* <!-- <span></span> --> */}
                    <FontAwesomeIcon icon={faBars} width="23" height="26" />
                  </a>
                  <Link href="/">
                    <a title="NiftyTrader" className="h_responsive_logo">
                      <img src="/images/logo.svg" alt="" />
                    </a>
                  </Link>
                </div>
                {/* <!-- Mobile Header --> */}
                {/* <!-- Start menu here --> */}
                <div className="headerfull">
                  <div className="wsmain clearfix">
                    <nav className="wsmenu clearfix">
                      <ul className="wsmenu-list" id="desktop-menu">
                        <li className="nifty-logo">
                          <Link href="/">
                            <a title="NiftyTrader">
                              <img
                                src="/images/logo.png"
                                alt=""
                                className="home-logo"
                                width="155"
                                height="36"
                              />
                              <img
                                src="/images/logo.svg"
                                alt=""
                                className="inner-page-logo"
                              />
                            </a>
                          </Link>
                        </li>
                        <li className="user-profile-nifty">
                          <img
                            src="/images/grid.png"
                            alt=""
                            className="home-grid"
                            height="18"
                            width="18"
                          />
                          <img
                            src="/images/grid-blue.png"
                            alt=""
                            className="inner-grid"
                            width="18"
                            height="18"
                          />
                          <ul className="grid_dropdown">
                            <li>
                              <Link href="/live-market-screener">
                                <div>
                                  <a title="Stock Screener">
                                    <div className="grid_dropdown_icon_left">
                                      <img
                                        src="/images/stockscreener-top.png"
                                        alt=""
                                        width="26"
                                        height="23"
                                      />
                                    </div>
                                    <div className="right_dropwon_grid">
                                      <span> Live Market Screener </span>
                                      {/* <p>Lorem ipsum dolor sit amet,</p> */}
                                    </div>
                                    <div className="clearfix"></div>
                                  </a>
                                </div>
                              </Link>
                            </li>
                            <li>
                              <a
                                href={
                                  process.env.OLD_SITE_URL + "stocks-analysis"
                                }
                                title="Stock Analysis"
                              >
                                <div className="grid_dropdown_icon_left grid_dropdown_icon_green">
                                  <img
                                    src="/images/StockdAnalysis.png"
                                    alt=""
                                    width="25"
                                    height="22"
                                  />
                                </div>
                                <div className="right_dropwon_grid">
                                  <span> Stock Analysis </span>
                                  {/* <p>Lorem ipsum dolor sit amet,</p> */}
                                </div>
                                <div className="clearfix"></div>
                              </a>
                            </li>
                            <li>
                              <a
                                onClick={() =>
                                  this.checkPermium("/live-analytics")
                                }
                                title="Live Analytics"
                              >
                                <div className="grid_dropdown_icon_left grid_dropdown_icon_blue">
                                  <img
                                    src="/images/LiveAnalyticss.png"
                                    alt=""
                                    width="25"
                                    height="21"
                                  />
                                </div>
                                <div className="right_dropwon_grid">
                                  <span> Nifty Live Analytics </span>
                                  {/* <p>Lorem ipsum dolor sit amet,</p> */}
                                </div>
                                <div className="clearfix"></div>
                              </a>
                            </li>
                          </ul>
                        </li>
                        <hr className="h_destop_hidden" />
                        <li aria-haspopup="true">
                          <a href="#" className="navtext">
                            {" "}
                            Options
                          </a>
                          <div className="wsmegamenu clearfix">
                            <div className="container-fluid">
                              <div className="row">
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy02 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Nifty OI
                                    </li>
                                    <li>
                                      <Link href="/live-nifty-open-interest">
                                        <a title="Nifty Open Interest Tracker">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Nifty Open Interest Tracker{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/nifty-live-change-in-oi">
                                        <a title="Change in Open Interest">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Change in Open Interest{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/nifty-put-call-ratio">
                                        <a title="Nifty Option Chain Put Call Ratio">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Nifty Option Chain Put Call Ratio{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/nifty-put-call-ratio-volume">
                                        <a title="Nifty Option Chain Put Call Ratio Volume">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Nifty Option Chain Put Call Ratio
                                          Volume{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    {/* <li>
                                      <Link href="/particepent-wise-oi">
                                        <a title="Particepentwise oi Table-view">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                        Particepentwise oi Table-view{" "}
                                        </a>
                                      </Link>
                                    </li> */}
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy02 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />
                                      Option Strategy
                                    </li>
                                    <li>
                                      <Link href="/stock-options-chart/nifty">
                                        <a title="Stock Options Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Stock Options Chart{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/options-trading">
                                        <a title="Stock Option Chain">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Stock Option Chain{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/options-max-pain-chart-live/nifty">
                                        <a title="Live Max Pain Chart For Derivative Stocks">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Live Max Pain Chart For Derivative
                                          Stocks{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/option-strategies">
                                        <a title="Option Strategies to Mint Money">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Option Strategies to Mint Money{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "option-strategy"
                                        }
                                        title="Option Strategy Optimizer"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Option Strategy Optimizer{" "}
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy02 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      BankNifty OI
                                    </li>
                                    <li>
                                      <Link href="/banknifty-live-oi-tracker">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Bank Nifty Open Interest Live Chart{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/bank-nifty-live-oi-change">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Bank Nifty Change In Open Interest{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/banknifty-intra-pcr-trend">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Bank Nifty PCR Live Chart{" "}
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/banknifty-intra-volume-pcr-trend">
                                        <a title="Bank Nifty Open Interest Live Chart Volume">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Bank Nifty Volume PCR Live Chart{" "}
                                        </a>
                                      </Link>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy02 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      USD INR OI
                                    </li>
                                    <li>
                                      <Link href="/usd-inr-options-live">
                                        <a title="USD INR Options Live">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          USD INR Options Live{" "}
                                        </a>
                                      </Link>
                                      <span className="wstmenutag bluetag">
                                        New
                                      </span>
                                    </li>
                                    <li>
                                      <Link href="/usd-inr-change-open-interest-live">
                                        <a title="USD INR Change in Open Interest Live">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          USD INR Change in Open Interest Live{" "}
                                        </a>
                                      </Link>
                                      <span className="wstmenutag bluetag">
                                        New
                                      </span>
                                    </li>
                                    <li>
                                      <Link href="/usdinr-put-call-ratio">
                                        <a title="USD INR Options Put Call Ratio">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          USD INR Options Put Call Ratio{" "}
                                        </a>
                                      </Link>
                                      <span className="wstmenutag bluetag">
                                        New
                                      </span>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy02 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Fin Nifty OI
                                    </li>
                                    <li>
                                      <Link href="/live-finnifty-open-interest">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Fin Nifty Open Interest Tracker
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/finnifty-live-change-in-oi">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Fin Nifty Change in Open Interest
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/finnifty-put-call-ratio">
                                        <a title="Bank Nifty Open Interest Live Chart">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Fin Nifty Option Chain Put Call Ratio
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <Link href="/finnifty-put-call-ratio-volume">
                                        <a title="Bank Nifty Open Interest Live Chart Volume">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Fin Nifty Option Chain Put Call Ratio
                                          Volume
                                        </a>
                                      </Link>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li aria-haspopup="true" className="active-menu">
                          <Link href="/sgx-nifty">
                            <a className="navtext" title="SGX Nifty">
                              {" "}
                              SGX Nifty
                            </a>
                          </Link>
                        </li>
                        <li aria-haspopup="true">
                          <a href="#" className="navtext">
                            Analytics
                          </a>
                          <div className="wsmegamenu clearfix halfmenu three-menu">
                            <div className="container-fluid">
                              <div className="row">
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Screeners
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "advanced-stock-screener"
                                        }
                                        title="Advanced Stock Screener"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Advanced Stock Screener{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <Link href="/live-market-screener">
                                        <div>
                                          <a title="Live Market Screener">
                                            <FontAwesomeIcon
                                              icon={faAngleRight}
                                              width="6"
                                              height="12"
                                            />{" "}
                                            Live Market Screener
                                          </a>
                                          <span className="wstmenutag redtag">
                                            Premium
                                          </span>
                                        </div>
                                      </Link>
                                    </li>
                                    <li>
                                      {/* <Link href="/options-screener"> */}
                                      <Link href="/options-screener">
                                        <div>
                                          <a title="Options Screener">
                                            <FontAwesomeIcon
                                              icon={faAngleRight}
                                              width="6"
                                              height="12"
                                            />{" "}
                                            Options Screener
                                          </a>
                                          {/* </Link> */}
                                          <span className="wstmenutag redtag">
                                            Premium
                                          </span>
                                        </div>
                                      </Link>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Live Analytics
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "stocks-analysis"
                                        }
                                        title="Stocks Analysis"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Stocks{" "}
                                      </a>
                                    </li>
                                    <li>
                                      {/* <Link href='/live-analytics'> */}
                                      <div>
                                        <a
                                          onClick={() =>
                                            this.checkPermium("/live-analytics")
                                          }
                                          title="Nifty"
                                        >
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Nifty{" "}
                                        </a>
                                        <span className="wstmenutag redtag">
                                          Premium
                                        </span>
                                      </div>
                                      {/* </Link> */}
                                    </li>
                                    <li>
                                      {/* <Link href='/banknifty-live-analysis'> */}
                                      <div>
                                        <a
                                          onClick={() =>
                                            this.checkPermium(
                                              "/banknifty-live-analysis"
                                            )
                                          }
                                          title="Bank Nifty"
                                        >
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Bank Nifty
                                        </a>
                                        <span className="wstmenutag redtag">
                                          Premium
                                        </span>
                                      </div>
                                      {/* </Link> */}
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Stock Stats
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "nse-stocks-volume"
                                        }
                                        title="Volume Shockers"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Volume Shockers{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "nse-stocks-price"
                                        }
                                        title="Intraday Breakouts"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Intraday Breakouts
                                      </a>
                                    </li>
                                    <li>
                                      <Link href="/gap-ups-gap-downs">
                                        <a title="Gap Up, Gap Down Stocks">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Gap Up, Gap Down Stocks
                                        </a>
                                      </Link>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "opening-price-clues"
                                        }
                                        title="Opening Price Clues"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Opening Price Clues{" "}
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Daily Stats
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "bulk-deals-data"
                                        }
                                        title="NSE Bulk Deals Data"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        NSE Bulk Deals Data
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL + "ban-list"
                                        }
                                        title="Ban List"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Ban List
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li aria-haspopup="true">
                          <a href="#" className="navtext">
                            Resources
                          </a>
                          <div className="wsmegamenu clearfix halfmenu three-menu">
                            <div className="container-fluid">
                              <div className="row">
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      References
                                    </li>
                                    {/* <!-- <li>
                                  <a href={process.env.OLD_SITE_URL + "free-course" target="_blank" title="Free Course">
                                    <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Free Course </a>
                                </li> --> */}
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL + "terms"
                                        }
                                        title="Trading and Investment Terminology"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Trading and Investment Terminology{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "investing-books"
                                        }
                                        title="Investing Books"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Investing Books
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "candlestick-patterns"
                                        }
                                        title="Candlestick Patterns"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Candlestick Patterns
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Brokers
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "broker-directory"
                                        }
                                        title="Broker Directory"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Broker Directory{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "select-best-broker"
                                        }
                                        title="Compare Brokers"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Compare Brokers
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "stock-brokers-in-india"
                                        }
                                        title="Stock Brokers in India"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Stock Brokers in India
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Stats
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL + "fii-stats"
                                        }
                                        title="FII Activity in F&O & cash"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        FII Activity in F&O & cash{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "fii-dii-activity"
                                        }
                                        title="FII DII Activity"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        FII DII Activity
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "nse-fo-lot-size"
                                        }
                                        title="NSE F&O Lot Size | NiftyTrader"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        NSE F&O Lot Size | NiftyTrader
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "bse-nse-trading-holidays"
                                        }
                                        title="BSE NSE Holidays List"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        BSE NSE Holidays List
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "commodity-trading-holidays"
                                        }
                                        title="Commodity Trading Holidays"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Commodity Trading Holidays
                                      </a>
                                    </li>
                                    <li>
                                      <Link href="/participant-wise-oi">
                                        <a title="Commodity Trading Holidays">
                                          <FontAwesomeIcon
                                            icon={faAngleRight}
                                            width="6"
                                            height="12"
                                          />{" "}
                                          Participant Wise OI
                                        </a>
                                      </Link>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-lg-3 col-md-12">
                                  <ul className="wstliststy06 clearfix">
                                    <li className="wstheading clearfix">
                                      <FontAwesomeIcon
                                        icon={faStream}
                                        width="12"
                                        height="12"
                                      />{" "}
                                      Calculators
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "option-pricing-calculator"
                                        }
                                        title="Option Pricing Calculator"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Option Pricing Calculator{" "}
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "fibonacci-calculator-2"
                                        }
                                        title="Fibonacci Calculator"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Fibonacci Calculator
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "pivot-calculator"
                                        }
                                        title="Pivot Calculator"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Pivot Calculator
                                      </a>
                                    </li>
                                    <li>
                                      <a
                                        href={
                                          process.env.OLD_SITE_URL +
                                          "developing-pivots"
                                        }
                                        title="Developing Pivot Calculator"
                                      >
                                        <FontAwesomeIcon
                                          icon={faAngleRight}
                                          width="6"
                                          height="12"
                                        />{" "}
                                        Developing Pivot Calculator
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li aria-haspopup="true" className="active-menu">
                          <a href={"/news"} className="navtext" title="News">
                            {" "}
                            News
                          </a>
                        </li>
                        {/* <!-- <li>
                      <a href="#" className="navtext">Premium</a>
                      <ul className="sub-menu">
                        <li className="">
                          <a href="#" title="Past Gap Up Gap Down Data" className="">
                            <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Past Gap Up Gap Down Data </a>
                        </li>
                        <li className="">
                          <a href="#" title="Advance Stock Screener" className="">
                            <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Advance Stock Screener </a>
                        </li>
                        <li className="">
                          <a href="#" title="Live Analytics" className="">
                            <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Live Analytics </a>
                        </li>
                        <li className="">
                          <a href="#" title="Bank Nifty Live Analysis" className="">
                            <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Bank Nifty Live Analysis </a>
                        </li>
                        <li className="">
                          <a href="  " title="Historical Bulk Deals Data" className="">
                            <FontAwesomeIcon icon={faAngleRight} width="6" height="12" /> Historical Bulk Deals Data </a>
                        </li>
                      </ul>
                    </li> --> */}
                        <hr className="h_destop_hidden" />
                        <li aria-haspopup="true">
                          <a
                            href={process.env.OLD_SITE_URL + "money/ipo/"}
                            className="navtext"
                            title="Money"
                          >
                            {" "}
                            M
                            <img
                              src="/images/money-icon.png"
                              className="innerlogo-money"
                              width="16"
                              height="22"
                            />
                            ney
                          </a>
                        </li>
                        {isLoggedIn ? (
                          <li aria-haspopup="true" className="active-menu">
                            <Link href="/watchlist">
                              <a className="navtext" title="Watchlist">
                                {" "}
                                Watchlist
                              </a>
                            </Link>
                          </li>
                        ) : (
                          <li
                            aria-haspopup="true"
                            className="active-menu"
                            onClick={() => $("#loginpopup").modal("show")}
                          >
                            <a className="navtext" title="Watchlist">
                              {" "}
                              Watchlist
                            </a>
                          </li>
                        )}

                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Stock Screener"
                          >
                            <i className="fas fa-chart-area"></i> Stock Screener
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Stock Analysis"
                          >
                            <i className="fal fa-chart-area hideIcon"></i> Stock
                            Analysis
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Live Analytics"
                          >
                            <i className="fas fa-analytics hideIcon"></i> Live
                            Analytics
                          </a>
                        </li>
                        <hr className="h_destop_hidden" />
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a href="#" className="navtext" title=" About Us">
                            {" "}
                            About Us
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Terms And Conditions"
                          >
                            Terms And Conditions
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Privacy Policy"
                          >
                            Privacy Policy
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a href="#" className="navtext" title="Disclaimers">
                            Disclaimers
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Limitation Of Liability"
                          >
                            Limitation Of Liability
                          </a>
                        </li>
                        <li aria-haspopup="true" className="h_destop_hidden">
                          <a
                            href="#"
                            className="navtext"
                            title="Refund and Cancellation Policy"
                          >
                            Refund and Cancellation Policy
                          </a>
                        </li>
                      </ul>{" "}
                      {/*<?php include('mobile-menu.php') ?> */}
                      <MobileMenu />
                    </nav>

                    {/* <Login setUserLoagged={this.setUserLoagged} /> */}
                    <Login />
                  </div>
                </div>
                {/* <!-- Search --> */}
                <div id="search-overlay" className="block-search-bar">
                  <div className="search-centered">
                    <div id="search-box">
                      <i id="close-btn" className="fa fa-times"></i>
                      <form a id="search-form">
                        <div className="dropDownMen">
                          {/* <!-- <label htmlFor="" className="select-cmpy mr-3">Select Stock </label> --> */}
                          <input
                            type="text"
                            className="typeahead stocks_query"
                            autoComplete="off"
                            spellCheck="true"
                            placeholder="Search Stocks"
                          />
                          <i className="fa fa-search" aria-hidden="true"></i>
                        </div>
                        {/* <!-- <div className="header-recently-search text-left">
                             <h6>
                               Recently search :
                             </h6>
                             <ul className="list-inline">
                             
                               <li>
                                 <a href="#" title="ACC">ACC</a>,
                               </li>
                               <li>
                                 <a href="#" title="3MINDIA">3MINDIA</a>,
                               </li>
                               <li>
                                 <a href="#" title="ABB">ABB</a>,
                               </li>
                             
                               <li>
                                 <a href="#" title="ABBOTINDIA">ABBOTINDIA</a>,
                               </li>
                               <li>
                                 <a href="#" title="ABBOTINDIA">ABBOTINDIA</a>,
                               </li>
                             </ul>
                             </div> --> */}
                        {/* <!-- <input id='search-text' placeholder='Search' type='text' /> --> */}
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/* <!-- Modal --> */}
          <div
            className="modal filter_modal filter_content_modal feedback-modal show"
            id="exampleModalCenter"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                {/* <!-- Modal Header --> */}
                <div className="modal-header">
                  <h4 className="modal-title">Feedback</h4>
                  <button type="button" className="close" data-dismiss="modal">
                    ×
                  </button>
                </div>
                <div className="modal-body">
                  {/* <!-- row start --> */}
                  <div className="row mr-0 ml-0">
                    <div className="col-xl-12">
                      <form className="feedback-form" id="feedback-form">
                        <div className="rating-box">
                          <div className="rating-heading">
                            {" "}
                            Enjoying Your <strong>NiftyTrader</strong>{" "}
                            experience.{" "}
                          </div>
                          <div className="star-rating">
                            <input
                              id="star-5"
                              type="radio"
                              name="rating"
                              value="star-5"
                            />
                            <label htmlFor="star-5" title="5 stars">
                              <i
                                className="active fa fa-star"
                                aria-hidden="true"
                              ></i>
                            </label>
                            <input
                              id="star-4"
                              type="radio"
                              name="rating"
                              value="star-4"
                              checked
                            />
                            <label htmlFor="star-4" title="4 stars">
                              <i
                                className="active fa fa-star"
                                aria-hidden="true"
                              ></i>
                            </label>
                            <input
                              id="star-3"
                              type="radio"
                              name="rating"
                              value="star-3"
                            />
                            <label htmlFor="star-3" title="3 stars">
                              <i
                                className="active fa fa-star"
                                aria-hidden="true"
                              ></i>
                            </label>
                            <input
                              id="star-2"
                              type="radio"
                              name="rating"
                              value="star-2"
                            />
                            <label htmlFor="star-2" title="2 stars">
                              <i
                                className="active fa fa-star"
                                aria-hidden="true"
                              ></i>
                            </label>
                            <input
                              id="star-1"
                              type="radio"
                              name="rating"
                              value="star-1"
                            />
                            <label htmlFor="star-1" title="1 star">
                              <i
                                className="active fa fa-star"
                                aria-hidden="true"
                              ></i>
                            </label>
                          </div>
                        </div>
                        <div className="custom-control custom-radio">
                          <input
                            type="radio"
                            name="customRadio"
                            className="custom-control-input"
                            id="customRadio1"
                          />
                          <label
                            className="custom-control-label mb-0"
                            htmlFor="customRadio1"
                          >
                            Very easy to use and navigate{" "}
                          </label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input
                            type="radio"
                            name="customRadio"
                            className="custom-control-input"
                            id="customRadio2"
                          />
                          <label
                            className="custom-control-label mb-0"
                            htmlFor="customRadio2"
                          >
                            Quite comprehensive and feature rich
                          </label>
                        </div>
                        <div className="custom-control custom-radio mb-2">
                          <input
                            type="radio"
                            name="customRadio"
                            className="custom-control-input"
                            id="customRadio3"
                          />
                          <label
                            className="custom-control-label mb-0"
                            htmlFor="customRadio3"
                          >
                            Nice look and feel
                          </label>
                        </div>
                        <div className="form-group">
                          <textarea
                            className="form-control"
                            id="feedbackForm"
                            name="feedback"
                            rows="3"
                            placeholder="Help us improve NiftyTrader"
                          ></textarea>
                        </div>
                        <button
                          type="submit"
                          className="btn feedback-submit-btn"
                        >
                          {" "}
                          Submit{" "}
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- go to top button close--> */}
          {/* <div className="feedback-box">
          <button type="button" className="btn feedback-btn" title="Feedback" data-toggle="modal"
            data-target="#exampleModalCenter">
            <FontAwesomeIcon icon={faCommentAlt} width="18" height="18" />
          </button>
        </div> */}
          <div className="socialShring">
            <ul style={{ display: "none" }}>
              <li>
                <a
                  href="https://in.linkedin.com/in/niftytrader"
                  className="header_linkedin"
                  target="_blank"
                  title="linkedin"
                >
                  <i className="fab fa-linkedin-in"></i>
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/channel/UC6JvXGzemVCW9FMUjkLBLrA"
                  className="header_youtube"
                  target="_blank"
                  title="Youtube"
                >
                  <i className="fab fa-youtube"></i>
                </a>
              </li>
              <li>
                <a
                  href="https://mobile.twitter.com/niftytraderin"
                  target="_blank"
                  className="header_twitter"
                  title="Twitter"
                >
                  <i className="fab fa-twitter"></i>
                </a>
              </li>
              <li>
                <a
                  href="https://www.facebook.com/niftytraderin"
                  target="_blank"
                  className="header_facebook"
                  title="Twitter"
                >
                  <i className="fab fa-facebook-f"></i>
                </a>
              </li>
            </ul>
          </div>
          {/* <button className="socialSharingBtn" title="Share">
          <FontAwesomeIcon icon={faShareAlt} width="18" height="18" />
        </button> */}
          {/* <!-- float-action-button close -->
        <!-- mobile android icon start --> */}
          {/* <a
          href="https://play.google.com/store/apps/details?id=in.niftytrader&hl=en_IN"
          target="_blank"
          title="Download Android App"
          className="mobile-android"
        >
          <img src="/images/android.svg" alt="Android" width="20" />
        </a> */}
          {/* <!-- Left Bottom Modal -->
    <!-- <div className="bottom-modal desktopModal " id="bottom-left-modal">
        <button type="button" className="closePopupBottom" title="Close">×</button>
             <h5>
                 Zerodha Broker (Free Delivery)
             </h5>
             <p>
                 India's No. 1 Broker with Best Software
                 Trade @ Flat Rs 20
             </p>
             <a href="#" className="modalClose" title="Open Instant Account">Open Instant Account</a>
        </div> -->
    <!-- Android modal -->
    <!-- <div className="bottom-modal androidPopup">
        <h5>
          <img src="/images/responsive_logo.svg" alt=""> Switch to NiftyTrader app for a richer and swifter experience
        </h5>
        <div className="text-center">
        <a href="#" className="modalClose" title="Continue here">Continue here</a>
        <a href="#" className="modalClose" title="Open in App">Open in App</a>
        </div>
        </div>  --> */}
          {/* <div
            className="modal login-modal"
            id="loginpopup"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="loginpopup"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <button
                    type="button"
                    className="close close-modal-btn"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                  <div className="row ml-0 mr-0">
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                      <form id="loginform" className="pl-3 pr-3 pb-4 text-center">
                        <h5 className="modal-title mt-3">LOGIN</h5>
                        <p className="mb-3 text-left p-0">
                          Welcome! Log into your account
                      </p>
                        <div className="form-group save-searchbar">
                          <input
                            className="form-control"
                            type="text"
                            placeholder="Enter your email id"
                            name="Email"
                            autoFocus="autofocus"
                          />
                        </div>
                        <div className="form-group save-searchbar">
                          <input
                            className="form-control"
                            type="password"
                            placeholder="Enter your password"
                            name="password"
                            autoFocus="autofocus"
                          />
                        </div>
                        <div className="form-group">
                          <button
                            type="submit"
                            className="btn modal-save-btn modal-login-btn w-100"
                            title="Login"
                          >
                            {" "}
                          Login{" "}
                          </button>
                        </div>
                        <p className="mt-3 mb-0 text-capitalize">
                          <a href="#" id="forgotpassword1">
                            {" "}
                          Forgot your password? Get help
                        </a>
                        </p>
                        <p className="text-capitalize">
                          <a href="#" id="Reg">
                            {" "}
                          Register Now
                        </a>
                        </p>
                        <h6 className="pt-0">Login with Social Network</h6>
                        <a
                          className="social-btn"
                          href="Login?SocialLogin=Google&amp;returnUrl="
                          title="Google"
                          target="_top"
                        >
                          <i className="fab fa-google"></i> Google
                      </a>
                        <a
                          className="social-btn social-f-btn"
                          href="Login?SocialLogin=Facebook&amp;returnUrl="
                          title="Facebook"
                          target="_top"
                        >
                          <i className="fab fa-facebook"></i> Facebook
                      </a>
                      </form>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-6 login-img col-sm-6 d-none d-sm-block">
                      <img src="/images/modal-login.png" alt="" className="img-fluid" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </React.Fragment>
    );
  }
}

// export default Header;
export default connect(mapStateToProps, mapDispatchToProps)(Header);
