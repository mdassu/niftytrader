import React, { Component } from 'react';
import HomeOtherSiteLinks from './HomeOtherSiteLinks';
import HomeOwlCarousel from './HomeOwlCarousel';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        {/* <!-- Start Main Searchbar --> */}
        <section className="home-conatiner">
          <section className="main-searchbar">
            <div className="container">
              {/* <!-- Start  Row --> */}
              <div className="row">
                <div className="col-12  text-center">
                  <p className="acess-now-text">FREE ACCESS NOW</p>
                  <h1>Most Comprehensive Stock Analysis</h1>
                  <div className="stocks-searcbar">
                    <form>
                      <div className="input-seachbar" id="the-basics">
                        <input type="text" placeholder="Search" className="typeahead" />
                        <img src="/images/search-icon.png" alt="" />
                      </div>
                      {/* <!-- <div id="the-basics">
                        <input className="typeahead" type="text" placeholder="States of USA">
              </div> --> */}
                    </form>
                  </div>
                  <div className="popularstocklabel">
                    <a href="#" title="SJVN">SJVN</a>
                    <a href="#" title="PEL">PEL</a>
                    <a href="#" title="ULTRACEMCO">ULTRACEMCO</a>
                    <a href="#" title="BIOCON">BIOCON</a>
                    <a href="#" title="AAC">AAC</a>
                    <a href="#" title="NESTLEIND">NESTLEIND</a>
                    <a href="#" title="CANFINHOME">CANFINHOME</a>
                    <a href="#" title="SHREECEM">SHREECEM</a>
                    <a href="#" title="IDFCFIRSTB">IDFCFIRSTB</a>
                    <a href="#" title="IDBI">IDBI</a>
                    <a href="#" title="EIHOTEL">EIHOTEL</a>
                    <a href="#" className="view-more" title="View More">View More</a>
                  </div>
                </div>
              </div>
              {/* <!-- End  Row --> */}
              {/* <?php  include('owl-carousel.php') ?>
    <?php  include('logos.php') ?> */}
              <HomeOwlCarousel />
              <HomeOtherSiteLinks />
            </div>
          </section>
        </section>
        {/* <!-- End Main Searchbar -->
<!-- Start News --> */}
        <section className="home-conatiner">
          <section className="news">
            <div className="container">
              {/* <!-- Start Row --> */}
              <div className="row">
                <div className="col-md-7 col-lg-8 col-xl-8">
                  <div className="news-m-width">
                    {/* <!-- Row Start --> */}
                    <div className="row">
                      <div className="col-md-6 col-lg-6 col-xl-6 col-6">
                        <h2>Latest News</h2>
                      </div>
                      <div className="col-md-6 col-lg-6 col-xl-6 col-6">
                        <div className="news-btns text-right">
                          <div className="btn-group btn-group-toggle" data-toggle="buttons">
                            <label className="btn active" title="English">
                              <input type="radio" name="options" autoComplete="off" defaultChecked />
                                English
                  </label>
                            <label className="btn" title="हिंदी समाचार">
                              <input type="radio" name="options" autoComplete="off" /> हिंदी समाचार
                  </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* <!-- Row End --> */}
                    <div className="owl-carousel owl-theme news-slider" id="news_slider">
                      <div className="item">
                        {/* <!-- Row Start --> */}
                        <div className="row news-cards">
                          <div className="col-md-12 col-lg-8 col-xl-8 col-12">
                            <a href="#"
                              title="Allcargo Logistics hits 20% upper circuit as promoters propose to delist company">
                              <div className="card">
                                <div className="card-horizontal">
                                  <div className="img-square-wrapper">
                                    <img className="owl-lazy" data-src="/images/news-img.jpg" alt="Card image cap" />
                                  </div>
                                  <div className="card-body">
                                    <h3 className="card-title">
                                      Allcargo Logistics hits 20% upper circuit as
                                      promoters
                                      propose to delist
                                      company
                          </h3>
                                    <p className="card-text">
                                      The Economic Times
                            <small>2 hours ago</small>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title=" DATA STORY: Nearly 70k coronavirus cases added in biggest
                                        single-day">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    DATA STORY: Nearly 70k coronavirus cases added in
                                    biggest
                                    single-day
                        </h3>
                                  <p className="card-text">
                                    Business Standard
                          <small>2 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="Indian Overseas Bank Q1 results: Reports net profit
                                        of Rs 121 crore">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    Indian Overseas Bank Q1 results: Reports net profit
                                    of Rs 121 crore
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>10 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="Govt plans stake sale in IRCTC, invites bids for
                                        managing sale">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    Govt plans stake sale in IRCTC, invites bids for
                                    managing sale
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>1 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="UAE, Israel agree to establish diplomatic ties in deal
                                        brokered">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    UAE, Israel agree to establish diplomatic ties in deal
                                    brokered
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>1 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                        </div>
                        {/* <!-- Row End --> */}
                      </div>
                      <div className="item">
                        {/* <!-- Row Start --> */}
                        <div className="row news-cards">
                          <div className="col-md-12 col-lg-8 col-xl-8 col-12">
                            <a href="#"
                              title="Allcargo Logistics hits 20% upper circuit as promoters propose to delist company">
                              <div className="card">
                                <div className="card-horizontal">
                                  <div className="img-square-wrapper">
                                    <img className="owl-lazy" data-src="/images/news-img.jpg" alt="Card image cap" />
                                  </div>
                                  <div className="card-body">
                                    <h3 className="card-title">
                                      Allcargo Logistics hits 20% upper circuit as
                                      promoters
                                      propose to delist
                                      company
                          </h3>
                                    <p className="card-text">
                                      The Economic Times
                            <small>2 hours ago</small>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title=" DATA STORY: Nearly 70k coronavirus cases added in biggest
                                            single-day">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    DATA STORY: Nearly 70k coronavirus cases added in
                                    biggest
                                    single-day
                        </h3>
                                  <p className="card-text">
                                    Business Standard
                          <small>2 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="Indian Overseas Bank Q1 results: Reports net profit
                                            of Rs 121 crore">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    Indian Overseas Bank Q1 results: Reports net profit
                                    of Rs 121 crore
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>10 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="Govt plans stake sale in IRCTC, invites bids for
                                            managing sale">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    Govt plans stake sale in IRCTC, invites bids for
                                    managing sale
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>1 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                            <a href="#" title="UAE, Israel agree to establish diplomatic ties in deal
                                            brokered">
                              <div className="card">
                                <div className="card-body">
                                  <h3 className="card-title">
                                    UAE, Israel agree to establish diplomatic ties in deal
                                    brokered
                        </h3>
                                  <p className="card-text">
                                    The Economic Times
                          <small>1 hours ago</small>
                                  </p>
                                </div>
                              </div>
                            </a>
                          </div>
                        </div>
                        {/* <!-- Row End --> */}
                      </div>
                    </div>
                    {/* <!-- Row Start --> */}
                    <div className="row">
                      <div className="col-12">
                        <div className="news-view-more-btn">
                          <a href="#" title="View More">View More</a>
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Start --> */}

                  </div>
                </div>
                <div className="col-md-5 col-lg-4 col-xl-4 brokerage-m-width">
                  <div className="brokerage-card">
                    <h3>Lowest Brokerage <span className="slash">/</span> Best Brokers <span className="slash">/</span>
            Free
            Tips</h3>
                    <form id="brokerage_form" className="brokerage-card-form">
                      <div className="formcontrol">
                        <div className="form-group">
                          <input type=" text" className="form-control" placeholder="Name" name="name" />
                          <div className="form-icon usericon">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-person" fill="currentColor"
                              xmlns="http://www.w3.org/2000/svg">
                              <path fillRule="evenodd"
                                d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                            </svg>
                          </div>
                        </div>
                      </div>
                      <div className="formcontrol">
                        <div className="form-icon emailicon">
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-envelope" fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd"
                              d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
                          </svg>
                        </div>
                        <div className="form-group">
                          <input type="email" className="form-control" placeholder="Email" name="brokerage_email" />
                        </div>

                      </div>

                      <div className="form-group country-select">
                        <input id="phone" name="phone" type="number" className="form-control" />
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn submit-btn" title="Submit">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              {/* <!-- End Row --> */}
            </div>
          </section>
        </section>
        {/* <!-- End News -->
        <!-- Start Table Tabing  --> */}
        <section className="table-tabing">
          <section className="home-conatiner">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li className="nav-item">
                      <a className="nav-link active" id="pills-top-gainers-tab" data-toggle="pill" href="#pills-top-gainers"
                        role="tab" aria-controls="pills-top-gainers" aria-selected="true">Top Gainers</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="pills-top-losers-tab" data-toggle="pill" href="#pills-top-losers" role="tab"
                        aria-controls="pills-top-losers" aria-selected="false">Top Losers</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="pills-open-low-tab" data-toggle="pill" href="#pills-open-low" role="tab"
                        aria-controls="pills-open-low" aria-selected="false">Same Open & Low</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="pills-open-high-tab" data-toggle="pill" href="#pills-open-high" role="tab"
                        aria-controls="pills-open-high" aria-selected="false">Same Open &
              High</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="pills-gap-up-tab" data-toggle="pill" href="#pills-gap-up" role="tab"
                        aria-controls="pills-gap-up" aria-selected="false">Gap Up</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="pills-gap-down-tab" data-toggle="pill" href="#pills-gap-down" role="tab"
                        aria-controls="pills-gap-down" aria-selected="false">Gap Down</a>
                    </li>
                  </ul>
                  <hr className="tab-line" />
                  <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-top-gainers" role="tabpanel"
                      aria-labelledby="pills-top-gainers-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>



                            {/* <?< 9 ; $i++) { ?> */}

                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <div id="sparkline_chart<?php echo $i; ?>" className="sparkline-chart"></div>
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill="currentColor"
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>

                            {/* <? php  } ?> */}


                          </tbody>
                        </table>
                      </div>

                    </div>
                    <div className="tab-pane fade" id="pills-top-losers" role="tabpanel" aria-labelledby="pills-top-losers-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* <?< 5 ; $i++) { ?> */}

                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <img src="/images/chart.png" alt="" width="100" />
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill=""
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>
                            {/* <? php  } ?> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="pills-open-low" role="tabpanel" aria-labelledby="pills-open-low-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* <?< 5 ; $i++) { ?> */}
                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <img src="/images/chart.png" alt="" width="100" />
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill=""
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>
                            {/* <? php  } ?> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="pills-open-high" role="tabpanel" aria-labelledby="pills-open-high-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* <?< 5 ; $i++) { ?> */}
                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <img src="/images/chart.png" alt="" width="100" />
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill=""
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>
                            {/* <? php  } ?> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="pills-gap-up" role="tabpanel" aria-labelledby="pills-gap-up-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* <?< 5 ; $i++) { ?> */}
                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <img src="/images/chart.png" alt="" width="100" />
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill=""
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>
                            {/* <? php  } ?> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="pills-gap-down" role="tabpanel" aria-labelledby="pills-gap-down-tab">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Symbol</th>
                              <th>Current Price</th>
                              <th className="min-w-250">Today L/H</th>
                              <th className="min-w-130">Change (%)</th>
                              <th>Prev. Close </th>
                              <th>Volume</th>
                              <th className="min-w-130">Day Chart</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* <?< 5 ; $i++) { ?> */}
                            <tr>
                              <td className="p-0">
                                <a href="#" title="ECLERX" className="table-symobol-link">ECLERX</a>
                              </td>
                              <td>623.15</td>
                              <td>
                                <div className="low-value text-left">544.50</div>
                                <div className="range-box">
                                  <div className="highvalue-range" style={{ width: '70%' }}></div>
                                  <div className="lowvalue-range" style={{ width: '30%' }}></div>
                                </div>
                                <div className="high-value text-right">623.15</div>
                              </td>
                              <td>
                                <span className="percent-green">
                                  <img src="/images/green-up-arrow.png" alt="" className="table-icon" />
                        03.85 (20.00%)</span>
                              </td>
                              <td>519.30</td>
                              <td>674,880</td>
                              <td>
                                <img src="/images/chart.png" alt="" width="100" />
                              </td>
                              <td>
                                <div className="table-hover-icons">
                                  <a href="#" className="mr-1 icon-tooltip" data-toggle="tooltip"
                                    data-placement="bottom" title="Add">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus-circle"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                      <path fillRule="evenodd"
                                        d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                  </a>
                                  <a href="#" className="mr-1" data-toggle="tooltip" data-placement="bottom"
                                    title="Watchlist">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill=""
                                      xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                    </svg></a>
                                  <a href="#" data-toggle="tooltip" data-placement="bottom" title="Create Alert">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-phone-vibrate"
                                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fillRule="evenodd"
                                        d="M10 3H6a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM6 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H6z" />
                                      <path fillRule="evenodd"
                                        d="M8 12a1 1 0 1 0 0-2 1 1 0 0 0 0 2zM1.599 4.058a.5.5 0 0 1 .208.676A6.967 6.967 0 0 0 1 8c0 1.18.292 2.292.807 3.266a.5.5 0 0 1-.884.468A7.968 7.968 0 0 1 0 8c0-1.347.334-2.619.923-3.734a.5.5 0 0 1 .676-.208zm12.802 0a.5.5 0 0 1 .676.208A7.967 7.967 0 0 1 16 8a7.967 7.967 0 0 1-.923 3.734.5.5 0 0 1-.884-.468A6.967 6.967 0 0 0 15 8c0-1.18-.292-2.292-.807-3.266a.5.5 0 0 1 .208-.676zM3.057 5.534a.5.5 0 0 1 .284.648A4.986 4.986 0 0 0 3 8c0 .642.12 1.255.34 1.818a.5.5 0 1 1-.93.364A5.986 5.986 0 0 1 2 8c0-.769.145-1.505.41-2.182a.5.5 0 0 1 .647-.284zm9.886 0a.5.5 0 0 1 .648.284C13.855 6.495 14 7.231 14 8c0 .769-.145 1.505-.41 2.182a.5.5 0 0 1-.93-.364C12.88 9.255 13 8.642 13 8c0-.642-.12-1.255-.34-1.818a.5.5 0 0 1 .283-.648z" />
                                    </svg>
                                  </a>
                                </div>
                              </td>
                            </tr>

                            {/* <? php  } ?> */}

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div className="view-more-stocks-btn text-center">
                    <a href="#" title="View More Stocks">View More Stocks</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>
      </React.Fragment >

    );
  }
}

export default Home;



