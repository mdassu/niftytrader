import React, { Component } from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEnvelope
} from "@fortawesome/free-solid-svg-icons";
import {
  faFacebookF,
  faTwitter,
  faLinkedinIn,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import { toast } from "react-toastify";
import { CALL_API } from '_services/CALL_API';
import { ErrorMessages, isEmailValid } from '_helper/MyValidations';

export default class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      newsLatterEmail: '',
      isSend: true
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  handleEmailField = (event) => {
    this.setState({
      newsLatterEmail: event.target.value
    });
  }

  formValidation = () => {
    var msgEmail = '';
    var isValid = false;


    // last name validation
    if (this.state.newsLatterEmail) {
      if (!isEmailValid(this.state.newsLatterEmail)) {
        msgEmail = (ErrorMessages.VALIDATION_ERROR_EMAIL);
      }
    } else {
      msgEmail = 'Please enter email id'
    }

    if (!msgEmail) {
      isValid = true;
    }


    if (isValid) {
      return true;
    } else {
      this.toastConfig["type"] = "error";
      toast(msgEmail, this.toastConfig);
      return false
    }


  }

  submitNewsLatter = () => {
    if (this.formValidation()) {
      this.setState({
        isSend: false
      })
      CALL_API('GET', process.env.ADD_NEWSLATTER_DATA, { email: this.state.newsLatterEmail }, res => {
        if (res.status) {
          this.setState({
            isSend: true,
            newsLatterEmail: ''
          })
          this.toastConfig["type"] = "success";
          toast(res['message'], this.toastConfig);
        } else {
          this.setState({
            isSend: true,
            newsLatterEmail: ''
          })
          this.toastConfig["type"] = "error";
          toast(res['message'], this.toastConfig);
        }
      });
    }
  }

  render() {
    const { newsLatterEmail, isSend } = this.state;
    return (
      <React.Fragment>
        <section className="h_footer">
          <div className="container p-0">
            <div className="row">
              <div className="col-lg-12 h_footer_link text-center">
                <h6 className="pb-0">Trading and Investment Terminology</h6>
              </div>
              <div className="col-lg-12">
                <ul className="h_footer_alpha">
                  <li><a href={process.env.OLD_SITE_URL + "terms/0-9"}>0-9</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/a"}>A</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/b"}>B</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/c"}>C</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/d"}>D</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/e"}>E</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/f"}>F</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/g"}>G</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/h"}>H</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/i"}>I</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/j"}>J</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/k"}>K</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/l"}>L</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/m"}>M</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/n"}>N</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/o"}>O</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/p"}>P</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/q"}>Q</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/r"}>R</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/s"}>S</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/t"}>T</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/u"}>U</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/v"}>V</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/w"}>W</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/x"}>X</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/y"}>Y</a></li>
                  <li><a href={process.env.OLD_SITE_URL + "terms/z"}>Z</a></li>
                </ul>
              </div>
              <div className="col-lg-3 pl-0">
                <div className="footer-content">
                  <a href={process.env.OLD_SITE_URL} title="NiftyTrader" className="active"><img src="/images/footerlogo.png" className="" alt=""
                    width="172" height="39" /></a>
                  <p> The team at NiftyTrader.in is always endeavoring to improve education about technical analysis approach to
            decipher </p>
                  <ul>
                    <li>
                      <a href="https://www.facebook.com/niftytraderin/" title="facebook" target="_blank">
                        <FontAwesomeIcon
                          icon={faFacebookF}
                          width="10"
                          height="16"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="https://twitter.com/niftytraderin" title="twitter" target="_blank">
                        <FontAwesomeIcon
                          icon={faTwitter}
                          width="16"
                          height="16"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="https://in.linkedin.com/in/niftytrader" title="linkedin" target="_blank">
                        <FontAwesomeIcon
                          icon={faLinkedinIn}
                          width="16"
                          height="16"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/niftytraderin/" title="instagram" target="_blank">
                        <FontAwesomeIcon
                          icon={faInstagram}
                          width="16"
                          height="16"
                        />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-6 p-0 hidden_mobile">
                <div className="row">
                  <div className="col-lg-4">
                    <ul className="h_footer_link">
                      <h6>Top 5 Brokers</h6>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "brokerage/zerodha"} target="_blank" title="Zerodha"> Zerodha</a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "brokerage/angel-broking"} target="_blank" title="Angel Broking"> Angel
                  Broking </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "brokerage/icici-direct"} target="_blank" title="ICICI Direct"> ICICI
                  Direct </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "5paisa-2020-brokerage-charges-trading-platforms/"} target="_blank"
                          title="Aditya Birla"> 5paisa Birla </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "brokerage/upstox"} target="_blank" title="Upstox"> Upstox</a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-4 p-0 hidden_mobile">
                    <ul className="h_footer_link">
                      <h6>Other Links</h6>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "about-us"} title="About Us" className="footer-link"><i className="fas fa-chevron-right"></i> About
                  Us</a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "terms-and-conditions"} title="Terms And Conditions" className="footer-link"><i
                          className="fas fa-chevron-right"></i> Terms And Conditions </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "privacy-policy"} title="Privacy Policy" className="footer-link"><i
                          className="fas fa-chevron-right"></i> Privacy Policy </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "disclaimers"} title="Disclaimers" className="footer-link"><i className="fas fa-chevron-right"></i>
                  Disclaimers </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "limitation-of-liability"} title="Limitation Of Liability" className="footer-link"><i
                          className="fas fa-chevron-right"></i> Limitation Of Liability </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "refund-and-cancellation-policy"} title="Refund and Cancellation Policy"
                          className="footer-link"><i className="fas fa-chevron-right"></i>Refund and Cancellation Policy </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-4 hidden_mobile">
                    <ul className="h_footer_link">
                      <h6>Other Links</h6>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "broker-directory"} title="Broker Directory" className="footer-link"><i
                          className="fas fa-chevron-right"></i> Broker Directory </a>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "opening-price-clues"} title="Opening Price Clues" className="footer-link"><i
                          className="fas fa-chevron-right"></i> Opening Price Clues </a>
                      </li>
                      <li>
                        <Link href="/advance-stock-screener">
                          <a title="Stock Screener" className="footer-link"><i
                            className="fas fa-chevron-right"></i> Stock Screener </a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/stock-options-chart/nifty">
                          <a title="Stock Options Chart" className="footer-link"><i
                            className="fas fa-chevron-right"></i> Stock Options Chart </a>
                        </Link>
                      </li>
                      <li>
                        <a href={process.env.OLD_SITE_URL + "contact-us"} title="Contact Us" className="footer-link"><i className="fas fa-chevron-right"></i>
                  Contact Us </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 pr-0">
                <ul className="h_footer_link">
                  <h6>Subscribe to Our Newsletter</h6>
                  <li><span>Sign up and receive the latest tips via email.</span></li>
                  <form id="SubscribeForm">
                    <div className="form-group has-search">
                      <div className="form-control-feedback">
                        <FontAwesomeIcon
                          icon={faEnvelope}
                          width="16"
                          height="16"
                        />
                      </div>
                      <input style={{ color: 'white' }} type="email" name="newsLatterEmail" className="form-control" value={newsLatterEmail} onChange={this.handleEmailField} placeholder="Email" />
                      <button type="button" onClick={this.submitNewsLatter} title="Subscribe" disabled={!isSend}>{isSend ? 'Subscribe' : 'Subscribing...'}</button>
                    </div>
                  </form>
                </ul>
              </div>
            </div>
          </div>
          <section className="h_copyright_footer">
            <p> Copyright © <a href={process.env.OLD_SITE_URL} title="Niftytrader">Niftytrader</a>. All rights reserved </p>
          </section>
        </section>
      </React.Fragment >
    )
  }
}
