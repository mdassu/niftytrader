import Link from 'next/link';

export default function HomeOwlCarousel(props) {
  return (
    <div className="stack-card-width">
      <div className="owl-carousel owl-theme stock-card-slider" id="stock_card_slider">
        <div className="item">
          <div className="stock-card">
            <div className="stoct-card-list nifty-50-bg">
              <div className="row">
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <span className="nifty-heading">NIFTY 50</span>
                  <h6> 11300.45
                <span className="volume-small percent-red"> -7.95(-0.07%)</span>
                  </h6>
                </div>
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <ul className="nifty-50-boxes">
                    <li>
                      Low
                  <span className="volume-high"> 11269.95</span>
                    </li>
                    <li>
                      High
                  <span className="volume-low"> 11359.30</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="card-btns">
                <a href="#" title="Contributors">Contributors</a>
                <a href="#" title="Live Analytics">Live Analytics</a>
                <a href="#" title="Options">Options</a>
                <a href="#" title="PCR">PCR</a>
              </div>
            </div>
          </div>
        </div>
        <div className="item">
          <div className="stock-card">
            <div className="stoct-card-list sgx-nifty-bg">
              <div className="row">
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <span className="nifty-heading">SGX NIFTY</span>
                  <h6> 11300.45
                <span className="volume-small percent-green"> -7.95(-0.07%)</span>
                  </h6>
                </div>
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <ul className="nifty-50-boxes">
                    <li>
                      Low
                  <span className="volume-high"> 11269.95</span>
                    </li>
                    <li>
                      High
                  <span className="volume-low"> 11359.30</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="card-btns">
                <a href="#" title="Auto Refresh">Auto Refresh</a>
              </div>
            </div>
          </div>
        </div>
        <div className="item">
          <div className="stock-card">
            <div className="stoct-card-list nifty-bank-bg">
              <div className="row">
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <span className="nifty-heading">NIFTY BANK</span>
                  <h6> 11300.45
                <span className="volume-small percent-red"> -7.95(-0.07%)</span>
                  </h6>
                </div>
                <div className="col-lg-6 col-6 col-sm-6 col-md-6">
                  <ul className="nifty-50-boxes">
                    <li>
                      Low
                  <span className="volume-high"> 11269.95</span>
                    </li>
                    <li>
                      High
                  <span className="volume-low"> 11359.30</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="card-btns">
                <a href="#" title="Live Analytics">Live Analytics</a>
                <a href="#" title="Options">Options</a>
                <a href="#" title="PCR">PCR</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

