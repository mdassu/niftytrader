import Link from 'next/link';
import React from 'react';
export default function Header(props) {
  return (
    <div id="menu" className="scroll_menu">
      <section className="menu">
        <nav className="navbar navbar-expand-lg  bg-white py-0">
          <button type="button" id="sidebarCollapse" className="btn sidebar_btn" >
            <img src="/images/menu-icon.svg" alt="" />
          </button>
          <Link href="/">
            <a className="navbar-brand" title="NiftyTrader"><img src="/images/logo.svg" /></a>
          </Link>
          <form>
            <div className="stocks-searcbar header-searchbar p-0 " id="header_stocks_search">
              <input type="text" placeholder="Search..." className="form-control typeahead" />
              <img src="/images/header-search-icon.png" alt="" />
            </div>
          </form>
          <div className="navbar-list">
            <ul className="navbar-nav ml-auto">
              {/* <!-- Jab User Login Nhi Ho --> */}
              <li className="nav-item">
                <a className="nav-link" href="#" title="Login">Login</a>
              </li>
              <li className="nav-item">
                <a className="nav-link signup-link" href="#" title="Sign Up">Sign
										Up</a>
              </li>
              {/* <!-- Jab User Login Ho --> */}
              <li className="nav-item menu-with-icon">
                <a className="nav-link watchlist-link" href="#" title="Notification">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-bell" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z" />
                    <path fillRule="evenodd"
                      d="M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z" />
                  </svg>
                </a>
              </li>
              <li className="nav-item menu-with-icon">
                <a className="nav-link watchlist-link" href="#" title="Watchlist">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd"
                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                  </svg>
                </a>
              </li>
              {/* <!-- <li className="nav-item">
                    <div href="#usermenu" className="user-profile" title="Connor Morgan">
                      <img src="/images/user-profile.jpg" alt=""> Connor Morgan <i className="fal fa-angle-down"></i>
                        <ul className="user-dropdown">
                          <li><a href="" title="Profile"><i className="fal fa-user"></i> Profile</a></li>
                          <li><a href="" title="Logout"><i className="far fa-sign-out"></i> Logout</a>
                          </li>
                        </ul>
                  </div>
                </li> --> */}
            </ul>
          </div>
        </nav>
      </section>
      {/* <!-- End Menu --> */}
    </div>
  )
}
