import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from "next/link";
import { decrypt } from "_helper/EncrDecrypt";
import { CALL_API } from "_services/CALL_API";
import Loader from "./Loader";
import TraderToolkit from "./TraderToolkit";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import StockSearch from "./StockSearch";

export default class RightSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latestNews: [],
    };
  }

  componentDidMount() {
    this.getLatestNews();
  }

  getLatestNews() {
    CALL_API("GET", process.env.RSS_FEED_DATA, {}, (res) => {
      if (res.status) {
        var latestNews = [];
        res["data"]
          .sort((a, b) => {
            return new Date(b.publishDate) - new Date(a.publishDate);
          })
          .map((item, key) => {
            if (key <= 19) {
              latestNews.push({
                source: item.source,
                title: item.title,
                url: item.url,
              });
            }
          });
        this.setState({
          latestNews: latestNews,
        });
      }
    });
  }

  render() {
    const { latestNews } = this.state;
    return (
      <div className="col-md-4 col-lg-3 col-sm-12 col-xl-3 col-12 p-0 page-side-bar">
        <div className="sidebar-search searchbar-desktop">
          {/* <div className="form-group">
            <form action="">
              <div className="ui-widget">
                <input
                  placeholder="Search"
                  className="stocksearching autoComplete-ui ui-autoComplete-input"
                  autoComplete="off"
                />
                <span className="fa fa-search"></span>
              </div>

              <a
                id="HideStockNavigator"
                style={{ display: 'none' }}
                href="stocks-analysis/acc"
                className="d-none"
              ></a>
            </form>
          </div> */}
          {/* <ReactSearchAutocomplete
            items={allSymbolList}
            onSelect={this.handleSymbolSelect}
            placeholder="Search"
          /> */}
          <StockSearch placeHolder="Search Stocks" />
        </div>
        {/* <div className="homepage-newsbox">
          <h5 className="text-uppercase">
            Latest News
              <a href={process.env.OLD_SITE_URL + "news/hindi"} title="हिंदी समाचार" className="hindi-news-btn"
            >हिंदी समाचार</a
            >
          </h5>
          <ul className="homepage-news-ul news-ul-scroll">
            {
              latestNews && latestNews.length > 0 ? latestNews.map((item, key) => {
                return (
                  <li key={key}>
                    <a href={item['url']} title={item['title']}
                    >{item['title']}</a
                    ><br />
                    <span
                    ><a
                      className="customDataNewsUrl"
                      href={item['url']}
                    >{item['source']}</a
                      ></span
                    >
                  </li>
                )
              }) : <Loader />
            }
            <div className="text-center view-all-home">
              <a href={process.env.OLD_SITE_URL + "news/"} title="View All"> View All</a>
            </div>
          </ul>
        </div> */}

        <TraderToolkit />

        <div className="homepage-newsbox right-sidebar-panel">
          <h5 className="text-uppercase">Nse Stocks Stats</h5>
          <ul className="inner-right-sidebar">
            <li>
              <Link href="/stock-options-chart/nifty">
                <a title="Stock Options Chart">
                  <i className="fas fa-angle-right"></i>Stock Options Chart
                </a>
              </Link>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "nse-stocks-volume"}
                target="_blank"
                title="NSE Stocks – Intraday Volume Traded"
              >
                <i className="fas fa-angle-right"></i>NSE Stocks – Intraday
                Volume Traded
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "nse-stocks-price"}
                title="NSE Stocks – Intraday Breakouts"
              >
                <i className="fas fa-angle-right"></i>NSE Stocks – Intraday
                Breakouts
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "bulk-deals-data"}
                title="NSE Stocks Bulk Deals Data"
              >
                <i className="fas fa-angle-right"></i>NSE Stocks Bulk Deals Data
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "nse-fo-lot-size"}
                title="NSE F&amp;O Lot Size | NiftyTrader"
              >
                <i className="fas fa-angle-right"></i>NSE F&amp;O Lot Size |
                Nifty Trader
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "stocks-analysis"}
                title="Intraday Bytes: NSE Stocks and Indices"
              >
                <i className="fas fa-angle-right"></i>Intraday Bytes: NSE Stocks
                and Indices
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "gap-ups-gap-downs"}
                title="Gap up Stocks | Gap down Stocks"
              >
                <i className="fas fa-angle-right"></i>Gap up Stocks | Gap down
                Stocks
              </a>
            </li>

            <li>
              <a
                href={process.env.OLD_SITE_URL + "options-trading"}
                target="_blank"
                title="NSE Stock Options Chain"
              >
                <i className="fas fa-angle-right"></i>NSE Stock Options Chain
              </a>
            </li>
          </ul>
        </div>

        <div className="ad-box">
          <a
            href={process.env.OLD_SITE_URL + "money/ipo/"}
            target="_blank"
            title="IPO Dashboard"
            className="d-block text-center"
          >
            <img src="/images/ad.png" alt="ad" className="img-fluid" />
          </a>
        </div>
      </div>
    );
  }
}
