import React, { Component } from 'react';
import RightSection from './RightSection';

class TermsConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {

  }

  render() {
    const {pageContent} = this.props;
    return (
      <div className="row">
        <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
          <div className="sidebar-search searchbar-responsive">
            <div className="form-group">
              <form action="">
                <div className="ui-widget">
                  <input placeholder="Search" className="stocksearching autocomplete-ui ui-autocomplete-input" autocomplete="off" /> <span className="fa fa-search"></span>
                </div>

                <a id="HideStockNavigator" style={{ display: 'none' }} href="stocks-analysis/acc" className="d-none"></a>
              </form>
            </div>
          </div>
          <h1 className="main-page-heading">{pageContent["page_Content_Title"]}</h1>

          <div className="nifty-chart-panel">

            <h2 className="mb-3">Registration</h2>
            <p>
              To register to use <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> and
                        must
                        ensure
                        that your details held by us are correct and
                        complete at all times.
                    </p>
            <p>
              When you register, you will be receive information from niftytrader about products, promotions or
              special
              offers which we feel may be of interest to you. In the event that you do not wish to be contacted for
              such
              purposes, You may unsubscribe from our contact list at any time by the Unsubscribe link in our
              newsletter,
                        by e-mailing us at <a href="mailto: care@niftytrader.in." title="care@niftytrader.in.">care@niftytrader.in.</a>
            </p>
            <p>By using www.niftytrader.in you consent to be bound by the following terms and conditions.</p>
            <p>Please note that these terms and conditions do not affect your statutory rights.</p>
            <h4>Password and security of your account</h4>
            <p>
              When you register with www.niftytrader.in you will be asked to create a password. You must keep this
              password secret and should not disclose it or share it with anyone. You will be responsible for all
              activities and orders that occur or are submitted under your password. If you know or suspect that
              someone
                        else knows your password you should notify us by contacting <a href="mailto: care@niftytrader.in." title="care@niftytrader.in.">care@niftytrader.in.</a>immediately.
                    </p>
            <p>
              If we believe or have reason to believe that there has been a breach of your security or misuse of
              www.niftytrader.in, we may require you to change your password or we may suspend your account at our
              discretion
                    </p>
            <h4>Copyright and intellectual property</h4>
            <p>
              The content of <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> is
                        protected by copyright, trademarks, and other intellectual
                        property
                        rights. You may retrieve and display the content of <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> on a computer screen, store such
                        content in electronic form on disk (but, excepting search engine computers, not on any server or other
                        storage device connected to a network) or print one copy of such content for your own personal,
                        non-commercial use, provided you keep intact all and any copyright and proprietary notices. You may not
                        otherwise reproduce, modify, copy or distribute or use any of the materials or content on
                        www.niftytrader.in without written permission from www.niftytrader.in of this site for commercial
                        purposes
                    </p>
            <h4>Availability</h4>
            <p>
              Although <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> strives to
                        offer you the best service possible, <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> makes no
                        promise that the services at www.niftytrader.in will meet your requirements. www.niftytrader.in cannot
                        guarantee that the service will be fault free. You must check the product on arrival If a fault occurs
                        in
                        the service you should report it to the delivery boy or via email to <a href="mailto: care@niftytrader.in." title="care@niftytrader.in.">care@niftytrader.in.</a> and we will
                        correct the fault as soon as we reasonably can.
                    </p>
            <p>
              Your access to www.niftytrader.in may be occasionally restricted to allow for repairs, maintenance or
              the
                        introduction of new facilities or services. <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> will attempt to restore the service as
                        soon
                        as it reasonably can.
                    </p>
            <p>
              Active items may not be true to current availability and items you order may not be in stock. We will
              not
              be liable to you/or any third party for any consequential or incidental damages (including but not
              limited
              to loss of revenue, loss of profits, loss of anticipated savings, wasted expenditure, loss of privacy
              and
              loss of data) or any other indirect, special or punitive damages whatsoever that arise out of or are
              related to stock levels.
                    </p>
            <h4>www.niftytrader.in right to suspend or cancel your registration</h4>
            <p>
              www.niftytrader.in may suspend or cancel your registration immediately at our discretion or if you
              breach
              any of your obligations under these terms and conditions. You can cancel this agreement at any time by
              informing us in formal. If you do so, you must stop using all services from NIFTYTRADER.
                    </p>
            <h4>www.niftytrader.in’s liability</h4>
            <p>
              NIFTYTRADER is provided by NIFTYTRADER without any warranties or guarantees. You must bear the risks
              associated with the use of the Internet.
                    </p>
            <p>
              NIFTYTRADER tries to ensure that material included on <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> is correct, reputable and of high
                        quality, it cannot accept responsibility if this is not the case. NIFTYTRADER will not be responsible
                        for
                        any errors or omissions or for the results obtained from the use of such information or for any
                        technical
                        problems you may experience with <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a>. If NIFTYTRADER is informed in written format of any
                        inaccuracies in the material on the site we will attempt to correct the inaccuracies as soon as we
                        reasonably can.
                    </p>
            <h4>Third Party Websites</h4>
            <p>
              As a convenience to our customers, niftytrader.in includes paytm payment gateway. the use of this
              gateway
              or material which are beyond its control. niftytrader.in is not responsible for this external content,
              or
              for any other site outside of niftytrader.in.please read and understand before using the services to
              respective websites.
                    </p>
            <h4>Applicable Law</h4>
            <p>
              These terms and conditions shall be governed by and construed in accordance with the court of laws of
              India and any disputes will be decided only by the Indian courts. All the legal will be taken with
              judicial area of Jodhpur.
                    </p>
            <h4>Web Browser Support</h4>
            <p>Internet Explorer 9&amp;10, Firefox, Chrome, Opera, Safari,mobile browsers like uc,chrome,opera etc.</p>
            <h4>Amendments</h4>
            <p>
              We may occasionally make changes to our terms and conditions. If this concerns you, you are advised to
              check the current terms and conditions prior to every purchase. If you continue to use
                        <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a>
                        after the date of a change in our terms and conditions, your use of <a href={process.env.OLD_SITE_URL} title="niftytrader">www.niftytrader.in</a> indicates your
                        agreement to be bound by said new terms and conditions.
                    </p>
          </div>

        </div>

        <RightSection />
      </div>
    );
  }
}

export default TermsConditions;