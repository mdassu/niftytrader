import React, { Component } from "react";
import { connect } from "react-redux";
import { CALL_API, CALL_SIGNALR_API } from "_services/CALL_API";
import HomeOtherSiteLinks from "./HomeOtherSiteLinks";
import HomeOwlCarousel from "./HomeOwlCarousel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faCaretUp,
  faEllipsisV,
  faExternalLinkAlt,
  faPlus,
  faRedoAlt,
} from "@fortawesome/free-solid-svg-icons";
import * as moment from "moment";
import * as signalR from "@microsoft/signalr";
import Link from "next/link";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import Loader from "./Loader";
import { ToastContainer, toast } from "react-toastify";
import {
  ErrorMessages,
  isNameValid,
  isEmailValid,
  isMobileNumberValid,
} from "_helper/MyValidations";
import ForexRatesTable from "./Tables/ForexRatesTable";
import { AuthenticationService } from "_services/AuthenticationService";
import Typewriter from "typewriter-effect";
import StockAlertModal from "./Modals/StockAlertModal";
// import Cookies from 'js-cookie';
import { encrypt } from "_helper/EncrDecrypt";
import AdComponent from "./AdComponent";
import { faStar } from "@fortawesome/free-regular-svg-icons";
// import CookieConsent, { Cookies } from "react-cookie-consent";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { userData } from "_redux/actions";

const mapStateToProps = (state) => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn,
});

const mapDispatchToProps = {};
import Router from "next/router";
import WatchListChart from "./Charts/WatchListChart";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCookieBar: true,
      expires: 30,
      nifty50Data: {},
      niftyBankData: {},
      sgxData: {},
      allSymbolList: [],
      allSymbolListData: [],
      forexData: [],
      formData: {
        name: "",
        email: "",
        phoneNumber: "",
      },
      error: false,
      errorMessage: {
        name: "",
        email: "",
        phoneNumber: "",
      },
      stockAlertData: {},
      stockAlertPopup: false,
      isLoaded: false,
      isSend: true,
      watchListGraphDt: [],
      graphWatchlistData: [],
      topBrokersData: [],
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
    this.connection = "";
  }

  componentDidMount() {
    // const script = document.createElement("script");
    // script.src = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
    // script.async = true;
    // document.body.appendChild(script);

    // (window.adsbygoogle = window.adsbygoogle || []).push({});

    this.getStockData();
    this.getSignalRData();
    this.getSymbolListData();
    this.getSymbolList();
    this.getForexRatesData();
    this.getTopBrokers();
  }

  getStockData = () => {
    CALL_API("GET", process.env.HOME_STOCK_DATA, {}, (res) => {
      if (res.status) {
        var data = res["data"];
        this.setState({
          nifty50Data: data["nifty50"],
          niftyBankData: data["niftybank"],
          sgxData: data["sgxValue"],
          isLoaded: true,
        });
      }
    });
  };

  getSignalRData() {
    var day = moment().format("dddd");
    var currentDate = moment().format("MM/DD/YYYY");
    var currentDateTime = moment().format();
    var checkDateTime = moment(currentDate + " 16:00:00").format();
    var checkStartDateTime = moment(currentDate + " 09:00:00").format();
    if (
      day != "Saturday" &&
      day != "Sunday" &&
      checkDateTime > currentDateTime &&
      checkStartDateTime < currentDateTime
    ) {
      this.connection = new signalR.HubConnectionBuilder()
        .withUrl(
          "https://signalr.niftytrader.in/NiftySignalRTest/niftySignalRHub"
        )
        .build();

      this.connection.on(
        "sendTickDataToAndroidAtSA",
        (InstrumentToken, SymbolName, TickData) => {
          if (SymbolName == "NIFTY BANK") {
            var niftyBankData = this.state.niftyBankData;
            niftyBankData["last_trade_price"] = TickData["lastPrice"];
            niftyBankData["prev_price"] = TickData["close"];
            niftyBankData["high"] = TickData["high"];
            niftyBankData["low"] = TickData["low"];

            this.setState({
              niftyBankData: niftyBankData,
            });
          }
          if (SymbolName == "NIFTY 50") {
            var nifty50Data = this.state.nifty50Data;
            nifty50Data["last_trade_price"] = TickData["lastPrice"];
            nifty50Data["prev_price"] = TickData["close"];
            nifty50Data["high"] = TickData["high"];
            nifty50Data["low"] = TickData["low"];

            this.setState({
              nifty50Data: nifty50Data,
            });
          }
        }
      );

      this.connection.start().then(() => {
        this.addSymbolForSignalR(this.connection.connectionId);
      });
    }
  }

  addSymbolForSignalR = (connectionId) => {
    CALL_SIGNALR_API(
      "GET",
      process.env.ADD_SYMBOL_FOR_SIGNALR,
      { Symbol: "NIFTY 50,NIFTY BANK", ConnectionId: connectionId },
      (res) => {}
    );
  };

  getSymbolList() {
    CALL_API("GET", process.env.STOCK_LIST_SEARCH_DATA, {}, (res) => {
      if (res.status) {
        var symbols = [];
        res["data"].map((item) => {
          symbols.push({
            id: item["symbol_name"],
            name: item["symbol_name"],
          });
        });
        this.setState({
          allSymbolList: symbols,
        });
      } else {
        this.setState({
          allSymbolList: [],
        });
      }
    });
  }

  getSymbolListData() {
    CALL_API("GET", process.env.STOCK_LIST_DATA, {}, (res) => {
      if (res.status) {
        this.setState(
          {
            allSymbolListData: res["data"],
          },
          () => {
            let data = [];
            this.state.allSymbolListData.slice(0, 15).map((item, key) => {
              return data.push(item["symbol_name"]);
            });

            let listOfSymbols = data.join(",");
            this.getWatchListChartData(listOfSymbols);
          }
        );
      } else {
        this.setState({
          allSymbolListData: [],
        });
      }
    });
  }

  getForexRatesData = () => {
    CALL_API("GET", process.env.HOME_FOREX_RATES_DATA, {}, (res) => {
      if (res.status) {
        this.setState({
          forexData: res["data"],
        });
      } else {
        this.setState({
          forexData: [],
        });
      }
    });
  };

  handleFormInput =
    (name) =>
    ({ target: { value } }) => {
      if (name == "terms") {
        this.setState({
          formData: {
            ...this.state.formData,
            [name]: "",
          },
        });
      } else {
        this.setState({
          formData: {
            ...this.state.formData,
            [name]: value,
          },
        });
      }
    };

  formValidation = () => {
    const { formData } = this.state;
    var msgName = "";
    var msgEmail = "";
    var msgMobile = "";

    var isValid = false;
    if (formData.name) {
      if (!isNameValid(formData.name)) {
        msgName = ErrorMessages.VALIDATION_ERROR_NAME;
      }
    } else {
      msgName = "Name is required";
    }

    // last name validation
    if (formData.email) {
      if (!isEmailValid(formData.email)) {
        msgEmail = ErrorMessages.VALIDATION_ERROR_EMAIL;
      }
    } else {
      msgEmail = "Email is required";
    }

    // mobile number. validation
    if (formData.phoneNumber) {
      if (!isMobileNumberValid(formData.phoneNumber)) {
        msgMobile = ErrorMessages.VALIDATION_ERROR_MOBILE;
      }
    } else {
      msgMobile = "Mobile number is required";
    }

    if (!msgName && !msgEmail && !msgMobile) {
      isValid = true;
    }

    if (isValid) {
      this.setState({
        error: true,
        errorMessage: {
          name: "",
          email: "",
          phoneNumber: "",
        },
      });
      return true;
    } else {
      this.setState({
        error: true,
        errorMessage: {
          name: msgName,
          email: msgEmail,
          phoneNumber: msgMobile,
        },
      });
      return false;
    }
  };

  submitLowestBrokerage = () => {
    if (this.formValidation()) {
      this.setState({
        isSend: false,
      });
      const { formData } = this.state;
      var params = {
        name: formData["name"],
        email: formData["email"],
        phoneno: formData["phoneNumber"],
      };
      CALL_API("GET", process.env.HOME_BROKERAGE_SUBMIT_DATA, params, (res) => {
        if (res.status) {
          this.setState({
            formData: {
              name: "",
              email: "",
              phoneNumber: "",
            },
            isSend: true,
          });
          this.toastConfig["type"] = "success";
          toast(res["message"], this.toastConfig);
        } else {
          this.setState({
            isSend: true,
          });
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      });
    }
  };

  handleSymbolSelect = (item) => {
    window.location.href =
      process.env.OLD_SITE_URL +
      "stocks-analysis/" +
      item["name"].split(" | ")[0];
  };

  openStockAlertModal = (stockData) => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      this.setState(
        {
          stockAlertPopup: true,
          stockAlertData: stockData,
        },
        () => {
          $("#stockAlertPopup").modal("show");
        }
      );
    } else {
      $("#loginpopup").modal("show");
    }
  };

  closeStockAlertModal = () => {
    this.setState(
      {
        stockAlertPopup: false,
        stockAlertData: {},
      },
      () => {
        $("#stockAlertPopup").modal("hide");
      }
    );
  };

  addToWatchlist = (symbol) => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      CALL_API(
        "POST",
        process.env.SYMBOL_ADD_TO_WATCHLIST_DATA,
        { symbol_list: symbol },
        (res) => {
          if (res.status) {
            this.toastConfig["type"] = "success";
            toast(symbol + " Added to your Watchlist", this.toastConfig);
          } else {
            this.toastConfig["type"] = "error";
            toast(res["message"], this.toastConfig);
          }
        }
      );
    } else {
      $("#loginpopup").modal("show");
    }
  };

  goToWatchlist = () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      Router.push("/watchlist");
    } else {
      $("#loginpopup").modal("show");
    }
  };

  getWatchListChartData = (symbol) => {
    CALL_API(
      "GET",
      `${process.env.WATCHLIST_STOCK_CHART_DATA}?symbol=${symbol}`,
      {},
      (res) => {
        if (res.status) {
          var data = res["data"];

          this.setState(
            {
              watchListGraphDt: data,
            },
            () => {
              let data = [];
              this.state.allSymbolListData.slice(0, 15).map((item, key) => {
                return data.push(item);
              });

              let gphWatchData = [];
              let result = data.map((item, key) => {
                return this.state.watchListGraphDt.filter((value) => {
                  if (item["symbol_name"] == value["symbol_name"]) {
                    let data = {
                      symbol: value["symbol_name"],
                      close: value["close"],
                      created: moment(value["created_at"]).format("LT"),
                    };
                    gphWatchData.push(data);
                  }
                });
              });
              this.setState(
                {
                  graphWatchlistData: gphWatchData,
                },
                () => {}
              );
            }
          );
        }
      }
    );
  };

  getTopBrokers = () => {
    CALL_API("GET", process.env.TOP_BROKERS, {}, (res) => {
      if (res.status) {
        var data = res["data"];
        this.setState({
          topBrokersData: data,
        });
      }
    });
  };

  stockRating = (rating) => {
    return rating === 0 ? (
      <ul>
        <li>
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating > 0 && rating < 1 ? (
      <ul>
        <li>
          <img src="/images/broker_star_as.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating > 1 && rating < 2 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_as.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating > 2 && rating < 3 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_as.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating > 3 && rating < 4 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_as.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating > 4 && rating < 5 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_as.png" alt="star" />
        </li>
      </ul>
    ) : rating > 5 && rating < 6 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
        </li>
      </ul>
    ) : rating == 1 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating == 2 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating == 3 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating == 4 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star.png" alt="star" />
        </li>
      </ul>
    ) : rating == 5 ? (
      <ul>
        <li>
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
          <img src="/images/broker_star_active.png" alt="star" />
        </li>
      </ul>
    ) : (
      ""
    );
  };

  render() {
    const {
      nifty50Data,
      niftyBankData,
      sgxData,
      allSymbolList,
      allSymbolListData,
      forexData,
      formData,
      error,
      errorMessage,
      stockAlertData,
      stockAlertPopup,
      isLoaded,
      isSend,
      topBrokersData,
    } = this.state;

    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 1,
      margin: 30,
      autoplay: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 4,
          },
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };

    var nifty50Change =
      nifty50Data["last_trade_price"] - nifty50Data["prev_price"];
    var nifty50ChangePer = (nifty50Change / nifty50Data["prev_price"]) * 100;

    var niftyBankChange =
      niftyBankData["last_trade_price"] - niftyBankData["prev_price"];
    var niftyBankChangePer =
      (niftyBankChange / niftyBankData["prev_price"]) * 100;

    var tableColumns = [
      { field: "forex_id", headerText: "SR NO." },
      { field: "currency_code", headerText: "CURRENCY CODE" },
      { field: "currency_value", headerText: "CURRENCY VALUE" },
    ];
    var tableRows = [];

    if (forexData && forexData.length > 0) {
      forexData.map((item, key) => {
        tableRows.push({
          forex_id: item["forex_id"],
          currency_code: item["currency_code"],
          currency_value: item["currency_value"],
        });
      });
    }

    var topGainers = [];
    var topLosers = [];
    var sameOpenAndLow = [];
    var sameOpenAndHigh = [];
    var gapUP = [];
    var gapDown = [];
    var topWatchList = [];
    if (allSymbolListData && allSymbolListData.length > 0) {
      topGainers = allSymbolListData
        .sort((a, b) => {
          return b.changePer - a.changePer;
        })
        .slice(0, 6);
      topLosers = allSymbolListData
        .sort((a, b) => {
          return a.changePer - b.changePer;
        })
        .slice(0, 6);

      topWatchList = allSymbolListData
        .sort((a, b) => {
          return b.changePer - a.changePer;
        })
        .slice(0, 15);

      sameOpenAndLow = allSymbolListData
        .filter((item) => {
          return (
            Number(item["today_open"]).toFixed(2) ==
            Number(item["today_low"]).toFixed(2)
          );
        })
        .sort((a, b) => {
          var nameA = a.symbol_name.toLowerCase(),
            nameB = b.symbol_name.toLowerCase();
          if (nameA < nameB) {
            return -1;
          }
        })
        .slice(0, 6);

      sameOpenAndHigh = allSymbolListData
        .filter((item) => {
          return (
            Number(item["today_open"]).toFixed(2) ==
            Number(item["today_high"]).toFixed(2)
          );
        })
        .sort((a, b) => {
          var nameA = a.symbol_name.toLowerCase(),
            nameB = b.symbol_name.toLowerCase();
          if (nameA < nameB) {
            return -1;
          }
        })
        .slice(0, 6);

      gapUP = allSymbolListData
        .filter((item) => {
          return (
            Number(item["today_open"]).toFixed(2) >
            Number(item["prev_high"]).toFixed(2)
          );
        })
        .sort((a, b) => {
          var nameA = a.symbol_name.toLowerCase(),
            nameB = b.symbol_name.toLowerCase();
          if (nameA < nameB) {
            return -1;
          }
        })
        .slice(0, 6);

      gapDown = allSymbolListData
        .filter((item) => {
          return (
            Number(item["today_open"]).toFixed(2) <
            Number(item["prev_low"]).toFixed(2)
          );
        })
        .sort((a, b) => {
          var nameA = a.symbol_name.toLowerCase(),
            nameB = b.symbol_name.toLowerCase();
          if (nameA < nameB) {
            return -1;
          }
        })
        .slice(0, 6);
    }

    var topGainersData = "";
    var topLosersData = "";
    var sameOpenAndLowData = "";
    var sameOpenAndHighData = "";
    var gapUPData = "";
    var gapDownData = "";
    topGainersData = topGainers.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    topLosersData = topLosers.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    sameOpenAndLowData = sameOpenAndLow.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    sameOpenAndHighData = sameOpenAndHigh.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    gapUPData = gapUP.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    gapDownData = gapDown.map((item, key) => {
      var todayLowPer =
        ((item["today_close"] - item["today_low"]) /
          (item["today_high"] - item["today_low"])) *
        100;
      return (
        <tr key={key}>
          <td>
            <a
              href={
                process.env.OLD_SITE_URL +
                "stocks-analysis/" +
                item["symbol_name"].toLowerCase()
              }
              title="IIFL"
            >
              {item["symbol_name"]}
            </a>
          </td>
          <td id="CMP_OF_IIFL">{Number(item["today_close"]).toFixed(2)}</td>
          <td>
            <div id="NEWLOW_OF_IIFL" className="number-box percent-red">
              {Number(item["today_low"]).toFixed(2)}
            </div>
            <div className="watchlist-range-box">
              <div
                id="LOW_OF_IIFL"
                className="watchlist-high-range"
                style={{ width: todayLowPer + "%" }}
              ></div>
              <div
                id="HIGH_OF_IIFL"
                className="watchlist-low-range"
                style={{ width: 100 - todayLowPer + "%" }}
              ></div>
              <img
                src="/images/watchlist-range-control.png"
                id="Range_OF_IIFL"
                className="watchlist-range-control"
                alt=""
                style={{ left: todayLowPer + "%" }}
              />
            </div>
            <div
              id="NEWHIGH_OF_IIFL"
              className="number-box percent-green text-right"
            >
              {Number(item["today_high"]).toFixed(2)}
            </div>
          </td>
          <td
            className={
              Math.sign(item["changeValue"]) == -1
                ? "h-down-price"
                : "h-up-price"
            }
          >
            <FontAwesomeIcon
              icon={
                Math.sign(item["changeValue"]) == -1 ? faCaretDown : faCaretUp
              }
              width="9"
              height="14"
            />{" "}
            <span
              className={
                Math.sign(item["changeValue"]) == -1
                  ? "percent-red"
                  : "percent-green"
              }
              id="CHANGE_OF_IIFL"
            >
              {Number(item["changeValue"]).toFixed(2)} (
              {Number(item["changePer"]).toFixed(2)}%)
            </span>
          </td>
          <td id="ClOSE_OF_IIFL">{item["prev_close"]}</td>
          <td id="VOLUME_OF_IIFL">
            {AuthenticationService.numberWithCommas(item["today_volume"])}
          </td>
          <td className="action-btns text-right">
            <div className="ellipsis-top">
              <button className="ellipsis-btn">
                <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
              </button>
              <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                {/* <li>
                  <a title="Add to Portfolio"
                  ><FontAwesomeIcon icon={faPlus} width="14" height="14" style={{ color: '#18b57e' }} /> Add to Portfolio
                          </a>
                </li> */}
                <li className="">
                  <a
                    onClick={() => this.addToWatchlist(item["symbol_name"])}
                    title="Add To Watchlist"
                  >
                    <FontAwesomeIcon
                      icon={faStar}
                      width="14"
                      height="14"
                      style={{ color: "#2196f3" }}
                    />{" "}
                    Add To Watchlist
                  </a>
                </li>
                <li>
                  <a
                    onClick={() => this.openStockAlertModal(item)}
                    title="Create Alert"
                  >
                    <img
                      src="/images/createalert.png"
                      alt=""
                      className="alertBTn"
                    />{" "}
                    Create Alert
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      );
    });

    return (
      <React.Fragment>
        {/* <ToastContainer /> */}
        {/* {isLoaded ? "" : (
          <Loader />
        )} */}
        <React.Fragment>
          <section className="hompage-top-content">
            <div className="container p-0">
              <div className="row three-ngx-nifty">
                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="new-sgx-nifty-box">
                    <a title="NIFTY 50">
                      <Link href="/live-analytics">
                        <div className="sgx-box-inner-content">
                          <h5 className="">
                            NIFTY 50
                            <span
                              className={
                                Math.sign(nifty50Change) != -1 &&
                                Math.sign(nifty50Change) != 0
                                  ? "percent-green"
                                  : "percent-red"
                              }
                              id="Stockchange_NIFTY_50"
                            >
                              {Number(nifty50Change).toFixed(2)} (
                              {Number(nifty50ChangePer).toFixed(2)} %)
                            </span>
                          </h5>
                          <ul className="sgx-value-ul">
                            <li>
                              <img
                                src={
                                  Math.sign(nifty50Change) != -1 &&
                                  Math.sign(nifty50Change) != 0
                                    ? "/images/top-green.jpg"
                                    : "/images/down-red.jpg"
                                }
                                className=""
                              />
                              <span id="StockLTP_NIFTY_50">
                                {Number(
                                  nifty50Data["last_trade_price"]
                                ).toFixed(2)}
                              </span>
                            </li>
                            <li className="float-right">
                              High
                              <br />
                              <span className="percent-green">
                                {Number(nifty50Data["high"]).toFixed(2)}
                              </span>
                            </li>
                            <li className="float-right">
                              Low
                              <br />
                              <span className="percent-red">
                                {Number(nifty50Data["low"]).toFixed(2)}
                              </span>
                            </li>
                          </ul>
                        </div>
                      </Link>
                      <div className="clearfix"></div>
                      <ul className="sgx-box-bottem">
                        <li>
                          <a
                            href={
                              process.env.OLD_SITE_URL + "nifty50-contributors"
                            }
                            title="Contributors"
                          >
                            Contributors
                          </a>
                        </li>

                        <li>
                          <Link href="/live-analytics">
                            <a title="Live Analytics">Live Analytics</a>
                          </Link>
                        </li>

                        <li>
                          <Link href="/live-nifty-open-interest">
                            <a title="Options">Options</a>
                          </Link>
                        </li>

                        <li>
                          <Link href="/nifty-put-call-ratio">
                            <a title="PCR">PCR</a>
                          </Link>
                        </li>
                      </ul>
                    </a>
                  </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="new-sgx-nifty-box">
                    <a href="javascrips:void(0)" title="SGX NIFTY">
                      <div className="sgx-box-inner-content">
                        <h5>
                          SGX NIFTY{" "}
                          <span
                            className={
                              Math.sign(sgxData["change"]) != -1 &&
                              Math.sign(sgxData["change"]) != 0
                                ? "percent-green"
                                : "percent-red"
                            }
                            id="Stockchange_NIFTY_50"
                          >
                            {Number(sgxData["change"]).toFixed(2)} (
                            {Number(sgxData["change_percent"]).toFixed(2)} %)
                          </span>
                        </h5>
                        <ul className="sgx-value-ul">
                          <li>
                            <img
                              src={
                                Math.sign(sgxData["change"]) != -1 &&
                                Math.sign(sgxData["change"]) != 0
                                  ? "images/top-green.jpg"
                                  : "images/down-red.jpg"
                              }
                              className=""
                            />
                            {Number(sgxData["close"]).toFixed(2)}
                          </li>
                          <li className="float-right">
                            High
                            <br />
                            <span className="percent-green">
                              {Number(sgxData["high"]).toFixed(2)}
                            </span>
                          </li>
                          <li className="float-right">
                            Low
                            <br />
                            <span className="percent-red">
                              {Number(sgxData["low"]).toFixed(2)}
                            </span>
                          </li>
                        </ul>
                      </div>
                      <ul className="sgx-box-bottem">
                        <li>
                          <Link href="/sgx-nifty">
                            <a title="Auto Refresh">
                              <FontAwesomeIcon
                                icon={faRedoAlt}
                                width="12"
                                height="12"
                              />{" "}
                              Auto Refresh
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </a>
                  </div>
                </div>

                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="new-sgx-nifty-box">
                    <a title="NIFTY BANK">
                      <Link href="/banknifty-live-analysis">
                        <div className="sgx-box-inner-content">
                          <h5 className="">
                            NIFTY BANK
                            <span
                              className={
                                Math.sign(niftyBankChange) != -1 &&
                                Math.sign(niftyBankChange) != 0
                                  ? "percent-green"
                                  : "percent-red"
                              }
                              id="Stockchange_NIFTY_50"
                            >
                              {Number(niftyBankChange).toFixed(2)} (
                              {Number(niftyBankChangePer).toFixed(2)} %)
                            </span>
                          </h5>
                          <ul className="sgx-value-ul">
                            <li>
                              <img
                                src={
                                  Math.sign(niftyBankChange) != -1 &&
                                  Math.sign(niftyBankChange) != 0
                                    ? "/images/top-green.jpg"
                                    : "/images/down-red.jpg"
                                }
                                className=""
                              />
                              <span id="StockLTP_NIFTY_50">
                                {Number(
                                  niftyBankData["last_trade_price"]
                                ).toFixed(2)}
                              </span>
                            </li>
                            <li className="float-right">
                              High
                              <br />
                              <span className="percent-green">
                                {Number(niftyBankData["high"]).toFixed(2)}
                              </span>
                            </li>
                            <li className="float-right">
                              Low
                              <br />
                              <span className="percent-red">
                                {Number(niftyBankData["low"]).toFixed(2)}
                              </span>
                            </li>
                          </ul>
                        </div>
                      </Link>
                      <ul className="sgx-box-bottem">
                        <li>
                          <Link href="/banknifty-live-analysis">
                            <a title="Live Analytics">Live Analytics</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="/banknifty-live-oi-tracker">
                            <a title="Options">Options</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="/banknifty-intra-pcr-trend">
                            <a title="PCR">PCR</a>
                          </Link>
                        </li>
                      </ul>
                    </a>
                  </div>
                </div>
              </div>

              <div className="row top_six_blocks">
                <div className="col pr-lg-1 pl-lg-0 angel-box-responsive">
                  <a
                    href="https://bit.ly/2Yr3HTI"
                    target="_blank"
                    title="zerodha"
                    className="angel-broking-box m-0 pr-0"
                  >
                    <img
                      src="/images/zerodha-2.png"
                      alt="zerodha"
                      title="zerodha"
                    />
                    <span>Zerodha (₹20/Trade) Open Account</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
                <div className="col pr-lg-1 pl-lg-1 angel-box-responsive-left">
                  <a
                    href="http://tinyurl.com/yxxcwzxn"
                    target="_blank"
                    title="Angel Broking"
                    className="angel-broking-box m-0"
                  >
                    <img
                      src="/images/angel-2.png"
                      alt="Angel Broking"
                      title="Angel Broking"
                    />
                    <span>Angel Broking Flat ₹20 per trade</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
                <div className="col pr-lg-1 pl-lg-1 angel-box-responsive">
                  <a
                    href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                    target="_blank"
                    title="Aliceblue"
                    className="angel-broking-box m-0"
                  >
                    <img src="/images/aliceblue.png" alt="" title="Aliceblue" />
                    <span>Aliceblue Flat ₹15/Trade</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
                <div className="col pr-lg-1 pl-lg-1 angel-box-responsive-left">
                  <a
                    href="https://bit.ly/3y9832D"
                    target="_blank"
                    title="Upstox"
                    className="angel-broking-box m-0 mr-0"
                  >
                    <img src="/images/upstox.png" alt="Upstox" title="Upstox" />
                    <span>Upstox Flat ₹20/Trade</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
                <div className="col pr-2 pr-lg-1 pl-lg-1">
                  <a
                    href="https://bit.ly/2ZdiIJa"
                    target="_blank"
                    title="Trader Smart"
                    className="angel-broking-box m-0"
                  >
                    <img
                      src="/images/TraderSmart.png"
                      alt="Trader Smart"
                      title="Trader Smart"
                    />
                    <span>Trader Smart ₹ 15 per Trade</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
                <div className="col pr-lg-0 pl-lg-1 angel-box-responsive-left">
                  <a
                    href="https://bit.ly/3gvR6WK"
                    target="_blank"
                    title="Compare all top brokers"
                    className="angel-broking-box m-0"
                  >
                    <img
                      src="/images/responsive_logo.svg"
                      alt="Compare all top brokers"
                      title="Compare all top brokers"
                    />
                    <span>Compare All Top Brokers</span>
                    <div className="clearfix"></div>
                  </a>
                </div>
              </div>
            </div>

            <section className="comprehensive">
              <div className="container p-0">
                <div className="row">
                  <div className="col-lg-9 col-xl-9 col-md-7 col-12 p-0 ">
                    <div className="dayhighlow-box margin-box2">
                      <div className="premium-tag"></div>
                      <div className="analysis-heading">
                        <h6>
                          Most Comprehensive Stock Analysis
                          <div id="typing-text" className="text-center">
                            <div id="text-type" className="">
                              <Typewriter
                                options={{
                                  strings: [
                                    "Of Indian Stocks",
                                    "Stock Performance & Charts",
                                    "Key Financials & Fundamentals",
                                    "Technicals - NR7, Gaps, Volume, Analysis & More",
                                  ],
                                  autoStart: true,
                                  loop: true,
                                  cursor: "",
                                }}
                              />
                            </div>
                          </div>
                        </h6>
                        <div className="home-autocomplete">
                          <ReactSearchAutocomplete
                            items={allSymbolList}
                            onSelect={this.handleSymbolSelect}
                            placeholder="Search"
                          />
                          {/* <form action="">
                          <div className="form-group">
                            <div className="ui-widget">
                              <input
                                placeholder="Search"
                                className="stocksearching autoComplete-ui ui-autoComplete-input"
                                autoComplete="off"
                              />
                              <span className="fa fa-search"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                              </svg></span>
                            </div>

                            <a
                              id="HideStockNavigator"
                              style={{ display: 'none' }}
                              href="stocks-analysis/acc"
                              className="d-none"
                            ></a>
                          </div>
                        </form> */}
                        </div>
                        <ul className="nifty50item1 text-center">
                          <span className="text-uppercase">Or Analyse:</span>
                          <li>
                            {" "}
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/sjvn"
                              }
                            >
                              SJVN{" "}
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL + "stocks-analysis/pel"
                              }
                            >
                              PEL
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/ultracemco"
                              }
                            >
                              ULTRACEMCO
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/biocon/"
                              }
                            >
                              BIOCON
                            </a>
                          </li>
                          <li>
                            {" "}
                            <a
                              href={
                                process.env.OLD_SITE_URL + "stocks-analysis/acc"
                              }
                            >
                              ACC
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/nestleind"
                              }
                            >
                              NESTLEIND
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/canfinhome"
                              }
                            >
                              CANFINHOME
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/shreecem"
                              }
                            >
                              SHREECEM
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/idfcfirstb"
                              }
                            >
                              IDFCFIRSTB
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/idbi"
                              }
                            >
                              IDBI
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/eihotel"
                              }
                            >
                              EIHOTEL
                            </a>
                          </li>
                          <li>
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/tatasteel"
                              }
                            >
                              TATASTEEL
                            </a>
                          </li>
                          <li>
                            {" "}
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/kscl"
                              }
                            >
                              KSCL
                            </a>
                          </li>
                        </ul>
                        <div className="col-xl-12 text-center">
                          <img
                            src="/images/nifty.gif"
                            alt="arrow-up"
                            className="gif-charts"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-xl-3 col-md-5 col-12 p-0">
                    <div className="dayhighlow-box Brokerage">
                      <div className="form-heading">
                        <h5>Lowest Brokerage l Best Brokers l Free Tips</h5>
                      </div>
                      <div className="Brokerage-form">
                        <form id="freeTipsForm">
                          <div className="form-group">
                            {/* <label htmlFor="formGroupExampleInput">Name</label> */}
                            <input
                              type="text"
                              name="name"
                              className="form-control"
                              id="formGroupExampleInput"
                              placeholder="Name"
                              value={formData["name"]}
                              onChange={this.handleFormInput("name")}
                            />
                            {error && errorMessage["name"] ? (
                              <div className="validation-message">
                                {errorMessage["name"]}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="form-group">
                            {/* <label htmlFor="email">Your Email</label> */}
                            <input
                              type="email"
                              name="email"
                              className="form-control"
                              id="formGroupExampleInput2"
                              placeholder="Email address"
                              value={formData["email"]}
                              onChange={this.handleFormInput("email")}
                            />
                            {error && errorMessage["email"] ? (
                              <div className="validation-message">
                                {errorMessage["email"]}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="form-group">
                            {/* <label htmlFor="text">Mobile No.</label> */}
                            <input
                              type="text"
                              name="phoneNumber"
                              className="form-control"
                              maxLength="10"
                              id="formGroupExampleInput2"
                              placeholder="Mobile No."
                              value={formData["phoneNumber"]}
                              onChange={this.handleFormInput("phoneNumber")}
                            />
                            {error && errorMessage["phoneNumber"] ? (
                              <div className="validation-message">
                                {errorMessage["phoneNumber"]}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="form-group text-center">
                            <button
                              type="button"
                              onClick={this.submitLowestBrokerage}
                              className="submit-btn"
                              title="Submit"
                              disabled={!isSend}
                            >
                              {isSend ? "Submit" : "Submitting..."}
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </section>

          <section className="home-page-tabing">
            <div className="container p-0">
              <div className="row">
                <div className="col-lg-12 p-0">
                  <ul
                    className="nav nav-tabs justify-content-center h_tabing"
                    id="NiftyHomeTabing"
                    role="tablist"
                  >
                    <li className="nav-item">
                      <span
                        className="nav-link active"
                        id="top-gainers"
                        data-toggle="tab"
                        data-target="#top-gainers-tab"
                        role="tab"
                        aria-controls="top-gainers-tab"
                        aria-selected="false"
                      >
                        Top Gainers
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link"
                        id="top-looser"
                        data-toggle="tab"
                        data-target="#top-looser-tab"
                        role="tab"
                        aria-controls="top-looser-tab"
                        aria-selected="false"
                      >
                        Top Losers
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link"
                        id="tradeing-stock"
                        data-toggle="tab"
                        data-target="#open-home-low"
                        role="tab"
                        aria-controls="tradeing-home"
                        aria-selected="true"
                      >
                        Same open &amp; low
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link"
                        id="most-active"
                        data-toggle="tab"
                        data-target="#open-home-high"
                        role="tab"
                        aria-controls="most-active-tab"
                        aria-selected="false"
                      >
                        Same open &amp; high
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link"
                        id="tradeing-stock"
                        data-toggle="tab"
                        data-target="#tradeing-gap-up"
                        role="tab"
                        aria-controls="tradeing-home"
                        aria-selected="true"
                      >
                        Gap up
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link"
                        id="most-active"
                        data-toggle="tab"
                        data-target="#tradeing-gap-down"
                        role="tab"
                        aria-controls="most-active-tab"
                        aria-selected="false"
                      >
                        Gap Down
                      </span>
                    </li>
                  </ul>
                  <div className="tab-content" id="niftyHomeTabingStock">
                    <div
                      className="tab-pane fade show active table-responsive watchlistTable"
                      id="top-gainers-tab"
                      role="tabpanel"
                      aria-labelledby="tradeing-stock"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{topGainersData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={process.env.OLD_SITE_URL + "nse-stocks-price"}
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>
                    <div
                      className="tab-pane fade table-responsive watchlistTable"
                      id="top-looser-tab"
                      role="tabpanel"
                      aria-labelledby="most-active"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{topLosersData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={process.env.OLD_SITE_URL + "nse-stocks-price"}
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>
                    <div
                      className="tab-pane fade table-responsive watchlistTable"
                      id="open-home-low"
                      role="tabpanel"
                      aria-labelledby="top-gainers"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{sameOpenAndLowData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={
                            process.env.OLD_SITE_URL + "opening-price-clues"
                          }
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>
                    <div
                      className="tab-pane fade table-responsive watchlistTable"
                      id="open-home-high"
                      role="tabpanel"
                      aria-labelledby="top-looser"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{sameOpenAndHighData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={
                            process.env.OLD_SITE_URL + "opening-price-clues"
                          }
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>

                    <div
                      className="tab-pane fade table-responsive watchlistTable"
                      id="tradeing-gap-up"
                      role="tabpanel"
                      aria-labelledby="top-looser"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{gapUPData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={process.env.OLD_SITE_URL + "gap-ups-gap-downs"}
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>

                    <div
                      className="tab-pane fade table-responsive watchlistTable"
                      id="tradeing-gap-down"
                      role="tabpanel"
                      aria-labelledby="top-looser"
                    >
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Symbol</th>
                            <th scope="col">Current Price</th>
                            <th>Today L/H</th>
                            <th scope="col">Change (%)</th>
                            <th scope="col">Prev. Close</th>
                            <th scope="col">Volume</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>{gapDownData}</tbody>
                      </table>
                      <div className="h_more_stock">
                        <a
                          href={process.env.OLD_SITE_URL + "gap-ups-gap-downs"}
                          title="View more stocks"
                        >
                          View more stocks{" "}
                          <i className="fas fa-chevron-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* Homepage Watchlist */}
          <section className="h_watchlist">
            <div className="container">
              <div className="row">
                <div className="col-12 p-0">
                  <h4
                    className="h_section_heading"
                    style={{ padding: "0px", margin: "0px" }}
                  >
                    Watchlist
                  </h4>
                </div>
                <div className="col-12 p-0">
                  <Slider {...settings}>
                    {topWatchList.map((item, key) => {
                      return (
                        <div className="watchlist_item">
                          <a
                            href=""
                            title={item["symbol_name"]}
                            className="watchlist_item_title"
                          >
                            {item["symbol_name"]}
                            <a
                              href={
                                process.env.OLD_SITE_URL +
                                "stocks-analysis/" +
                                item["symbol_name"].toLowerCase()
                              }
                            >
                              <FontAwesomeIcon
                                icon={faExternalLinkAlt}
                                width="14"
                                height="14"
                              />
                            </a>

                            <div className="clearfix"></div>
                          </a>
                          <span>
                            Vol:{" "}
                            {parseInt(item["today_volume"]).toLocaleString()}
                          </span>
                          <h5>{item["today_close"]}</h5>
                          {item["changeValue"] > 0 ? (
                            <div className="h_green_watchlist">
                              <img
                                src="images/up-icon.png"
                                alt=""
                                height="8"
                                width="10"
                              />{" "}
                              {item["changeValue"]} (
                              {Number(item["changePer"]).toFixed(2)}%)
                            </div>
                          ) : (
                            <div className="h_green_watchlist">
                              <img
                                src="images/dow-icon.png"
                                alt=""
                                height="8"
                                width="10"
                              />{" "}
                              {item["changeValue"]} (
                              {Number(item["changePer"]).toFixed(2)}%)
                            </div>
                          )}

                          {item["symbol_name"] ===
                            topWatchList[key]["symbol_name"] && (
                            <div className="h_chart_watchlist">
                              <img src="/images/watchlist.jpg" alt="" />
                              {/* <WatchListChart
                                  symbolName={item["symbol_name"]}
                                  graphWatchlistData={
                                    this.state.graphWatchlistData
                                  }
                                  topWatchList={topWatchList}
                                /> */}
                            </div>
                          )}

                          <a
                            href="javascript:void(0);"
                            title="Add to wacthlist"
                            onClick={() =>
                              this.addToWatchlist(item["symbol_name"])
                            }
                            className="h_addto_watchlist"
                          >
                            <FontAwesomeIcon
                              icon={faStar}
                              width="14"
                              height="14"
                            />{" "}
                            Add to watchlist
                          </a>
                        </div>
                      );
                    })}
                  </Slider>
                </div>
                <div className="col-lg-12 h_create_watchlist text-center">
                  <p>All your instruments in one place</p>
                  <a
                    href="javascript:void(0);"
                    title="Go to Watchlist"
                    onClick={this.goToWatchlist}
                  >
                    Go to Watchlist
                  </a>
                </div>
              </div>
            </div>
          </section>

          {/* End Homepage Watchlist */}
          {/* <AdComponent adType="CM_NIFTYTRADER_WC1_RSPV" /> */}
          <section className="h_research_analysis_tool">
            <div className="container p-0">
              <div className="row">
                <div className="col-lg-12 text-center p-0">
                  <h5 className="h_subheading">Research Analysis Tools</h5>
                  <ul>
                    <li>
                      <a
                        href={
                          process.env.OLD_SITE_URL + "fibonacci-calculator-2"
                        }
                        title="Fibonacci Calculator"
                      >
                        <div className="h_research_analysis_icon">
                          <img
                            data-src="/images/fabu.png"
                            className="lazyloaded"
                            alt=""
                            width="40"
                            height="40"
                            src="/images/fabu.png"
                          />
                        </div>
                        <span> Fibonacci Calculator</span>
                      </a>
                    </li>
                    <li>
                      <a
                        href={
                          process.env.OLD_SITE_URL + "fibonacci-calculator-2"
                        }
                        title="Advanced Fibonacci Calculator"
                      >
                        <div className="h_research_analysis_icon">
                          <img
                            data-src="/images/advaced.png"
                            className="lazyloaded"
                            alt=""
                            width="39"
                            height="42"
                            src="/images/advaced.png"
                          />
                        </div>
                        <span className="AdvancedFibonacciCalculator">
                          Advanced Fibonacci Calculator
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        href={
                          process.env.OLD_SITE_URL + "option-pricing-calculator"
                        }
                        title="Black Scholes Calculator"
                      >
                        <div className="h_research_analysis_icon">
                          <img
                            data-src="/images/block.png"
                            className="lazyloaded"
                            alt=""
                            width="39"
                            height="40"
                            src="/images/block.png"
                          />
                        </div>
                        <span>Black Scholes Calculator</span>
                      </a>
                    </li>
                    <li>
                      <a
                        href={process.env.OLD_SITE_URL + "pivot-calculator"}
                        title="Pivot Point Calculator"
                      >
                        <div className="h_research_analysis_icon">
                          <img
                            data-src="/images/piovt.png"
                            className="lazyloaded"
                            alt=""
                            height="40"
                            width="40"
                            src="/images/piovt.png"
                          />
                        </div>
                        <span> Pivot Point Calculator</span>
                      </a>
                    </li>
                    <li>
                      <a
                        href={process.env.OLD_SITE_URL + "developing-pivots"}
                        title="Developing Pivots"
                      >
                        <div className="h_research_analysis_icon">
                          <img
                            data-src="/images/developing.png"
                            className="lazyloaded"
                            alt=""
                            width="40"
                            height="36"
                            src="/images/developing.png"
                          />
                        </div>
                        <span>Developing Pivots</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </section>

          <section className="section-banner d-none d-md-block">
            <div className="container p-0">
              <div className="row">
                <div className="col-12 p-0">
                  <div className="ad-box">
                    <a
                      href={
                        process.env.OLD_SITE_URL +
                        "money/ipo/blog/top-5-upcoming-ipos-in-2020.php"
                      }
                      title="Top 5 Upcoming IPOs in 2020"
                      className="d-block text-center"
                      style={{ margin: "0px 0px 40px 0px" }}
                      target="_blank"
                    >
                      <img
                        data-src="/images/top-5-ad.jpg"
                        alt=""
                        className="img-fluid lazyloaded"
                        src="/images/top-5-ad.jpg"
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* New Brokers */}
          <section className="third-section d-none d-md-block new_brokers">
            <div className="container p-0">
              <div className="row">
                <div className="col-12 p-0 text-center">
                  <h4 className="h_section_heading mb-5">Top 5 Brokers</h4>
                </div>
                <div className="col-12 p-0">
                  {topBrokersData.map((item, key) => {
                    return (
                      <div className="new_broker_box">
                        <div className="new_brokers_logo">
                          <a href="" target="_blank" title={item["Broker"]}>
                            <img src={item["Broker_Image"]} alt="logo" />
                          </a>
                        </div>
                        <div className="new_broker_content">
                          <div className="br_content_heading">
                            <a
                              href={item["affiliate_link"]}
                              target="_blank"
                              title={item["Broker"]}
                              className="new_broker_title"
                            >
                              {item["Broker"]}

                              <FontAwesomeIcon
                                icon={faExternalLinkAlt}
                                width="12"
                                height="12"
                              />
                            </a>
                            <ul> {this.stockRating(item["broker_rating"])} </ul>
                            <span>({item["broker_rating"]})</span>

                            <a
                              href={item["affiliate_link"]}
                              target="_blank"
                              title="Open Account"
                              className="new_broker_btn"
                            >
                              Open Account
                            </a>
                          </div>
                          <div className="br_content_features">
                            <div className="br_service">
                              <span>{item["service_type"]}</span>
                            </div>
                            <ul>
                              <li>
                                <img
                                  src="/images/broker_equity.png"
                                  alt="img"
                                />{" "}
                                Equity
                              </li>
                              <li>
                                <img
                                  src="/images/broker_equity_f.png"
                                  alt="img"
                                />{" "}
                                Futures
                              </li>
                              <li>
                                <img
                                  src="/images/broker_equity.png"
                                  alt="img"
                                />{" "}
                                Options
                              </li>
                              <li>
                                <img src="/images/broker_price.png" alt="img" />{" "}
                                Currency
                              </li>
                              <li>
                                <img
                                  src="/images/broker_commodity.png"
                                  alt="img"
                                />{" "}
                                Commodity
                              </li>
                            </ul>
                          </div>
                          <div className="br_account_charges">
                            <ul>
                              <li>
                                Account Opening Charge{" "}
                                <p>{item["acc_opening_chrg"]}</p>
                              </li>
                              <li>
                                Account Maintenance Charge{" "}
                                <p>{item["acc_main_chrg"]}</p>
                              </li>
                              <li>
                                Equity Delivery Brokerage{" "}
                                <p>{item["equity_deliv_brok"]}</p>
                              </li>
                              <li>
                                Equity Intra Brokerage{" "}
                                <p>{item["equity_intra_brok"]}</p>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </section>
          {/* <section className="third-section d-none d-md-block">
                <div className="container p-0">
                  <div className="row">
                    <div className="col-lg-3 col-xl-3 col-md-6 col-12">
                      <a href="https://bit.ly/2Yr3HTI" title="Zerodha" target="_blank"></a>
                      <div className="Performance-box zerodha-box text-center">
                        <a href="https://bit.ly/2Yr3HTI" title="Zerodha" target="_blank"></a
                        ><a
                          href="https://bit.ly/2Yr3HTI"
                          className=""
                          title="Zerodha"
                          target="_blank"
                        ><div className="zerodha-img">
                            <img
                              data-src="/images/zerodha.jpg"
                              className="lazyloaded"
                              alt=""
                              target="_blank"
                              src="/images/zerodha.jpg"
                            /></div
                          ></a>
                        <a href="https://bit.ly/2Yr3HTI" title="Zerodha" target="_blank"
                        ><span className="customers-btn">over 6 lakh customers</span></a
                        >
                        <h5>Free Equity Delivery Trades Flat 20 Per Trad</h5>
                        <a
                          href="https://bit.ly/2Yr3HTI"
                          title="Enquiry Now"
                          className="customers-btn enquiry-btn"
                          target="_blank"
                        >Enquiry Now</a
                        >
                        <p>
                          Get 100% brokerage refund if in 60 days you have made net profits
          </p>
                      </div>
                    </div>
                    <div className="col-lg-3 col-xl-3 col-md-6 col-12">
                      <a
                        href="http://tinyurl.com/yxxcwzxn"
                        title="Angel Broking"
                        target="_blank"
                      ></a>
                      <div className="Performance-box zerodha-box text-center angel-resp">
                        <a
                          href="http://tinyurl.com/yxxcwzxn"
                          title="Angel Broking"
                          target="_blank"
                        ></a
                        ><a
                          href="http://tinyurl.com/yxxcwzxn"
                          className=""
                          title="Angel Broking"
                          target="_blank"
                        ><div className="zerodha-img">
                            <img
                              data-src="/images/angel.jpg"
                              className="lazyloaded"
                              alt=""
                              target="_blank"
                              src="/images/angel.jpg"
                            /></div
                          ></a>
                        <a
                          href="http://tinyurl.com/yxxcwzxn"
                          target="_blank"
                          title="Angel Broking"
                        ><span className="customers-btn">over 6 lakh customers</span></a
                        >
                        <h5>Leading Retail Broker in India</h5>
                        <a
                          href="http://tinyurl.com/yxxcwzxn"
                          title="Request Instant Call Back"
                          className="customers-btn enquiry-btn"
                          target="_blank"
                        >Request Instant Call Back</a
                        >
                        <p>Rs 0 account opening fee on Demat Acct</p>
                      </div>
                    </div>
                    <div className="col-lg-3 col-xl-3 col-md-6 col-12">
                      <a
                        href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                        title="Aliceblue"
                        target="_blank"
                      ></a>
                      <div className="Performance-box zerodha-box text-center">
                        <a
                          href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                          title="Aliceblue"
                          target="_blank"
                        ></a
                        ><a
                          href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                          className=""
                          title="Aliceblue"
                          target="_blank"
                        ><div className="zerodha-img">
                            <img
                              data-src="/images/newalicelogo-small.png"
                              className="lazyloaded"
                              alt=""
                              src="/images/newalicelogo-small.png"
                            /></div
                          ></a>
                        <a
                          href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                          target="_blank"
                          title="Aliceblue"
                        ><span className="customers-btn">over 6 lakh customers</span></a
                        >
                        <h5>Multiple Plans to suit Your Trading style</h5>
                        <a
                          href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
                          title="Enquiry Now"
                          className="customers-btn enquiry-btn"
                          target="_blank"
                        >Enquiry Now</a
                        >
                        <p>
                          20 times limit in Bracket order with Auto trailing SL Get 0%
                          Commission
          </p>
                      </div>
                    </div>
                    <div className="col-lg-3 col-xl-3 col-md-6 col-12">
                      <a
                        href="https://bit.ly/3dUNTyV"
                        className="block"
                        title="5Paisa"
                        target="_blank"
                      ></a>
                      <div className="Performance-box zerodha-box mx margin-box1 text-center">
                        <a
                          href="https://bit.ly/3dUNTyV"
                          className="block"
                          title="5Paisa"
                          target="_blank"
                        ></a
                        ><a
                          href="https://bit.ly/3dUNTyV"
                          className=""
                          title="5Paisa"
                          target="_blank"
                        ><div className="zerodha-img">
                            <img
                              data-src="/images/paisa-logo.png"
                              className="lazyloaded"
                              alt=""
                              src="/images/paisa-logo.png"
                            /></div
                          ></a>
                        <a href="https://bit.ly/3dUNTyV" target="_blank" title="5Paisa"
                        ><span className="customers-btn">over 6 lakh customers</span></a
                        >
                        <h5>
                          Pay just Rs. 10 per order! India's Fastest Growing Discount Broker
          </h5>
                        <a
                          href="https://bit.ly/3dUNTyV"
                          title="Request Instant Call Back"
                          className="customers-btn enquiry-btn"
                          target="_blank"
                        >Request Instant Call Back</a
                        >
                        <p>0% Brokerage Instant Paperless Trading and Demat Account</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section> */}
          {/* <AdComponent adType="CM_NIFTYTRADER_WC2_RSPV" /> */}
          <section className="forexrate">
            <div className="container p-0">
              <div className="row">
                <div className="col-lg-12 text-left p-3">
                  <h5 className="h_subheading black_heading forex-heading">
                    Forex Rates
                  </h5>
                </div>
              </div>
              <div className="row">
                <div className="nifty-chart-panel">
                  <div className="row m-0">
                    <div className="col-lg-12 p-0">
                      {/* <!-- here Data Table --> */}
                      <div className="nifty-datatable table-responsive forexrate-table-nifty">
                        {tableRows && tableRows.length > 0 ? (
                          <ForexRatesTable
                            tableColumns={tableColumns}
                            tableRows={tableRows}
                          />
                        ) : (
                          <ForexRatesTable tableColumns={[]} tableRows={[]} />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* <AdComponent adType="CM_NIFTYTRADER_WC3_RSPV" /> */}
          <section className="h_download_our_app">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 h_app_left">
                  <div className="dwnload_app_content">
                    <span>Download Now !</span>
                    <h5>NiftyTrader App</h5>
                    <p>Stock Screener, NSE BSE Market Pulse: NiftyTrader</p>
                    <div className="qr-box">
                      <img src="/images/nifty-app-qr.png" alt="QR Code" />
                    </div>
                    <a
                      href="https://play.google.com/store/apps/details?id=in.niftytrader&hl=en_IN"
                      target="_blank"
                      className="googl-play-btn"
                    >
                      <img
                        src="/images/google-play-app.png"
                        alt="Google play Button"
                      />
                    </a>
                    <h6>Over 200k+ Downloads in India</h6>
                  </div>
                </div>
                <div className="col-lg-6 text-right h_app_right">
                  <div className="app-img-box">
                    <a
                      href="#"
                      className="you_tube_vd_btn"
                      data-toggle="modal"
                      data-target="#vedioModal"
                    >
                      <img
                        className="vd_btnpulse"
                        src="/images/play.svg"
                        alt="Img"
                        style={{ width: "40px" }}
                      />
                    </a>
                    <img
                      src="/images/dwnload-app-mobile.png"
                      className="img-fluid"
                      alt="App-img"
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="section-banner d-none d-md-block">
            <div className="container p-0">
              <div className="row">
                <div className="col-12 p-0">
                  <div className="ad-box">
                    <a
                      href={process.env.OLD_SITE_URL + "free-course/"}
                      title="Free Course"
                      className="d-block text-center"
                      target="_blank"
                    >
                      <img
                        data-src="/images/email-banner-2.gif"
                        alt=""
                        className="img-fluid lazyloaded"
                        src="/images/email-banner-2.gif"
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </React.Fragment>
        <div
          className="modal filter_modal filter_content_modal feedback-modal show createalert-for-banking"
          id="stockAlertPopup"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="stockAlertPopup"
          aria-hidden="true"
        >
          {stockAlertPopup ? (
            <StockAlertModal
              stockAlertData={stockAlertData}
              closeStockAlertModal={this.closeStockAlertModal}
            />
          ) : (
            ""
          )}
        </div>
        <div
          className="modal fade login_access_modal"
          id="vedioModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="vedioModal"
          aria-hidden="true"
        >
          <button
            type="button"
            style={{ position: "fixed", right: "15px", top: "15px" }}
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <div className="modal-dialog">
            <div className="modal-content bg-transparent p-0">
              <div className="modal-body" style={{ padding: "0px !important" }}>
                <iframe
                  style={{ marginTop: "130px" }}
                  width="530"
                  height="300"
                  src="https://www.youtube.com/embed/DmpaM8usK9Q"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

{
  /* export default Home; */
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);
