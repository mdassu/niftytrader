import React from 'react';

export default class AdComponent extends React.Component {
  componentDidMount() {
    (window.adsbygoogle = window.adsbygoogle || []).push({});
  }

  render() {
    return (
      <React.Fragment>
        {
          this.props.adType == 'CM_NIFTYTRADER_WC1_RSPV' ? <ins
            className="adsbygoogle"
            style={{ display: 'block', margin: '10px 0px', textAlign: 'center' }}
            data-ad-client="ca-pub-5132448513373852"
            data-ad-slot="7822154660"
            data-ad-format="auto"
            data-full-width-responsive="true"
          /> : ''
        }

        {
          this.props.adType == 'CM_NIFTYTRADER_WC2_RSPV' ? <ins
            className="adsbygoogle"
            style={{ display: 'block', margin: '10px 0px', textAlign: 'center' }}
            data-ad-client="ca-pub-5132448513373852"
            data-ad-slot="7973363755"
            data-ad-format="auto"
            data-full-width-responsive="true"
          /> : ''
        }

        {
          this.props.adType == 'CM_NIFTYTRADER_WC3_RSPV' ? <ins
            className="adsbygoogle"
            style={{ display: 'block', margin: '10px 0px', textAlign: 'center' }}
            data-ad-client="ca-pub-5132448513373852"
            data-ad-slot="5495567742"
            data-ad-format="auto"
            data-full-width-responsive="true"
          /> : ''
        }

        {
          this.props.adType == 'CM_NIFTYTRADER_EOP_RSPV' ? <ins
            className="adsbygoogle"
            style={{ display: 'block', margin: '10px 0px', textAlign: 'center' }}
            data-ad-client="ca-pub-5132448513373852"
            data-ad-slot="3629975828"
            data-ad-format="auto"
            data-full-width-responsive="true"
          /> : ''
        }

      </React.Fragment>
    );
  }
}