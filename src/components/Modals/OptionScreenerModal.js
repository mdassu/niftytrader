import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark } from "@fortawesome/free-regular-svg-icons";
import MultiSelect from "react-multi-select-component";
import { useEffect, useState } from "react";

export default function OptionScreeenerModal(props) {
  var symbols = [];
  var selectedSymbols = [];
  if (props.filterType === "indices") {
    symbols = props.twoStocks;
    selectedSymbols = [props.twoStocks[0]];
  } else {
    symbols = props.allStocks;
    selectedSymbols = [props.allStocks[0]];
  }



  useEffect(() => {
    $(function () {
      document.getElementById("exampleFormControlSelect1").value = 100000;
      document.getElementById("exampleFormControlSelect2").value = 100000;
    });
  }, []);

  useEffect(() => {
    if (props.includeOiNone !== false) {
      $(function () {
        document.getElementById("exampleFormControlSelect1").value = null;
      });
    }

    if (props.includeVolNone !== false) {
      $(function () {
        document.getElementById("exampleFormControlSelect2").value = null;
      });
    }
  }, [props.includeOiNone, props.includeVolNone]);

  return (
    <div className="modal-dialog modal-lg">
      <div className="modal-content">
        <div className="modal-content">
          <div className="modal-body">
            <form className="option_form">
              <div className="container">
                <div className="filter-header">
                  {/* <!-- row start --> */}
                  <div className="row">
                    <div className="col-5 pr-0">
                      {/* <!-- <h3>All Filters</h3> --> */}
                      <div className="custom-control custom-radio custom-control-inline">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="indices"
                          name="filterType"
                          checked={props.filterType === "indices" ? true : false}
                          value="indices"
                          title="Indices"
                          onChange={props.changeFilterType}
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="indices"
                          title="Indices"
                        >
                          Indices
                        </label>
                      </div>
                      <div className="custom-control custom-radio custom-control-inline">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="stock"
                          name="filterType"
                          value="stocks"
                          checked={props.filterType === "stocks" ? true : false}
                          onChange={props.changeFilterType}
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="stock"
                          title="Stocks"
                        >
                          Stocks
                        </label>
                      </div>
                    </div>
                    <div className="col-7 text-right pl-0">
                      <div className="clear_btn">
                        <button
                          className="mr-3"
                          type="reset"
                          title="Clear"
                          onClick={props.resetFilter}
                        >
                          Clear
                          {/* <div className="badge clear_badge">5</div> */}
                        </button>
                      </div>
                      <div className="filter_btns">
                        <button
                          type="button"
                          className="filter_cancel"
                          id="save_modal"
                          title="Save"
                          data-toggle="modal"
                          data-target="#save-filter"
                        >
                          {/* <!-- <img title="Filter" src="img/advance-stock/save_An.svg" className="filter_img" alt="Filter"> --> */}
                          <FontAwesomeIcon
                            icon={faBookmark}
                            width="15"
                            height="13"
                            className="filter_img"
                          />

                          <b className="responsive_nonebtn">Save</b>
                        </button>
                        {/* <!-- <button className="filter_cancel ml-3" data-dismiss="modal" title="Cancel">Cancel</button> --> */}
                        <button
                          type="button"
                          title="Apply Now"
                          id="apply_filter"
                          onClick={props.applyFilter}
                        >
                          <img
                            title="Apply"
                            src="/images/advance-stock/apply01.svg"
                            alt="apply.svg"
                            className="modal_apply_img1"
                          />
                          <img
                            title="Apply"
                            src="/images/advance-stock/apply.svg"
                            alt="apply.svg"
                            className="modal_apply_img"
                          />{" "}
                          Apply Now
                        </button>
                      </div>
                    </div>
                  </div>
                  {/* <!-- row close --> */}
                </div>
                <div
                  className="filter-body col-lg-3-layout"
                  id="options_selected"
                >
                  <div className="row" id="indices_options">
                    <div className="col-lg-3 col-6 col-md-4">
                      <div className="filter_check_innerdiv indices_filter_options">
                        <h5>Symbol:</h5>
                        {symbols && symbols.length > 0 ? (
                          <MultiSelect
                            options={props.allStocks}
                            value={props.selectedFilterFields["symbol"]}
                            onChange={props.handleFilterFields}
                            labelledBy={"Select Symbol"}
                            placeholder="Select Symbol"
                            name="symbol"
                          />
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    <div className="col-lg-9">
                      <div className="row">
                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Open Interest:</h5>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="openinterest_increase"
                                name="openinterest_increase"
                                checked={
                                  props.selectedFilterFields[
                                  "openinterest_increase"
                                  ]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="openinterest_increase"
                              >
                                Increase
                              </label>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="openinterest_decrease"
                                name="openinterest_decrease"
                                checked={
                                  props.selectedFilterFields[
                                  "openinterest_decrease"
                                  ]
                                }
                                onChange={props.handleFilterFields}
                              />

                              <label
                                className="custom-control-label"
                                htmlFor="openinterest_decrease"
                              >
                                Decrease
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Type:</h5>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="type_call"
                                name="type_call"
                                checked={
                                  props.selectedFilterFields["type_call"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="type_call"
                              >
                                Call
                              </label>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="type_put"
                                name="type_put"
                                checked={props.selectedFilterFields["type_put"]}
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="type_put"
                              >
                                Put
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Include If:</h5>
                            <div className="">
                              <span>
                                <label
                                  for="exampleFormControlSelect1"
                                  style={{ marginRight: "5px !important;" }}
                                  className="filter_select_lable"
                                >
                                  OI &nbsp;&gt;&nbsp;
                                </label>

                                <select
                                  className="filter_select"
                                  id="exampleFormControlSelect1"
                                  onChange={props.handleFilterFields}
                                  name="include_if_oi_Select"
                                >
                                  <option
                                    value="null"
                                    selected={
                                      props.includeOiNone === true ||
                                        props.oiSelectVal == "null"
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    None
                                  </option>
                                  <option
                                    value="0"
                                    selected={
                                      props.oiSelectVal == 0 ? "selected" : ""
                                    }
                                  >
                                    0
                                  </option>
                                  <option
                                    value="1000"
                                    selected={
                                      props.oiSelectVal == 1000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    1000
                                  </option>
                                  <option
                                    value="10000"
                                    selected={
                                      props.oiSelectVal == 10000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    10000
                                  </option>
                                  <option
                                    value="50000"
                                    selected={
                                      props.oiSelectVal == 50000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    50000
                                  </option>
                                  <option
                                    value="100000"
                                    selected={
                                      props.includeOiCheck === false ||
                                        props.oiSelectVal == 100000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    100000
                                  </option>
                                </select>
                              </span>
                            </div>
                            <div className=" ">
                              <span>
                                <label
                                  for="exampleFormControlSelect2"
                                  style={{ marginRight: "5px !important;" }}
                                  className="filter_select_lable"
                                >
                                  Volume &nbsp;&gt;&nbsp;
                                </label>

                                <select
                                  id="exampleFormControlSelect2"
                                  name="include_if_volume_Select"
                                  onChange={props.handleFilterFields}
                                  className="filter_select"
                                >
                                  <option
                                    value="null"
                                    selected={
                                      props.includeVolNone === true ||
                                        props.volSelectVal == "null"
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    None
                                  </option>
                                  <option
                                    value="0"
                                    selected={
                                      props.volSelectVal == 0 ? "selected" : ""
                                    }
                                  >
                                    0
                                  </option>
                                  <option
                                    value="1000"
                                    selected={
                                      props.volSelectVal == 1000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    1000
                                  </option>
                                  <option
                                    value="10000"
                                    selected={
                                      props.volSelectVal == 10000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    10000
                                  </option>
                                  <option
                                    value="50000"
                                    selected={
                                      props.volSelectVal == 50000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    50000
                                  </option>
                                  <option
                                    value="100000"
                                    selected={
                                      props.includeVolCheck === false ||
                                        props.volSelectVal == 100000
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    100000
                                  </option>
                                </select>
                              </span>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="include_Ltp"
                                name="include_Ltp"
                                checked={
                                  props.selectedFilterFields["include_Ltp"]
                                }
                                onChange={props.handleFilterFields}
                              />

                              <label
                                className="custom-control-label"
                                htmlFor="include_Ltp"
                              >
                                Option LTP &nbsp; &gt; 0<span></span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Opening Price Clues:</h5>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="price_open_high"
                                name="price_open_high"
                                checked={
                                  props.selectedFilterFields["price_open_high"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="price_open_high"
                              >
                                Open = High
                              </label>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="price_open_low"
                                name="price_open_low"
                                checked={
                                  props.selectedFilterFields["price_open_low"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="price_open_low"
                              >
                                Open = Low
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Option LTP:</h5>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="ltp_increase"
                                name="ltp_increase"
                                checked={
                                  props.selectedFilterFields["ltp_increase"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="ltp_increase"
                              >
                                Increase
                              </label>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="ltp_decrease"
                                name="ltp_decrease"
                                checked={
                                  props.selectedFilterFields["ltp_decrease"]
                                }
                                onChange={props.handleFilterFields}
                              />

                              <label
                                className="custom-control-label"
                                htmlFor="ltp_decrease"
                              >
                                Decrease
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Series:</h5>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="series_current"
                                name="series_current"
                                checked={
                                  props.selectedFilterFields["series_current"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="series_current"
                              >
                                Current
                              </label>
                            </div>
                            <div className="custom-control custom-checkbox">
                              <input
                                className="custom-control-input"
                                type="checkbox"
                                id="series_farther"
                                name="series_farther"
                                checked={
                                  props.selectedFilterFields["series_farther"]
                                }
                                onChange={props.handleFilterFields}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="series_farther"
                              >
                                Farther
                              </label>
                            </div>
                          </div>
                        </div>

                        {props.filterType == "indices" ? (
                          <div className="col-lg-3 col-6 col-md-4">
                            <div className="filter_check_innerdiv">
                              <h5>Expiry:</h5>
                              <div className="custom-control custom-checkbox">
                                <input
                                  className="custom-control-input"
                                  type="checkbox"
                                  id="expiry_m"
                                  name="expiry_m"
                                  checked={
                                    props.selectedFilterFields["expiry_m"]
                                  }
                                  onChange={props.handleFilterFields}
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="expiry_m"
                                >
                                  Monthly
                                </label>
                              </div>
                              <div className="custom-control custom-checkbox">
                                <input
                                  className="custom-control-input"
                                  type="checkbox"
                                  id="weekly"
                                  name="weekly"
                                  checked={props.selectedFilterFields["weekly"]}
                                  onChange={props.handleFilterFields}
                                />

                                <label
                                  className="custom-control-label"
                                  htmlFor="weekly"
                                >
                                  Weekly
                                </label>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="col-lg-3 col-6 col-md-4">
                            <div className="filter_check_innerdiv">
                              <h5>Underlying Price:</h5>
                              <div className="custom-control custom-checkbox">
                                <input
                                  className="custom-control-input"
                                  type="checkbox"
                                  id="Underlyingprice_Increase"
                                  name="Underlyingprice_Increase"
                                  checked={
                                    props.selectedFilterFields[
                                    "Underlyingprice_Increase"
                                    ]
                                  }
                                  onChange={props.handleFilterFields}
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="Underlyingprice_Increase"
                                >
                                  Increase
                                </label>
                              </div>
                              <div className="custom-control custom-checkbox">
                                <input
                                  className="custom-control-input"
                                  type="checkbox"
                                  id="Underlyingprice_decrease"
                                  name="Underlyingprice_decrease"
                                  checked={
                                    props.selectedFilterFields[
                                    "Underlyingprice_decrease"
                                    ]
                                  }
                                  onChange={props.handleFilterFields}
                                />

                                <label
                                  className="custom-control-label"
                                  htmlFor="Underlyingprice_decrease"
                                >
                                  Decrease
                                </label>
                              </div>
                            </div>
                          </div>
                        )}

                        <div className="col-lg-3 col-6 col-md-4">
                          <div className="filter_check_innerdiv">
                            <h5>Proximity(%):</h5>
                            <div className=" ">
                              <span>
                                <label
                                  for="exampleFormControlSelect2"
                                  style={{ marginRight: "5px !important;" }}
                                  className="filter_select_lable"
                                >
                                  Range &nbsp;&lt;=&nbsp;
                                </label>
                                <select
                                  id="proximity_select"
                                  onChange={props.handleFilterFields}
                                  name="proximity_select"
                                  className="filter_select"
                                >
                                  <option
                                    value="null"
                                    selected={
                                      props.includeProxNone === true ||
                                        props.proximityCheck === false ||
                                        props.proxSelectVal == "null"
                                        ? "selected"
                                        : ""
                                    }
                                  >
                                    None
                              </option>
                                  <option
                                    value="2"
                                    selected={
                                      props.proxSelectVal == 2 ? "selected" : ""
                                    }
                                  >
                                    2%
                              </option>
                                  <option
                                    value="3"
                                    selected={
                                      props.proxSelectVal == 3 ? "selected" : ""
                                    }
                                  >
                                    3%
                              </option>
                                  <option
                                    value="5"
                                    selected={
                                      props.proxSelectVal == 5 ? "selected" : ""
                                    }
                                  >
                                    5%
                              </option>
                                  <option
                                    value="10"
                                    selected={
                                      props.proxSelectVal == 10 ? "selected" : ""
                                    }
                                  >
                                    10%
                              </option>
                                </select>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
