import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBookmark
} from "@fortawesome/free-regular-svg-icons";
import MultiSelect from "react-multi-select-component";
import { faCaretUp, faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { Component } from "react";
import Cookies from 'js-cookie';
import { decrypt } from "_helper/EncrDecrypt";
import { toast } from 'react-toastify';
import { CALL_API } from "_services/CALL_API";

export default class StockAlertModal extends Component {

  constructor(props) {
    super(props);
    this.initStateData = {
      symbolName: '',
      lastTradePrice: '',
      changeValue: '',
      changePer: '',
      formData: {
        master_id: null,
        symbol: "",
        deliver_method: 2,
        is_Used: true,
        created_at: null,
        updated_at: null,
        lstchild: [],
        bindmodel: null,
        company_name: null,
        last_trade_price: null,
        chang: null,
        pers: null,
        result: null
      },
      aboveValueError: '',
      belowValueError: '',
      aboveValue: '',
      belowValue: '',
      aboveValueStatus: false,
      belowValueStatus: false,
      highVolumeStatus: false,
      high52Status: false,
      low52Status: false,
      highVolumeAlertType: 0,
      highLowAlertType: 2
    };
    this.state = {
      symbolName: '',
      lastTradePrice: '',
      changeValue: '',
      changePer: '',
      formData: {
        master_id: null,
        symbol: "",
        deliver_method: 2,
        is_Used: true,
        created_at: null,
        updated_at: null,
        lstchild: [],
        bindmodel: null,
        company_name: null,
        last_trade_price: null,
        chang: null,
        pers: null,
        result: null
      },
      aboveValueError: '',
      belowValueError: '',
      aboveValue: '',
      belowValue: '',
      aboveValueStatus: false,
      belowValueStatus: false,
      highVolumeStatus: false,
      high52Status: false,
      low52Status: false,
      isEditable: false,
      highVolumeAlertType: 0,
      highLowAlertType: 2
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  componentDidMount() {
    var formData = this.state.formData;
    formData['symbol'] = this.props.stockAlertData['symbol_name'] ? this.props.stockAlertData['symbol_name'] : this.props.stockAlertData['symbol']
    this.setState({
      symbolName: formData['symbol'],
      formData: formData,
      changeValue: this.props.stockAlertData['changeValue'] ? this.props.stockAlertData['changeValue'] : '',
      changePer: this.props.stockAlertData['changePer'] ? this.props.stockAlertData['changePer'] : '',
      lastTradePrice: this.props.stockAlertData['today_close'] ? this.props.stockAlertData['today_close'] : ''
    }, () => {
      this.getStockAlertDataBySymbol();
    });
  }

  componentDidUpdate() {
    var symbolName = this.props.stockAlertData['symbol_name'] ? this.props.stockAlertData['symbol_name'] : this.props.stockAlertData['symbol']
    if (symbolName != this.state.symbolName) {
      var stateData = Object.assign({}, this.initStateData);

      stateData['symbolName'] = symbolName;
      stateData['formData']['symbol'] = symbolName;
      this.setState(stateData, () => {
        this.getStockAlertDataBySymbol();
      });
    }
  }

  getStockAlertDataBySymbol = () => {
    CALL_API('POST', process.env.GET_STOCK_ALERT_DATA, { Symbol: this.state.symbolName }, res => {
      if (res.status) {
        var data = res['data'];
        var formData = this.state.formData;
        formData['master_id'] = data['alertMaster']['master_id'];
        formData['symbol'] = data['alertMaster']['symbol'];
        formData['deliver_method'] = data['alertMaster']['deliver_method'];
        formData['lstchild'] = data['lstchild'];
        var aboveData = formData['lstchild'].filter(item => {
          return item['column_Name'] == 'AboveINR';
        });
        var belowData = formData['lstchild'].filter(item => {
          return item['column_Name'] == 'BelowINR';
        });
        var highVolume = formData['lstchild'].filter(item => {
          return item['column_Name'] == 'highvolumeday';
        });
        var high52 = formData['lstchild'].filter(item => {
          return item['column_Name'] == 'high52';
        });
        var low52 = formData['lstchild'].filter(item => {
          return item['column_Name'] == 'Low52';
        });

        if (aboveData.length > 0) {
          this.setState({
            aboveValue: aboveData[0]['column_Value'],
            aboveValueStatus: true,
          })
        }

        if (belowData.length > 0) {
          this.setState({
            belowValue: belowData[0]['column_Value'],
            belowValueStatus: true,
          })
        }
        if (highVolume.length > 0) {
          this.setState({
            highVolumeStatus: true,
            highVolumeAlertType: highVolume[0]['alert_type']
          })
        }
        if (high52.length > 0) {
          this.setState({
            high52Status: true,
            highLowAlertType: high52[0]['alert_type']
          })
        }
        if (low52.length > 0) {
          this.setState({
            low52Status: true,
            highLowAlertType: low52[0]['alert_type']
          })
        }

        this.setState({
          formData: formData,
          changeValue: data['alertMaster']['chang'],
          changePer: data['alertMaster']['pers'],
          lastTradePrice: data['alertMaster']['last_trade_price'],
          isEditable: true
        });
      } else {
        this.setState({
          changeValue: this.props.stockAlertData['changeValue'] ? this.props.stockAlertData['changeValue'] : '',
          changePer: this.props.stockAlertData['changePer'] ? this.props.stockAlertData['changePer'] : '',
          lastTradePrice: this.props.stockAlertData['today_close'] ? this.props.stockAlertData['today_close'] : '',
          isEditable: false
        });
      }
    });
  }

  priceMovesOption = (event, type) => {
    if (type == 'above') {
      const formData = this.state.formData;
      var status = true;
      if (event.target.checked) {
        formData['lstchild'].push({
          id: null,
          master_id: null,
          column_Name: "AboveINR",
          column_Condition: ">",
          column_Value: '',
          alert_type: 0,
          notification_Status: null,
          set_Sent_Flag: null,
          message: null
        });
      } else {
        formData['lstchild'].map((item, key) => {
          if (item['column_Name'] == 'AboveINR') {
            formData['lstchild'].splice(key, 1);
          }
        })
        status = false;
      }
      this.setState({
        formData: formData,
        aboveValueStatus: status
      });
    } else if (type == 'below') {
      const formData = this.state.formData;
      var status = true;
      if (event.target.checked) {
        formData['lstchild'].push({
          id: null,
          master_id: null,
          column_Name: "BelowINR",
          column_Condition: "<",
          column_Value: '',
          alert_type: 0,
          notification_Status: null,
          set_Sent_Flag: null,
          message: null
        });
      } else {
        formData['lstchild'].map((item, key) => {
          if (item['column_Name'] == 'BelowINR') {
            formData['lstchild'].splice(key, 1);
          }
        })
        status = false;
      }
      this.setState({
        formData: formData,
        belowValueStatus: status
      });
    } else if (type == 'highVolumeDay') {
      const formData = this.state.formData;
      var status = true;
      if (event.target.checked) {
        formData['lstchild'].push({
          id: null,
          master_id: null,
          column_Name: "highvolumeday",
          column_Condition: ">",
          column_Value: 0.0,
          alert_type: this.state.highVolumeAlertType,
          notification_Status: null,
          set_Sent_Flag: null,
          message: null
        });
      } else {
        formData['lstchild'].map((item, key) => {
          if (item['column_Name'] == 'highvolumeday') {
            formData['lstchild'].splice(key, 1);
          }
        })
        status = false;
        this.setState({
          highVolumeAlertType: 0
        });
      }
      this.setState({
        formData: formData,
        highVolumeStatus: status
      });
    } else if (type == '52WeekHigh') {
      const formData = this.state.formData;
      var status = true;
      if (event.target.checked) {
        formData['lstchild'].push({
          id: null,
          master_id: null,
          column_Name: "high52",
          column_Condition: ">",
          column_Value: 0.0,
          alert_type: this.state.highLowAlertType,
          notification_Status: null,
          set_Sent_Flag: null,
          message: null
        });
      } else {
        formData['lstchild'].map((item, key) => {
          if (item['column_Name'] == 'high52') {
            formData['lstchild'].splice(key, 1);
          }
        })
        status = false;
      }
      this.setState({
        formData: formData,
        high52Status: status
      }, () => {
        if (!this.state.low52Status && !this.state.high52Status) {
          this.setState({
            highLowAlertType: 2
          });
        }
      });
    } else if (type == '52WeekLow') {
      const formData = this.state.formData;
      var status = true;
      if (event.target.checked) {
        formData['lstchild'].push({
          id: null,
          master_id: null,
          column_Name: "Low52",
          column_Condition: "<",
          column_Value: 0.0,
          alert_type: this.state.highLowAlertType,
          notification_Status: null,
          set_Sent_Flag: null,
          message: null
        });
      } else {
        formData['lstchild'].map((item, key) => {
          if (item['column_Name'] == 'Low52') {
            formData['lstchild'].splice(key, 1);
          }
        })
        status = false;
      }
      this.setState({
        formData: formData,
        low52Status: status
      }, () => {
        if (!this.state.low52Status && !this.state.high52Status) {
          this.setState({
            highLowAlertType: 2
          });
        }
      });
    }
  }

  handleValueChange = (event, type) => {
    const formData = this.state.formData;
    formData['lstchild'].map((item, key) => {
      if (item['column_Name'] == type) {
        item['column_Value'] = event.target.value;
      }
    })
    var aboveValue = this.state.aboveValue;
    var belowValue = this.state.belowValue;
    var aboveError = '';
    var belowError = '';
    if (type == 'AboveINR') {
      aboveValue = Number(event.target.value);
      if (aboveValue <= Number(this.state.lastTradePrice).toFixed(2)) {
        aboveError = 'Above INR must be greater than stock current price.';
      } else {
        aboveError = '';
      }
    } else {
      belowValue = Number(event.target.value);
      if (belowValue >= Number(this.state.lastTradePrice).toFixed(2)) {
        belowError = 'Below INR must be less than stock current price.';
      } else {
        belowError = '';
      }
    }
    this.setState({
      formData: formData,
      aboveValue: aboveValue,
      belowValue: belowValue,
      aboveValueError: aboveError,
      belowValueError: belowError,
    });
  }

  changeHighVolumeAlertType = () => {
    if (this.state.highVolumeStatus) {
      var value = 0;
      if (this.state.highVolumeAlertType == 0) {
        value = 1
      }
      const formData = this.state.formData;
      formData['lstchild'].map((item, key) => {
        if (item['column_Name'] == 'highvolumeday') {
          item['Alert_type'] = value;
        }
      })
      this.setState({
        formData: formData,
        highVolumeAlertType: value
      });
    } else {
      this.toastConfig['type'] = 'error';
      toast('Please check "Send An Alert On High Volume Day"', this.toastConfig);
    }
  }

  changeHighLowAlertType = () => {
    if (this.state.high52Status || this.state.low52Status) {
      var value = 2;
      if (this.state.highLowAlertType == 2) {
        value = 3
      }
      const formData = this.state.formData;
      formData['lstchild'].map((item, key) => {
        if (item['column_Name'] == 'high52' || item['column_Name'] == 'Low52') {
          item['Alert_type'] = value;
        }
      })
      this.setState({
        formData: formData,
        highLowAlertType: value
      });
    } else {
      this.toastConfig['type'] = 'error';
      toast('Please check "New 52 Week High" or "New 52 Week Low"', this.toastConfig);
    }
  }

  createAlert = () => {
    if (this.state.formData['lstchild'].length > 0) {
      var aboveValue = this.state.formData['lstchild'].filter(item => {
        return item['column_Name'] == 'AboveINR' && item['column_Value'] == '';
      });
      var belowValue = this.state.formData['lstchild'].filter(item => {
        return item['column_Name'] == 'BelowINR' && item['column_Value'] == '';
      });
      if (aboveValue.length > 0) {
        this.setState({
          aboveValueError: 'Please enter value in Above INR'
        });
      } else if (belowValue.length > 0) {
        this.setState({
          belowValueError: 'Please enter value in Below INR'
        });
      } else {
        var params = this.state.formData;
        CALL_API('POST', process.env.SAVE_STOCK_ALERTS_DATA, params, res => {
          if (res.status) {
            this.toastConfig['type'] = 'success';
            toast('Stock Alert created successfully.', this.toastConfig);
            $('#stockAlertPopup').modal('hide');
          } else {
            this.toastConfig['type'] = 'error';
            toast(res['message'], this.toastConfig);
          }
        })
      }
    } else {
      this.toastConfig['type'] = 'error';
      toast('Enter/Check atleast one filter.', this.toastConfig);
    }

  }

  render() {
    const { formData, changeValue, changePer, symbolName, lastTradePrice, highVolumeAlertType, highLowAlertType, aboveValueError, belowValueError, aboveValue, belowValue, aboveValueStatus, belowValueStatus, highVolumeStatus, high52Status, low52Status, isEditable } = this.state;

    return (

      <div className="modal-dialog modal-lg alert-modal-nifty" role="document">
        <form>
          <div className="modal-content">

            <div className="modal-header">
              <div className="stock-analysis-heading p-0">
                <h2>
                  <b>  {!isEditable ? 'Create' : 'Edit'} Alert for  {symbolName}</b>
                  <span className={Math.sign(changeValue) == -1 ? 'newstockLTPDown' : 'newstockLTPUP'} id="StockLTP">{Number(lastTradePrice).toFixed(2)}</span>

                  <FontAwesomeIcon icon={Math.sign(changeValue) == -1 ? faCaretDown : faCaretUp} width="14" height="14" className={Math.sign(changeValue) == -1 ? 'percent-red' : 'percent-green'} />
                  <b className={Math.sign(changeValue) == -1 ? 'color-text-red' : 'color-text'} id="StockPriceChange">{Number(changeValue).toFixed(2)} ({Number(changePer).toFixed(2)}%)</b>

                </h2>
              </div>
              <div className="dilogContent">
                {!isEditable ? 'Add' : 'Edit'} an alert for a stock you are interested in and get notification on
                your desktop, app and email. You can configure alerts on Price, volume
                change and 52 week high / low.
                        </div>
            </div>

            <div className="modal-body">

              <div className="row mr-0 ml-0">
                <div className="col-xl-12">
                  <div className="feedback-form row" id="add-to-watchlist-portfolio">
                    <div className="col-lg-12">
                      <label className="headingLabel">When Price moves</label>
                    </div>

                    <div className="form-group col-lg-6 col-md-6 col-sm-6 col-12 mb-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox">
                        <input type="checkbox" className="custom-control-input" id="above" name="above" onChange={($event) => this.priceMovesOption($event, 'above')} checked={aboveValueStatus} />
                        <label className="custom-control-label" htmlFor="above">  Above INR </label>
                      </div>
                      <input type="number" step=".01" className="form-control" id="AboveINR" placeholder="Above INR" name="AboveINR" value={aboveValue} onChange={($event) => this.handleValueChange($event, 'AboveINR')} disabled={!aboveValueStatus} />

                    </div>

                    <div className="form-group col-lg-6 col-md-6 col-sm-6 col-12 mb-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox">
                        <input type="checkbox" className="custom-control-input" id="below" onChange={($event) => this.priceMovesOption($event, 'below')} checked={belowValueStatus} />
                        <label className="custom-control-label" htmlFor="below"> Below INR  </label>
                      </div>
                      <input type="number" step=".01" className="form-control" id="BelowINR" placeholder="Below INR" name="BelowINR" value={belowValue} onChange={($event) => this.handleValueChange($event, 'BelowINR')} disabled={!belowValueStatus} />
                    </div>
                    <div className="form-group col-12 mb-0">
                      {aboveValueError != '' ? (<div className="validation-message">{aboveValueError}</div>) : ''}
                      {belowValueError != '' ? (<div className="validation-message">{belowValueError}</div>) : ''}
                    </div>
                    <hr className="horizontal-line" />
                    <div className="col-lg-8 col-md-6 col-sm-6 col-8 pr-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox" >
                        <input type="checkbox" className="custom-control-input" id="highVolumeDay" onChange={($event) => this.priceMovesOption($event, 'highVolumeDay')} checked={highVolumeStatus} />
                        <label className="custom-control-label" htmlFor="highVolumeDay">
                          Send an alert on high volume day
                                            </label>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-6 col-4 pl-0 text-right">
                      <a id="oneTime" onClick={this.changeHighVolumeAlertType}>{highVolumeAlertType == 0 ? 'One time alert' : 'Recurring alert'}</a>
                    </div>
                    <hr className="horizontal-line" />
                    <div className="col-lg-4 col-md-4 col-sm-4 col-4 pr-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox" >
                        <input type="checkbox" className="custom-control-input" id="52WeekHigh" onChange={($event) => this.priceMovesOption($event, '52WeekHigh')} checked={high52Status} />
                        <label className="custom-control-label" htmlFor="52WeekHigh">
                          New 52 week high
                                            </label>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-4 col-4 p-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox" >
                        <input type="checkbox" className="custom-control-input" id="52WeekLow" onChange={($event) => this.priceMovesOption($event, '52WeekLow')} checked={low52Status} />
                        <label className="custom-control-label" htmlFor="52WeekLow">
                          New 52 week low
                                            </label>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-4 col-4 text-right">
                      <a id="seconTime" onClick={this.changeHighLowAlertType}>{highLowAlertType == 2 ? 'One time alert' : 'Recurring alert'}</a>
                    </div>
                    <hr className="horizontal-line" />
                    <div className="col-lg-12">
                      <label className="headingLabel">Delivery methods</label>
                    </div>
                    {/* <div className="col-lg-6 col-md-6 col-sm-6 col-6 pr-0">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox" >
                        <input type="checkbox" className="custom-control-input" id="DesktopNotifications" />
                        <label className="custom-control-label" htmlFor="DesktopNotifications">
                          Desktop notifications
                                            </label>
                      </div>
                    </div> */}
                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox" >
                        <input type="checkbox" className="custom-control-input" id="EmailNotifications" checked disabled />
                        <label className="custom-control-label" htmlFor="EmailNotifications">
                          Email notifications
                                            </label>
                      </div>
                    </div>

                    <div className="form-group col-lg-12 col-md-12 col-sm-12 col-12">
                      <button type="button" className="btn create-btn" onClick={this.createAlert}>
                        {!isEditable ? 'Create' : 'Update'}
                      </button>
                      <button style={{ margin: '20px 0px 0px 7px' }} type="button" className="btn btn-danger" onClick={this.props.closeStockAlertModal}>Close</button>
                    </div>

                  </div>
                </div>
              </div>
            </div >
          </div >

        </form >
      </div >
    )
  }
}