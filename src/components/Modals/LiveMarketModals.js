import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBookmark
} from "@fortawesome/free-regular-svg-icons";
export default function LiveMarketModal(props) {
  const industries = Array.from(new Set(props.industriesList)).sort();
  return (

    <div className="modal-dialog">
      <div className="modal-content">
        {/* <!-- Modal Header --> */}
        <div className="modal-header">
          <h4 className="modal-title">Stock Screener Filters</h4>
          <button type="button" className="close" data-dismiss="modal">
            {" "}
                  &times;{" "}
          </button>
        </div>
        {/* <!-- Modal body --> */}
        <div className="modal-body">
          <div className="row tabing_screener">
            <div className="col-xl-6 col-6 col-md-6 pr-0">
              <nav className="nav nav-tabs" id="myTab" role="tablist">
                <a
                  className="nav-item nav-link active text-uppercase"
                  id="nav-home-tab"
                  data-toggle="tab"
                  href="#nav-technical"
                  role="tab"
                  aria-controls="nav-technical"
                  aria-selected="true"
                >
                  Technical
                      </a>
                <a
                  className="nav-item nav-link text-uppercase"
                  id="nav-profile-tab"
                  data-toggle="tab"
                  href="#nav-financial"
                  role="tab"
                  aria-controls="nav-financial"
                  aria-selected="false"
                >
                  Financial
                      </a>
                <a
                  className="nav-item nav-link text-uppercase"
                  id="nav-profile-tab"
                  data-toggle="tab"
                  href="#nav-general"
                  role="tab"
                  aria-controls="nav-general"
                  aria-selected="false"
                >
                  General
                      </a>
              </nav>
            </div>
            <div className="col-xl-6 col-6 col-md-6 text-right">
              <div className="right_panel_btn">
                <button className="refresh_btn common_btns" onClick={props.resetFilter}>
                  <img title="Reset" src="/images/reset.svg" alt="reset" />
                  {/* <div className="badge clear_badge">5</div> */}
                </button>
                <button
                  data-toggle="modal"
                  data-target="#save-filter"
                  className="save_filters text-uppercase common_btns"
                  title="Save"
                >
                  <FontAwesomeIcon icon={faBookmark} width="15" height="13" style={{ marginRight: '7px' }} />
                  <b className="responsive_nonebtn">Save</b>
                </button>
                <button className="stock_filter text-uppercase common_btns" onClick={props.applyFilter}>
                  <img
                    title="Apply"
                    src="/images/advance-stock/apply01.svg"
                    alt="apply01.svg"
                  />
                  <b className="responsive_nonebtn">Apply</b>
                </button>
              </div>
            </div>
          </div>
          <div className="tab-content" id="nav-tabContent">
            <div
              className="tab-pane fade show active"
              id="nav-technical"
              role="tabpanel"
              aria-labelledby="nav-home-tab"
            >
              {/* <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>NR7</h6>
                  <ul>
                    <li>
                      <span> Today:</span>
                      <label className="stock-checkbox">
                        Is NR7 Day <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayNR7']} name="todayNR7" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <span> Yesterday:</span>
                      <label className="stock-checkbox">
                        Was NR7 Day <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayNR7']} name="yesterdayNR7" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>Gap Up / Gap Down</h6>
                  <ul>
                    <li>
                      <span> Today:</span>
                      <label className="stock-checkbox float-left">
                        Gap Up <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayGapUP']} name="todayGapUP" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Gap Down <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayGapDown']} name="todayGapDown" />
                        <span className="checkmark"></span>
                      </label>
                      <div className="clearfix"></div>
                    </li>
                    <li>
                      <span> Yesterday:</span>
                      <label className="stock-checkbox float-left">
                        Gap Up <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayGapUP']} name="yesterdayGapUP" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Gap Down <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayGapDown']} name="yesterdayGapDown" />
                        <span className="checkmark"></span>
                      </label>
                      <div className="clearfix"></div>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row -->
            <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Opening Price Clues</h6>
                  <ul>
                    <li>
                      <span> Today:</span>
                      <label className="stock-checkbox">
                        Stock with same open & high{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayStockOpenHigh']} name="todayStockOpenHigh" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Stock with same open & low{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayStockOpenLow']} name="todayStockOpenLow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <span> Yesterday:</span>
                      <label className="stock-checkbox">
                        Stock with same open & high{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayStockOpenHigh']} name="yesterdayStockOpenHigh" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Stock with same open & low{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayStockOpenLow']} name="yesterdayStockOpenLow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>Range Breakout</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        20 Day Range (Up) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range20DayUP']} name="range20DayUP" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        50 Day Range (Up) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range50DayUP']} name="range50DayUP" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        200 Day Range (Up){" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range200DayUP']} name="range200DayUP" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        52 Week New High{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range52WeekHigh']} name="range52WeekHigh" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        20 Day Range (Down) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range20DayDown']} name="range20DayDown" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        50 Day Range (Down) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range50DayDown']} name="range50DayDown" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        200 Day Range (Down) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range200DayDown']} name="range200DayDown" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        52 Week New Low <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range52WeekLow']} name="range52WeekLow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row -->
            <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Price Action</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Higher High Higher Low{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['higherHighHigherLow']} name="higherHighHigherLow" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Lower High Lower Low{" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['lowerHighLowerLow']} name="lowerHighLowerLow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        Inside Day <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['insideDay']} name="insideDay" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Outside Day <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['outsideDay']} name="outsideDay" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-12 col-md-6 col-sm-6">
                  <h6>Price Range</h6>
                  <ul>
                    <li className="volume_shocker">
                      <label className="stock-checkbox">
                        0~100 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range0To100']} name="range0To100" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        100~500 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range100To500']} name="range100To500" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        500~1000 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range500To1000']} name="range500To1000" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li className="volume_shocker">
                      <label className="stock-checkbox">
                        1000~2000 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['range1000To2000']} name="range1000To2000" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Above 2000 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['rangeAbove2000']} name="rangeAbove2000" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row -->
            <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Candlestick</h6>
                  <ul>
                    <li>
                      <span> Today:</span>
                      <label className="stock-checkbox">
                        Bullish (Close Near High){" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayBullishHigh']} name="todayBullishHigh" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Bearish (Close Near Low) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayBearishLow']} name="todayBearishLow" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Netural <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayNetural']} name="todayNetural" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <span> Yesterday:</span>
                      <label className="stock-checkbox">
                        Bullish (Close Near High){" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayBullishHigh']} name="yesterdayBullishHigh" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Bearish (Close Near Low) <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayBearishLow']} name="yesterdayBearishLow" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Netural <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayNetural']} name="yesterdayNetural" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>Moving Average</h6>
                  <ul>
                    <li className="moving_average">
                      <span> Today:</span>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayAbove20SMA']} name="todayAbove20SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayBelow20SMA']} name="todayBelow20SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        20 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayAbove50SMA']} name="todayAbove50SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayBelow50SMA']} name="todayBelow50SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        50 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayAbove200SMA']} name="todayAbove200SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayBelow200SMA']} name="todayBelow200SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        200 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                    </li>
                    <li className="moving_average">
                      <span> Yesterday:</span>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayAbove20SMA']} name="yesterdayAbove20SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayBelow20SMA']} name="yesterdayBelow20SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        20 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayAbove50SMA']} name="yesterdayAbove50SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayBelow50SMA']} name="yesterdayBelow50SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        50 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayAbove200SMA']} name="yesterdayAbove200SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['yesterdayBelow200SMA']} name="yesterdayBelow200SMA" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox float-left stock-sma">
                        200 Day SMA{" "}
                      </label>
                      <div className="clearfix"></div>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Volume Shocker</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        High Volume Day (Today){" "}
                        <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['todayHighVolumeDay']} name="todayHighVolumeDay" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>VWAP</h6>
                  <ul>
                    <li className="moving_average">
                      <label className="stock-checkbox float-left">
                        Above <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['vwapAbove']} name="vwapAbove" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li className="moving_average">
                      <label className="stock-checkbox float-left">
                        Below <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['vwapBelow']} name="vwapBelow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>

              </div>

              <div className="row stock_screener_filters">

                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>Max Pain</h6>
                  <ul>
                    <li className="moving_average">
                      <label className="stock-checkbox float-left">
                        LTP Above Max Pain <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['maxPainAbove']} name="maxPainAbove" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li className="moving_average">
                      <label className="stock-checkbox float-left">
                        LTP Below Max Pain <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['maxPainBelow']} name="maxPainBelow" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>

              </div>
              {/* <!-- close Row --> */}
            </div>
            <div
              className="tab-pane fade"
              id="nav-financial"
              role="tabpanel"
              aria-labelledby="nav-profile-tab"
            >
              {/* <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Market Cap</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Below 1,000 Cr. <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['marketCapBelow1000']} name="marketCapBelow1000" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        5,000 Cr - 20,000 Cr. <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['marketCap5000To20000']} name="marketCap5000To20000" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Above 50,000 Cr. <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['marketCapAbove50000']} name="marketCapAbove50000" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        1,000 Cr - 5,000 Cr. <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['marketCap1000To5000']} name="marketCap1000To5000" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        20,000 Cr - 50,000 Cr. <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['marketCap20000To50000']} name="marketCap20000To50000" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>Stock PE</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        {" "}
                              Below 5 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPEBelow5']} name="stockPEBelow5" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        10 - 20 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPE10To20']} name="stockPE10To20" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        50 - 100 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPE50To100']} name="stockPE50To100" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        5 - 10 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPE5To10']} name="stockPE5To10" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        20 - 50 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPE20To50']} name="stockPE20To50" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Above 100 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['stockPEAbove100']} name="stockPEAbove100" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row -->
            <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>Dividend Yield</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        0 - 1% <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['dividendYield0To1']} name="dividendYield0To1" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        2 - 5% <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['dividendYield2To5']} name="dividendYield2To5" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        1 - 2% <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['dividendYield1To2']} name="dividendYield1To2" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              Above 5% <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['dividendYieldAbove5']} name="dividendYieldAbove5" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">
                  <h6>ROCE</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Below 5 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roceBelow5']} name="roceBelow5" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              10 - 20 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roce10To20']} name="roce10To20" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              50 - 70 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roce50To70']} name="roce50To70" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        {" "}
                              5 - 10 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roce5To10']} name="roce5To10" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              20 - 50 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roce20To50']} name="roce20To50" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              70 - 100 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roce70To100']} name="roce70To100" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row --> */}
              {/* <!-- start row --> */}
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-sm-6 col-md-6 responsive_filter">
                  <h6>ROE</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Below 0 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roeBelow0']} name="roeBelow0" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              10 - 20 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roe10To20']} name="roe10To20" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              Above 50 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roeAbove50']} name="roeAbove50" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        0 - 10 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roe0To10']} name="roe0To10" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        20 - 50 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['roe20To50']} name="roe20To50" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-12 col-md-6 col-sm-6">
                  <h6>Sales growth</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Below 0 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowthBelow0']} name="salesGrowthBelow0" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              5 - 10 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowth5To10']} name="salesGrowth5To10" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              15 - 20 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowth15To20']} name="salesGrowth15To20" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <label className="stock-checkbox">
                        0 - 5 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowth0To5']} name="salesGrowth0To5" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        10 - 15 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowth10To15']} name="salesGrowth10To15" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        Above 20 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['salesGrowthAbove20']} name="salesGrowthAbove20" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-12 col-md-6 col-sm-6">
                  <h6>Piotroski Score</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        0~2 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['piotroskiScore0To2']} name="piotroskiScore0To2" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              3~7 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['piotroskiScore3To7']} name="piotroskiScore3To7" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              8~9 <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['piotroskiScore8To9']} name="piotroskiScore8To9" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              {/* <!-- close Row --> */}
            </div>
            <div
              className="tab-pane fade"
              id="nav-general"
              role="tabpanel"
              aria-labelledby="nav-profile-tab"
            >
              {/* <!-- start row --> */}

              <div className="row stock_screener_filters">
                <div className="col-xl-6 col-12 col-md-6 col-sm-6">
                  <h6>Category</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Nifty 50 Stocks <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['nifty50Stocks']} name="nifty50Stocks" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              FnO Stocks <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['fnoStocks']} name="fnoStocks" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-12 col-md-6 col-sm-6">
                  <h6>Industry Type</h6>
                  <ul>
                    <li>
                      <label className="stock-checkbox">
                        Financial <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['financial']} name="financial" />
                        <span className="checkmark"></span>
                      </label>
                      <label className="stock-checkbox">
                        {" "}
                              Non-Financial <input type="checkbox" onChange={props.handleFilterFields} checked={props.selectedFilterFields['nonFinancial']} name="nonFinancial" />
                        <span className="checkmark"></span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row stock_screener_filters">
                <div className="col-xl-6  col-sm-6 col-md-6 responsive_filter">
                  <h6>Industry</h6>
                  <ul className="IN">
                    <li>
                      <select className="form-control" bind="Select" id="exampleFormControlSelect1" onChange={props.handleFilterFields} selected={props.selectedFilterFields['industry']} name="industry">
                        <option selected="" value="Select">Select</option>
                        {
                          industries && industries.length > 0 ?
                            industries.map((item, key) => {
                              return (
                                <option value={item}>{item}</option>
                              )
                            }) : ''
                        }

                        {/* <option value="Advertising &amp; Media">Advertising &amp; Media</option>
                          <option value="Aerospace">Aerospace</option>
                          <option value="Agrochemicals">Agrochemicals</option>
                          <option value="Airlines">Airlines</option>
                          <option value="Airport Services">Airport Services</option>
                          <option value="Aluminium">Aluminium</option>
                          <option value="Asset Management Cos.">Asset Management Cos.</option>
                          <option value="Auto Parts &amp; Equipment">Auto Parts &amp; Equipment</option>
                          <option value="Auto Tyres &amp; Rubber Products">Auto Tyres &amp; Rubber Products</option>
                          <option value="Banks">Banks</option>
                          <option value="Biotechnology">Biotechnology</option>
                          <option value="BPO/KPO">BPO/KPO</option>
                          <option value="Breweries &amp; Distilleries">Breweries &amp; Distilleries</option>
                          <option value="Broadcasting &amp; Cable TV">Broadcasting &amp; Cable TV</option>
                          <option value="Carbon Black">Carbon Black</option>
                          <option value="Cars &amp; Utility Vehicles">Cars &amp; Utility Vehicles</option>
                          <option value="Cement &amp; Cement Products">Cement &amp; Cement Products</option>
                          <option value="CHEMICALS">CHEMICALS</option>
                          <option value="Cigarettes-Tobacco Products">Cigarettes-Tobacco Products</option>
                          <option value="Coal">Coal</option>
                          <option value="Comm.Trading  &amp; Distribution">Comm.Trading  &amp; Distribution</option>
                          <option value="Commercial Vehicles">Commercial Vehicles</option>
                          <option value="Commodity Chemicals">Commodity Chemicals</option>
                          <option value="Construction &amp; Engineering">Construction &amp; Engineering</option>
                          <option value="Consulting Services">Consulting Services</option>
                          <option value="Consumer Electronics">Consumer Electronics</option>
                          <option value="CONSUMER GOODS">CONSUMER GOODS</option>
                          <option value="Containers &amp; Packaging">Containers &amp; Packaging</option>
                          <option value="Copper">Copper</option>
                          <option value="Defence">Defence</option>
                          <option value="Department Stores">Department Stores</option>
                          <option value="Diversified">Diversified</option>
                          <option value="Electric Utilities">Electric Utilities</option>
                          <option value="Exploration &amp; Production">Exploration &amp; Production</option>
                          <option value="Fertilizers">Fertilizers</option>
                          <option value="Finance (including NBFCs)">Finance (including NBFCs)</option>
                          <option value="Financial Institutions">Financial Institutions</option>
                          <option value="FINANCIAL SERVICES">FINANCIAL SERVICES</option>
                          <option value="Footwear">Footwear</option>
                          <option value="Forest Products">Forest Products</option>
                          <option value="Furniture-Furnishing-Paints">Furniture-Furnishing-Paints</option>
                          <option value="General Insurance">General Insurance</option>
                          <option value="Healthcare Facilities">Healthcare Facilities</option>
                          <option value="Healthcare Services">Healthcare Services</option>
                          <option value="Heavy Electrical Equipment">Heavy Electrical Equipment</option>
                          <option value="Holding Companies">Holding Companies</option>
                          <option value="Hotels">Hotels</option>
                          <option value="Household Appliances">Household Appliances</option>
                          <option value="Household Products">Household Products</option>
                          <option value="Houseware">Houseware</option>
                          <option value="Housing Finance ">Housing Finance </option>
                          <option value="Industrial Gases">Industrial Gases</option>
                          <option value="Industrial Machinery">Industrial Machinery</option>
                          <option value="INDUSTRIAL MANUFACTURING">INDUSTRIAL MANUFACTURING</option>
                          <option value="Integrated Oil &amp; Gas">Integrated Oil &amp; Gas</option>
                          <option value="Internet &amp; Catalogue Retail">Internet &amp; Catalogue Retail</option>
                          <option value="Internet Software &amp; Services">Internet Software &amp; Services</option>
                          <option value="Investment Companies">Investment Companies</option>
                          <option value="Iron &amp; Steel Products">Iron &amp; Steel Products</option>
                          <option value="Iron &amp; Steel/Interm.Products">Iron &amp; Steel/Interm.Products</option>
                          <option value="IT Consulting &amp; Software">IT Consulting &amp; Software</option>
                          <option value="IT Software Products">IT Software Products</option>
                          <option value="Life Insurance">Life Insurance</option>
                          <option value="Marine Port &amp; Services">Marine Port &amp; Services</option>
                          <option value="MEDIA &amp; ENTERTAINMENT">MEDIA &amp; ENTERTAINMENT</option>
                          <option value="Mining">Mining</option>
                          <option value="Misc.Commercial Services">Misc.Commercial Services</option>
                          <option value="Movies &amp; Entertainment">Movies &amp; Entertainment</option>
                          <option value="Non-alcoholic Beverages">Non-alcoholic Beverages</option>
                          <option value="OIL &amp; GAS">OIL &amp; GAS</option>
                          <option value="Oil Marketing &amp; Distribution">Oil Marketing &amp; Distribution</option>
                          <option value="Other Agricultural Products">Other Agricultural Products</option>
                          <option value="Other Apparels &amp; Accessories">Other Apparels &amp; Accessories</option>
                          <option value="Other Elect.Equip./ Prod.">Other Elect.Equip./ Prod.</option>
                          <option value="Other Financial Services">Other Financial Services</option>
                          <option value="Other Industrial Goods">Other Industrial Goods</option>
                          <option value="Other Industrial Products">Other Industrial Products</option>
                          <option value="Packaged Foods">Packaged Foods</option>
                          <option value="Paper &amp; Paper Products">Paper &amp; Paper Products</option>
                          <option value="Personal Products">Personal Products</option>
                          <option value="Petrochemicals">Petrochemicals</option>
                          <option value="Pharmaceuticals">Pharmaceuticals</option>
                          <option value="Plastic Products">Plastic Products</option>
                          <option value="Publishing">Publishing</option>
                          <option value="Realty">Realty</option>
                          <option value="Refineries/ Petro-Products">Refineries/ Petro-Products</option>
                          <option value="Restaurants">Restaurants</option>
                          <option value="Roads &amp; Highways">Roads &amp; Highways</option>
                          <option value="Shipping">Shipping</option>
                          <option value="Sp.Consumer Services">Sp.Consumer Services</option>
                          <option value="Specialty Chemicals">Specialty Chemicals</option>
                          <option value="Specialty Retail">Specialty Retail</option>
                          <option value="Sugar">Sugar</option>
                          <option value="Tea &amp; Coffee">Tea &amp; Coffee</option>
                          <option value="Telecom - Alternate Carriers">Telecom - Alternate Carriers</option>
                          <option value="Telecom Cables">Telecom Cables</option>
                          <option value="Telecom Equipment">Telecom Equipment</option>
                          <option value="Telecom Services">Telecom Services</option>
                          <option value="Textiles">Textiles</option>
                          <option value="Transport Related Services">Transport Related Services</option>
                          <option value="Transportation - Logistics">Transportation - Logistics</option>
                          <option value="Travel Support Services">Travel Support Services</option>
                          <option value="Utilities:Non-Elec.">Utilities:Non-Elec.</option>
                          <option value="Zinc">Zinc</option> */}
                      </select>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-sm-6 col-md-6">

                </div>
              </div>
              {/* <!-- close Row --> */}
            </div>
          </div>
        </div>
      </div>
    </div>
    // </div >
  )
}