import React, { Component } from "react";
import { connect } from 'react-redux';
import StockAlertsTable from "components/Tables/StockAlertsTable";
import { CALL_API } from "_services/CALL_API";
import { AuthenticationService } from "_services/AuthenticationService";
import Router from "next/router";
import Loader from "./Loader";
import * as moment from 'moment';
import { faEdit, faTrashAlt, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import StockAlertModal from './Modals/StockAlertModal';
import { toast } from 'react-toastify';
import SEOPageContent from "./SEOPageContent";

const mapStateToProps = state => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn
});

const mapDispatchToProps = {
};

class StockAlerts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alertList: [],
      stockAlertData: {},
      stockAlertPopup: false,
      isLoaded: false
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.getAlertsData();
    } else {
      Router.push('/');
    }
  }
  closeStockAlertModal = () => {
    this.setState({
      stockAlertPopup: false,
      stockAlertData: {}
    }, () => {
      $('#stockAlertPopup').modal('hide');
    });
  }
  getAlertsData() {
    var params = {};
    CALL_API('POST', process.env.STOCK_ALERTS_DATA, params, res => {
      if (res.status) {
        this.setState({
          alertList: res['data']['alertMaster'],
          isLoaded: true
        });
      } else {
        this.setState({
          alertList: [],
          isLoaded: true
        });
      }
    })
  }

  openStockAlertModal = (stockData) => {
    this.setState({
      stockAlertPopup: true,
      stockAlertData: stockData
    }, () => {
      $('#stockAlertPopup').modal('show');
    });
  }

  deleteStockAlert = (masterId) => {
    CALL_API('POST', process.env.DELETE_STOCK_ALERTS_DATA, { master_id: masterId }, res => {
      if (res.status) {
        this.toastConfig['type'] = 'success';
        toast('Alert has been deleted successfully.', this.toastConfig);
        this.getAlertsData();
      } else {
        this.toastConfig['type'] = 'error';
        toast('Alert not deleted.', this.toastConfig);
      }
    })
  }

  render() {
    const { alertList, stockAlertData, stockAlertPopup, isLoaded } = this.state;
    const { pageContent } = this.props;
    var tableColumns = [];
    var tableRows = [];

    tableColumns = [
      { field: 'symbol', headerText: 'SYMBOL NAME' },
      { field: 'deliver_method', headerText: 'DELIVER METHOD' },
      { field: 'created_at', headerText: 'CREATED DATE' },
      { field: 'action', headerText: 'ACTION' },
    ];

    if (alertList && alertList.length > 0) {
      alertList.sort((a, b) => {
        var nameA = a.symbol.toLowerCase(), nameB = b.symbol.toLowerCase();
        if (nameA < nameB) {
          return -1
        }
      });
      alertList.map((item, key) => {
        var deliverMethod = '';
        if (item['deliver_method'] == 1) {
          deliverMethod = 'Desktop';
        } else if (item['deliver_method'] == 2) {
          deliverMethod = 'Email';
        } else if (item['deliver_method'] == 3) {
          deliverMethod = 'Desktop & Email';
        }
        tableRows.push({
          symbol: item['symbol'],
          deliver_method: deliverMethod,
          created_at: moment(item['created_at']).format('DD-MM-YYYY hh:mm A')
        });
      });
    }
    var alertListbody = '';
    if (alertList && alertList.length > 0) {
      alertList.sort((a, b) => {
        var nameA = a.symbol.toLowerCase(), nameB = b.symbol.toLowerCase();
        if (nameA < nameB) {
          return -1
        }
      });
      alertListbody = alertList.map((item, key) => {
        var deliverMethod = '';
        if (item['deliver_method'] == 1) {
          deliverMethod = 'Desktop';
        } else if (item['deliver_method'] == 2) {
          deliverMethod = 'Email';
        } else if (item['deliver_method'] == 3) {
          deliverMethod = 'Desktop & Email';
        }
        return (
          <React.Fragment>
            <tr key={key}>
              <td className="text-capitalize">
                <a href={process.env.OLD_SITE_URL + 'stocks-analysis/' + item["symbol"].toLowerCase()} title={item["symbol"]}>
                  {item["symbol"]}
                </a>
              </td>
              <td>{deliverMethod}</td>
              <td>
                {moment(item['created_at']).format('DD-MM-YYYY hh:mm A')}
              </td>
              <td className="action-btns">
                <div className="ellipsis-top">
                  <button className="ellipsis-btn">
                    <FontAwesomeIcon icon={faEllipsisV} width="14" height="14" />
                  </button>
                  <ul className="watchlist-dropdown-ellips homepage-dropdwn">
                    <li className="" onClick={() => this.openStockAlertModal(item)}>
                      <a title="Edit Alert">
                        <FontAwesomeIcon icon={faEdit} width="14" height="14" style={{ color: '#2196f3' }} /> Edit
                      </a>
                    </li>
                    <li className="" onClick={() => this.deleteStockAlert(item['master_id'])}>
                      <a title="Delete Alert">
                        <FontAwesomeIcon icon={faTrashAlt} width="14" height="14" style={{ color: 'red' }} /> Delete
                      </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
          </React.Fragment >
        );
      });
    }
    return (
      <React.Fragment>
        {
          isLoaded ? "" : <Loader />
        }
        <React.Fragment>
          <div className="row">
            <div className="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 p-0">
              <div className="nifty-datatable">
                <h1 className="main-page-heading">Saved Stock Alerts</h1>
                <div className="nifty-chart-panel mb-4">
                  <div className="row m-0">
                    <div className="col-lg-12 p-0">
                      {/* {
                          tableRows && tableRows.length > 0 ?
                            <StockAlertsTable tableColumns={tableColumns} tableRows={tableRows} /> : <StockAlertsTable tableColumns={[]} tableRows={[]} />
                        } */}
                      {
                        alertListbody != '' ? (
                          <div className="nifty-chart-panel mt-3 mb-3">
                            <div className="row m-0">
                              <div className="col-12 p-0">
                                <div className="nifty-datatable table-responsive watchlistTable">
                                  <table className="table">
                                    <thead>
                                      <tr>
                                        <th>SYMBOL NAME</th>
                                        <th>DELIVERY METHOD</th>
                                        <th>CREATED DATE</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>{alertListbody}</tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="row mr-0 ml-0">
                            <div className="col-12 pr-0 pl-0 padding0 text-center">
                              <img
                                src="/images/empty-watchlist.jpg"
                                alt="Stock Alert Empty"
                                title="Stock Alert Empty"
                                className="img-fluid"
                              />
                              <h6 className="mb-3">
                                You are not stock alert anything yet
                                </h6>
                            </div>
                          </div>
                        )
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {
            pageContent && pageContent['page_Content'] != '' ?
              (<div className="row">
                <div className="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 p-0">
                  <SEOPageContent pageContent={pageContent['page_Contnet']} />
                </div>
              </div>) : ''
          }
        </React.Fragment>
        <div className="modal filter_modal filter_content_modal feedback-modal show createalert-for-banking"
          id="stockAlertPopup" tabIndex="-1" role="dialog" aria-labelledby="stockAlertPopup"
          aria-hidden="true">
          {
            stockAlertPopup ? (
              <StockAlertModal stockAlertData={stockAlertData} closeStockAlertModal={this.closeStockAlertModal} />
            ) : ''
          }
        </div>
      </React.Fragment>

    );
  }
}

// export default StockAlerts;
export default connect(mapStateToProps, mapDispatchToProps)(StockAlerts);