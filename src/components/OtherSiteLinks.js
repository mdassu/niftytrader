export default function OtherSiteLinks(props) {
  return (
    <div className="container p-0">
      <div className="row top-three-nifty-boxes">
        <div className="col pr-lg-1 angel-box-responsive">
          <a
            href="https://bit.ly/2Yr3HTI"
            target="_blank"
            title="zerodha"
            className="open-account-box"
          ><img src="/images/zerodha-2.png" alt="zerodha" title="zerodha" />
            <span>Zerodha (₹20/Trade) Open Account</span>
            <div className="clearfix"></div
            ></a>
        </div>
        <div className="col pr-lg-1 pl-lg-1 angel-box-responsive-left">
          <a
            href="http://tinyurl.com/yxxcwzxn"
            target="_blank"
            title="Angel Broking"
            className="open-account-box"
          ><img
              src="/images/angel-2.png"
              alt="Angel Broking"
              title="Angel Broking" />
            <span>Angel Broking Flat ₹20 per trade</span>
            <div className="clearfix"></div
            ></a>
        </div>
        <div className="col pr-lg-1 pl-lg-1 angel-box-responsive">
          <a
            href="https://alicebluepartner.com/open-myaccount/?P=PN73&amp;nifty_trader"
            target="_blank"
            title="Aliceblue"
            className="open-account-box"
          ><img src="/images/aliceblue.png" alt="" title="Aliceblue" />
            <span>Aliceblue Flat ₹15/Trade</span>
            <div className="clearfix"></div
            ></a>
        </div>
        <div className="col pr-lg-1 pl-lg-1 angel-box-responsive-left">
          <a
            href="https://bit.ly/3y9832D"
            target="_blank"
            title="Upstox"
            className="open-account-box mr-0"
            id="ui-id-21"
          ><img src="/images/upstox.png" alt="Upstox" title="Upstox" />
            <span>Upstox Flat ₹20/Trade</span>
            <div className="clearfix"></div
            ></a>
        </div>
        <div className="col pr-2 pr-lg-1 pl-lg-1">
          <a
            href="https://bit.ly/2ZdiIJa"
            target="_blank"
            title="Trader Smart"
            className="open-account-box"
            id="ui-id-34"
          ><img
              src="/images/TraderSmart.png"
              alt="Trader Smart"
              title="Trader Smart" />
            <span>Trader Smart ₹ 15 per Trade</span>
            <div className="clearfix"></div
            ></a>
        </div>
        <div className="col pl-lg-1 angel-box-responsive-left">
          <a
            href="https://bit.ly/3gvR6WK"
            target="_blank"
            title="Compare all top brokers"
            className="open-account-box"
            id="ui-id-30"
          ><img
              src="/images/responsive_logo.svg"
              alt="Compare all top brokers"
              title="Compare all top brokers" />
            <span>Compare All Top Brokers</span>
            <div className="clearfix"></div
            ></a>
        </div>
      </div>
    </div>
  )
}
