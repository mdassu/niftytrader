import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CALL_API } from '_services/CALL_API';
import Loader from './Loader';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimesCircle,
  faCheckCircle
} from "@fortawesome/free-regular-svg-icons";
import { AuthenticationService } from '_services/AuthenticationService';
import SEOPageContent from './SEOPageContent';

const mapStateToProps = state => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn
});

const mapDispatchToProps = {
};

class Plans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      plansList: [],
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getPlansList();
  }

  getPlansList() {
    CALL_API('GET', process.env.PLANS_LIST_DATA, {}, res => {
      if (res.status) {
        this.setState({
          plansList: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          plansList: [],
          isLoaded: true
        });
      }
    });
  }

  buyNow = (planId) => {
    const { isLoggedIn, currentUser } = this.props;
    if (isLoggedIn) {
      window.location.href = "https://www1.niftytrader.in/PaytmPayment/PaymentRequestApi?plan_features_id=" + planId + "&User_Id=" + currentUser['user_id'];
    } else {
      $('#loginpopup').modal('show');
    }
  }

  render() {
    const { plansList, isLoaded } = this.state;
    const { pageContent } = this.props;

    var plans = '';
    if (plansList && plansList.length > 0) {
      plans = plansList.map((plan, key) => {
        return (
          <div className="col-lg-4">
            <div className="main_plan_box">
              <img src="images/plans/plan_1_img.png" alt="img" />
              <h5>{plan['plan_duration']} {plan['plan_duration'] > 1 ? 'Months' : 'Month'} Plan</h5>
              <div className="main_plan_price">
                <sub className="cut_price"><sup>₹</sup> {Number(plan['plan_old_price']).toFixed(2)} </sub><sup className="main_sup"> ₹</sup>{Number(plan['plan_pricing']).toFixed(2)} <sub>/ {plan['plan_duration'] == 1 ? 'Month' : plan['plan_duration'] == 3 ? 'Quarterly' : 'Year'}</sub>
              </div>
              <div className="plan_bottom_box">
                <p><span>{(100 - (Number(plan['plan_pricing']).toFixed(2) / Number(plan['plan_old_price']).toFixed(2)) * 100).toFixed(2)}% off</span> {plan['plan_short_message']}</p>
                <a onClick={() => this.buyNow(plan['plan_features_id'])} title="Subscribe">Subscribe</a>
              </div>
            </div>
          </div>
        )
      });
    }

    return (
      <React.Fragment>
        {
          isLoaded ? "" : <Loader />
        }
        <React.Fragment>
          {/* <section className="section1"> */}
          <section className="prime_plan_section">
            <div className="container p-0">
              <div className="row">
                <div className="col-md-12 p-0 mb-2 text-center">
                  <h1 className="prime_access_heading">Premium Access</h1>
                  <p className="prime_access_desc">
                    Get access to the best of NiftyTrader (website/app) features by becoming a prime member.
                        </p>
                </div>
                {/* <div className="row"> */}
                {
                  plansList && plansList.length > 0 ? (
                    <div className="col-12 p-0">
                      <div className="row">
                        {/* {plans} */}


                        <div className="col-lg-4">
                          <div className="main_plan_box">
                            <img src="images/plans/plan_1_img.png" alt="img" />
                            <h5>{plansList[0]['plan_duration']} {plansList[0]['plan_duration'] > 1 ? 'Months' : 'Month'} Plan</h5>
                            <div className="main_plan_price">
                              <sub className="cut_price"><sup>₹</sup> {Math.floor(Number(plansList[0]['plan_old_price']))} </sub><sup className="main_sup"> ₹</sup>{Math.floor(Number(plansList[0]['plan_pricing']))} <sub>/ {plansList[0]['plan_duration'] == 1 ? 'Month' : plansList[0]['plan_duration'] == 3 ? 'Quarterly' : 'Year'}</sub>
                            </div>
                            <div className="plan_bottom_box">
                              <p><span> {Math.floor(100 - (Number(plansList[0]['plan_pricing']) / Number(plansList[0]['plan_old_price'])) * 100)}% off</span> {plansList[0]['plan_short_message']}</p>
                              <a onClick={() => this.buyNow(plansList[0]['plan_features_id'])} title="Subscribe">Subscribe</a>
                            </div>
                          </div>
                        </div>


                        <div className="col-lg-4">
                          <div className="main_plan_box">
                            <img src="images/plans/plan_3_img.png" alt="img" />
                            <h5>{plansList[2]['plan_duration']} {plansList[2]['plan_duration'] > 1 ? 'Months' : 'Month'} Plan</h5>
                            <div className="main_plan_price">
                              <sub className="cut_price"><sup>₹</sup> {Math.floor(Number(plansList[2]['plan_old_price']))} </sub><sup className="main_sup"> ₹</sup>{Math.floor(Number(plansList[2]['plan_pricing']))} <sub>/ {plansList[2]['plan_duration'] == 1 ? 'Month' : plansList[2]['plan_duration'] == 3 ? 'Quarterly' : 'Year'}</sub>
                            </div>
                            <div className="plan_bottom_box">
                              <p><span>{Math.floor(100 - (Number(plansList[2]['plan_pricing']) / Number(plansList[2]['plan_old_price'])) * 100)}% off</span> {plansList[2]['plan_short_message']}</p>
                              <a onClick={() => this.buyNow(plansList[2]['plan_features_id'])} title="Subscribe">Subscribe</a>
                            </div>
                          </div>
                        </div>



                        <div className="col-lg-4">
                          <div className="main_plan_box">
                            <img src="images/plans/plan_2_img.png" alt="img" />
                            <h5>{plansList[1]['plan_duration']} {plansList[1]['plan_duration'] > 1 ? 'Months' : 'Month'} Plan</h5>
                            <div className="main_plan_price">
                              <sub className="cut_price"><sup>₹</sup> {Math.floor(Number(plansList[1]['plan_old_price']))} </sub><sup className="main_sup"> ₹</sup>{Math.floor(Number(plansList[1]['plan_pricing']))} <sub>/ {plansList[1]['plan_duration'] == 1 ? 'Month' : plansList[1]['plan_duration'] == 3 ? 'Quarterly' : 'Year'}</sub>
                            </div>
                            <div className="plan_bottom_box">
                              <p><span>{Math.floor(100 - (Number(plansList[1]['plan_pricing']) / Number(plansList[1]['plan_old_price'])) * 100)}% off</span> {plansList[1]['plan_short_message']}</p>
                              <a onClick={() => this.buyNow(plansList[1]['plan_features_id'])} title="Subscribe">Subscribe</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : ''
                }

                {/* </div> */}
              </div>
            </div>
          </section>
          <section className="include_features_section">
            <div className="container p-0">
              <div className="row">
                <div className="col-12 p-0">
                  <div className="row">
                    <div className="col-12 mb-5">
                      <h2 className="prime_access_heading text-center">Powerful features included in all plans</h2>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/adv_scr_feature.png" alt="img" />
                        <h5>Advanced Screener (EOD)</h5>
                        <p>Eod Research & Analysis: Data Export, Custom Filters & Daily alerts</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/liv_mar_feature.png" alt="img" />
                        <h5>Live Market Screener</h5>
                        <p>Real Time Technical & Financial Filters</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/option_scr_feature.png" alt="img" />
                        <h5>Options Screener</h5>
                        <p>Filter option chair to find relevant options</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/ads_free_feature.png" alt="img" />
                        <h5>Ads Free Experience</h5>
                        <p>Enjoy Hassle-free & Ads-free experience with complete access on app</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/comp_stock_feature.png" alt="img" />
                        <h5>Comprehensive Stock Analysis</h5>
                        <p>Detailed Analysis of top stocks</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/unlimited_watch.png" alt="img" />
                        <h5>Unlimited Watchlists</h5>
                        <p>Create & track stocks</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/nifty_bank_feature.png" alt="img" />
                        <h5>Nifty & Bank Nifty Live Analytics</h5>
                        <p>Trend, Momentum and more</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/alert_nifty_feature.png" alt="img" />
                        <h5>Alerts Nifty Trader</h5>
                        <p>In-App, Email & Desktop native call and important triggers</p>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="plan_feature_content">
                        <img src="images/plans/option_chain_feature.png" alt="img" />
                        <h5>Option Chain Analysis</h5>
                        <p>Technical study of option chain</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* </section> */}
          {
            pageContent && pageContent['page_Content'] != '' ?
              (<div className="row">
                <div className="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 p-0">
                  <SEOPageContent pageContent={pageContent['page_Contnet']} />
                </div>
              </div>) : ''
          }
          {/* <!-- section eod screener -->  */}
        </React.Fragment>
      </React.Fragment>
    )
  }
}

// export default Plans;
export default connect(mapStateToProps, mapDispatchToProps)(Plans);