import React from "react";
import parse from 'html-react-parser';


export default function SEOPageContent(props) {
  const { pageContent } = props;
  return (
    <React.Fragment>
      {
        pageContent != '' && pageContent != undefined ? (
          <div className="max_plain_content">
            <p className="mb-0">{parse(pageContent)}</p>
          </div>
        ) : ''
      }
    </React.Fragment>
  );
}
