import React, { useState, useEffect, Component } from 'react';
import Link from 'next/link'

class MarketUpdates extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    var marquee = document.getElementsByTagName('marquee')[0];
    marquee.onmouseover = function () {
      this.stop();
    }
    marquee.onmouseout = function () {
      this.start();
    }
  }

  render() {
    const { newsMarket } = this.props;
    return (
      <div className="container p-0">
        <div className="row nifty-top-news-bar-row">
          <div className="col-xl-12 col-lg-12 col-md-12 p-0">
            <div className="nifty-top-news-bar">
              <span className="update-class">market update</span>
              <marquee
              >
                {
                  newsMarket && newsMarket.length > 0 ? newsMarket.map((item, key) => {
                    return (
                      <a
                        key={key}
                        href={item['url']}
                        title={item['title']}
                      >
                        <img src="/images/news-before-arrow.png" className="" alt="arrow" />
                        {item['title']}
                      </a>
                    )
                  }) : ''
                }
              </marquee>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MarketUpdates;
