import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CALL_API } from '_services/CALL_API';
import * as moment from 'moment';
import Link from 'next/link';

const mapStateToProps = state => ({
  currentUser: state.userData.userData
});

const mapDispatchToProps = (dispatch) => ({
  setUserData: data => {
    dispatch(userData(data))
  },
  setUserLoggedIn: data => {
    dispatch(userLoggedIn(data))
  }
});

class ThankYouPurchase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planDetails: {},
      planName: ''
    }
  }

  componentDidMount() {
    this.getPlanDetails();
    this.getCurrentUserData()
  }

  getPlanDetails() {
    CALL_API('POST', process.env.USER_PLAN_DATA, {}, res => {
      if (res.status) {
        res['data'][0]['order_plans_json'] = JSON.parse(res['data'][0]['order_plans_json']);
        this.setState({
          planDetails: res['data'][0],
          planName: res['data'][0]['order_plans_json'][0]['plan_name']
        });
      }
    });
  }

  getCurrentUserData = () => {
    if (Cookies.get('_accessToken')) {
      CALL_API('POST', process.env.CURRENT_USER_DATA, {}, res => {
        if (res.status) {
          let data = res.data;
          let userData = data.user_details;
          let planData = data.plan_data;
          let user = {
            name: userData['name'],
            city: userData['city'],
            country: userData['country'],
            date_of_birth: userData['date_of_birth'],
            email: userData['email'],
            gender: userData['gender'],
            industry: userData['industry'],
            membership_flag: data['membership_flag'],
            occupation: userData['occupation'],
            phone_no: userData['phone_no'],
            pincode: userData['pincode'],
            plan_type: planData['type'],
            state: userData['state'],
            user_id: userData['id']
          }
          this.props.setUserData(user)
          this.props.setUserLoggedIn(true)
        }
      });
    }
  }

  render() {
    const { planDetails, planName } = this.state;
    return (
      <div className="row">
        <div className="col-lg-12 p-0  text-center">
          <div className="content-body page404">
            <img src="/images/thank you.png" alt="" className="img-fluid" />
            <h1>
              Thank you for the Purchase.
              </h1>
            <p>Your order has been placed successfully.</p>
            <div className="row mt-4" >
              <div className="col-md-7 col-lg-7 col-xl-7 col-sm-7 col-12">
                <div className="nifty-datatable">
                  <div className="nifty-chart-panel mb-4 ">
                    <div className="row m-0">
                      <div className="col-lg-12">
                        <div className="nifty-datatable table-responsive ">
                          <h4 className="pb-1 text-left">Order details:</h4>
                          <table className="table">
                            <thead>
                              <tr>
                                <th >Order Id</th>
                                <th >Date</th>
                                <th >Transaction Id</th>
                                <th >Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="text-left">#{planDetails['order_id']}  </td>
                                <td className="text-left">{moment(planDetails['order_date_time']).format('MM/DD/YYYY hh:mm:ss A')}</td>
                                <td className="text-left">{planDetails['txn_payment_id']}</td>
                                <td className="text-left">Rs. {Number(planDetails['order_total_with_gst']).toFixed(2)}/-</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-5 col-lg-5 col-xl-5 col-sm-5 col-12 ">
                <div className="nifty-datatable">
                  <div className="nifty-chart-panel mb-4 ">
                    <div className="row m-0">
                      <div className="col-lg-12">
                        <div className="nifty-datatable table-responsive ">
                          <h4 className="pb-1 text-left">Plan details:</h4>
                          <table className="table">
                            <thead>
                              <tr>
                                <th >Plan</th>
                                <th >Status</th>
                                <th >Duration</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="text-left">{planName}</td>
                                {planDetails['is_active'] == "P" ? <td className="text-left">Active</td> : <td className="text-left percent-red">Deactive</td>}

                                {/* If plan has expired the show this class */}
                                {/* <td className="text-left percent-red">Deactive</td> */}
                                <td className="text-left">{moment(planDetails['start_date']).format('MM/DD/YYYY')} to {moment(planDetails['end_date']).format('MM/DD/YYYY')}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Link href="/">
              <a title=" Back to Homepage" className="text-center">
                Back to Homepage
                </a>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

// export default ThankYouPurchase;
export default connect(mapStateToProps, mapDispatchToProps)(ThankYouPurchase);