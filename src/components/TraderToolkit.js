import Link from 'next/link';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleRight
} from "@fortawesome/free-solid-svg-icons";

export default function TraderToolkit() {
  return (
    <div className="homepage-newsbox right-sidebar-panel">
      <h5 className="text-uppercase">Trader’s Toolkit</h5>
      <ul className="inner-right-sidebar">
        <li>
          <Link href="/options-max-pain-chart-live/nifty">
            <a
              title="Options Max Pain Analysis Chart"
            >
              <FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />Options Max Pain Analysis
                  Chart</a>
          </Link>
        </li>

        <li>
          <Link href="/option-strategies">
            <a href="/option-strategies" title="Nifty Option Strategy"
            ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />Nifty Option Strategy</a>
          </Link>
        </li>

        <li>
          <a href={process.env.OLD_SITE_URL + "select-best-broker"} title="Compare Brokers"
          ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />Compare Brokers</a
          >
        </li>

        <li className="toolkit-item clearfix">
          <a
            href={process.env.OLD_SITE_URL + "options-trading"}
            target="_blank"
            title="NSE Stocks Option Chain"
          ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />NSE Stocks Option Chain</a
          >
        </li>

        <li>
          <a
            href={process.env.OLD_SITE_URL + "fii-dii-activity"}
            title="FII DII Activity for whole month"
          ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />FII DII Activity for whole
                  month</a
          >
        </li>

        <li className="toolkit-item clearfix">
          <a
            href={process.env.OLD_SITE_URL + "pivot-calculator"}
            target="_blank"
            title="Pivot Point Calculator | Fibonacci Pivots"
          ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />Pivot Point Calculator |
                  Fibonacci Pivots
                </a>
        </li>
        <li>
          <a
            href={process.env.OLD_SITE_URL + "option-pricing-calculator"}
            title="Black Scholes Option Pricing Calculator"
          ><FontAwesomeIcon icon={faAngleRight} width="6.5" height="13" style={{ marginRight: '10px' }} />Black Scholes Option Pricing
                  Calculator
                </a>
        </li>
      </ul>
    </div>
  )
}