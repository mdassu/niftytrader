import Loader from "components/Loader";
import { set } from "js-cookie";
import React, { Component } from "react";
import { CALL_API } from "_services/CALL_API";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import OptionsScreenerTable from "components/Tables/OptionsScreenerTable";
import OptionScreenerModal from "components/Modals/OptionScreenerModal";
import { AuthenticationService } from "_services/AuthenticationService";
import { ToastContainer, toast } from "react-toastify";
import Router from "next/router";
import SEOPageContent from "components/SEOPageContent";
import { ThumbSettings } from "@syncfusion/ej2-charts";
import TreeTable from "components/Tables/TreeTable";
import Link from "next/link";
import { connect } from "react-redux";
import { Screener } from "./ScreenerIntitalData";

class OptionScreener extends Component {
  constructor(props) {
    super(props);
    this.intiSelectedFilterFields = Screener.intiSelectedFilterFields;

    this.twoStocks = [
      { label: "NIFTY", value: "NIFTY" },
      { label: "BANKNIFTY", value: "BANKNIFTY" },
    ];
    this.state = {
      intiMarketScreenerData: [],
      marketScreenerData: [],
      selectedFilterFields: Object.assign({}, this.intiSelectedFilterFields),
      tempSelectedFilterFields: Object.assign(
        {},
        this.intiSelectedFilterFields
      ),
      filterName: "",
      savedFilterList: "",
      currentUser: {},
      editableScreenerId: null,
      filterType: "indices",
      allStocks: [],
      stocks: this.twoStocks,
      appliedFilterStatus: false,
      isLoaded: false,
      includeOiCheck: true,
      includeVolCheck: true,
      proximityCheck: false,
      includeOiNone: false,
      includeVolNone: false,
      includeProxNone: false,
      oiSelectVal: null,
      volSelectVal: null,
      proxSelectVal: null,
      primeMember: false,
      userLogin: this.props.isLoggedIn,
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };

    this.fieldName = Screener.fieldName;
  }

  componentDidMount() {
    const { userData, isLoggedIn } = this.props;

    this.setState({
      userLogin: isLoggedIn,
    });

    if (userData !== null || userData !== undefined) {
      if (userData.membership_flag === "1") {
        this.getSymbolList();
        this.getLiveMarketData();
        this.getFilterList();
        this.setState({
          primeMember: true,
          appliedFilterStatus: true,
        });
      } else {
        this.setState({
          primeMember: false,
          appliedFilterStatus: false,
          isLoaded: true
        });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { userData, isLoggedIn } = this.props;

    if (prevProps !== this.props) {
      this.setState(
        {
          userLogin: isLoggedIn,
          primeMember: userData.membership_flag == "1" ? true : false,
        },
        () => {
          if (isLoggedIn && userData.membership_flag == "0") {
            Router.push("/primeplans");
          }
        }
      );
    }

    if (prevProps.userData !== userData) {
      if (userData !== null || userData !== undefined) {
        if (userData.membership_flag == "1") {
          this.getSymbolList();
          this.getLiveMarketData();
          this.getFilterList();
          this.setState({
            primeMember: true,
            appliedFilterStatus: true,
          });
        } else {
          this.setState({
            primeMember: false,
            appliedFilterStatus: false,
          });
        }
      }
    }

    if (prevState.selectedFilterFields !== this.state.selectedFilterFields) {
      this.fieldName["exclude_oi"][
        "fieldName"
      ] = `OI > ${this.state.selectedFilterFields["include_oi_val"]}`;
      this.fieldName["exclude_volume"][
        "fieldName"
      ] = `Volume > ${this.state.selectedFilterFields["include_volume_val"]}`;
      this.fieldName["proximity"][
        "fieldName"
      ] = `Proximity > ${this.state.selectedFilterFields["proximity_val"]}%`;

      this.setState(
        {
          oiSelectVal: this.state.selectedFilterFields["include_oi_val"],
          volSelectVal: this.state.selectedFilterFields["include_volume_val"],
          proxSelectVal: this.state.selectedFilterFields["proximity_val"],
        },
        () => { }
      );
    }
  }

  getSymbolList() {
    CALL_API("GET", process.env.SYMBOL_LIST_DATA, {}, (res) => {
      if (res.status) {
        var allStocks = [];
        res["data"].map((item) => {
          if (item["symbol_name"] != "NIFTY" || item["symbol"] != "BANKNIFTY") {
            allStocks.push({
              label: item["symbol_name"],
              value: item["symbol_name"],
            });
          }
        });
        this.setState({
          allStocks: allStocks,
        });
      } else {
        this.setState({
          stockList: [],
        });
      }
    });
  }

  getLiveMarketData() {
    var params = Object.assign({}, this.state.selectedFilterFields);
    var symbols = [];
    params["symbol"].map((item) => {
      symbols.push(item["label"]);
    });
    params["symbol"] = symbols;
    CALL_API("POST", process.env.OPTIONS_SCREENER_DATA, params, (res) => {
      if (res.status) {
        if (res["data"].length > 0) {
          this.setState(
            {
              intiMarketScreenerData: res["data"],
              isLoaded: true,
            },
            () => {
              this.calculateTableRows();
            }
          );
        } else {
          this.setState({
            intiMarketScreenerData: [],
            marketScreenerData: [],
            isLoaded: true,
          });
        }
        $("#saved-filter").modal("hide");
      } else {
        this.setState({
          intiMarketScreenerData: [],
          marketScreenerData: [],
          isLoaded: true,
        });
        $("#all_filters").modal("hide");
      }
    });
  }

  calculateTableRows() {
    const { selectedFilterFields, intiMarketScreenerData } = this.state;
    var tableData = [];
    var months = [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ];
    intiMarketScreenerData.map((item, key) => {
      var procced = false;
      if (selectedFilterFields["expiry_m"] && !selectedFilterFields["weekly"]) {
        var exist = 0;
        months.map((month, key) => {
          if (item["instType"] && item["instType"].search(month) != -1) {
            exist++;
          }
          if (key == 11) {
            if (exist > 0) {
              procced = false;
            } else {
              procced = true;
            }
          }
        });
      } else if (
        !selectedFilterFields["expiry_m"] &&
        selectedFilterFields["weekly"]
      ) {
        var exist = 0;
        months.map((month, key) => {
          if (item["instType"] && item["instType"].search(month) != -1) {
            exist++;
          }
          if (key == 11) {
            if (exist > 0) {
              procced = true;
            } else {
              procced = false;
            }
          }
        });
      } else if (
        selectedFilterFields["expiry_m"] &&
        selectedFilterFields["weekly"]
      ) {
        procced = false;
      }

      if (
        selectedFilterFields["exclude_oi"] ||
        selectedFilterFields["exclude_volume"] ||
        selectedFilterFields["include_Ltp"]
      ) {
        if (
          selectedFilterFields["exclude_oi"] &&
          !selectedFilterFields["exclude_volume"] &&
          !selectedFilterFields["include_Ltp"]
        ) {
          if (item["oi"] <= 0) {
            procced = true;
          }
        } else if (
          selectedFilterFields["exclude_volume"] &&
          !selectedFilterFields["exclude_oi"] &&
          !selectedFilterFields["include_Ltp"]
        ) {
          if (item["volume"] <= 0) {
            procced = true;
          }
        } else if (
          selectedFilterFields["include_Ltp"] &&
          !selectedFilterFields["exclude_volume"] &&
          !selectedFilterFields["exclude_oi"]
        ) {
          if (item["ltp"] <= 0) {
            procced = true;
          }
        }
      }

      if (
        selectedFilterFields["exclude_oi"] &&
        selectedFilterFields["exclude_volume"] &&
        selectedFilterFields["include_Ltp"]
      ) {
        if (item["oi"] <= 0) {
          procced = true;
        }
        if (item["volume"] <= 0) {
          procced = true;
        }
        if (item["ltp"] <= 0) {
          procced = true;
        }
      }

      if (!procced) {
        tableData.push(item);
      }
      if (intiMarketScreenerData.length - 1 == key) {
        this.setState({
          marketScreenerData: tableData,
        });
      }
    });
    $("#all_filters").modal("hide");
  }

  handleFilterFields = (event) => {
    var tempSelectedFilterFields = Object.assign(
      {},
      this.state.tempSelectedFilterFields
    );

    if (Array.isArray(event)) {
      tempSelectedFilterFields["symbol"] = event;
    } else {
      if (event.target.type === "checkbox") {
        tempSelectedFilterFields[event.target.name] = event.target.checked;
      } else {
        if (event.target.name === "include_if_oi_Select") {
          tempSelectedFilterFields["include_oi"] = true;
          tempSelectedFilterFields["include_oi_val"] =
            event.target.value === "null" ? null : parseInt(event.target.value);
          tempSelectedFilterFields["exclude_oi"] = true;
          this.fieldName["exclude_oi"][
            "fieldName"
          ] = `OI > ${event.target.value}`;
          this.setState({
            includeOiNone: false,
            includeOiCheck: true,
          });
        }

        if (event.target.name === "include_if_volume_Select") {
          tempSelectedFilterFields["include_volume"] = true;
          tempSelectedFilterFields["include_volume_val"] =
            event.target.value === "null" ? null : parseInt(event.target.value);
          tempSelectedFilterFields["exclude_volume"] = true;
          this.fieldName["exclude_volume"][
            "fieldName"
          ] = `Volume > ${event.target.value}`;
          this.setState({
            includeVolNone: false,
            includeVolCheck: true,
          });
        }

        if (event.target.name === "proximity_select") {
          tempSelectedFilterFields["proximity"] = true;
          tempSelectedFilterFields["proximity_val"] =
            event.target.value === "null" ? null : parseInt(event.target.value);
          this.fieldName["proximity"][
            "fieldName"
          ] = `Proximity <= ${event.target.value}%`;
          this.setState({
            includeProxNone: false,
            proximityCheck: true,
          });
        }
      }
    }
    this.setState({
      tempSelectedFilterFields: tempSelectedFilterFields,
      selectedFilterFields: tempSelectedFilterFields,
    });
  };

  applyFilter = () => {
    if (this.state.tempSelectedFilterFields["symbol"].length > 0) {
      this.setState(
        {
          selectedFilterFields: this.state.tempSelectedFilterFields,
        },
        () => {
          this.getLiveMarketData();
        }
      );
    }
  };

  resetFilter = () => {
    this.fieldName["exclude_oi"]["fieldName"] = "OI > 100000";
    this.fieldName["exclude_volume"]["fieldName"] = "Volume > 100000";

    this.setState(
      {
        includeOiCheck: false,
        includeVolCheck: false,
        proximityCheck: false,
        includeVolNone: false,
        includeOiNone: false,
        includeProxNone: false,
        selectedFilterFields: this.intiSelectedFilterFields,
        filterType: "indices",
      },
      () => {
        this.getLiveMarketData();
        // $('#all_filter').modal('hide');
      }
    );
  };

  resetCheckBox = () => {
    this.setState({
      tempSelectedFilterFields: this.intiSelectedFilterFields,
    });
  };

  removeFilterField = (key) => {
    var selectedFilterFields = this.state.selectedFilterFields;
    selectedFilterFields[key] = false;

    if (key === "exclude_oi") {
      selectedFilterFields["include_oi_val"] = null;

      this.setState({
        includeOiNone: true,
      });
    } else if (key === "exclude_volume") {
      selectedFilterFields["include_volume_val"] = null;
      this.setState({
        includeVolNone: true,
      });
    } else if (key === "proximity") {
      selectedFilterFields["proximity_val"] = null;
      this.setState({
        includeProxNone: true,
      });
    }

    var trueValueCount = Object.keys(selectedFilterFields).filter((item) => {
      return selectedFilterFields[item];
    }).length;

    if (trueValueCount) {
      this.setState(
        {
          selectedFilterFields: selectedFilterFields,
        },
        () => {
          this.applyFilter();
        }
      );
    } else {
      this.resetFilter();
    }
  };

  openFilterPopup = () => {
    this.setState(
      {
        tempSelectedFilterFields: Object.assign(
          {},
          this.state.selectedFilterFields
        ),
      },
      () => {
        $("#all_filters").modal("show");
      }
    );
  };

  changeFilterType = (event) => {
    var tempSelectedFilterFields = Object.assign(
      {},
      this.state.tempSelectedFilterFields
    );
    tempSelectedFilterFields["Underlyingprice_Increase"] = false;
    tempSelectedFilterFields["Underlyingprice_decrease"] = false;
    tempSelectedFilterFields["weekly"] = false;
    tempSelectedFilterFields["expiry_m"] = false;

    if (event.target ? event.target.value == "indices" : event == "indices") {
      this.setState({
        stocks: this.twoStocks,
      });
      tempSelectedFilterFields["symbol"] = [this.twoStocks[0]];
      tempSelectedFilterFields["FilterType"] = true;
    } else {
      this.setState({
        stocks: this.state.allStocks,
      });
      tempSelectedFilterFields["symbol"] = [this.state.allStocks[0]];
      tempSelectedFilterFields["FilterType"] = false;
    }

    this.setState({
      tempSelectedFilterFields: tempSelectedFilterFields,
    });
    this.setState({
      filterType: event.target ? event.target.value : event,
    });
  };

  getFilterList = () => {
    var params = {
      screener_user_id: this.state.currentUser["user_id"],
    };
    CALL_API(
      "POST",
      process.env.OPTIONS_SCREENER_FILTER_LIST_DATA,
      params,
      (res) => {
        if (res.status) {
          this.setState({
            savedFilterList: res["data"],
          });
        } else {
          this.setState({
            savedFilterList: [],
          });
        }
      }
    );
  };

  handleFilterNameChange = (event) => {
    this.setState({
      filterName: event.target.value,
    });
  };

  saveFilter = () => {
    if (this.state.filterName != "") {
      if (this.state.editableScreenerId == null) {
        var params = {
          screener_name: this.state.filterName,
          screener_json: JSON.stringify(this.state.tempSelectedFilterFields),
          screener_user_id: this.state.currentUser["user_id"],
          screener_id: null,
          Source_App: null,
        };

        CALL_API(
          "POST",
          process.env.SAVE_OPTIONS_SCREENER_FILTER_DATA,
          params,
          (res) => {
            if (res.status) {
              $("#save-filter").modal("hide");
              this.getFilterList();
              this.setState({
                filterName: "",
              });
              this.toastConfig["type"] = "success";
              toast(res["message"], this.toastConfig);
            } else {
              this.toastConfig["type"] = "error";
              toast(res["message"], this.toastConfig);
            }
          }
        );
      } else {
        var params = {
          screener_name: this.state.filterName,
          screener_json: JSON.stringify(this.state.tempSelectedFilterFields),
          screener_user_id: this.state.currentUser["user_id"],
          screener_id: this.state.editableScreenerId,
          Source_App: null,
        };

        CALL_API(
          "POST",
          process.env.UPDATE_OPTIONS_SCREENER_FILTER_DATA,
          params,
          (res) => {
            if (res.status) {
              $("#save-filter").modal("hide");
              this.getFilterList();
              this.setState({
                filterName: "",
              });
              this.toastConfig["type"] = "success";
              toast(res["message"], this.toastConfig);
            } else {
              this.toastConfig["type"] = "error";
              toast(res["message"], this.toastConfig);
            }
          }
        );
      }
    } else {
      this.toastConfig["type"] = "error";
      toast("Please enter filter name", this.toastConfig);
    }
  };

  deleteFilter = (filterId, key) => {
    var params = {
      screener_id: filterId,
    };

    CALL_API(
      "POST",
      process.env.DELETE_OPTIONS_SCREENER_FILTER_DATA,
      params,
      (res) => {
        if (res.status) {
          this.toastConfig["type"] = "success";
          toast(res["message"], this.toastConfig);
          var filterList = this.state.savedFilterList;
          filterList.splice(1, key);
          this.setState(
            {
              savedFilterList: filterList,
            },
            () => {
              if (this.state.editableScreenerId == filterId) {
                this.setState(
                  {
                    appliedFilterStatus: false,
                    selectedFilterFields: Object.assign(
                      {},
                      this.intiSelectedFilterFields
                    ),
                    tempSelectedFilterFields: Object.assign(
                      {},
                      this.intiSelectedFilterFields
                    ),
                  },
                  () => {
                    this.getLiveMarketData();
                    this.setState({
                      appliedFilterStatus: true,
                    });
                  }
                );
              }
            }
          );
          this.getFilterList();
        } else {
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      }
    );
  };

  editFilter = (filterId) => {
    var filterData = this.state.savedFilterList.filter((item) => {
      return item["screener_id"] == filterId;
    })[0];
    $("#saved-filter").modal("hide");
    this.setState(
      {
        tempSelectedFilterFields: JSON.parse(filterData["screener_json"]),
        filterName: filterData["screener_name"],
        editableScreenerId: filterId,
      },
      () => {
        $("#all_filters").modal("show");
      }
    );
  };

  savedFilterApply = (filterId) => {
    var filterData = this.state.savedFilterList.filter((item) => {
      return item["screener_id"] == filterId;
    })[0];
    $("#saved-filter").modal("hide");
    this.setState(
      {
        selectedFilterFields: JSON.parse(filterData["screener_json"]),
        tempSelectedFilterFields: JSON.parse(filterData["screener_json"]),
        editableScreenerId: filterId,
      },
      () => {
        this.setState(
          {
            filterType:
              this.state.tempSelectedFilterFields["FilterType"] === true
                ? "indices"
                : "stocks",
          },
          () => {
            this.changeFilterType(this.state.filterType);
          }
        );

        this.applyFilter();
      }
    );
  };

  openSavedFilterPopup = () => {
    if (this.state.savedFilterList.length > 0) {
      $("#saved-filter").modal("show");
    } else {
      this.toastConfig["type"] = "error";
      toast("No filter found...", this.toastConfig);
    }
  };

  explorePlans = () => {
    const { isLoggedIn } = this.props;
    const { primeMember, userLogin } = this.state;

    if (userLogin) {
      if (primeMember !== true) {
        this.setState({
          primeMember: false,
        });
        Router.push("/primeplans");
      } else {
        this.setState({
          primeMember: true,
        });
      }
    } else {
      $("#loginpopup").modal("show");
    }
  };

  render() {
    const {
      marketScreenerData,
      selectedFilterFields,
      filterType,
      tempSelectedFilterFields,
      savedFilterList,
      filterName,
      editableScreenerId,
      stocks,
      appliedFilterStatus,
      isLoaded,
    } = this.state;

    const { title, pageContent } = this.props;
    var tableColumns = [
      { field: "instType", headerText: "INST. TYPE" },
      { field: "oi", headerText: "OI" },
      { field: "changeOi", headerText: "CHNG IN OI" },
      { field: "ltp", headerText: "LTP" },
      { field: "changeltp", headerText: "CHNG IN LTP" },
      { field: "openVal", headerText: "OPEN" },
      { field: "highval", headerText: "HIGH" },
      { field: "lowVal", headerText: "LOW" },
      { field: "volume", headerText: "VOLUME" },
    ];
    var tableRows = [];

    if (marketScreenerData && marketScreenerData.length > 0) {
      marketScreenerData.map((item, key) => {
        tableRows.push({
          instType: item["instType"],
          oi: Number(item["oi"]),
          changeOi: Number(item["changeOi"]),
          ltp: Number(item["ltp"]).toFixed(2),
          changeltp: Number(item["changeltp"]).toFixed(2),
          openVal: Number(item["openVal"]).toFixed(2),
          highval: Number(item["highval"]).toFixed(2),
          lowVal: Number(item["lowVal"]).toFixed(2),
          volume: Number(item["volume"]),
        });
      });
    }

    var savedList = "";
    if (savedFilterList && savedFilterList.length > 0) {
      savedList = savedFilterList.map((item, key) => {
        return (
          <li key={key}>
            <h5
              className="text-left float-left filter-h-color"
              style={{ minWidth: "82%" }}
              onClick={() => this.savedFilterApply(item["screener_id"])}
            >
              {item["screener_name"]}
            </h5>
            <div className="save_btn text-right float-right">
              <button
                className="edit"
                title="Edit"
                onClick={() => this.editFilter(item["screener_id"])}
              >
                <FontAwesomeIcon icon={faEdit} width="18" height="16" />
                {/* <!-- <img src="/images/edit01.svg" alt="edit01" title="Edit" /> --> */}
              </button>
              <button
                className="delete"
                title="Delete"
                onClick={() => this.deleteFilter(item["screener_id"], key)}
              >
                <i className="far fa-trash-alt"></i>
                <FontAwesomeIcon icon={faTrashAlt} width="14" height="16" />
                {/* <!-- <img src="/images/advance-stock/Delete_new.svg" title="Delete" className="delete_btn" alt="Delete01"> --> */}
              </button>
            </div>
          </li>
        );
      });
    }

    return (
      <React.Fragment>
        {/* <ToastContainer /> */}
        {
          isLoaded ? "" : <Loader />
        }

        <div className="row">
          <div className="col-xl-6 col-md-6 col-sm-6 pl-lg-0">
            <h1 className="main-page-heading stock-screener stock_main_heading">
              {" "}
              {pageContent["page_Content_Title"]}
            </h1>
          </div>
          {this.state.primeMember && (
            <div className="col-xl-6 col-md-6 col-sm-6 advance_stock_screener pr-lg-0 pb-2">
              <div className="text-right right_refresh_btns">
                <button className="refresh_btn" onClick={this.resetFilter}>
                  <img src="/images/reset.svg" title="Reset" alt="reset" />
                  {/* <div className="badge clear_badge">5</div> */}
                </button>
                {/* <button
                    title="Filter"
                    className="stock_filter text-uppercase"
                    onClick={this.openFilterPopup}
                  >
                    <FontAwesomeIcon icon={faFilter} width="14" height="14" />Columns Filter{" "}
                  </button> */}
                <button
                  onClick={this.openSavedFilterPopup}
                  title="Saved Filters"
                  className="save_filters text-uppercase"
                >
                  {" "}
                    Saved Filters{" "}
                </button>
                <button
                  // data-toggle="modal"
                  // data-target="#all_filters"
                  title="Filter"
                  className="stock_filter text-uppercase"
                  onClick={this.openFilterPopup}
                >
                  <FontAwesomeIcon icon={faFilter} width="14" height="14" />{" "}
                    Filter{" "}
                </button>
              </div>
            </div>
          )}

          {appliedFilterStatus ? (
            <div className="row m-0">
              <div className="col-12 col-sm-12 col-xl-12 col-lg-12 col-md-12 pl-0 pl-15 pr-0 pr-15">
                <span className="slider_heading">Applied Filters:</span>
                <div
                  className="stock_apply_filter filtering stock_apply_filter_2"
                  id="eod_filter"
                >
                  <ul>
                    <li>
                      Symbol:{" "}
                      <span>({selectedFilterFields["symbol"].length})</span>
                    </li>
                    {Object.keys(this.intiSelectedFilterFields).map(
                      (item, key) => {
                        return (
                          <React.Fragment key={key}>
                            {this.state.selectedFilterFields[item] &&
                              this.fieldName[item] ? (
                              this.fieldName[item]["fieldName"] ===
                                "Volume > null" ? (
                                ""
                              ) : this.fieldName[item]["fieldName"] ===
                                "OI > null" ? (
                                ""
                              ) : this.fieldName[item]["fieldName"] ===
                                "Proximity <= null%" ? (
                                ""
                              ) : this.fieldName[item]["fieldName"] ===
                                "Proximity > null%" ? (
                                ""
                              ) : (
                                <li>
                                  {this.fieldName[item]["groupName"]}:{" "}
                                  <img
                                    title="Remove"
                                    className="close_img"
                                    src="/images/cross.svg"
                                    alt="Remove"
                                    onClick={() =>
                                      this.removeFilterField(item)
                                    }
                                  />
                                  <span>
                                    {this.fieldName[item]["fieldName"]}
                                  </span>
                                </li>
                              )
                            ) : (
                              ""
                            )}
                          </React.Fragment>
                        );
                      }
                    )}
                  </ul>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}

          <div className="col-lg-12 p-0">
            <div className="">
              <div className="row m-0">
                {this.state.primeMember === true ? (
                  <div className="col-lg-12 p-0">
                    <div className="nifty-datatable table-responsive live-m-table-nifty">
                      {tableRows && tableRows.length > 0 ? (
                        <OptionsScreenerTable
                          tableColumns={tableColumns}
                          tableRows={tableRows}
                        />
                      ) : (
                        <Loader />
                      )}
                    </div>
                  </div>
                ) : (
                  <div className="col-md-12 mt-3 mb-4 p-0">
                    <div className=" text-center">
                      <img
                        src="/images/premium-feature.png"
                        className="chart_blur_img img-fluid"
                      />
                      <div className="">
                        <div className="m-auto">
                          <div className="blur_options_content">
                            <span>Premium Feature</span>
                            <p>This feature available for Prime Members</p>

                            <a
                              href="javascript:void(0);"
                              onClick={this.explorePlans}
                              title="Explore Premium Plans"
                            >
                              Explore Premium Plans
                              </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            {/* // <!-- Content Panel for nifty --> */}
            {/* <div className="yesbank-box open-int-box pricetable-box mb-5">
                <p>
                  {" "}
                  The advance stock screener is a part of the NiftyTrader
                  Premium tools.{" "}
                </p>
                <p>
                  {" "}
                  For those of you not familiar with the concept of stock
                  screener, a stock screener is a sorting and filtering tool,
                  that is often used by traders as well as investors to find
                  stocks that fit their strategy.
                </p>
                <p>
                  {" "}
                  The advance stock screener at NiftyTrader comes with more
                  advance filters like volume shockers, price action, range
                  breakout, opening price clues, gap up / gap down, candlesticks
                  and more. You can also use previous day’s data to filter
                  stocks.{" "}
                </p>
                <p>
                  {" "}
                  The advance stock screener, helps you filter from over 300 NSE
                  stocks, to find what you are looking for. For example you can
                  filter out stocks based on their 20 day SMA or 52 week price
                  change.{" "}
                </p>
                <p>
                  {" "}
                  Investors as well as traders use stock screeners. Investors
                  often filter out stocks based on their fundamentals or
                  financials. They use criteria like Market Cap, P/E, ROE, Sales
                  etc. Traders and technical analysts filter stocks based on
                  technical criteria like Price Action, NR7, Range Brakeout etc.{" "}
                </p>
              </div> */}
            {/* // <!-- Ennd Content Panel for nifty --> */}
            {pageContent && pageContent["page_Content"] != "" ? (
              <SEOPageContent pageContent={pageContent["page_Content"]} />
            ) : (
              ""
            )}
          </div>
        </div>

        <div
          className="modal fade eod_filter_modal  l-0"
          id="all_filters"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="myLargeModalLabel"
          aria-hidden="true"
        >
          <OptionScreenerModal
            handleFilterFields={this.handleFilterFields}
            applyFilter={this.applyFilter}
            resetFilter={this.resetCheckBox}
            selectedFilterFields={tempSelectedFilterFields}
            filterType={filterType}
            changeFilterType={this.changeFilterType}
            twoStocks={this.twoStocks}
            allStocks={stocks}
            openSaveFilter={this.openSaveFilter}
            includeOiCheck={this.state.includeOiCheck}
            includeVolCheck={this.state.includeVolCheck}
            proximityCheck={this.state.proximityCheck}
            includeOiNone={this.state.includeOiNone}
            includeVolNone={this.state.includeVolNone}
            includeProxNone={this.state.includeProxNone}
            oiSelectVal={this.state.oiSelectVal}
            volSelectVal={this.state.volSelectVal}
            proxSelectVal={this.state.proxSelectVal}
          />
        </div>
        {/* <!-- ========== Start saved Filter Modal==========--> */}
        <div
          className="modal filter_modal filter_content_modal"
          id="saved-filter"
        >
          <div className="modal-dialog modal-dialog-centered mx-auto">
            <div className="modal-content">
              {/* <!-- Modal Header --> */}
              <div className="modal-header">
                <h4 className="modal-title">Saved Filters</h4>
                <button type="button" className="close" data-dismiss="modal">
                  {" "}
                  &times;{" "}
                </button>
              </div>
              {/* <!-- Modal body --> */}
              <div className="modal-body">
                {/* <!-- start row --> */}
                <div className="row stock_screener_filters stock_saved_btn">
                  <div className="col-xl-12 p-0">
                    <ul>
                      {savedList != "" ? (
                        savedList
                      ) : (
                        <h5 className="text-center">No Filter Found...</h5>
                      )}
                      <div className="clearfix"></div>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- ========== close saved Filter Modal==========-->
<!-- save filter modal start --> */}
        <div
          className="modal filter_modal filter_content_modal"
          id="save-filter"
        >
          <div className="modal-dialog modal-dialog-centered mx-auto">
            <div className="modal-content">
              {/* <!-- Modal Header --> */}
              <div className="modal-header">
                <h4 className="modal-title">Save Filter</h4>
                <button type="button" className="close" data-dismiss="modal">
                  {" "}
                  &times;{" "}
                </button>
              </div>
              {/* <!-- Modal body --> */}
              <div className="modal-body">
                {/* <!-- start row --> */}
                <div className="row stock_screener_filters">
                  <div className="col-xl-12">
                    <form
                      className="feedback-form p-0"
                      id="feedback-form"
                      noValidate="novalidate"
                    >
                      <div className="form-group has-feedback">
                        <label htmlFor="">Filter Name</label>
                        <input
                          type="text"
                          name=""
                          id=""
                          className="form-control"
                          onChange={this.handleFilterNameChange}
                          value={filterName}
                        />
                      </div>
                      <button
                        type="button"
                        className="feedback-submit-btn text-uppercase"
                        onClick={this.saveFilter}
                      >
                        {" "}
                        {editableScreenerId == null ? "Save" : "Update"}{" "}
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    userData: state.userData.userData,
    isLoggedIn: state.userData.isLoggedIn,
  };
};

export default connect(mapStateToProps, {})(OptionScreener);
