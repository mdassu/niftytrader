import Loader from "components/Loader";
import LiveMarketModal from "components/Modals/LiveMarketModals";
import LiveMarketScreenerTable from "components/Tables/LiveMarketScreenerTable";
import { set } from "js-cookie";
import React, { Component } from "react";
import { CALL_API } from "_services/CALL_API";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import { AuthenticationService } from "_services/AuthenticationService";
import { ToastContainer, toast } from "react-toastify";
import Router from "next/router";
import SEOPageContent from "components/SEOPageContent";
import Link from "next/link";
import { connect } from "react-redux";
import { LiveMarket } from "./ScreenerIntitalData";

let mapStateToProps = (state) => {
  return {
    userData: state.userData.userData,
    isLoggedIn: state.userData.isLoggedIn,
  };
};

class LiveMarketScreener extends Component {
  constructor(props) {
    super(props);
    this.intiSelectedFilterFields = LiveMarket.intiSelectedFilterFields;
    this.fieldName = LiveMarket.fieldName;
    this.state = {
      intiMarketScreenerData: [],
      marketScreenerData: [],
      industriesList: [],
      selectedFilterFields: Object.assign({}, this.intiSelectedFilterFields),
      tempSelectedFilterFields: Object.assign(
        {},
        this.intiSelectedFilterFields
      ),
      filterName: "",
      savedFilterList: "",
      currentUser: {},
      editableScreenerId: null,
      appliedFilterStatus: false,
      isLoaded: false,
      primeMember: false,
      userLogin: this.props.isLoggedIn,
    };
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  componentDidMount() {
    const { userData, isLoggedIn } = this.props;
    this.setState({
      userLogin: isLoggedIn,
    });

    if (userData !== null || userData !== undefined) {
      if (userData.membership_flag === "1") {
        this.getFilterList();
        this.getLiveMarketData();
        this.setState({
          primeMember: true,
        });
      } else {
        this.setState({
          primeMember: false,
          isLoaded: true
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { userData, isLoggedIn } = this.props;

    if (prevProps !== this.props) {
      this.setState(
        {
          userLogin: isLoggedIn,
          primeMember: userData.membership_flag == "1" ? true : false,
        },
        () => {
          if (isLoggedIn && userData.membership_flag == "0") {
            Router.push("/primeplans");
          }
        }
      );
    }

    if (prevProps.userData !== userData) {
      if (userData !== null || userData !== undefined) {
        if (userData.membership_flag == "1") {
          this.getFilterList();
          this.getLiveMarketData();
          this.setState({
            primeMember: true,
            appliedFilterStatus: true,
          });
        } else {
          this.setState({
            primeMember: false,
            appliedFilterStatus: false,
          });
        }
      }
    }
  }

  getLiveMarketData() {
    CALL_API("GET", process.env.LIVE_MARKET_SCREENER_DATA, {}, (res) => {
      if (res.status) {
        this.setState(
          {
            intiMarketScreenerData: res["data"],
            marketScreenerData: res["data"],
            appliedFilterStatus: false,
            isLoaded: true,
          },
          () => {
            var industriesList = [];
            this.state.intiMarketScreenerData.map((item, key) => {
              if (item["industry"] != null) {
                industriesList.push(item["industry"]);
              }
            });
            this.setState({
              industriesList: industriesList,
            });
          }
        );
      } else {
        this.setState({
          intiMarketScreenerData: [],
          marketScreenerData: [],
          isLoaded: true,
        });
      }
    });
  }

  handleFilterFields = (event) => {
    var tempSelectedFilterFields = this.state.tempSelectedFilterFields;

    if (event.target.name == "industry") {
      tempSelectedFilterFields[event.target.name] = event.target.value;
    } else {
      tempSelectedFilterFields[event.target.name] = event.target.checked;
    }
    this.setState({
      tempSelectedFilterFields: { ...tempSelectedFilterFields },
    });
  };

  applyFilter = () => {
    // _HasFilterApplied = true;
    var marketScreenerData = this.state.intiMarketScreenerData;
    this.setState({
      selectedFilterFields: this.state.tempSelectedFilterFields,
    });
    var selectedFilterFields = this.state.tempSelectedFilterFields;

    if (
      selectedFilterFields["industry"] &&
      selectedFilterFields["industry"] != null
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["industry"] == selectedFilterFields["industry"];
      });
    }

    // NR7 Day
    if (selectedFilterFields["todayNR7"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["nr7"] == "Yes";
      });
    }
    if (selectedFilterFields["yesterdayNR7"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["day_before_nr7"] == "Yes";
      });
    }

    // Gap Up / Gap Down
    if (
      selectedFilterFields["todayGapUP"] ||
      selectedFilterFields["todayGapDown"]
    ) {
      if (
        selectedFilterFields["todayGapUP"] &&
        !selectedFilterFields["todayGapDown"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_open"] > item["prev_high"];
        });
      } else if (
        selectedFilterFields["todayGapDown"] &&
        !selectedFilterFields["todayGapUP"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_open"] < item["prev_low"];
        });
      }
    }
    if (
      selectedFilterFields["todayGapUP"] &&
      selectedFilterFields["todayGapDown"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_open"] > item["prev_high"] ||
          item["today_open"] < item["prev_low"]
        );
      });
    }

    if (
      selectedFilterFields["yesterdayGapUP"] ||
      selectedFilterFields["yesterdayGapDown"]
    ) {
      if (
        selectedFilterFields["yesterdayGapUP"] &&
        !selectedFilterFields["yesterdayGapDown"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_open"] > item["day_before_high"];
        });
      } else if (
        selectedFilterFields["yesterdayGapDown"] &&
        !selectedFilterFields["yesterdayGapUP"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_open"] < item["day_before_low"];
        });
      }
    }
    if (
      selectedFilterFields["yesterdayGapUP"] &&
      selectedFilterFields["yesterdayGapDown"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["prev_open"] > item["day_before_high"] ||
          item["prev_open"] < item["day_before_low"]
        );
      });
    }

    //---------------Opening Price Clues------------------------//

    if (
      selectedFilterFields["todayStockOpenHigh"] ||
      selectedFilterFields["todayStockOpenLow"]
    ) {
      if (
        selectedFilterFields["todayStockOpenHigh"] &&
        !selectedFilterFields["todayStockOpenLow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_open"] == item["today_high"];
        });
      } else if (
        selectedFilterFields["todayStockOpenLow"] &&
        !selectedFilterFields["todayStockOpenHigh"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_open"] <= item["today_low"];
        });
      }
    }
    if (
      selectedFilterFields["todayStockOpenHigh"] &&
      selectedFilterFields["todayStockOpenLow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_open"] == item["today_high"] ||
          item["today_open"] <= item["today_low"]
        );
      });
    }

    if (
      selectedFilterFields["yesterdayStockOpenHigh"] ||
      selectedFilterFields["yesterdayStockOpenLow"]
    ) {
      if (
        selectedFilterFields["yesterdayStockOpenHigh"] &&
        !selectedFilterFields["yesterdayStockOpenLow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          item["prev_open"] == item["prev_high"];
        });
      } else if (
        selectedFilterFields["yesterdayStockOpenLow"] &&
        !selectedFilterFields["yesterdayStockOpenHigh"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          item["prev_open"] <= item["prev_low"];
        });
      }
    }
    if (
      selectedFilterFields["yesterdayStockOpenHigh"] &&
      selectedFilterFields["yesterdayStockOpenLow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        item["prev_open"] == item["prev_high"] ||
          item["prev_open"] <= item["prev_low"];
      });
    }

    //-----------------------------Range Breakout-----------------------------------------//

    if (
      selectedFilterFields["range20DayUP"] ||
      selectedFilterFields["range20DayDown"]
    ) {
      if (
        selectedFilterFields["range20DayUP"] &&
        !selectedFilterFields["range20DayDown"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_high"] > item["high_day20"];
        });
      } else if (
        selectedFilterFields["range20DayDown"] &&
        !selectedFilterFields["range20DayUP"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_low"] < item["low_day20"];
        });
      }
    }
    if (
      selectedFilterFields["range20DayUP"] &&
      selectedFilterFields["range20DayDown"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] > item["high_day20"] ||
          item["today_low"] < item["low_day20"]
        );
      });
    }

    if (
      selectedFilterFields["range50DayUP"] ||
      selectedFilterFields["range50DayDown"]
    ) {
      if (
        selectedFilterFields["range50DayUP"] &&
        !selectedFilterFields["range50DayDown"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_high"] > item["high_day50"];
        });
      } else if (
        selectedFilterFields["range50DayDown"] &&
        !selectedFilterFields["range50DayUP"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_low"] < item["low_day50"];
        });
      }
    }
    if (
      selectedFilterFields["range50DayUP"] &&
      selectedFilterFields["range50DayDown"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] > item["high_day50"] ||
          item["today_low"] < item["low_day50"]
        );
      });
    }

    if (
      selectedFilterFields["range200DayUP"] ||
      selectedFilterFields["range200DayDown"]
    ) {
      if (
        selectedFilterFields["range200DayUP"] &&
        !selectedFilterFields["range200DayDown"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_high"] > item["high_day200"];
        });
      } else if (
        selectedFilterFields["range200DayDown"] &&
        !selectedFilterFields["range200DayUP"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_low"] < item["low_day200"];
        });
      }
    }
    if (
      selectedFilterFields["range200DayUP"] &&
      selectedFilterFields["range200DayDown"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] > item["high_day200"] ||
          item["today_low"] < item["low_day200"]
        );
      });
    }

    if (
      selectedFilterFields["range52WeekHigh"] ||
      selectedFilterFields["range52WeekLow"]
    ) {
      if (
        selectedFilterFields["range52WeekHigh"] &&
        !selectedFilterFields["range52WeekLow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_high"] > item["today_high52"];
        });
      } else if (
        selectedFilterFields["range52WeekLow"] &&
        !selectedFilterFields["range52WeekHigh"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_low"] < item["today_low52"];
        });
      }
    }
    if (
      selectedFilterFields["range52WeekHigh"] &&
      selectedFilterFields["range52WeekLow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] > item["today_high52"] ||
          item["today_low"] < item["today_low52"]
        );
      });
    }

    //--------------------------Price Action-------------------------------------//

    if (
      selectedFilterFields["higherHighHigherLow"] ||
      selectedFilterFields["lowerHighLowerLow"]
    ) {
      if (
        selectedFilterFields["higherHighHigherLow"] &&
        !selectedFilterFields["lowerHighLowerLow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return (
            item["today_high"] > item["prev_high"] &&
            item["today_low"] > item["prev_low"]
          );
        });
      } else if (
        selectedFilterFields["lowerHighLowerLow"] &&
        !selectedFilterFields["higherHighHigherLow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return (
            item["today_high"] < item["prev_high"] &&
            item["today_low"] < item["prev_low"]
          );
        });
      }
    }
    if (
      selectedFilterFields["higherHighHigherLow"] &&
      selectedFilterFields["lowerHighLowerLow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] > item["prev_high"] ||
          item["today_low"] > item["prev_low"] ||
          item["today_high"] < item["prev_high"] ||
          item["today_low"] < item["prev_low"]
        );
      });
    }

    if (
      selectedFilterFields["insideDay"] ||
      selectedFilterFields["outsideDay"]
    ) {
      if (
        selectedFilterFields["insideDay"] &&
        !selectedFilterFields["outsideDay"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return (
            item["today_high"] < item["prev_high"] &&
            item["today_low"] > item["prev_low"]
          );
        });
      } else if (
        selectedFilterFields["outsideDay"] &&
        !selectedFilterFields["insideDay"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return (
            item["today_high"] > item["prev_high"] &&
            item["today_low"] < item["prev_low"]
          );
        });
      }
    }
    if (
      selectedFilterFields["insideDay"] &&
      selectedFilterFields["outsideDay"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_high"] < item["prev_high"] ||
          item["today_low"] > item["prev_low"] ||
          item["today_high"] > item["prev_high"] ||
          item["today_low"] < item["prev_low"]
        );
      });
    }

    //-------------------------------Price Range------------------------------//

    if (
      selectedFilterFields["range0To100"] ||
      selectedFilterFields["range100To500"] ||
      selectedFilterFields["range500To1000"] ||
      selectedFilterFields["range1000To2000"] ||
      selectedFilterFields["rangeAbove2000"]
    ) {
      if (
        selectedFilterFields["range0To100"] &&
        !selectedFilterFields["range100To500"] &&
        !selectedFilterFields["range500To1000"] &&
        !selectedFilterFields["range1000To2000"] &&
        !selectedFilterFields["rangeAbove2000"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] < 100;
        });
      } else if (
        selectedFilterFields["range100To500"] &&
        !selectedFilterFields["range0To100"] &&
        !selectedFilterFields["range500To1000"] &&
        !selectedFilterFields["range1000To2000"] &&
        !selectedFilterFields["rangeAbove2000"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] >= 100 && item["current_price"] < 500;
        });
      } else if (
        selectedFilterFields["range500To1000"] &&
        !selectedFilterFields["range0To100"] &&
        !selectedFilterFields["range100To500"] &&
        !selectedFilterFields["range1000To2000"] &&
        !selectedFilterFields["rangeAbove2000"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] >= 500 && item["current_price"] < 1000;
        });
      } else if (
        selectedFilterFields["range1000To2000"] &&
        !selectedFilterFields["range0To100"] &&
        !selectedFilterFields["range100To500"] &&
        !selectedFilterFields["range500To1000"] &&
        !selectedFilterFields["rangeAbove2000"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] >= 1000 && item["current_price"] <= 2000;
        });
      } else if (
        selectedFilterFields["rangeAbove2000"] &&
        !selectedFilterFields["range0To100"] &&
        !selectedFilterFields["range100To500"] &&
        !selectedFilterFields["range1000To2000"] &&
        !selectedFilterFields["range500To1000"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] > 2000;
        });
      }
    }

    if (
      selectedFilterFields["range0To100"] &&
      selectedFilterFields["range100To500"] &&
      selectedFilterFields["range500To1000"] &&
      selectedFilterFields["range1000To2000"] &&
      selectedFilterFields["rangeAbove2000"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["current_price"] < 100 ||
          (item["current_price"] >= 100 && item["current_price"] < 500) ||
          (item["current_price"] >= 500 && item["current_price"] < 1000) ||
          (item["current_price"] >= 1000 && item["current_price"] <= 2000) ||
          item["current_price"] > 2000
        );
      });
    }

    //------------------------------------------Volume Shocker---------------------------------------//
    if (selectedFilterFields["todayHighVolumeDay"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["volume"] >= item["average_volume50"] * 2;
      });
    }
    //--------------------------------------Candlestick------------------------------------//

    if (
      selectedFilterFields["todayBullishHigh"] ||
      selectedFilterFields["todayBearishLow"] ||
      selectedFilterFields["todayNetural"]
    ) {
      if (
        selectedFilterFields["todayBullishHigh"] &&
        !selectedFilterFields["todayBearishLow"] &&
        !selectedFilterFields["todayNetural"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["today_high"] - item["today_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) >=
              Number(0.75)
            );
          });
      } else if (
        selectedFilterFields["todayBearishLow"] &&
        !selectedFilterFields["todayBullishHigh"] &&
        !selectedFilterFields["todayNetural"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["today_high"] - item["today_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) <=
              Number(0.25)
            );
          });
      } else if (
        selectedFilterFields["todayNetural"] &&
        !selectedFilterFields["todayBullishHigh"] &&
        !selectedFilterFields["todayBearishLow"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["today_high"] - item["today_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) <
              Number(0.75) &&
              (item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) >
              Number(0.25)
            );
          });
      }
    }
    if (
      selectedFilterFields["todayBullishHigh"] &&
      selectedFilterFields["todayBearishLow"] &&
      selectedFilterFields["todayNetural"]
    ) {
      marketScreenerData = marketScreenerData
        .filter((item) => {
          return item["today_high"] - item["today_low"] != 0;
        })
        .filter((item) => {
          return (
            (item["today_close"] - item["today_low"]) /
            (item["today_high"] - item["today_low"]) >=
            Number(0.75) ||
            (item["today_close"] - item["today_low"]) /
            (item["today_high"] - item["today_low"]) <=
            Number(0.25) ||
            ((item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) <
              Number(0.75) &&
              (item["today_close"] - item["today_low"]) /
              (item["today_high"] - item["today_low"]) >
              Number(0.25))
          );
        });
    }

    if (
      selectedFilterFields["yesterdayBullishHigh"] ||
      selectedFilterFields["yesterdayBearishLow"] ||
      selectedFilterFields["yesterdayNetural"]
    ) {
      if (
        selectedFilterFields["yesterdayBullishHigh"] &&
        !selectedFilterFields["yesterdayBearishLow"] &&
        !selectedFilterFields["yesterdayNetural"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["prev_high"] - item["prev_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) >=
              Number(0.75)
            );
          });
      } else if (
        selectedFilterFields["yesterdayBearishLow"] &&
        !selectedFilterFields["yesterdayBullishHigh"] &&
        !selectedFilterFields["yesterdayNetural"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["prev_high"] - item["prev_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) <=
              Number(0.25)
            );
          });
      } else if (
        selectedFilterFields["yesterdayNetural"] &&
        !selectedFilterFields["yesterdayBullishHigh"] &&
        !selectedFilterFields["yesterdayBearishLow"]
      ) {
        marketScreenerData = marketScreenerData
          .filter((item) => {
            return item["prev_high"] - item["prev_low"] != 0;
          })
          .filter((item) => {
            return (
              (item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) <
              Number(0.75) &&
              (item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) >
              Number(0.25)
            );
          });
      }
    }
    if (
      selectedFilterFields["yesterdayBullishHigh"] &&
      selectedFilterFields["yesterdayBearishLow"] &&
      selectedFilterFields["yesterdayNetural"]
    ) {
      marketScreenerData = marketScreenerData
        .filter((item) => {
          return item["prev_high"] - item["prev_low"] != 0;
        })
        .filter((item) => {
          return (
            (item["prev_close"] - item["prev_low"]) /
            (item["prev_high"] - item["prev_low"]) >=
            Number(0.75) ||
            (item["prev_close"] - item["prev_low"]) /
            (item["prev_high"] - item["prev_low"]) <=
            Number(0.25) ||
            ((item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) <
              Number(0.75) &&
              (item["prev_close"] - item["prev_low"]) /
              (item["prev_high"] - item["prev_low"]) >
              Number(0.25))
          );
        });
    }

    if (
      selectedFilterFields["vwapAbove"] ||
      selectedFilterFields["vwapBelow"]
    ) {
      if (
        selectedFilterFields["vwapAbove"] &&
        !selectedFilterFields["vwapBelow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] >= item["average_price"];
        });
      } else if (
        selectedFilterFields["vwapBelow"] &&
        !selectedFilterFields["vwapAbove"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["current_price"] < item["average_price"];
        });
      }
    }
    if (
      selectedFilterFields["vwapAbove"] &&
      selectedFilterFields["vwapBelow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["current_price"] >= item["average_price"] ||
          item["current_price"] < item["average_price"]
        );
      });
    }

    //------------------------------LTP Max Pain----------------------------------------//
    if (
      selectedFilterFields["maxPainAbove"] ||
      selectedFilterFields["maxPainBelow"]
    ) {
      if (
        selectedFilterFields["maxPainAbove"] &&
        !selectedFilterFields["maxPainBelow"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] > item["max_pain"] && item["max_pain"] != null;
        });
      } else if (
        selectedFilterFields["maxPainBelow"] &&
        !selectedFilterFields["maxPainAbove"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] < item["max_pain"] && item["max_pain"] != null;
        });
      }
    }
    if (
      selectedFilterFields["maxPainAbove"] &&
      selectedFilterFields["maxPainBelow"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          (item["today_close"] > item["max_pain"] && item["max_pain"] != null) ||
          (item["today_close"] < item["max_pain"] && item["max_pain"] != null)
        );
      });
    }

    //------------------------------Moving Average-------------------------------------//
    if (
      selectedFilterFields["todayAbove20SMA"] ||
      selectedFilterFields["todayBelow20SMA"]
    ) {
      if (
        selectedFilterFields["todayAbove20SMA"] &&
        !selectedFilterFields["todayBelow20SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] >= item["average_close20"];
        });
      } else if (
        selectedFilterFields["todayBelow20SMA"] &&
        !selectedFilterFields["todayAbove20SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] < item["average_close20"];
        });
      }
    }
    if (
      selectedFilterFields["todayAbove20SMA"] &&
      selectedFilterFields["todayBelow20SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_close"] >= item["average_close20"] ||
          item["today_close"] < item["average_close20"]
        );
      });
    }

    if (
      selectedFilterFields["todayAbove50SMA"] ||
      selectedFilterFields["todayBelow50SMA"]
    ) {
      if (
        selectedFilterFields["todayAbove50SMA"] &&
        !selectedFilterFields["todayBelow50SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] >= item["average_close50"];
        });
      } else if (
        selectedFilterFields["todayBelow50SMA"] &&
        !selectedFilterFields["todayAbove50SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] < item["average_close50"];
        });
      }
    }
    if (
      selectedFilterFields["todayAbove50SMA"] &&
      selectedFilterFields["todayBelow50SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_close"] >= item["average_close50"] ||
          item["today_close"] < item["average_close50"]
        );
      });
    }

    if (
      selectedFilterFields["todayAbove200SMA"] ||
      selectedFilterFields["todayBelow200SMA"]
    ) {
      if (
        selectedFilterFields["todayAbove200SMA"] &&
        !selectedFilterFields["todayBelow200SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] >= item["average_close200"];
        });
      } else if (
        selectedFilterFields["todayBelow200SMA"] &&
        !selectedFilterFields["todayAbove200SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["today_close"] < item["average_close200"];
        });
      }
    }
    if (
      selectedFilterFields["todayAbove200SMA"] &&
      selectedFilterFields["todayBelow200SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["today_close"] >= item["average_close200"] ||
          item["today_close"] < item["average_close200"]
        );
      });
    }

    if (
      selectedFilterFields["yesterdayAbove20SMA"] ||
      selectedFilterFields["yesterdayBelow20SMA"]
    ) {
      if (
        selectedFilterFields["yesterdayAbove20SMA"] &&
        !selectedFilterFields["yesterdayBelow20SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] >= item["day_before_average_close20"];
        });
      } else if (
        selectedFilterFields["yesterdayBelow20SMA"] &&
        !selectedFilterFields["yesterdayAbove20SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] < item["day_before_average_close20"];
        });
      }
    }
    if (
      selectedFilterFields["yesterdayAbove20SMA"] &&
      selectedFilterFields["yesterdayBelow20SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["prev_close"] >= item["day_before_average_close20"] ||
          item["prev_close"] < item["day_before_average_close20"]
        );
      });
    }

    if (
      selectedFilterFields["yesterdayAbove50SMA"] ||
      selectedFilterFields["yesterdayBelow50SMA"]
    ) {
      if (
        selectedFilterFields["yesterdayAbove50SMA"] &&
        !selectedFilterFields["yesterdayBelow50SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] >= item["day_before_average_close50"];
        });
      } else if (
        selectedFilterFields["yesterdayBelow50SMA"] &&
        !selectedFilterFields["yesterdayAbove50SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] < item["day_before_average_close50"];
        });
      }
    }
    if (
      selectedFilterFields["yesterdayAbove50SMA"] &&
      selectedFilterFields["yesterdayBelow50SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["prev_close"] >= item["day_before_average_close50"] ||
          item["prev_close"] < item["day_before_average_close50"]
        );
      });
    }

    if (
      selectedFilterFields["yesterdayAbove200SMA"] ||
      selectedFilterFields["yesterdayBelow200SMA"]
    ) {
      if (
        selectedFilterFields["yesterdayAbove200SMA"] &&
        !selectedFilterFields["yesterdayBelow200SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] >= item["day_before_average_close200"];
        });
      } else if (
        selectedFilterFields["yesterdayBelow200SMA"] &&
        !selectedFilterFields["yesterdayAbove200SMA"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["prev_close"] < item["day_before_average_close200"];
        });
      }
    }
    if (
      selectedFilterFields["yesterdayAbove200SMA"] &&
      selectedFilterFields["yesterdayBelow200SMA"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["prev_close"] >= item["day_before_average_close200"] ||
          item["prev_close"] < item["day_before_average_close200"]
        );
      });
    }

    //-------------------Financial Filter--------------------------------------------//

    //-------------------------------Market Cap------------------------------//
    if (selectedFilterFields["marketCapBelow1000"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["market_cap"] < 1000;
      });
    }

    if (selectedFilterFields["marketCap1000To5000"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["market_cap"] >= 1000 && item["market_cap"] < 5000;
      });
    }

    if (selectedFilterFields["marketCap5000To20000"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["market_cap"] >= 5000 && item["market_cap"] < 20000;
      });
    }

    if (selectedFilterFields["marketCap20000To50000"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["market_cap"] >= 20000 && item["market_cap"] <= 50000;
      });
    }

    if (selectedFilterFields["marketCapAbove50000"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["market_cap"] > 50000;
      });
    }
    if (selectedFilterFields["MarketCap_None"]) {
    }

    //-------------------------------Stock PE------------------------------//

    if (selectedFilterFields["stockPEBelow5"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] < 5;
      });
    }

    if (selectedFilterFields["stockPE10To20"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] >= 10 && item["stock_pe"] < 20;
      });
    }

    if (selectedFilterFields["stockPE50To100"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] >= 50 && item["stock_pe"] <= 100;
      });
    }

    if (selectedFilterFields["StockPE_Below_None"]) {
    }

    if (selectedFilterFields["stockPE5To10"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] >= 5 && item["stock_pe"] < 10;
      });
    }
    if (selectedFilterFields["stockPE20To50"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] >= 20 && item["stock_pe"] < 50;
      });
    }
    if (selectedFilterFields["stockPEAbove100"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["stock_pe"] > 100;
      });
    }

    //-------------------------------Dividend Yield------------------------------//

    if (selectedFilterFields["dividendYield0To1"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["dividend_yield"] >= 0 && item["dividend_yield"] < 1;
      });
    }

    if (selectedFilterFields["dividendYield2To5"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["dividend_yield"] >= 2 && item["dividend_yield"] <= 5;
      });
    }

    if (selectedFilterFields["DividendYield_Below_None"]) {
    }

    if (selectedFilterFields["dividendYield1To2"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["dividend_yield"] >= 1 && item["dividend_yield"] < 2;
      });
    }

    if (selectedFilterFields["dividendYieldAbove5"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["dividend_yield"] > 5;
      });
    }

    //-------------------------------ROCE------------------------------//

    if (selectedFilterFields["roceBelow5"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] < 5;
      });
    }

    if (selectedFilterFields["roce10To20"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] >= 10 && item["roce"] < 20;
      });
    }

    if (selectedFilterFields["roce50To70"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] >= 50 && item["roce"] < 70;
      });
    }

    if (selectedFilterFields["ROCE_None"]) {
    }

    if (selectedFilterFields["roce5To10"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] >= 5 && item["roce"] < 10;
      });
    }
    if (selectedFilterFields["roce20To50"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] >= 20 && item["roce"] < 50;
      });
    }

    if (selectedFilterFields["roce70To100"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roce"] >= 70 && item["roce"] <= 100;
      });
    }
    // //-------------------------------ROE------------------------------//

    if (selectedFilterFields["roeBelow0"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roe"] < 0;
      });
    }

    if (selectedFilterFields["roe10To20"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roe"] >= 10 && item["roe"] < 20;
      });
    }

    if (selectedFilterFields["roeAbove50"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roe"] > 50;
      });
    }

    if (selectedFilterFields["roe0To10"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roe"] >= 0 && item["roe"] < 10;
      });
    }

    if (selectedFilterFields["roe20To50"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["roe"] >= 20 && item["roe"] <= 50;
      });
    }
    if (selectedFilterFields["ROCE_None"]) {
    }
    //-------------------------------Sales Growth------------------------------//

    if (selectedFilterFields["salesGrowthBelow0"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] < 0;
      });
    }

    if (selectedFilterFields["salesGrowth5To10"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] >= 5 && item["sales_growth"] < 10;
      });
    }

    if (selectedFilterFields["salesGrowth15To20"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] >= 15 && item["sales_growth"] <= 20;
      });
    }

    if (selectedFilterFields["Salesgrowth_None"]) {
    }

    if (selectedFilterFields["salesGrowth0To5"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] >= 0 && item["sales_growth"] < 5;
      });
    }
    if (selectedFilterFields["salesGrowth10To15"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] >= 10 && item["sales_growth"] < 15;
      });
    }
    if (selectedFilterFields["salesGrowthAbove20"]) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["sales_growth"] > 20;
      });
    }

    // Piotroski Score

    if (
      selectedFilterFields["piotroskiScore0To2"] ||
      selectedFilterFields["piotroskiScore3To7"] ||
      selectedFilterFields["piotroskiScore8To9"]
    ) {
      if (
        selectedFilterFields["piotroskiScore0To2"] &&
        !selectedFilterFields["piotroskiScore3To7"] &&
        !selectedFilterFields["piotroskiScore8To9"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["piotroski_Score"] <= 2;
        });
      } else if (
        selectedFilterFields["piotroskiScore3To7"] &&
        !selectedFilterFields["piotroskiScore0To2"] &&
        !selectedFilterFields["piotroskiScore8To9"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["piotroski_Score"] >= 3 && item["piotroski_Score"] <= 7;
        });
      } else if (
        selectedFilterFields["piotroskiScore8To9"] &&
        !selectedFilterFields["piotroskiScore0To2"] &&
        !selectedFilterFields["piotroskiScore3To7"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["piotroski_Score"] >= 8 && item["piotroski_Score"] <= 9;
        });
      }
    }
    if (
      selectedFilterFields["piotroskiScore0To2"] &&
      selectedFilterFields["piotroskiScore3To7"] &&
      selectedFilterFields["piotroskiScore8To9"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["piotroski_Score"] <= 9;
      });
    }

    // Category
    if (
      selectedFilterFields["fnoStocks"] == true ||
      selectedFilterFields["nifty50Stocks"] == true
    ) {
      if (
        selectedFilterFields["fnoStocks"] == true &&
        !selectedFilterFields["nifty50Stocks"] == true
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["category"] != null;
        });
      } else if (
        selectedFilterFields["nifty50Stocks"] == true &&
        !selectedFilterFields["fnoStocks"] == true
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["nifty50"] != null;
        });
      }
    }
    if (
      selectedFilterFields["fnoStocks"] == true &&
      selectedFilterFields["nifty50Stocks"] == true
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return item["category"] != null || item["nifty50"] != null;
      });
    }

    if (
      selectedFilterFields["financial"] ||
      selectedFilterFields["nonFinancial"]
    ) {
      if (
        selectedFilterFields["financial"] &&
        !selectedFilterFields["nonFinancial"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return item["finIndustry"] == "Yes";
        });
      } else if (
        selectedFilterFields["nonFinancial"] &&
        !selectedFilterFields["financial"]
      ) {
        marketScreenerData = marketScreenerData.filter((item) => {
          return (
            item["finIndustry"] == "No" ||
            item["finIndustry"] == null ||
            item["finIndustry"] == ""
          );
        });
      }
    }
    if (
      selectedFilterFields["financial"] &&
      selectedFilterFields["nonFinancial"]
    ) {
      marketScreenerData = marketScreenerData.filter((item) => {
        return (
          item["finIndustry"] == "Yes" ||
          item["finIndustry"] == "No" ||
          item["finIndustry"] == null ||
          item["finIndustry"] == ""
        );
      });
    }

    this.setState({
      marketScreenerData: marketScreenerData,
      appliedFilterStatus: true,
      // editableScreenerId: null
    });
    $("#filter").modal("hide");
  };

  resetFilter = () => {
    this.setState(
      {
        marketScreenerData: this.state.intiMarketScreenerData,
        selectedFilterFields: this.intiSelectedFilterFields,
        appliedFilterStatus: false,
      },
      () => {
        $("#filter").modal("hide");
      }
    );
  };

  resetFilterFileds = () => {
    this.setState({
      tempSelectedFilterFields: Object.assign(
        {},
        this.state.selectedFilterFields
      ),
    });
  };

  removeFilterField = (key) => {
    var selectedFilterFields = this.state.selectedFilterFields;
    selectedFilterFields[key] = false;
    var trueValueCount = Object.keys(selectedFilterFields).filter((item) => {
      return selectedFilterFields[item];
    }).length;
    if (trueValueCount) {
      this.setState(
        {
          selectedFilterFields: selectedFilterFields,
        },
        () => {
          this.applyFilter();
        }
      );
    } else {
      this.resetFilter();
    }
  };

  openFilterPopup = () => {
    this.setState(
      {
        tempSelectedFilterFields: Object.assign(
          {},
          this.state.selectedFilterFields
        ),
      },
      () => {
        $("#filter").modal("show");
      }
    );
  };

  getFilterList = () => {
    var params = {
      screener_user_id: this.state.currentUser["user_id"],
    };
    CALL_API(
      "POST",
      process.env.LIVE_MARKET_FILTER_LIST_DATA,
      params,
      (res) => {
        if (res.status) {
          this.setState({
            savedFilterList: res["data"],
          });
        } else {
          this.setState({
            savedFilterList: [],
          });
        }
      }
    );
  };

  handleFilterNameChange = (event) => {
    this.setState({
      filterName: event.target.value,
    });
  };

  saveFilter = () => {
    if (this.state.filterName != "") {
      if (this.state.editableScreenerId == null) {
        var params = {
          screener_name: this.state.filterName,
          screener_json: JSON.stringify(this.state.tempSelectedFilterFields),
          screener_user_id: this.state.currentUser["user_id"],
          screener_id: null,
          Source_App: null,
        };

        CALL_API(
          "POST",
          process.env.SAVE_LIVE_MARKET_FILTER_DATA,
          params,
          (res) => {
            if (res.status) {
              $("#save-filter").modal("hide");
              this.getFilterList();
              this.setState({
                filterName: "",
              });
              this.toastConfig["type"] = "success";
              toast(res["message"], this.toastConfig);
            } else {
              this.toastConfig["type"] = "error";
              toast(res["message"], this.toastConfig);
            }
          }
        );
      } else {
        var params = {
          screener_name: this.state.filterName,
          screener_json: JSON.stringify(this.state.tempSelectedFilterFields),
          screener_user_id: this.state.currentUser["user_id"],
          screener_id: this.state.editableScreenerId,
          Source_App: null,
        };

        CALL_API(
          "POST",
          process.env.UPDATE_LIVE_MARKET_FILTER_DATA,
          params,
          (res) => {
            if (res.status) {
              $("#save-filter").modal("hide");
              this.getFilterList();
              this.setState({
                filterName: "",
              });
              this.toastConfig["type"] = "success";
              toast(res["message"], this.toastConfig);
            } else {
              this.toastConfig["type"] = "error";
              toast(res["message"], this.toastConfig);
            }
          }
        );
      }
    } else {
      this.toastConfig["type"] = "error";
      toast("Please enter filter name", this.toastConfig);
    }
  };

  deleteFilter = (filterId, key) => {
    var params = {
      screener_id: filterId,
    };

    CALL_API(
      "POST",
      process.env.DELETE_LIVE_MARKET_FILTER_DATA,
      params,
      (res) => {
        if (res.status) {
          this.toastConfig["type"] = "success";
          toast(res["message"], this.toastConfig);
          var filterList = this.state.savedFilterList;
          filterList.splice(1, key);
          this.setState(
            {
              savedFilterList: filterList,
            },
            () => {
              if (this.state.editableScreenerId == filterId) {
                this.setState({
                  appliedFilterStatus: false,
                });
                this.getLiveMarketData();
              }
            }
          );
          this.getFilterList();
        } else {
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      }
    );
  };

  editFilter = (filterId) => {
    var filterData = this.state.savedFilterList.filter((item) => {
      return item["screener_id"] == filterId;
    })[0];
    $("#saved-filter").modal("hide");
    this.setState(
      {
        tempSelectedFilterFields: JSON.parse(filterData["screener_json"]),
        filterName: filterData["screener_name"],
        editableScreenerId: filterId,
      },
      () => {
        $("#filter").modal("show");
      }
    );
  };

  savedFilterApply = (filterId) => {
    var filterData = this.state.savedFilterList.filter((item) => {
      return item["screener_id"] == filterId;
    })[0];
    $("#saved-filter").modal("hide");
    this.setState(
      {
        selectedFilterFields: JSON.parse(filterData["screener_json"]),
        tempSelectedFilterFields: JSON.parse(filterData["screener_json"]),
        editableScreenerId: filterId,
      },
      () => {
        this.applyFilter();
      }
    );
  };

  openSavedFilterPopup = () => {
    if (this.state.savedFilterList.length > 0) {
      $("#saved-filter").modal("show");
    } else {
      this.toastConfig["type"] = "error";
      toast("No filter found...", this.toastConfig);
    }
  };

  explorePlans = () => {
    const { isLoggedIn } = this.props;
    const { primeMember, userLogin } = this.state;

    if (userLogin) {
      if (primeMember !== true) {
        this.setState({
          primeMember: false,
        });
        Router.push("/primeplans");
      } else {
        this.setState({
          primeMember: true,
        });
      }
    } else {
      $("#loginpopup").modal("show");
    }
  };

  render() {
    const {
      marketScreenerData,
      industriesList,
      selectedFilterFields,
      tempSelectedFilterFields,
      savedFilterList,
      filterName,
      editableScreenerId,
      appliedFilterStatus,
      isLoaded,
    } = this.state;
    const { title, pageContent } = this.props;
    var screenerData = "";
    var tableColumns = [
      { field: "stock", headerText: "SYMBOL" },
      { field: "current_price", headerText: "CURRENT PRICE" },
      { field: "change", headerText: "CHANGE" },
      { field: "change_per", headerText: "CHANGE %" },
      { field: "today_high", headerText: "HIGH" },
      { field: "today_low", headerText: "LOW" },
      { field: "market_cap", headerText: "MARKET CAP" },
      { field: "stock_pe", headerText: "STOCK P/E" },
      { field: "roe", headerText: "ROE" },
    ];
    var tableRows = [];

    if (marketScreenerData && marketScreenerData.length > 0) {
      screenerData = marketScreenerData.map((item, key) => {
        var change = item["current_price"] - item["prev_close"];
        var changePer = (change / item["prev_close"]) * 100;
        tableRows.push({
          stock: item["stock"],
          current_price: Number(item["current_price"]).toFixed(2),
          change: Number(change).toFixed(2),
          change_per:
            Math.sign(Number(changePer).toFixed(2)) == 1
              ? "+" + Number(changePer).toFixed(2) + "%"
              : Number(changePer).toFixed(2) + "%",
          today_high: Number(item["today_high"]).toFixed(2),
          today_low: Number(item["today_low"]).toFixed(2),
          market_cap: Number(item["market_cap"]).toFixed(2),
          stock_pe: Number(item["stock_pe"]).toFixed(2),
          roe: Number(item["roe"]).toFixed(2),
        });
      });
    }

    var savedList = "";
    if (savedFilterList && savedFilterList.length > 0) {
      savedList = savedFilterList.map((item, key) => {
        return (
          <li key={key}>
            <h5
              className="text-left float-left filter-h-color"
              style={{ minWidth: "82%" }}
              onClick={() => this.savedFilterApply(item["screener_id"])}
            >
              {item["screener_name"]}
            </h5>
            <div className="save_btn text-right float-right">
              <button
                className="edit"
                title="Edit"
                onClick={() => this.editFilter(item["screener_id"])}
              >
                <FontAwesomeIcon icon={faEdit} width="18" height="16" />
                {/* <!-- <img src="/images/edit01.svg" alt="edit01" title="Edit" /> --> */}
              </button>
              <button
                className="delete"
                title="Delete"
                onClick={() => this.deleteFilter(item["screener_id"], key)}
              >
                <i className="far fa-trash-alt"></i>
                <FontAwesomeIcon icon={faTrashAlt} width="14" height="16" />
                {/* <!-- <img src="/images/advance-stock/Delete_new.svg" title="Delete" className="delete_btn" alt="Delete01"> --> */}
              </button>
            </div>
          </li>
        );
      });
    }
    return (
      <React.Fragment>
        {/* <ToastContainer /> */}
        {
          isLoaded ? "" : <Loader />
        }
        <div className="row">
          <div className="col-xl-6 col-md-6 col-sm-6 pl-lg-0">
            <h1 className="main-page-heading stock-screener stock_main_heading">
              {pageContent["page_Content_Title"]}
            </h1>
          </div>{" "}
          {this.state.primeMember && (
            <div className="col-xl-6 col-md-6 col-sm-6 advance_stock_screener pr-lg-0 pb-2">
              <div className="text-right right_refresh_btns">
                <button className="refresh_btn" onClick={this.resetFilter}>
                  <img src="/images/reset.svg" title="Reset" alt="reset" />
                  {/* <div className="badge clear_badge">5</div> */}
                </button>
                <button
                  onClick={this.openSavedFilterPopup}
                  title="Saved Filters"
                  className="save_filters text-uppercase"
                >
                  {" "}
                    Saved Filters{" "}
                </button>
                <button
                  title="Filter"
                  className="stock_filter text-uppercase"
                  onClick={this.openFilterPopup}
                >
                  <FontAwesomeIcon icon={faFilter} width="14" height="14" />{" "}
                    Filter{" "}
                </button>
              </div>
            </div>
          )}
          {appliedFilterStatus ? (
            <div className="row m-0">
              <div className="col-12 col-sm-12 col-xl-12 col-lg-12 col-md-12 pl-0 pl-15 pr-0 pr-15">
                <span className="slider_heading">Applied Filters:</span>
                <div
                  className="stock_apply_filter filtering stock_apply_filter_2"
                  id="eod_filter"
                >
                  <ul>
                    {Object.keys(this.intiSelectedFilterFields).map(
                      (item, key) => {
                        return (
                          <React.Fragment key={key}>
                            {selectedFilterFields[item] ? (
                              <li>
                                {this.fieldName[item]["groupName"]}:{" "}
                                <img
                                  title="Remove"
                                  className="close_img"
                                  src="/images/cross.svg"
                                  alt="Remove"
                                  onClick={() => this.removeFilterField(item)}
                                />
                                <span>
                                  {this.fieldName[item]["groupName"] ==
                                    "Industry"
                                    ? selectedFilterFields[item]
                                    : this.fieldName[item]["fieldName"]}
                                </span>
                              </li>
                            ) : (
                              ""
                            )}
                          </React.Fragment>
                        );
                      }
                    )}
                  </ul>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="col-lg-12 p-0">
            <div className="">
              <div className="row m-0">
                <div className="col-lg-12 p-0">
                  {this.state.primeMember ? (
                    <div className="nifty-datatable table-responsive live-m-table-nifty">
                      {tableRows && tableRows.length > 0 ? (
                        <LiveMarketScreenerTable
                          tableColumns={tableColumns}
                          tableRows={tableRows}
                        />
                      ) : (
                        <Loader />
                      )}
                      {/* <table className="table">
                        <thead>
                          <tr>
                            <th>SYMBOL</th>
                            <th>CURRENT PRICE</th>
                            <th>CHANGE</th>
                            <th>CHANGE %</th>
                            <th>HIGH</th>
                            <th>LOW</th>
                            <th>MARKET CAP</th>
                            <th>STOCK P/E</th>
                            <th>ROE</th>
                          </tr>
                        </thead>
                        <tbody>
                          {screenerData && screenerData != '' ? screenerData : ''}
                        </tbody>
                      </table> */}
                    </div>
                  ) : (
                    <div className="">
                      <div className="col-md-12 mt-3 mb-4 p-0">
                        <div className="part_wise_oi_box text-center">
                          <img
                            src="/images/premium-feature.png"
                            className="options_blur_img img-fluid"
                          />
                          <div className="">
                            <div className="m-auto">
                              <div className="blur_options_content">
                                <span>Premium Feature</span>
                                <p>
                                  This feature available for Prime Members
                                  </p>

                                <a
                                  href="javascript:void(0);"
                                  onClick={this.explorePlans}
                                  title="Explore Premium Plans"
                                >
                                  Explore Premium Plans
                                  </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
            {/* // <!-- Content Panel for nifty --> */}
            <div className="yesbank-box open-int-box pricetable-box mb-5">
              <p>
                {" "}
                  The Live Market Screener is a part of the NiftyTrader Premium
                  tools.{" "}
              </p>
              <p>
                {" "}
                  For those of you not familiar with the concept of stock
                  screener, a stock screener is a sorting and filtering tool,
                  that is often used by traders as well as investors to find
                  stocks that fit their strategy.
                </p>
              <p>
                {" "}
                  The Live Market Screener at NiftyTrader comes with more
                  advance filters like volume shockers, price action, range
                  breakout, opening price clues, gap up / gap down, candlesticks
                  and more. You can also use previous day’s data to filter
                  stocks.{" "}
              </p>
              <p>
                {" "}
                  The Live Market Screener, helps you filter from over 300 NSE
                  stocks, to find what you are looking for. For example you can
                  filter out stocks based on their 20 day SMA or 52 week price
                  change.{" "}
              </p>
              <p>
                {" "}
                  Investors as well as traders use stock screeners. Investors
                  often filter out stocks based on their fundamentals or
                  financials. They use criteria like Market Cap, P/E, ROE, Sales
                  etc. Traders and technical analysts filter stocks based on
                  technical criteria like Price Action, NR7, Range Brakeout etc.{" "}
              </p>
            </div>
            {/* // <!-- Ennd Content Panel for nifty --> */}
            {pageContent && pageContent["page_Content"] != "" ? (
              <SEOPageContent pageContent={pageContent["page_Content"]} />
            ) : (
              ""
            )}
          </div>
        </div>

        {/* <!-- ========== Start Filter Modal==========--> */}
        <div
          className="modal fade filter_modal stock_filter_modal eod_filter_modal l-0"
          id="filter"
        >
          <LiveMarketModal
            handleFilterFields={this.handleFilterFields}
            applyFilter={this.applyFilter}
            industriesList={industriesList}
            resetFilter={this.resetFilterFileds}
            selectedFilterFields={tempSelectedFilterFields}
          />
        </div>
        {/* <!-- ========== End Filter Modal==========--> */}
        {/* <!-- ========== Start saved Filter Modal==========--> */}
        <div
          className="modal filter_modal filter_content_modal"
          id="saved-filter"
        >
          <div className="modal-dialog modal-dialog-centered mx-auto">
            <div className="modal-content">
              {/* <!-- Modal Header --> */}
              <div className="modal-header">
                <h4 className="modal-title">Saved Filters</h4>
                <button type="button" className="close" data-dismiss="modal">
                  {" "}
                  &times;{" "}
                </button>
              </div>
              {/* <!-- Modal body --> */}
              <div className="modal-body">
                {/* <!-- start row --> */}
                <div className="row stock_screener_filters stock_saved_btn">
                  <div className="col-xl-12 p-0">
                    <ul>
                      {savedList && savedList != "" ? (
                        savedList
                      ) : (
                        <h5 className="text-center">No Filter Found...</h5>
                      )}
                      <div className="clearfix"></div>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- ========== End Filter Modal==========--> */}
        {/* <!-- save filter modal start --> */}
        <div
          className="modal filter_modal filter_content_modal"
          id="save-filter"
        >
          <div className="modal-dialog modal-dialog-centered mx-auto">
            <div className="modal-content">
              {/* <!-- Modal Header --> */}
              <div className="modal-header">
                <h4 className="modal-title">Save Filter</h4>
                <button type="button" className="close" data-dismiss="modal">
                  {" "}
                  &times;{" "}
                </button>
              </div>
              {/* <!-- Modal body --> */}
              <div className="modal-body">
                {/* <!-- start row --> */}
                <div className="row stock_screener_filters">
                  <div className="col-xl-12">
                    <form
                      className="feedback-form p-0"
                      id="feedback-form"
                      noValidate="novalidate"
                    >
                      <div className="form-group has-feedback">
                        <label htmlFor="">Filter Name</label>
                        <input
                          type="text"
                          name=""
                          id=""
                          className="form-control"
                          onChange={this.handleFilterNameChange}
                          value={filterName}
                        />
                      </div>
                      <button
                        type="button"
                        className="feedback-submit-btn text-uppercase"
                        onClick={this.saveFilter}
                      >
                        {" "}
                        {editableScreenerId == null ? "Save" : "Update"}{" "}
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(mapStateToProps, {})(LiveMarketScreener);
