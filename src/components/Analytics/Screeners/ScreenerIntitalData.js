export const LiveMarket = {
  intiSelectedFilterFields: {
    todayNR7: false,
    yesterdayNR7: false,
    todayGapUP: false,
    todayGapDown: false,
    yesterdayGapUP: false,
    yesterdayGapDown: false,
    todayStockOpenHigh: false,
    todayStockOpenLow: false,
    yesterdayStockOpenHigh: false,
    yesterdayStockOpenLow: false,
    range20DayUP: false,
    range50DayUP: false,
    range200DayUP: false,
    range52WeekHigh: false,
    range20DayDown: false,
    range50DayDown: false,
    range200DayDown: false,
    range52WeekLow: false,
    higherHighHigherLow: false,
    lowerHighLowerLow: false,
    insideDay: false,
    outsideDay: false,
    range0To100: false,
    range100To500: false,
    range500To1000: false,
    range1000To2000: false,
    rangeAbove2000: false,
    todayBullishHigh: false,
    todayBearishLow: false,
    todayNetural: false,
    yesterdayBullishHigh: false,
    yesterdayBearishLow: false,
    yesterdayNetural: false,
    todayAbove20SMA: false,
    todayBelow20SMA: false,
    todayAbove50SMA: false,
    todayBelow50SMA: false,
    todayAbove200SMA: false,
    todayBelow200SMA: false,
    yesterdayAbove20SMA: false,
    yesterdayBelow20SMA: false,
    yesterdayAbove50SMA: false,
    yesterdayBelow50SMA: false,
    yesterdayAbove200SMA: false,
    yesterdayBelow200SMA: false,
    todayHighVolumeDay: false,
    vwapAbove: false,
    vwapBelow: false,
    marketCapBelow1000: false,
    marketCap5000To20000: false,
    marketCapAbove50000: false,
    marketCap1000To5000: false,
    marketCap20000To50000: false,
    stockPEBelow5: false,
    stockPE10To20: false,
    stockPE50To100: false,
    stockPE5To10: false,
    stockPE20To50: false,
    stockPEAbove100: false,
    dividendYield0To1: false,
    dividendYield2To5: false,
    dividendYield1To2: false,
    dividendYieldAbove5: false,
    roceBelow5: false,
    roce10To20: false,
    roce50To70: false,
    roce5To10: false,
    roce20To50: false,
    roce70To100: false,
    roeBelow0: false,
    roe10To20: false,
    roeAbove50: false,
    roe0To10: false,
    roe20To50: false,
    salesGrowthBelow0: false,
    salesGrowth5To10: false,
    salesGrowth15To20: false,
    salesGrowth0To5: false,
    salesGrowth10To15: false,
    salesGrowthAbove20: false,
    piotroskiScore0To2: false,
    piotroskiScore3To7: false,
    piotroskiScore8To9: false,
    nifty50Stocks: false,
    fnoStocks: false,
    financial: false,
    nonFinancial: false,
    industry: "",
    maxPainAbove: false,
    maxPainBelow: false,
  },
  fieldName: {
    todayNR7: {
      groupName: "Today",
      fieldName: "Is NR7 Day",
    },
    yesterdayNR7: {
      groupName: "Yesterday",
      fieldName: "Was NR7 Day",
    },
    todayGapUP: {
      groupName: "Today",
      fieldName: "Gap Up",
    },
    todayGapDown: {
      groupName: "Today",
      fieldName: "Gap Down",
    },
    yesterdayGapUP: {
      groupName: "Yesterday",
      fieldName: "Gap Up",
    },
    yesterdayGapDown: {
      groupName: "Yesterday",
      fieldName: "Gap Down",
    },
    todayStockOpenHigh: {
      groupName: "Today",
      fieldName: "Stock with same open & high",
    },
    todayStockOpenLow: {
      groupName: "Today",
      fieldName: "Stock with same open & low",
    },
    yesterdayStockOpenHigh: {
      groupName: "Yesterday",
      fieldName: "Stock with same open & high",
    },
    yesterdayStockOpenLow: {
      groupName: "Yesterday",
      fieldName: "Stock with same open & low",
    },
    range20DayUP: {
      groupName: "Range Breakout",
      fieldName: "20 Day Range (Up)",
    },
    range50DayUP: {
      groupName: "Range Breakout",
      fieldName: "50 Day Range (Up)",
    },
    range200DayUP: {
      groupName: "Range Breakout",
      fieldName: "200 Day Range (Up)",
    },
    range52WeekHigh: {
      groupName: "Range Breakout",
      fieldName: "52 Week New High",
    },
    range20DayDown: {
      groupName: "Range Breakout",
      fieldName: "20 Day Range (Down)",
    },
    range50DayDown: {
      groupName: "Range Breakout",
      fieldName: "50 Day Range (Down)",
    },
    range200DayDown: {
      groupName: "Range Breakout",
      fieldName: "200 Day Range (Down)",
    },
    range52WeekLow: {
      groupName: "Range Breakout",
      fieldName: "52 Week New Low",
    },
    higherHighHigherLow: {
      groupName: "Price Action",
      fieldName: "Higher High Higher Low",
    },
    lowerHighLowerLow: {
      groupName: "Price Action",
      fieldName: "Lower High Lower Low",
    },
    insideDay: {
      groupName: "Price Action",
      fieldName: "Inside Day",
    },
    outsideDay: {
      groupName: "Price Action",
      fieldName: "Outside Day",
    },
    range0To100: {
      groupName: "Price Range",
      fieldName: "0~100",
    },
    range100To500: {
      groupName: "Price Range",
      fieldName: "100~500",
    },
    range500To1000: {
      groupName: "Price Range",
      fieldName: "500~1000",
    },
    range1000To2000: {
      groupName: "Price Range",
      fieldName: "1000~2000",
    },
    rangeAbove2000: {
      groupName: "Price Range",
      fieldName: "Above 2000",
    },
    todayBullishHigh: {
      groupName: "Today",
      fieldName: "Bullish (Close Near High)",
    },
    todayBearishLow: {
      groupName: "Today",
      fieldName: "Bearish (Close Near Low)",
    },
    todayNetural: {
      groupName: "Today",
      fieldName: "Netural",
    },
    yesterdayBullishHigh: {
      groupName: "Yesterday",
      fieldName: "Bullish (Close Near High)",
    },
    yesterdayBearishLow: {
      groupName: "Yesterday",
      fieldName: "Bearish (Close Near Low)",
    },
    yesterdayNetural: {
      groupName: "Yesterday",
      fieldName: "Netural",
    },
    todayAbove20SMA: {
      groupName: "Moving Average Today",
      fieldName: "Above 20 Day SMA",
    },
    todayBelow20SMA: {
      groupName: "Moving Average Today",
      fieldName: "Below 20 Day SMA",
    },
    todayAbove50SMA: {
      groupName: "Moving Average Today",
      fieldName: "Above 50 Day SMA",
    },
    todayBelow50SMA: {
      groupName: "Moving Average Today",
      fieldName: "Below 50 Day SMA",
    },
    todayAbove200SMA: {
      groupName: "Moving Average Today",
      fieldName: "Above 200 Day SMA",
    },
    todayBelow200SMA: {
      groupName: "Moving Average Today",
      fieldName: "Below 200 Day SMA",
    },
    yesterdayAbove20SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Above 20 Day SMA",
    },
    yesterdayBelow20SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Below 20 Day SMA",
    },
    yesterdayAbove50SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Above 50 Day SMA",
    },
    yesterdayBelow50SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Below 50 Day SMA",
    },
    yesterdayAbove200SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Above 200 Day SMA",
    },
    yesterdayBelow200SMA: {
      groupName: "Moving Average Yesterday",
      fieldName: "Below 200 Day SMA",
    },
    todayHighVolumeDay: {
      groupName: "Volume Shocker",
      fieldName: "High Volume Day (Today)",
    },
    vwapAbove: {
      groupName: "VWAP",
      fieldName: "Above",
    },
    vwapBelow: {
      groupName: "VWAP",
      fieldName: "Below",
    },
    marketCapBelow1000: {
      groupName: "Market Cap",
      fieldName: "Below 1,000 Cr.",
    },
    marketCap5000To20000: {
      groupName: "Market Cap",
      fieldName: "5,000 Cr - 20,000 Cr.",
    },
    marketCapAbove50000: {
      groupName: "Market Cap",
      fieldName: "Above 50,000 Cr.",
    },
    marketCap1000To5000: {
      groupName: "Market Cap",
      fieldName: "1,000 Cr - 5,000 Cr.",
    },
    marketCap20000To50000: {
      groupName: "Market Cap",
      fieldName: "20,000 Cr - 50,000 Cr.",
    },
    stockPEBelow5: {
      groupName: "Stock PE",
      fieldName: "Below 5",
    },
    stockPE10To20: {
      groupName: "Stock PE",
      fieldName: "10 - 20",
    },
    stockPE50To100: {
      groupName: "Stock PE",
      fieldName: "50 - 100",
    },
    stockPE5To10: {
      groupName: "Stock PE",
      fieldName: "5 - 10",
    },
    stockPE20To50: {
      groupName: "Stock PE",
      fieldName: "20 - 50",
    },
    stockPEAbove100: {
      groupName: "Stock PE",
      fieldName: "Above 100",
    },
    dividendYield0To1: {
      groupName: "Dividend Yield",
      fieldName: "0 - 1%",
    },
    dividendYield2To5: {
      groupName: "Dividend Yield",
      fieldName: "2 - 5%",
    },
    dividendYield1To2: {
      groupName: "Dividend Yield",
      fieldName: "1 - 2%",
    },
    dividendYieldAbove5: {
      groupName: "Dividend Yield",
      fieldName: "Above 5%",
    },
    roceBelow5: {
      groupName: "ROCE",
      fieldName: "Below 5",
    },
    roce10To20: {
      groupName: "ROCE",
      fieldName: "10 - 20",
    },
    roce50To70: {
      groupName: "ROCE",
      fieldName: "50 - 70",
    },
    roce5To10: {
      groupName: "ROCE",
      fieldName: "5 - 10",
    },
    roce20To50: {
      groupName: "ROCE",
      fieldName: "20 - 50",
    },
    roce70To100: {
      groupName: "ROCE",
      fieldName: "70 - 100",
    },
    roeBelow0: {
      groupName: "ROE",
      fieldName: "Below 0",
    },
    roe10To20: {
      groupName: "ROE",
      fieldName: "10 - 20",
    },
    roeAbove50: {
      groupName: "ROE",
      fieldName: "Above 50",
    },
    roe0To10: {
      groupName: "ROE",
      fieldName: "0 - 10",
    },
    roe20To50: {
      groupName: "ROE",
      fieldName: "20 - 50",
    },
    salesGrowthBelow0: {
      groupName: "Sales growth",
      fieldName: "Below 0",
    },
    salesGrowth5To10: {
      groupName: "Sales growth",
      fieldName: "5 - 10",
    },
    salesGrowth15To20: {
      groupName: "Sales growth",
      fieldName: "15 - 20",
    },
    salesGrowth0To5: {
      groupName: "Sales growth",
      fieldName: "0 - 5",
    },
    salesGrowth10To15: {
      groupName: "Sales growth",
      fieldName: "10 - 15",
    },
    salesGrowthAbove20: {
      groupName: "Sales growth",
      fieldName: "Above 20",
    },
    piotroskiScore0To2: {
      groupName: "Piotroski Score",
      fieldName: "0~2",
    },
    piotroskiScore3To7: {
      groupName: "Piotroski Score",
      fieldName: "3~7",
    },
    piotroskiScore8To9: {
      groupName: "Piotroski Score",
      fieldName: "8~9",
    },
    nifty50Stocks: {
      groupName: "Category",
      fieldName: "Nifty 50 Stocks",
    },
    fnoStocks: {
      groupName: "Category",
      fieldName: "FnO Stocks",
    },
    financial: {
      groupName: "Category",
      fieldName: "Financial",
    },
    nonFinancial: {
      groupName: "Category",
      fieldName: "Non-Financial",
    },
    industry: {
      groupName: "Industry",
      fieldName: "Industry",
    },
    maxPainAbove: {
      groupName: "LTP Max Pain",
      fieldName: "Above",
    },
    maxPainBelow: {
      groupName: "LTP Max Pain",
      fieldName: "Below",
    },
  }
}

export const Screener = {
  intiSelectedFilterFields: {
    openinterest_increase: false,
    openinterest_decrease: false,
    ltp_increase: false,
    ltp_decrease: false,
    type_call: true,
    type_put: true,
    series_current: true,
    series_farther: false,
    price_open_high: false,
    price_open_low: false,
    symbol: [{ label: "NIFTY", value: "NIFTY" }],
    MWPLAbove80: false,
    MWPL50_80: false,
    MWPLbelow50: false,
    lotsizeupto100: false,
    lotsizeupto1000: false,
    lotsizeupto5000: false,
    lotsizeupto5000plus: false,
    Underlyingprice_Increase: false,
    Underlyingprice_decrease: false,
    FilterType: true,
    exclude_oi: true,
    exclude_volume: true,
    include_Ltp: true,
    Monthly: false,
    weekly: false,
    expiry_m: false,
    include_oi: true,
    include_volume: true,
    proximity: false,
    include_oi_val: 100000,
    include_volume_val: 100000,
    proximity_val: null,
  },
  fieldName: {
    openinterest_increase: {
      groupName: "Open Interest",
      fieldName: "Increase",
    },
    openinterest_decrease: {
      groupName: "Open Interest",
      fieldName: "Decrease",
    },
    ltp_increase: {
      groupName: "Option LTP",
      fieldName: "Increase",
    },
    ltp_decrease: {
      groupName: "Option LTP",
      fieldName: "Decrease",
    },
    type_call: {
      groupName: "Type",
      fieldName: "Call",
    },
    type_put: {
      groupName: "Type",
      fieldName: "Put",
    },
    series_current: {
      groupName: "Series",
      fieldName: "Current",
    },
    series_farther: {
      groupName: "Series",
      fieldName: "Farther",
    },
    Underlyingprice_Increase: {
      groupName: "Underlying Price",
      fieldName: "Increase",
    },
    Underlyingprice_decrease: {
      groupName: "Underlying Price",
      fieldName: "Decrease",
    },
    exclude_oi: {
      groupName: "Include If",
      fieldName: `OI > 100000`,
    },
    exclude_volume: {
      groupName: "Include If",
      fieldName: `Volume > 100000`,
    },
    include_Ltp: {
      groupName: "Include If",
      fieldName: "LTP > 0",
    },
    proximity: {
      groupName: "Proximity",
      fieldName: `null`,
    },
    weekly: {
      groupName: "Expiry",
      fieldName: "Weekly",
    },
    expiry_m: {
      groupName: "Expiry",
      fieldName: "Monthly",
    },
    price_open_high: {
      groupName: "Opening Price Clues",
      fieldName: "Open = High",
    },
    price_open_low: {
      groupName: "Opening Price Clues",
      fieldName: "Open = Low",
    },
  }
};