import React, { Component } from 'react';
import RightSection from 'components/RightSection';
import { CALL_API, CALL_SIGNALR_API } from '_services/CALL_API';
import Loader from 'components/Loader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCaretUp,
  faCaretDown
} from "@fortawesome/free-solid-svg-icons";

class GapUpGapDowns extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: '',
      gapDateList: [],
      gapUpData: [],
      gapDownsData: [],
      isLoaded: false
    };
  }

  componentDidMount() {
    this.getGapDateList();
  }

  getGapDateList() {
    CALL_API('GET', process.env.GAP_DATE_LIST_DATA, {}, res => {
      this.setState({
        gapDateList: res['data'],
        selectedDate: res['data'][0]
      }, () => {
        this.getGapData()
      });
    });
  }

  getGapData() {
    CALL_API('POST', process.env.GAP_UP_GAP_DOWNS_DATA, { Date: this.state.selectedDate }, res => {
      var gapUpData = res['data']['gap_up_stocks'].sort(function (a, b) {
        if (a.symbol_name < b.symbol_name) { return -1; }
        if (a.symbol_name > b.symbol_name) { return 1; }
        return 0;
      })
      var gapDownsData = res['data']['gap_down_stocks'].sort(function (a, b) {
        if (a.symbol_name < b.symbol_name) { return -1; }
        if (a.symbol_name > b.symbol_name) { return 1; }
        return 0;
      })
      this.setState({
        gapUpData: gapUpData,
        gapDownsData: gapDownsData,
        isLoaded: true
      });
    });
  }

  setSelectedDate = (event) => {
    this.setState({
      selectedDate: event.target.value,
      isLoaded: false
    }, () => {
      this.getGapData();
    });
  }

  render() {
    const { gapDateList, gapUpData, gapDownsData, isLoaded } = this.state;
    let dateList = '';
    let gapUpDataList = '';
    let gapDownsDataList = '';
    if (gapDateList && gapDateList.length > 0) {
      dateList = gapDateList.map((item, key) => {
        return <option key={key}>{item}</option>
      });
    }

    if (gapUpData && gapUpData.length > 0) {
      gapUpDataList = gapUpData.map((item, key) => {
        let change = item['today_close'] - item['prev_close']
        let per = (change / item['prev_close']) * 100;
        return <tr className={`${item['today_low'] > item['prev_high'] ? 'tr-green' : 'tr-orange'}`}>
          <td className="text-uppercase">
            <a title={item['symbol_name']} href={`/stocks-analysis/${item['symbol_name'].toLowerCase()}`} style={{ color: '#319df4' }}>{item['symbol_name']}</a>
          </td>
          <td>{Number(item['today_close']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>{Number(item['today_open']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>{Number(item['today_high']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>{Number(item['today_low']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>{Number(item['prev_high']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>{Number(item['prev_close']).toFixed(2)}</td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'}`}>
            {Number(item['today_close']).toFixed(2)}{" "}
            {
              per >= 0 ? (
                <span className=""><FontAwesomeIcon
                  icon={faCaretUp}
                  width="7.5"
                  height="12"
                />{" "}(+{Number(per).toFixed(2)}%)</span>
              ) : (
                <span className=""><FontAwesomeIcon
                  icon={faCaretDown}
                  width="7.5"
                  height="12"
                />{" "}({Number(per).toFixed(2)}%)</span>
              )
            }
          </td>
          <td className={`${item['today_low'] > item['prev_high'] ? 'percent-green' : 'percent-orange'} text-center`}>
            < label className="percent-orange">{item['today_low'] > item['prev_high'] ? 'Gap UnFiiled' : 'Gap Fiiled'}</label>
          </td>
        </tr >
      });
    }

    if (gapDownsData && gapDownsData.length > 0) {
      gapDownsDataList = gapDownsData.map((item, key) => {
        let change = item['today_close'] - item['prev_close']
        let per = (change / item['prev_close']) * 100;
        return <tr className={`${item['today_high'] >= item['prev_low'] ? 'tr-orange' : 'tr-red'}`}>
          <td className="text-uppercase">
            <a title={item['symbol_name']} href={`/stocks-analysis/${item['symbol_name'].toLowerCase()}`} style={{ color: '#319df4' }}>{item['symbol_name']}</a>
          </td>
          <td>{item['today_close']}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>{Number(item['today_open']).toFixed(2)}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>{Number(item['today_high']).toFixed(2)}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>{Number(item['today_low']).toFixed(2)}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>{Number(item['prev_low']).toFixed(2)}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>{Number(item['prev_close']).toFixed(2)}</td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'}`}>
            {item['today_close']}{" "}
            {
              per >= 0 ? (
                <span className=""><FontAwesomeIcon
                  icon={faCaretUp}
                  width="6"
                  height="12"
                />{" "}(+{Number(per).toFixed(2)}%)</span>
              ) : (
                <span className=""><FontAwesomeIcon
                  icon={faCaretDown}
                  width="6"
                  height="12"
                />{" "}({Number(per).toFixed(2)}%)</span>
              )
            }
          </td>
          <td className={`${item['today_high'] >= item['prev_low'] ? 'percent-orange' : 'percent-red'} text-center`}>
            < label className="percent-orange">{item['today_high'] >= item['prev_low'] ? 'Gap Fiiled' : 'Gap UnFiiled'}</label>
          </td>
        </tr >
      });
    }
    return (
      <React.Fragment>
        {
          isLoaded ? '' : <Loader />
        }
        <section className="section1">
          <div className="container p-0">
            <div className="row">
              <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
                <div className="sidebar-search searchbar-responsive">
                  <div className="form-group">
                    <form action="">
                      <div className="ui-widget">
                        <input
                          placeholder="Search"
                          className="stocksearching autocomplete-ui ui-autocomplete-input"
                          autocomplete="off"
                        />
                        <span className="fa fa-search"></span>
                      </div>

                      <a
                        id="HideStockNavigator"
                        style={{ display: 'none' }}
                        href="stocks-analysis/acc"
                        className="d-none"
                      ></a>
                    </form>
                  </div>
                </div>
                <h1 className="main-page-heading">Gap Up Gap Down Data</h1>
                <div className="nifty-chart-panel">
                  <div className="gap-up-box">
                    <div className="row m-0">
                      <div className="col-md-8 col-xl-9 col-lg-9 col-6 p-0">
                        <ul
                          className="nav nav-pills flex-column"
                          id="pills-tab"
                          role="tablist"
                        >
                          <li className="nav-item">
                            <a
                              href="#pills-home"
                              className="nav-link active"
                              id="pills-home-tab"
                              data-toggle="pill"
                              role="tab"
                              aria-controls="pills-home"
                              aria-selected="true"
                              title="GAP UP"
                            >GAP UP</a
                            >
                          </li>
                          <li className="nav-item">
                            <a
                              href="#pills-gap"
                              className="nav-link"
                              id="pills-gap-tab"
                              data-toggle="pill"
                              role="tab"
                              aria-controls="pills-gap"
                              aria-selected="true"
                              title="GAP DOWN"
                            >GAP DOWN</a
                            >
                          </li>
                        </ul>
                      </div>
                      <div className="col-md-4 col-xl-3 col-lg-3 col-6 p-0">
                        <div className="select-dropdown">
                          <div className="form-group">
                            <label for="exampleFormControlSelect1">Date</label>
                            <select id="exampleFormControlSelect1" onChange={($event) => this.setSelectedDate($event)}>
                              {dateList}
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div
                    className="gap-up-content pricetable-box illustration-box border-top-0 p-0 mr-0"
                  >
                    <div className="tab-content" id="pills-tabContent">
                      <div
                        className="tab-pane fade show active"
                        id="pills-home"
                        role="tabpanel"
                        aria-labelledby="pills-home-tab"
                      >
                        <h2 className="mb-3">
                          Gap Up Stocks
                          <span className="gap-small-h">
                            (If Today's Open is higher than previous High -&gt; Gap Up)
                          </span>
                        </h2>
                        <div className="nifty-datatable table-responsive">
                          <table className="table" id="table">
                            <thead>
                              <tr>
                                <th className="">SYMBOL</th>
                                <th className="">Price</th>
                                <th className="">Open</th>
                                <th className="">High</th>
                                <th className="">Low</th>
                                <th className="">Prev High</th>
                                <th className="">Prev Close</th>
                                <th className="">Gap Up</th>
                                <th className="no-sort text-center">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              {gapUpDataList ? gapUpDataList : ''}
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <div
                        className="tab-pane fade show"
                        id="pills-gap"
                        role="tabpanel"
                        aria-labelledby="pills-gap-tab"
                      >
                        <h2 className="mb-3">
                          Gap Downs Stocks
                          <span className="gap-small-h">
                            (If Today's Open is higher than previous Low -&gt; Gap Down)
                          </span>
                        </h2>
                        <div className="nifty-datatable table-responsive">
                          <table className="table" id="table">
                            <thead>
                              <tr>
                                <th className="">SYMBOL</th>
                                <th className="">Price</th>
                                <th className="">Open</th>
                                <th className="">High</th>
                                <th className="">Low</th>
                                <th className="">Prev Low</th>
                                <th className="">Prev Close</th>
                                <th className="">Gap Down</th>
                                <th className="no-sort text-center">Status</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              {gapDownsDataList ? gapDownsDataList : ''}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <RightSection />
            </div>
          </div>
        </section>
      </React.Fragment >
    );
  }
}

export default GapUpGapDowns;