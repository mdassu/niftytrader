import React, { Component } from "react";
import { connect } from "react-redux";
import { CALL_API, CALL_SIGNALR_API } from "_services/CALL_API";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faCaretUp,
  faCircle,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import Loader from "components/Loader";
import * as moment from "moment";
import Nifty50Contents from "../LiveAnalyticsContent/Nifty50Contents";
import NiftyBankContents from "../LiveAnalyticsContent/NiftyBankContents";
import CandleStickChart from "components/Charts/CandleStickChart";
import Performance from "components/Analytics/LiveAnalytics/Performance";
import DayHighLowRange from "./DayHighLowRange";
import TraderToolkit from "components/TraderToolkit";
import * as signalR from "@microsoft/signalr";
import { AuthenticationService } from "_services/AuthenticationService";
import Router from "next/router";
import StockSearch from "components/StockSearch";
import SEOPageContent from "components/SEOPageContent";
import Link from "next/link";

const mapStateToProps = (state) => ({
  userData: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn,
});

const mapDispatchToProps = {};

class Analytics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      symbolSpotData: {},
      pageData: {},
      chartData: [],
      isLoaded: false,
      primeMember: false,
      userLogin: this.props.isLoggedIn,
    };
    this.connection = "";
  }

  componentDidMount() {
    const { userData, isLoggedIn } = this.props;
    // if (AuthenticationService.isLogged) {
    //   var currentUser = AuthenticationService.currentUserValue;
    //   if (currentUser['membership_flag'] == 1) {
    //     this.getSignalRData();
    //     this.getSymbolSpotData();
    //     this.getPageData();
    //     this.getChartData();
    //   } else {
    //     Router.push('/primeplans');
    //   }
    // } else {
    //   Router.push('/primeplans');
    // }

    this.setState({
      isLoaded: true,
      userLogin: isLoggedIn,
    });



    if (userData != null || userData !== undefined) {
      if (userData.membership_flag == 1) {
        this.getSignalRData();
        this.getSymbolSpotData();
        this.getPageData();
        this.getChartData();
        this.setState({
          primeMember: true,
        });
      } else {
        this.setState({
          primeMember: false,
          isLoaded: true
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { userData, isLoggedIn } = this.props;

    if (prevProps !== this.props) {
      this.setState(
        {
          userLogin: isLoggedIn,
          primeMember: userData.membership_flag == "1" ? true : false,
        },
        () => {
          if (isLoggedIn && userData.membership_flag == "0") {
            Router.push("/primeplans");
          }
        }
      );
    }

    if (prevProps !== this.props) {
      if (userData != null || userData !== undefined) {
        if (userData.membership_flag == 1) {
          this.getSignalRData();
          this.getSymbolSpotData();
          this.getPageData();
          this.getChartData();
          this.setState({
            primeMember: true,
          });
        } else {
          this.setState({
            primeMember: false,
          });
        }
      }
    }
  }

  getSignalRData = () => {
    if (this.props.symbol) {
      var day = moment().format("dddd");
      var currentDate = moment().format("MM/DD/YYYY");
      var currentDateTime = moment().format();
      var checkDateTime = moment(currentDate + " 16:00:00").format();
      var checkStartDateTime = moment(currentDate + " 09:00:00").format();
      if (
        day != "Saturday" &&
        day != "Sunday" &&
        checkDateTime > currentDateTime &&
        checkStartDateTime < currentDateTime
      ) {
        this.connection = new signalR.HubConnectionBuilder()
          .withUrl(
            "https://signalr.niftytrader.in/NiftySignalRTest/niftySignalRHub"
          )
          .build();

        this.connection.on(
          "sendTickDataToAndroidAtSA",
          (InstrumentToken, SymbolName, TickData) => {
            var spotData = {
              high: TickData["high"],
              low: TickData["low"],
              nifty_value: TickData["lastPrice"],
              open: TickData["open"],
              previous_close: TickData["close"],
            };
            this.setState({
              symbolSpotData: spotData,
            });
          }
        );

        this.connection.start().then(() => {
          this.addSymbolForSignalR(this.connection.connectionId);
        });
      }
    }
  };

  addSymbolForSignalR = (connectionId) => {
    CALL_SIGNALR_API(
      "GET",
      process.env.ADD_SYMBOL_FOR_SIGNALR,
      { Symbol: this.props.symbol, ConnectionId: connectionId },
      (res) => { }
    );
  };

  getPageData() {
    CALL_API(
      "GET",
      process.env.Live_Analytics_DATA,
      { RequestType: this.props.reqType, Symbolname: this.props.symbol },
      (res) => {
        if (res.status) {
          this.setState({
            pageData: res["data"],
            isLoaded: true,
          });
        } else {
          this.setState({
            pageData: [],
            isLoaded: true,
          });
        }
      }
    );
  }

  getSymbolSpotData() {
    CALL_API(
      "POST",
      process.env.SYMBOL_SPOT_DATA,
      { symbol: this.props.symbol },
      (res) => {
        this.setState({
          symbolSpotData: res["data"],
        });
      }
    );
  }

  getChartData() {
    CALL_API(
      "GET",
      process.env.Live_Analytics_CHART_DATA,
      { symbol: this.props.symbol },
      (res) => {
        this.setState({
          chartData: res["data"],
        });
      }
    );
  }

  explorePlans = () => {
    const { isLoggedIn } = this.props;
    const { primeMember, userLogin } = this.state;

    if (userLogin) {
      if (primeMember !== true) {
        this.setState({
          primeMember: false,
        });
        Router.push("/primeplans");
      } else {
        this.setState({
          primeMember: true,
        });
      }
    } else {
      $("#loginpopup").modal("show");
    }
  };

  componentWillUnmount = () => {
    if (this.props.symbol) {
      if (this.connection != "") {
        this.connection.stop();
      }
    }
  };

  render() {
    const { title, symbol, sarType, sar10Type, pageContent } = this.props;
    const {
      symbolSpotData,
      pageData,
      chartData,
      isLoaded,
      primeMember,
    } = this.state;
    var liveAnalyticsData = "";
    var tempLiveAnalyticsData = [];
    var pageBlogData = [];
    var performanceData = [];
    var a = 0.5,
      b = 0.75,
      c = 0.8,
      d = 1.56;
    var buyFrom = "0.00";
    var buyTo = "0.00";
    var buyLongs = "0.00";
    var buyBreakDownTarget = "0.00";
    var sellFrom = "0.00";
    var sellTo = "0.00";
    var sellShorts = "0.00";
    var sellBreakDownTarget = "0.00";
    var msgMomentum = "";
    var msgATR = "";
    var current_atr = 0;
    var headingATR = "";
    var JNSAR = sarType;
    var JNSAR10 = sar10Type;
    var jnsarColor = "";
    var jnsar10Color = "";
    var headingJnsar = "";
    var headingJnsar10 = "";
    var msgJnsar = "";
    var totalOIFrom = "";
    var totalOITo = "";
    var msgNewsOI = "";
    var msgOIChange = "";
    var msgOIChange = "";
    var jnsar_msg_1 = "";
    var dayHighLowRange = "";
    var beartrademsg = "";
    var bulltrademsg = "";
    if (pageData && Object.keys(pageData).length > 0) {
      pageBlogData = pageData["blog_data"];
      performanceData = pageData["performance"];
      tempLiveAnalyticsData = pageData["blog_data"]
        .sort((a, b) => {
          return b["created_at"] - a["created_at"];
        })
        .slice(0, 7);
      var blogEmaSma = pageData["blog_ema_sma"];
      buyFrom = blogEmaSma["close"] - b * blogEmaSma["atr"];
      buyTo = blogEmaSma["close"] - a * blogEmaSma["atr"];
      buyLongs = blogEmaSma["close"] - c * blogEmaSma["atr"];
      buyBreakDownTarget = blogEmaSma["close"] - d * blogEmaSma["atr"];

      sellFrom = blogEmaSma["close"] + a * blogEmaSma["atr"];
      sellTo = blogEmaSma["close"] + b * blogEmaSma["atr"];
      sellShorts = blogEmaSma["close"] + c * blogEmaSma["atr"];
      sellBreakDownTarget = blogEmaSma["close"] + d * blogEmaSma["atr"];

      beartrademsg =
        "Sell OTM Calls. Preferably " +
        pageData["calls_change_oi_intra"]["strike_price"] +
        " or higher strikes.";
      bulltrademsg =
        "Sell OTM Puts. Preferably " +
        pageData["puts_change_oi_intra"]["strike_price"] +
        " or lower strikes.";

      // Based on Momentum
      var momentum = pageData["momentum"];
      if (
        momentum["last_trigger"] == "Upside" &&
        tempLiveAnalyticsData[0]["close"] > tempLiveAnalyticsData[0]["hema"]
      ) {
        msgMomentum = "Buy & Hold, Upward Momentum continues to be strong";
      } else if (
        momentum["last_trigger"] == "Upside" &&
        tempLiveAnalyticsData[0]["close"] < tempLiveAnalyticsData[0]["hema"]
      ) {
        msgMomentum = "Sell on rises to CEMA/LEMA";
      } else if (
        momentum["last_trigger"] == "Downside" &&
        tempLiveAnalyticsData[0]["close"] < tempLiveAnalyticsData[0]["lema"]
      ) {
        msgMomentum = "Sell & Hold, Downward Momentum continues to be strong";
      } else if (
        momentum["last_trigger"] == "Downside" &&
        tempLiveAnalyticsData[0]["close"] >= tempLiveAnalyticsData[0]["lema"]
      ) {
        msgMomentum = "Buy on dips to CEMA/HEMA";
      }

      // Based on ATR
      if (symbolSpotData["nifty_value"] < buyBreakDownTarget) {
        current_atr = 1;
        headingATR = symbol + " broken down. Already below breakdown target.";
        msgATR = "Wait";
      } else if (
        symbolSpotData["nifty_value"] > buyBreakDownTarget &&
        symbolSpotData["nifty_value"] < buyLongs
      ) {
        current_atr = 2;
        headingATR = symbol + "  broken down.";
        msgATR =
          "Short     SL = " +
          Number(buyTo).toFixed(2) +
          " (" +
          Number(buyTo - symbolSpotData["nifty_value"]).toFixed(2) +
          " points). Risk:Reward::" +
          Number(buyTo - symbolSpotData["nifty_value"]).toFixed(2) +
          ":" +
          Number(symbolSpotData["nifty_value"] - buyBreakDownTarget).toFixed(2);
      } else if (
        symbolSpotData["nifty_value"] > buyLongs &&
        symbolSpotData["nifty_value"] < buyFrom
      ) {
        current_atr = 3;
        headingATR =
          symbol +
          " near breakdown zone. High Risk-reward longs. Keep strict SL of long positions at " +
          Number(buyLongs).toFixed(2);
        msgATR =
          "Long SL = " +
          Number(buyLongs).toFixed(2) +
          " (" +
          Number(symbolSpotData["nifty_value"] - buyLongs).toFixed(2) +
          " points).  Risk:Reward::" +
          Number(symbolSpotData["nifty_value"] - buyLongs).toFixed(2) +
          ":" +
          Number(sellFrom - symbolSpotData["nifty_value"]).toFixed(2);
      } else if (
        symbolSpotData["nifty_value"] > buyFrom &&
        symbolSpotData["nifty_value"] < buyTo
      ) {
        current_atr = 4;
        headingATR = symbol + " in buy zone. " + bulltrademsg;
        msgATR =
          "Long       SL = " +
          Number(buyLongs).toFixed(2) +
          " (" +
          Number(symbolSpotData["nifty_value"] - buyLongs).toFixed(2) +
          " points).  Risk:Reward::" +
          Number(symbolSpotData["nifty_value"] - buyLongs).toFixed(2) +
          ": " +
          Number(sellFrom - symbolSpotData["nifty_value"]).toFixed(2);
      } else if (
        symbolSpotData["nifty_value"] > buyTo &&
        symbolSpotData["nifty_value"] < sellFrom
      ) {
        current_atr = 5;
        if (symbolSpotData["nifty_value"] < buyTo + 12) {
          headingATR = symbol + " approaching Buy zone.";
          msgATR =
            "Look for Support to enter Long trade (Buy Future or sell Puts).";
        } else if (symbolSpotData["nifty_value"] > sellFrom - 12) {
          headingATR = symbol + " approaching Sell zone.";
          msgATR =
            "Look for Resistance to enter Short trade (Sell Future or sell Calls).";
        } else {
          headingATR = "Wait";
          msgATR = "Wait for signal.";
        }
      } else if (
        symbolSpotData["nifty_value"] > sellFrom &&
        symbolSpotData["nifty_value"] < sellTo
      ) {
        current_atr = 6;
        headingATR = symbol + " in sell zone. " + beartrademsg;
        msgATR =
          "Short       SL = " +
          Number(sellShorts).toFixed(2) +
          " (" +
          Number(sellShorts - symbolSpotData["nifty_value"]).toFixed(2) +
          " points). Risk:Reward::" +
          Number(sellShorts - symbolSpotData["nifty_value"]).toFixed(2) +
          ":" +
          Number(symbolSpotData["nifty_value"] - buyTo).toFixed(2);
      } else if (
        symbolSpotData["nifty_value"] > sellTo &&
        symbolSpotData["nifty_value"] < sellShorts
      ) {
        current_atr = 7;
        headingATR =
          symbol +
          " near breakout zone. High Risk-reward shorts. Keep strict SL of short positions at " +
          Number(sellShorts).toFixed(2);
        msgATR =
          "Short       SL = " +
          Number(sellShorts).toFixed(2) +
          " (" +
          Number(sellShorts - symbolSpotData["nifty_value"]).toFixed(2) +
          " points). Risk:Reward::" +
          Number(sellShorts - symbolSpotData["nifty_value"]).toFixed(2) +
          ":" +
          Number(symbolSpotData["nifty_value"] - buyTo).toFixed(2);
      } else if (
        symbolSpotData["nifty_value"] > sellShorts &&
        symbolSpotData["nifty_value"] < sellBreakDownTarget
      ) {
        current_atr = 8;
        headingATR =
          symbol +
          " broken out. Heading towards breakout target " +
          sellBreakDownTarget;
        msgATR =
          "Long SL = " +
          Number(sellFrom).toFixed(2) +
          " (" +
          Number(symbolSpotData["nifty_value"] - sellFrom).toFixed(2) +
          " points). Risk:Reward::" +
          Number(symbolSpotData["nifty_value"] - sellFrom).toFixed(2) +
          ":" +
          Number(sellBreakDownTarget - symbolSpotData["nifty_value"]).toFixed(
            2
          );
      } else if (symbolSpotData["nifty_value"] > sellBreakDownTarget) {
        current_atr = 9;
        headingATR = symbol + " broken out. Already above breakout target.";
        msgATR = "Wait";
      }

      var jnsarData = pageData["jnsar"];
      var j10sar = pageData["j10sar"];
      if (jnsarData["jnsar_direction"] == "Downside") {
        jnsarColor = "percent-red";
      } else if (jnsarData["jnsar_direction"] == "Upside") {
        jnsarColor = "percent-green";
      }

      if (j10sar["j10sar_direction"] == "Downside") {
        jnsar10Color = "percent-red";
      } else if (j10sar["j10sar_direction"] == "Upside") {
        jnsar10Color = "percent-green";
      }

      headingJnsar =
        "Last " +
        JNSAR +
        " was triggered on " +
        jnsarData["jnsar_direction"] +
        " at " +
        jnsarData["jnsar_value"] +
        " on date " +
        moment(jnsarData["jnsar_time"]).format("MM/DD/YYYY hh:mm:ss A");
      headingJnsar10 =
        "Last " +
        JNSAR10 +
        " was triggered on " +
        j10sar["j10sar_direction"] +
        " at " +
        j10sar["j10sar_value"] +
        " on date " +
        moment(j10sar["j10sar_time"]).format("MM/DD/YYYY hh:mm:ss A");

      var diffNiftyJnsar = Math.abs(
        symbolSpotData["nifty_value"] - tempLiveAnalyticsData[0]["jnsar"]
      );
      var diffNiftyJ10sar = Math.abs(
        symbolSpotData["nifty_value"] - tempLiveAnalyticsData[0]["j10sar"]
      );

      if (diffNiftyJ10sar <= 50 && diffNiftyJnsar <= 50) {
        msgJnsar =
          symbol +
          " is currently trading near " +
          JNSAR +
          " & " +
          JNSAR10 +
          ". SAR direction change is possible.";
      } else if (diffNiftyJnsar <= 50) {
        msgJnsar =
          symbol +
          " is currently trading near " +
          JNSAR +
          ". SAR direction change is possible.";
      } else if (diffNiftyJ10sar <= 50) {
        msgJnsar =
          symbol +
          " is currently trading near " +
          JNSAR10 +
          ". SAR direction change is possible.";
      } else {
        msgJnsar =
          symbol +
          " Spot currently trading far from " +
          JNSAR +
          " and " +
          JNSAR10 +
          "";
      }

      // Based on OI

      var callOIStrikepriceFilter = pageData["call_oi_strikeprice_filter"];
      var putsOIStrikepriceFilter = pageData["puts_oi_strikeprice_filter"];
      if (
        callOIStrikepriceFilter["calls_oi"] > putsOIStrikepriceFilter["puts_oi"]
      ) {
        totalOIFrom =
          "Highest Calls at strike price " +
          Number(callOIStrikepriceFilter["strike_price"]).toFixed(2) +
          " to act as resistance";
        totalOITo =
          "Highest Puts at strike price " +
          Number(putsOIStrikepriceFilter["strike_price"]).toFixed(2) +
          " to act as support";
      } else {
        totalOIFrom =
          "Highest Puts at strike price " +
          Number(putsOIStrikepriceFilter["strike_price"]).toFixed(2) +
          " to act as support";
        totalOITo =
          "Highest Calls at strike price " +
          Number(callOIStrikepriceFilter["strike_price"]).toFixed(2) +
          " to act as resistance";
      }
      msgNewsOI =
        Number(putsOIStrikepriceFilter["strike_price"]).toFixed(2) +
        " ~ " +
        Number(callOIStrikepriceFilter["strike_price"]).toFixed(2) +
        " range likely to hold";

      //Highest OI change
      var callsChangeOIIntra = pageData["calls_change_oi_intra"];
      var putsChangeOIIntra = pageData["puts_change_oi_intra"];
      if (
        callsChangeOIIntra["calls_change_oi"] >
        putsChangeOIIntra["puts_change_oi"]
      ) {
        jnsar_msg_1 =
          "Highest Calls writing at strike price " +
          callsChangeOIIntra["strike_price"];

        if (
          symbolSpotData["nifty_value"] > callsChangeOIIntra["strike_price"]
        ) {
          msgOIChange = "percent-red";
          msgOIChange =
            "Ultra Bearish OI. Sell Nifty Futures or Sell ITM Calls";
        } else {
          msgOIChange = "percent-red";
          msgOIChange =
            "Bearish OI. Sell OTM Calls. Preferably " +
            callsChangeOIIntra["strike_price"] +
            " or higher strikes.";
        }
      } else {
        jnsar_msg_1 =
          "Highest Puts writing at strike price " +
          putsChangeOIIntra["strike_price"];

        if (symbolSpotData["nifty_value"] < putsChangeOIIntra["strike_price"]) {
          msgOIChange = "percent-green";
          msgOIChange = "Ultra Bullish OI. Buy Nifty Futures or Sell ITM Puts";
        } else {
          msgOIChange = "percent-green";
          msgOIChange =
            "Bullish OI. Sell OTM Puts. Preferably " +
            putsChangeOIIntra["strike_price"] +
            " or lower strikes.";
        }
      }

      dayHighLowRange = {
        time10AM: {
          high:
            pageData["quote10"] && pageData["quote10"] != null
              ? Number(pageData["quote10"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote10"] && pageData["quote10"] != null
              ? Number(pageData["quote10"]["low"]).toFixed(2)
              : null,
        },
        time11AM: {
          high:
            pageData["quote11"] && pageData["quote11"] != null
              ? Number(pageData["quote11"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote11"] && pageData["quote11"] != null
              ? Number(pageData["quote11"]["low"]).toFixed(2)
              : null,
        },
        time12PM: {
          high:
            pageData["quote12"] && pageData["quote12"] != null
              ? Number(pageData["quote12"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote12"] && pageData["quote12"] != null
              ? Number(pageData["quote12"]["low"]).toFixed(2)
              : null,
        },
        time1PM: {
          high:
            pageData["quote13"] && pageData["quote13"] != null
              ? Number(pageData["quote13"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote13"] && pageData["quote13"] != null
              ? Number(pageData["quote13"]["low"]).toFixed(2)
              : null,
        },
        time2PM: {
          high:
            pageData["quote14"] && pageData["quote14"] != null
              ? Number(pageData["quote14"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote14"] && pageData["quote14"] != null
              ? Number(pageData["quote14"]["low"]).toFixed(2)
              : null,
        },
        time3PM: {
          high:
            pageData["quote15"] && pageData["quote15"] != null
              ? Number(pageData["quote15"]["high"]).toFixed(2)
              : null,
          low:
            pageData["quote15"] && pageData["quote15"] != null
              ? Number(pageData["quote15"]["low"]).toFixed(2)
              : null,
        },
      };
    }

    if (symbolSpotData) {
      var niftyValue = symbolSpotData["nifty_value"];
      var niftyValueDiff = Number(
        symbolSpotData["nifty_value"] - symbolSpotData["previous_close"]
      ).toFixed(2);
      var changePer = Number(
        (niftyValueDiff / symbolSpotData["nifty_value"]) * 100
      ).toFixed(2);
    }

    if (tempLiveAnalyticsData && tempLiveAnalyticsData.length > 0) {
      liveAnalyticsData = tempLiveAnalyticsData.map((item, key) => {
        // const index = pageBlogData.indexOf(0);
        // if (pageBlogData.length > 0) {
        // pageBlogData.splice(key, 1);
        // }
        return (
          <tr key={key}>
            <td>{moment(item["created_at"]).format("DD-MM-YYYY")}</td>
            <td>
              <span
                className={
                  item["high"] > pageBlogData[key + 1]["high"] &&
                    item["high"] > pageBlogData[key + 2]["high"]
                    ? "percent-green"
                    : item["high"] < pageBlogData[key + 1]["high"] &&
                      item["high"] < pageBlogData[key + 2]["high"]
                      ? "percent-red"
                      : "percent-black"
                }
              >
                {Number(item["high"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["low"] > pageBlogData[key + 1]["low"] &&
                    item["low"] > pageBlogData[key + 2]["low"]
                    ? "percent-green"
                    : item["low"] < pageBlogData[key + 1]["low"] &&
                      item["low"] < pageBlogData[key + 2]["low"]
                      ? "percent-red"
                      : "percent-black"
                }
              >
                {Number(item["low"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] > pageBlogData[key + 1]["close"] &&
                    item["close"] > pageBlogData[key + 2]["close"]
                    ? "percent-green"
                    : item["close"] < pageBlogData[key + 1]["close"] &&
                      item["close"] < pageBlogData[key + 2]["close"]
                      ? "percent-red"
                      : "percent-black"
                }
              >
                {Number(item["close"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["cema"] ? "percent-red" : "percent-green"
                }
              >
                {Number(item["cema"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["hema"] ? "percent-red" : "percent-green"
                }
              >
                {Number(item["hema"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["lema"] ? "percent-red" : "percent-green"
                }
              >
                {Number(item["lema"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["jnsar"]
                    ? "percent-red"
                    : "percent-green"
                }
              >
                {Number(item["jnsar"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["j10sar"]
                    ? "percent-red"
                    : "percent-green"
                }
              >
                {Number(item["j10sar"]).toFixed(2)}
              </span>
            </td>
            <td>
              <span
                className={
                  item["close"] < item["pivot"]
                    ? "percent-red"
                    : "percent-green"
                }
              >
                {Number(item["pivot"]).toFixed(2)}
              </span>
            </td>
          </tr>
        );
      });
    }

    return (
      <React.Fragment>
        {
          isLoaded ? "" : <Loader />
        }
        <div className="row">
          {/* <!-- Left Full panel --> */}
          {primeMember ? (
            <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
              {/* <!-- pricetable-box start --> <? */}
              <h1 className="main-page-heading">
                {pageContent["page_Content_Title"]}
              </h1>
              <div className="nifty-chart-panel mb-4">
                <div className="row justify-content-center align-items-center m-0">
                  <div className="col-lg-12 p-0">
                    {symbolSpotData ? (
                      <ul className="analytics-menu">
                        <li className="first-li">
                          <p className="current-price">Current price</p>
                          <h4>
                            <span
                              id="StockLTP"
                              className={
                                Math.sign(niftyValueDiff) == -1
                                  ? "newstockLTPDown"
                                  : "newstockLTPUP"
                              }
                            >
                              {niftyValue}
                            </span>{" "}
                            {Math.sign(niftyValueDiff) == -1 ? (
                              <FontAwesomeIcon
                                icon={faCaretDown}
                                width="10"
                                height="16"
                                className="percent-red"
                              />
                            ) : (
                              <FontAwesomeIcon
                                icon={faCaretUp}
                                width="10"
                                height="16"
                                className="percent-green"
                              />
                            )}{" "}
                            <span
                              id="StockPriceChange"
                              className={
                                Math.sign(niftyValueDiff) == -1
                                  ? "color-text-red"
                                  : "color-text"
                              }
                            >
                              {niftyValueDiff} ({changePer}%){" "}
                            </span>
                          </h4>
                        </li>
                        <li>
                          <h5>Open Price</h5>
                          <p className="percent-green">
                            {symbolSpotData["open"]}
                          </p>
                        </li>
                        <li>
                          <h5>Prev. Close</h5>
                          <p className="current-price">
                            {symbolSpotData["previous_close"]}
                          </p>
                        </li>
                        <li>
                          <h5>High</h5>
                          <p className="percent-green">
                            {symbolSpotData["high"]}
                          </p>
                        </li>
                        <li>
                          <h5>Low</h5>
                          <p className="percent-red">
                            {symbolSpotData["low"]}
                          </p>
                        </li>
                      </ul>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
              <div className="nifty-chart-panel pb-0 mb-4">
                <div className="row m-0">
                  <div className="col-lg-12 p-0">
                    <h5>Live Analytics</h5>
                    <div className="nifty-datatable table-responsive bankniftyDataTable">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>DATE</th>
                            <th>HIGH</th>
                            <th>LOW</th>
                            <th>CLOSE</th>
                            <th>5 EMA</th>
                            <th>5 HEMA</th>
                            <th>5 LEMA</th>
                            <th>{sarType}</th>
                            <th>{sar10Type}</th>
                            <th>PIVOT</th>
                          </tr>
                        </thead>
                        <tbody>
                          {liveAnalyticsData && liveAnalyticsData != ""
                            ? liveAnalyticsData
                            : ""}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="nifty-chart-panel pb-0">
                <div className="row m-0">
                  <div className="col-lg-12 p-0">
                    <h5>{title} ATR Levels for Session</h5>
                    <div className="nifty-datatable table-responsive bankniftyDataTable">
                      <table className="table" id="table-sort">
                        <tbody>
                          <tr>
                            <td>Buy Zone :</td>
                            <td className="percent-green">
                              From {Number(buyFrom).toFixed(2)} to{" "}
                              {Number(buyTo).toFixed(2)}
                            </td>
                          </tr>
                          <tr>
                            <td>Stop Loss for Longs (Breakdown Below) :</td>
                            <td className="percent-red">
                              {Number(buyLongs).toFixed(2)}
                            </td>
                          </tr>
                          <tr>
                            <td>Breakdown Target :</td>
                            <td className="percent-red">
                              {Number(buyBreakDownTarget).toFixed(2)}
                            </td>
                          </tr>
                          <tr>
                            <td>Sell Zone :</td>
                            <td className="percent-red">
                              From {Number(sellFrom).toFixed(2)} to{" "}
                              {Number(sellTo).toFixed(2)}
                            </td>
                          </tr>
                          <tr>
                            <td>Stop Loss for Shorts (Breakout Above):</td>
                            <td className="percent-green">
                              {Number(sellShorts).toFixed(2)}
                            </td>
                          </tr>
                          <tr>
                            <td>Breakdown Target :</td>
                            <td className="percent-green">
                              {Number(sellBreakDownTarget).toFixed(2)}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- Content Panel for nifty --> */}
              <div className="yesbank-box open-int-box pricetable-box">
                <ul className="momentum-content">
                  <h4>Based on Momentum (Prev Close):</h4>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {msgMomentum}
                  </li>
                </ul>
                <ul className="momentum-content">
                  <h4>Based on ATR: {headingATR}</h4>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                      Use these levels for intraday trades only.
                    </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {msgATR}
                  </li>
                </ul>
                <ul className="momentum-content">
                  <h4>Based on {JNSAR}</h4>
                  <li className={jnsarColor}>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                      className={jnsarColor}
                    />{" "}
                    {headingJnsar}
                  </li>
                  <li className={jnsar10Color}>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                      className={jnsar10Color}
                    />{" "}
                    {headingJnsar10}
                  </li>
                  <li className="percent-text">
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {msgJnsar}
                  </li>
                </ul>
                <ul className="momentum-content">
                  <h4>Based on OI: {msgNewsOI}</h4>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {totalOIFrom}
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {totalOITo}
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCircle}
                      width="6"
                      height="6"
                      style={{
                        marginRight: "10px",
                        width: ".5em",
                        height: ".5em",
                      }}
                    />{" "}
                    {msgOIChange}
                  </li>
                </ul>
              </div>
              {/* <!-- Ennd Content Panel for nifty --> */}
              {/* <!-- Chart  --> */}
              <div className="nifty-chart-panel">
                <div className="row m-0">
                  <div className="col-lg-12 p-0">
                    <div
                      className="chart-inner-div"
                      style={{ height: "400px" }}
                    >
                      {chartData && chartData.length > 0 ? (
                        <CandleStickChart chartData={chartData} />
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- !-- Chart  --> */}
              {symbol == "NIFTY 50" ? (
                <Nifty50Contents />
              ) : (
                <NiftyBankContents />
              )}
              {pageContent && pageContent["page_Content"] != "" ? (
                <SEOPageContent pageContent={pageContent["page_Content"]} />
              ) : (
                ""
              )}
            </div>
          ) : (
            <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
              {/* <!-- pricetable-box start --> <? */}
              <h1 className="main-page-heading">
                {pageContent["page_Content_Title"]}
              </h1>
              <div className="">
                <div className="col-md-12 mt-3 mb-4 p-0">
                  <div className="part_wise_oi_box text-center">
                    <img
                      src="/images/premium-feature.png"
                      className="options_blur_img img-fluid"
                    />
                    <div className="">
                      <div className="m-auto">
                        <div className="blur_options_content">
                          <span>Premium Feature</span>
                          <p>This feature available for Prime Members</p>

                          <a
                            href="javascript:void(0);"
                            onClick={this.explorePlans}
                            title="Explore Premium Plans"
                          >
                            Explore Premium Plans
                            </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                {pageContent && pageContent["page_Content"] != "" ? (
                  <SEOPageContent pageContent={pageContent["page_Content"]} />
                ) : (
                  ""
                )}
              </div>
            </div>
          )}

          {/* <!-- Right Panel --> */}
          {/* <?php include('sidebar.php'); ?> */}
          {/* <!-- End Right panel --> */}

          {primeMember ? (
            <div className="col-md-4 col-lg-3 col-sm-12 col-xl-3 col-12 p-0 page-side-bar">
              {/* <!-- searchbar start --> */}

              <div className="sidebar-search searchbar-desktop">
                <div className="form-group">
                  <div className="ui-widget">
                    {/* <input placeholder="Search" className="stocksearching autocomplete-ui" /> <span><FontAwesomeIcon icon={faSearch} width="16" height="16" />
                    </span> */}
                    <StockSearch placeHolder="Search Stocks" />
                  </div>
                  <a
                    id="HideStockNavigator"
                    style={{ display: "none" }}
                    href="stocks-analysis/acc"
                    className="d-none"
                  ></a>
                </div>
              </div>

              <Performance performanceData={performanceData} />

              <DayHighLowRange dayHighLowRange={dayHighLowRange} />

              <TraderToolkit />

              <div className="ad-box">
                <a
                  href={process.env.OLD_SITE_URL + "money/ipo/"}
                  target="_blank"
                  title="IPO Dashboard"
                  className="d-block text-center"
                >
                  <img src="/images/ad.png" alt="ad" className="img-fluid" />
                </a>
              </div>
            </div>
          ) : (
            <div className="col-md-4 col-lg-3 col-sm-12 col-xl-3 col-12 p-0 page-side-bar">
              {/* <!-- searchbar start --> */}

              <TraderToolkit />

              <div className="ad-box">
                <a
                  href={process.env.OLD_SITE_URL + "money/ipo/"}
                  target="_blank"
                  title="IPO Dashboard"
                  className="d-block text-center"
                >
                  <img src="/images/ad.png" alt="ad" className="img-fluid" />
                </a>
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

// export default Analytics;
export default connect(mapStateToProps, mapDispatchToProps)(Analytics);
