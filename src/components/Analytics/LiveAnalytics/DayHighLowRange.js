export default function DayHighLowRange(props) {
  const { dayHighLowRange } = props
  return (
    <div className="stock-analysis-aside mt-4 nifty-datatable table-responsive">
      <h5 className="text-uppercase">Day High Low Range</h5>
      <table className="table" border="0">
        <thead>
          <tr>
            <th scope="col">time</th>
            <th scope="col" width="50%" className="text-center">
              lowest
            </th>
            <th scope="col">highest</th>
          </tr>
        </thead>
        {
          dayHighLowRange && (
            <tbody>
            <tr>
              <td scope="row">10 AM </td>
              <td className="text-center">
                <span>{dayHighLowRange['time10AM']['low'] && dayHighLowRange['time10AM']['low'] != null ? dayHighLowRange['time10AM']['low'] : '-'}</span>
              </td>
              <td className="text-center">
                <span>{dayHighLowRange['time10AM']['high'] && dayHighLowRange['time10AM']['high'] != null ? dayHighLowRange['time10AM']['high'] : '-'}</span>
              </td>
            </tr>
            <tr>
              <td scope="row">11 AM </td>
              <td className="text-center">
                <span className={dayHighLowRange['time11AM']['low'] != null && dayHighLowRange['time11AM']['low'] < dayHighLowRange['time10AM']['low'] ? 'percent-red' : ''}>{dayHighLowRange['time11AM']['low'] && dayHighLowRange['time11AM']['low'] != null ? dayHighLowRange['time11AM']['low'] : '-'}</span>
              </td>
              <td className="text-center">
                <span className={dayHighLowRange['time11AM']['high'] != null && dayHighLowRange['time11AM']['high'] > dayHighLowRange['time10AM']['high'] ? 'percent-green' : ''}>{dayHighLowRange['time11AM']['high'] && dayHighLowRange['time11AM']['high'] != null ? dayHighLowRange['time11AM']['high'] : '-'}</span>
              </td>
            </tr>
            <tr>
              <td scope="row">12 PM</td>
              <td className="text-center"><span className={dayHighLowRange['time12PM']['low'] != null && dayHighLowRange['time12PM']['low'] < dayHighLowRange['time11AM']['low'] ? 'percent-red' : ''}>{dayHighLowRange['time12PM']['low'] && dayHighLowRange['time12PM']['low'] != null ? dayHighLowRange['time12PM']['low'] : '-'}</span></td>
              <td className="text-center"><span className={dayHighLowRange['time12PM']['high'] != null && dayHighLowRange['time12PM']['high'] > dayHighLowRange['time11AM']['high'] ? 'percent-green' : ''}>{dayHighLowRange['time12PM']['high'] && dayHighLowRange['time12PM']['high'] != null ? dayHighLowRange['time12PM']['high'] : '-'}</span></td>
            </tr>
            <tr>
              <td scope="row">1 PM</td>
              <td className="text-center"><span className={dayHighLowRange['time1PM']['low'] != null && dayHighLowRange['time1PM']['low'] < dayHighLowRange['time12PM']['low'] ? 'percent-red' : ''}>{dayHighLowRange['time1PM']['low'] && dayHighLowRange['time1PM']['low'] != null ? dayHighLowRange['time1PM']['low'] : '-'}</span></td>
              <td className="text-center"><span className={dayHighLowRange['time1PM']['high'] != null && dayHighLowRange['time1PM']['high'] > dayHighLowRange['time12PM']['high'] ? 'percent-green' : ''}>{dayHighLowRange['time1PM']['high'] && dayHighLowRange['time1PM']['high'] != null ? dayHighLowRange['time1PM']['high'] : '-'}</span></td>
            </tr>
            <tr>
              <td scope="row">2 PM</td>
              <td className="text-center"><span className={dayHighLowRange['time2PM']['low'] < dayHighLowRange['time1PM']['low'] ? 'percent-red' : ''}>{dayHighLowRange['time2PM']['low'] && dayHighLowRange['time2PM']['low'] != null ? dayHighLowRange['time2PM']['low'] : '-'}</span></td>
              <td className="text-center"><span className={dayHighLowRange['time2PM']['high'] > dayHighLowRange['time1PM']['high'] ? 'percent-green' : ''}>{dayHighLowRange['time2PM']['high'] && dayHighLowRange['time2PM']['high'] != null ? dayHighLowRange['time2PM']['high'] : '-'}</span></td>
            </tr>
            <tr>
              <td scope="row">3 PM</td>
              <td className="text-center"><span className={dayHighLowRange['time3PM']['low'] < dayHighLowRange['time2PM']['low'] ? 'percent-red' : ''}>{dayHighLowRange['time3PM']['low'] && dayHighLowRange['time3PM']['low'] != null ? dayHighLowRange['time3PM']['low'] : '-'}</span></td>
              <td className="text-center"><span className={dayHighLowRange['time3PM']['high'] < dayHighLowRange['time2PM']['high'] ? 'percent-green' : ''}>{dayHighLowRange['time3PM']['high'] && dayHighLowRange['time3PM']['high'] != null ? dayHighLowRange['time3PM']['high'] : '-'}</span></td>
            </tr>
          </tbody>
          )
        }
       
      </table>
    </div>
  );
}
