export default function Performance(props) {
  const { performanceData } = props;
  return (
    <div className="stock-analysis-aside nifty-datatable table-responsive">
      <h5 className="text-uppercase">
        PERFORMANCE
                        </h5>
      <table className="table" border="0">
        <thead>
          <tr>
            <th scope="col">Period</th>
            <th scope="col"></th>
            <th scope="col">% Chg</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td scope="row">5 Day</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">

                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">

                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_5_days']).toFixed(2)}%</span>

            </td>
          </tr>
          <tr>
            <td scope="row">15 day</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">

                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">

                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_15_days']).toFixed(2)}%</span>
            </td>
          </tr>
          <tr>
            <td scope="row">1 month</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">

                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">

                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_1_month']).toFixed(2)}%</span>
            </td>
          </tr>
          <tr>
            <td scope="row">3 month</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">
                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">
                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_3_month']).toFixed(2)}%</span>
            </td>
          </tr>
          <tr>
            <td scope="row">YTD</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">

                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">
                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_ytd_month']).toFixed(2)}%</span>
            </td>
          </tr>
          <tr>
            <td scope="row">1 Year</td>
            <td>
              <span>
                <div className="row bar">
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0 ">
                  </div>
                  <div className="col-lg-6 col-md-6 col-6 col-sm-6 col-xs-6 p-0">
                  </div>
                  <div className="border-r"></div>
                </div>
              </span>
            </td>
            <td>
              <span className="percent-green">{Number(performanceData['performance_year']).toFixed(2)}%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}