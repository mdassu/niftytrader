import React from 'react';
export default function Nifty50Contents() {
  return (
    <React.Fragment>
      <div className="yesbank-box open-int-box pricetable-box">
        <p>
          {" "}
                  The Nifty or Nifty 50 is NSE’s benchmark stock market index.
                  Introduced in 1996, nifty comprises of 50 actively traded NSE
                  stocks from 13 different sectors of the Indian economy.{" "}
        </p>
        <p>
          {" "}
                  Nifty is owned and managed by the NSE Strategic Investment
                  Corporation Limited.{" "}
        </p>
      </div>
      <div className="nifty-chart-panel pb-0 mb-4">
        <div className="row m-0">
          <div className="col-lg-12 p-0">
            <h5 className="pb-2">
              Nifty 50 weightage is divided as follows:
                    </h5>
            <div className="nifty-datatable table-responsive bankniftyDataTable">
              <table className="table">
                <thead>
                  <tr>
                    <th>Industry</th>
                    <th>Weightage</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Financial Services</td>
                    <td>39.47%</td>
                  </tr>
                  <tr>
                    <td>Energy</td>
                    <td>15.31%</td>
                  </tr>
                  <tr>
                    <td>Financial Services</td>
                    <td>39.47%</td>
                  </tr>
                  <tr>
                    <td>IT</td>
                    <td>13.01%</td>
                  </tr>
                  <tr>
                    <td>Consumer Goods</td>
                    <td>12.38%</td>
                  </tr>
                  <tr>
                    <td>Automobile</td>
                    <td>6.11%</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="yesbank-box open-int-box pricetable-box">
        <p>
          {" "}
                  The term Nifty is derived from two words. The words ‘National’
                  and ‘Fifty’ are combined to form Nifty. As of 2017, It
                  represented around 63% of the free float market capitalization
                  of the stocks listed on the National Stock Exchange.{" "}
        </p>
        <p>
          {" "}
                  Note: Nifty 50 Nifty Bank, Nifty IT, Nifty Pharma, Nifty SERV
                  SECTOR, Nifty Next 50, etc. are all different indices of the
                  National Stock Exchange.{" "}
        </p>
      </div>
      <div className="nifty-chart-panel pb-0">
        <div className="row m-0">
          <div className="col-lg-12 p-0">
            <h5 className="pb-2">
              {" "}
                      The Nifty 50 comprises of the following stock:{" "}
            </h5>
            <div className="nifty-datatable table-responsive bankniftyDataTable">
              <table className="table" id="table-sort">
                <thead>
                  <tr>
                    <th>Company</th>
                    <th>Symbol</th>
                    <th>Industry</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Adani Ports</td>
                    <td>
                      <a
                        title="ADANIPORTS"
                        href="stocks-analysis/adaniports"
                      >
                        ADANIPORTS
                              </a>
                    </td>
                    <td>Infrastructure</td>
                  </tr>
                  <tr>
                    <td>Asian Paints Ltd</td>
                    <td>
                      <a
                        title="ASIANPAINT"
                        href="stocks-analysis/asianpaint"
                      >
                        ASIANPAINT
                              </a>
                    </td>
                    <td>Consumer Goods</td>
                  </tr>
                  <tr>
                    <td>Axis Bank</td>
                    <td>
                      <a
                        title="AXISBANK"
                        href="stocks-analysis/axisbank"
                      >
                        AXISBANK
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>Bajaj Auto</td>
                    <td>
                      <a
                        title="BAJAJ-AUTO"
                        href="stocks-analysis/bajaj-auto"
                      >
                        BAJAJ-AUTO
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>Bajaj Finance</td>
                    <td>
                      <a
                        title="BAJFINANCE"
                        href="stocks-analysis/bajfinance"
                      >
                        BAJFINANCE
                              </a>
                    </td>
                    <td>Financial Services</td>
                  </tr>
                  <tr>
                    <td>Bajaj Finserv</td>
                    <td>
                      <a
                        title="BAJAJFINSV"
                        href="stocks-analysis/bajajfinsv"
                      >
                        BAJAJFINSV
                              </a>
                    </td>
                    <td>Financial Services</td>
                  </tr>
                  <tr>
                    <td>Bharti Airtel</td>
                    <td>
                      <a
                        title="BHARTIARTL"
                        href="stocks-analysis/bhartiartl"
                      >
                        BHARTIARTL
                              </a>
                    </td>
                    <td>Telecommunication</td>
                  </tr>
                  <tr>
                    <td>Bharti Infratel Ltd.</td>
                    <td>
                      <a
                        title="INFRATEL"
                        href="stocks-analysis/infratel"
                      >
                        INFRATEL
                              </a>
                    </td>
                    <td>Telecommunication</td>
                  </tr>
                  <tr>
                    <td>BPCL</td>
                    <td>
                      <a title="BPCL" href="stocks-analysis/bpcl">
                        BPCL
                              </a>
                    </td>
                    <td>Energy - Oil &amp; Gas</td>
                  </tr>
                  <tr>
                    <td>Cipla</td>
                    <td>
                      <a title="CIPLA" href="stocks-analysis/cipla">
                        CIPLA
                              </a>
                    </td>
                    <td>Pharmaceuticals</td>
                  </tr>
                  <tr>
                    <td>Coal India</td>
                    <td>
                      <a
                        title="COALINDIA"
                        href="stocks-analysis/coalindia"
                      >
                        COALINDIA
                              </a>
                    </td>
                    <td>Energy &amp; Mining</td>
                  </tr>
                  <tr>
                    <td>Dr Reddy Lab</td>
                    <td>
                      <a title="DRREDDY" href="stocks-analysis/drreddy">
                        DRREDDY
                              </a>
                    </td>
                    <td>Pharmaceuticals</td>
                  </tr>
                  <tr>
                    <td>Eicher Motors</td>
                    <td>
                      <a
                        title="EICHERMOT"
                        href="stocks-analysis/eichermot"
                      >
                        EICHERMOT
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>GAIL</td>
                    <td>
                      <a title="GAIL" href="stocks-analysis/gail">
                        GAIL
                              </a>
                    </td>
                    <td>Energy - Oil &amp; Gas</td>
                  </tr>
                  <tr>
                    <td>Grasim Industries</td>
                    <td>
                      <a title="GRASIM" href="stocks-analysis/grasim">
                        GRASIM
                              </a>
                    </td>
                    <td>Cement</td>
                  </tr>
                  <tr>
                    <td>HCL Technologies</td>
                    <td>
                      <a title="HCLTECH" href="stocks-analysis/hcltech">
                        HCLTECH
                              </a>
                    </td>
                    <td>Information Technology</td>
                  </tr>
                  <tr>
                    <td>HDFC</td>
                    <td>
                      <a title="HDFC" href="stocks-analysis/hdfc">
                        HDFC
                              </a>
                    </td>
                    <td>Financial Services</td>
                  </tr>
                  <tr>
                    <td>HDFC Bank</td>
                    <td>
                      <a
                        title="HDFCBANK"
                        href="stocks-analysis/hdfcbank"
                      >
                        HDFCBANK
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>Hero MotoCorp</td>
                    <td>
                      <a
                        title="HEROMOTOCO"
                        href="stocks-analysis/heromotoco"
                      >
                        HEROMOTOCO
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>Hindalco Industries</td>
                    <td>
                      <a
                        title="HINDALCO"
                        href="stocks-analysis/hindalco"
                      >
                        HINDALCO
                              </a>
                    </td>
                    <td>Metals</td>
                  </tr>
                  <tr>
                    <td>Hindustan Unilever</td>
                    <td>
                      <a
                        title="HINDUNILVR"
                        href="stocks-analysis/hindunilvr"
                      >
                        HINDUNILVR
                              </a>
                    </td>
                    <td>Consumer Goods</td>
                  </tr>
                  <tr>
                    <td>Britannia Industries</td>
                    <td>
                      <a
                        title="BRITANNIA"
                        href="stocks-analysis/britannia"
                      >
                        BRITANNIA
                              </a>
                    </td>
                    <td>Consumer Goods</td>
                  </tr>
                  <tr>
                    <td>ICICI Bank</td>
                    <td>
                      <a
                        title="ICICIBANK"
                        href="stocks-analysis/icicibank"
                      >
                        ICICIBANK
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>IndusInd Bank</td>
                    <td>
                      <a
                        title="INDUSINDBK"
                        href="stocks-analysis/indusindbk"
                      >
                        INDUSINDBK
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>Infosys</td>
                    <td>
                      <a title="INFY" href="stocks-analysis/infy">
                        INFY
                              </a>
                    </td>
                    <td>Information Technology</td>
                  </tr>
                  <tr>
                    <td>IOC</td>
                    <td>
                      <a title="IOC" href="stocks-analysis/ioc">
                        IOC
                              </a>
                    </td>
                    <td>Energy - Oil &amp; Gas</td>
                  </tr>
                  <tr>
                    <td>ITC Limited</td>
                    <td>
                      <a title="ITC" href="stocks-analysis/itc">
                        ITC
                              </a>
                    </td>
                    <td>Consumer Goods</td>
                  </tr>
                  <tr>
                    <td>JSW Steel</td>
                    <td>
                      <a
                        title="JSWSTEEL"
                        href="stocks-analysis/jswsteel"
                      >
                        JSWSTEEL
                              </a>
                    </td>
                    <td>Metals</td>
                  </tr>
                  <tr>
                    <td>Kotak Mahindra Bank</td>
                    <td>
                      <a
                        title="KOTAKBANK"
                        href="stocks-analysis/kotakbank"
                      >
                        KOTAKBANK
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>Larsen &amp; Toubro</td>
                    <td>
                      <a title="LT" href="stocks-analysis/lt">
                        LT
                              </a>
                    </td>
                    <td>Construction</td>
                  </tr>
                  <tr>
                    <td>Mahindra &amp; Mahindra</td>
                    <td>
                      <a title="M&amp;M" href="stocks-analysis/m&amp;m">
                        M&amp;M
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>Maruti Suzuki</td>
                    <td>
                      <a title="MARUTI" href="stocks-analysis/maruti">
                        MARUTI
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>Nestle India Ltd</td>
                    <td>
                      <a
                        title="NESTLEIND"
                        href="stocks-analysis/nestleind"
                      >
                        NESTLEIND
                              </a>
                    </td>
                    <td>Food Processing</td>
                  </tr>
                  <tr>
                    <td>NTPC Limited</td>
                    <td>
                      <a title="NTPC" href="stocks-analysis/ntpc">
                        NTPC
                              </a>
                    </td>
                    <td>Energy - Power</td>
                  </tr>
                  <tr>
                    <td>ONGC</td>
                    <td>
                      <a title="ONGC" href="stocks-analysis/ongc">
                        ONGC
                              </a>
                    </td>
                    <td>Energy - Oil &amp; Gas</td>
                  </tr>
                  <tr>
                    <td>PowerGrid Corporation of India</td>
                    <td>
                      <a
                        title="POWERGRID"
                        href="stocks-analysis/powergrid"
                      >
                        POWERGRID
                              </a>
                    </td>
                    <td>Energy - Power</td>
                  </tr>
                  <tr>
                    <td>Reliance Industries</td>
                    <td>
                      <a
                        title="RELIANCE"
                        href="stocks-analysis/reliance"
                      >
                        RELIANCE
                              </a>
                    </td>
                    <td>Energy - Oil &amp; Gas</td>
                  </tr>
                  <tr>
                    <td>State Bank of India</td>
                    <td>
                      <a title="SBIN" href="stocks-analysis/sbin">
                        SBIN
                              </a>
                    </td>
                    <td>Banking</td>
                  </tr>
                  <tr>
                    <td>Sun Pharmaceutical</td>
                    <td>
                      <a
                        title="SUNPHARMA"
                        href="stocks-analysis/sunpharma"
                      >
                        SUNPHARMA
                              </a>
                    </td>
                    <td>Pharmaceuticals</td>
                  </tr>
                  <tr>
                    <td>Tata Consultancy Services</td>
                    <td>
                      <a title="TCS" href="stocks-analysis/tcs">
                        TCS
                              </a>
                    </td>
                    <td>Information Technology</td>
                  </tr>
                  <tr>
                    <td>Tata Motors</td>
                    <td>
                      <a
                        title="TATAMOTORS"
                        href="stocks-analysis/tatamotors"
                      >
                        TATAMOTORS
                              </a>
                    </td>
                    <td>Automobile</td>
                  </tr>
                  <tr>
                    <td>Tata Steel</td>
                    <td>
                      <a
                        title="TATASTEEL"
                        href="stocks-analysis/tatasteel"
                      >
                        TATASTEEL
                              </a>
                    </td>
                    <td>Metals</td>
                  </tr>
                  <tr>
                    <td>Tech Mahindra</td>
                    <td>
                      <a title="TECHM" href="stocks-analysis/techm">
                        TECHM
                              </a>
                    </td>
                    <td>Information Technology</td>
                  </tr>
                  <tr>
                    <td>Titan Company</td>
                    <td>
                      <a title="TITAN" href="stocks-analysis/titan">
                        TITAN
                              </a>
                    </td>
                    <td>Consumer Goods</td>
                  </tr>
                  <tr>
                    <td>UltraTech Cement</td>
                    <td>
                      <a
                        title="ULTRACEMCO"
                        href="stocks-analysis/ultracemco"
                      >
                        ULTRACEMCO
                              </a>
                    </td>
                    <td>Cement</td>
                  </tr>
                  <tr>
                    <td>United Phosphorus Limited</td>
                    <td>
                      <a title="UPL" href="stocks-analysis/upl">
                        UPL
                              </a>
                    </td>
                    <td>Chemicals</td>
                  </tr>
                  <tr>
                    <td>Vedanta</td>
                    <td>
                      <a title="VEDL" href="stocks-analysis/vedl">
                        VEDL
                              </a>
                    </td>
                    <td>Metals</td>
                  </tr>
                  <tr>
                    <td>Wipro</td>
                    <td>
                      <a title="WIPRO" href="stocks-analysis/wipro">
                        WIPRO
                              </a>
                    </td>
                    <td>Information Technology</td>
                  </tr>
                  <tr>
                    <td>Shree Cement</td>
                    <td>
                      <a
                        title="SHREECEM"
                        href="stocks-analysis/shreecem"
                      >
                        SHREECEM
                              </a>
                    </td>
                    <td>Cement</td>
                  </tr>
                  <tr>
                    <td>Zee Entertainment Enterprises</td>
                    <td>
                      <a title="ZEEL" href="stocks-analysis/zeel">
                        ZEEL
                              </a>
                    </td>
                    <td>Media &amp; Entertainment</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="yesbank-box open-int-box pricetable-box">
        <p>
          {" "}
                  November 3, 1995 is set as the base period for Nifty 50. 1000
                  is the base value and Rs2.06 trillion is set as the base
                  capital for Nifty 50.{" "}
        </p>
      </div>
      <div className="nifty-chart-panel pb-0 mb-4">
        <div className="row m-0">
          <div className="col-lg-12 p-0">
            <h5 className="pb-2">
              {" "}
                      Following are the annual returns of Nifty 50:{" "}
            </h5>
            <div className="nifty-datatable table-responsive bankniftyDataTable">
              <table className="table">
                <thead>
                  <tr>
                    <th>Year</th>
                    <th>Closing Level</th>
                    <th>Percentage Change</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2001</td>
                    <td>1059.05</td>
                    <td>−13.94%</td>
                  </tr>
                  <tr>
                    <td>2002</td>
                    <td>1093.5</td>
                    <td>3.25%</td>
                  </tr>
                  <tr>
                    <td>2003</td>
                    <td>1879.75</td>
                    <td>71.9%</td>
                  </tr>
                  <tr>
                    <td>2004</td>
                    <td>2080.5</td>
                    <td>10.68%</td>
                  </tr>
                  <tr>
                    <td>2005</td>
                    <td>2836.55</td>
                    <td>36.34%</td>
                  </tr>
                  <tr>
                    <td>2006</td>
                    <td>3966.4</td>
                    <td>39.83%</td>
                  </tr>
                  <tr>
                    <td>2007</td>
                    <td>6138.6</td>
                    <td>54.77%</td>
                  </tr>
                  <tr>
                    <td>2008</td>
                    <td>2959.15</td>
                    <td>−51.79%</td>
                  </tr>
                  <tr>
                    <td>2009</td>
                    <td>5201.05</td>
                    <td>75.76%</td>
                  </tr>
                  <tr>
                    <td>2010</td>
                    <td>6134.5</td>
                    <td>17.95%</td>
                  </tr>
                  <tr>
                    <td>2011</td>
                    <td>4624.3</td>
                    <td>−24.62%</td>
                  </tr>
                  <tr>
                    <td>2012</td>
                    <td>5905.1</td>
                    <td>27.7%</td>
                  </tr>
                  <tr>
                    <td>2013</td>
                    <td>6304</td>
                    <td>6.76%</td>
                  </tr>
                  <tr>
                    <td>2014</td>
                    <td>8282.7</td>
                    <td>31.39%</td>
                  </tr>
                  <tr>
                    <td>2015</td>
                    <td>7964.35</td>
                    <td>−3.84%</td>
                  </tr>
                  <tr>
                    <td>2016</td>
                    <td>8185.8</td>
                    <td>3.01%</td>
                  </tr>
                  <tr>
                    <td>2017</td>
                    <td>10530.7</td>
                    <td>28.65%</td>
                  </tr>
                  <tr>
                    <td>2018</td>
                    <td>10862.55</td>
                    <td>3.15%</td>
                  </tr>
                  <tr>
                    <td>2019</td>
                    <td>12168.45</td>
                    <td>12.02%</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </React.Fragment>
  )
}