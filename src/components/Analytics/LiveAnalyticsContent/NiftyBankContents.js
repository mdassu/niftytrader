import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircle,
} from "@fortawesome/free-solid-svg-icons";
export default function NiftyBankContents() {
  return (
    <React.Fragment>
      <div className="yesbank-box open-int-box pricetable-box">
        <p>
          Bank Nifty is the representstion of 12 Large Cap stocks from the
          banking sector that trade on the National stock Exchange. The index
          was started in the year 2000 by India Index Service and Product
          Limited (IISL), which is a subsidiary of NSE.
                        </p>
        <p>
          Bank Nifty provides investors and traders a benchmark that offers a
          reflection of the Indian Banking Industry. It is calculated using
          the free float market capitalization method. The base date for Bank
          Nifty is Jan 1, 2000. The base value is 1000.
                        </p>
        <p>
          Bank Nifty represents around 13.34% of the free float market
          capitalization of NSE stocks. Also, it represents 86.13% of the free
          float market capitalization of the Banking sector stocks. (As of
          December 30,2011)
                        </p>
        <p>
          Futures contracts of Bank Nifty derivatives were started in June,
          2005.
                        </p>

        <h3>Bank Nifty’s criteria for stock selection:</h3>
        <ul className="list-style">
          <li>
            Company's market capitalisation rank in the universe should be
            less than 500.
                            </li>
          <li>
            Company's turnover rank in the universe should be less than 500.
                            </li>
          <li>
            Company's trading frequency should be at least 90% in the last six
            months.
                            </li>
          <li>Company should have a positive networth.</li>
          <li>
            A company which comes out with a IPO will be eligible for
            inclusion in the index, if it fulfills the normal eligibility
            criteria for the index for a 3 month period instead of a 6 month
            period.
                            </li>
        </ul>
      </div>
      <div className="nifty-chart-panel pb-0">
        <div className="row m-0">
          <div className="col-lg-12 p-0">
            <h5 className="pb-2">Bank Nifty Constituents:</h5>
            <div className="nifty-datatable table-responsive bankniftyDataTable">

              <table className="table" id="table-sort">
                <thead>
                  <tr>
                    <th className="">Company Name</th>
                    <th className="">Symbol</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Axis Bank Ltd.
                                                </td>
                    <td><a title="AXISBANK" href="stocks-analysis/axisbank">AXISBANK</a></td>
                  </tr>
                  <tr>
                    <td>
                      Bandhan Bank Ltd.
                                                </td>
                    <td>
                      <a title="BANDHANBNK" href="stocks-analysis/bandhanbnk">BANDHANBNK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Bank of Baroda
                                                </td>
                    <td>
                      <a title="BANKBARODA" href="stocks-analysis/bankbaroda">BANKBARODA</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Federal Bank Ltd.
                                                </td>
                    <td>
                      <a title="FEDERALBNK" href="stocks-analysis/federalbnk">FEDERALBNK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      HDFC Bank Ltd.
                                                </td>
                    <td>
                      <a title="HDFCBANK" href="stocks-analysis/hdfcbank">HDFCBANK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      ICICI Bank Ltd.
                                                </td>
                    <td>
                      <a title="ICICIBANK" href="stocks-analysis/icicibank">ICICIBANK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      IDFC First Bank Ltd.
                                                </td>
                    <td>
                      <a title="IDFCFIRSTB" href="stocks-analysis/idfcfirstb">IDFCFIRSTB</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      IndusInd Bank Ltd.
                                                </td>
                    <td>
                      <a title="INDUSINDBK" href="stocks-analysis/indusindbk">INDUSINDBK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Kotak Mahindra Bank Ltd.
                                                </td>
                    <td>
                      <a title="KOTAKBANK" href="stocks-analysis/kotakbank">KOTAKBANK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Punjab National Bank
                                                </td>
                    <td>
                      <a title="PNB" href="stocks-analysis/pnb">PNB</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      RBL Bank Ltd.
                                                </td>
                    <td>
                      <a title="RBLBANK" href="stocks-analysis/rblbank">RBLBANK</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      State Bank of India
                                                </td>
                    <td>
                      <a title="SBIN" href="stocks-analysis/sbin">SBIN</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}