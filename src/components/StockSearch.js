import React, { Component } from 'react';
import Cookies from "js-cookie";
import Link from "next/link";
import { decrypt } from "_helper/EncrDecrypt";
import { CALL_API } from "_services/CALL_API";
import Loader from "./Loader";
import TraderToolkit from './TraderToolkit';
import { ReactSearchAutocomplete } from "react-search-autocomplete";

export default class StockSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latestNews: [],
      allSymbolList: []
    }
  }

  componentDidMount() {
    this.getSymbolListData();
  }

  getSymbolListData() {
    CALL_API("GET", process.env.STOCK_LIST_SEARCH_DATA, {}, (res) => {
      if (res.status) {
        var symbols = [];
        res["data"].map((item) => {
          symbols.push({
            id: item["symbol_name"],
            name: item["symbol_name"],
          });
        });
        this.setState({
          allSymbolList: symbols,
        });
      } else {
        this.setState({
          allSymbolList: [],
        });
      }
    });
  }

  handleSymbolSelect = (item) => {
    window.location.href = process.env.OLD_SITE_URL + 'stocks-analysis/' + item['name'].split(' | ')[0];
  }

  render() {
    const { allSymbolList } = this.state;
    return (
      <ReactSearchAutocomplete
        items={allSymbolList}
        onSelect={this.handleSymbolSelect}
        placeholder={this.props.placeHolder}
      />
    )
  }

}
