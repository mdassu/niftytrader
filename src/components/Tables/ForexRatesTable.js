import {
  ColumnDirective,
  ColumnsDirective,
  Filter,
  GridComponent,
  Group,
  Inject,
  Page,
  Toolbar,
  PageSettingsModel,
  Search,
  Sort,
  Grid,
  Resize
} from "@syncfusion/ej2-react-grids";
import { getValue, setValue } from '@syncfusion/ej2-base';
import * as React from "react";
import '@syncfusion/ej2-base/styles/material.css';
// import '@syncfusion/ej2-buttons/styles/material.css';
import '@syncfusion/ej2-calendars/styles/material.css';
import '@syncfusion/ej2-dropdowns/styles/material.css';
import '@syncfusion/ej2-inputs/styles/material.css';
// import '@syncfusion/ej2-navigations/styles/material.css';
import '@syncfusion/ej2-popups/styles/material.css';
import '@syncfusion/ej2-splitbuttons/styles/material.css';
import "@syncfusion/ej2-react-grids/styles/material.css";

export default class ForexRatesTable extends React.Component {
  constructor(props) {
    super(props);
    this.pageSettings = { pageSize: 10, pageSizes: true, pageCount: 5 };
    this.toolbarOptions = ['Search'];
  }

  filterOptions = {
    type: 'Menu',
  };

  render() {
    const { tableColumns, tableRows } = this.props;
    return (
      <GridComponent
        dataSource={tableRows}
        dataBound={this.dataBound}
        allowPaging={true}
        pageSettings={this.pageSettings}
        className="table"
        toolbar={this.toolbarOptions}
        allowSorting={true}
        allowFiltering={true}
        filterSettings={{
          type: 'Menu',

        }}

      >
        <ColumnsDirective>
          {
            tableColumns && tableColumns.length > 0 ?
              tableColumns.map((item, key) => {
                return (
                  <ColumnDirective key={key} field={item['field']} headerText={item['headerText']} allowFiltering={true} />
                )
              }) : ''
          }
        </ColumnsDirective>
        <Inject services={[Page, Sort, Filter, Group, Search, Toolbar, Resize]} />
      </GridComponent>
    );
  }
}
