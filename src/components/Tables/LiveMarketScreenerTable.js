import {
  ColumnDirective,
  ColumnsDirective,
  Filter,
  GridComponent,
  Group,
  Inject,
  Page,
  Toolbar,
  GroupSettingsModel,
  PageSettingsModel,
  Search,
  Sort,
  Resize,
  DetailRow,
} from "@syncfusion/ej2-react-grids";
import { getValue, setValue } from "@syncfusion/ej2-base";
import * as React from "react";
import "@syncfusion/ej2-base/styles/material.css";
// import "@syncfusion/ej2-buttons/styles/material.css";
import "@syncfusion/ej2-calendars/styles/material.css";
import "@syncfusion/ej2-dropdowns/styles/material.css";
import "@syncfusion/ej2-inputs/styles/material.css";
// import "@syncfusion/ej2-navigations/styles/material.css";
import "@syncfusion/ej2-popups/styles/material.css";
import "@syncfusion/ej2-splitbuttons/styles/material.css";
import "@syncfusion/ej2-react-grids/styles/material.css";

export default class LiveMarketScreenerTable extends React.Component {
  constructor(props) {
    super(props);
    this.pageSettings = { pageSize: 10, pageSizes: true, pageCount: 5 };
    this.toolbarOptions = ["Search"];
    // this.groupSettings= { columns: ['stock'] };

    this.childGridOptions = {
      columns: [
        {
          field: "OrderID",
          headerText: "Order ID",
          textAlign: "Right",
          width: 120,
        },
        { field: "CustomerID", headerText: "Customer ID", width: 150 },
        { field: "ShipCity", headerText: "Current", width: 150 },
        { field: "ShipName", headerText: "Stock", width: 150 },
      ],
      dataSource: "",
      queryString: "stock",
    };
  }
  customizeCell(args) {
    if (args.column.field === "change" || args.column.field === "change_per") {
      var changeValue = getValue("change", args.data);
      if (Math.sign(changeValue) == -1) {
        args.cell.classList.add("percent-red");
        if (args.column.field === "change_per") {
          args.cell.classList.add("percent-red");
        }
      } else if (Math.sign(changeValue) == 1) {
        args.cell.classList.add("percent-green");
        if (args.column.field === "change_per") {
          args.cell.classList.add("percent-green");
        }
      }
    }
  }

  // dataManger = [{ Order: 100, ShipName: 'Berlin', instType: 'NIFTY2150614300CE', template: '<div className="d-inline-block wd-40">Chart 1</div><div className="d-inline-block wd-60 adv_expanded_data"><div className="row"><div className="col-6 pl-0" style="margin-bottom: 20px"> <a href="stocks-analysis/3iinfotech" target="_blank" className="inside_stock_name" > 3IINFOTECH<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-box-arrow-up-right" viewBox="0 0 16 16" > <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" ></path> <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" ></path> </svg> </a></div><div className="col-6 text-right"><div className="stock-analysis-right"><ul style="padding-right: 14px !important" className="adv-stock-analysis-right" ><li className="stock-analysis-add-to-watchlist"> <a href="javascript:void(0);" ><span className="stock-analysis-box">Add to watchlist</span></a ></li><li style="cursor: pointer" className="stock-analysis-alert-btn" title="Save alert" > <a className="stock-analysis-box" href="#" data-toggle="modal" data-target="#create-alert-for-banking" >Create alert</a ></li></ul></div></div><div className="col-4"><div className="inside_st_details"> 52 Week Range <span>1.15 ~ 9.30</span></div></div><div className="col-4"><div className="inside_st_details"> 5 EMA <span className="percent-green">7.65</span></div></div><div className="col-4"><div className="inside_st_details"> 20 EMA <span className="percent-green">7.45</span></div></div><div className="col-4"><div className="inside_st_details"> 50 SMA <span className="percent-green">7.35</span></div></div><div className="col-4"><div className="inside_st_details"> 200 SMA <span className="percent-green">5.00</span></div></div><div className="col-4"><div className="inside_st_details"> AVG VOL <span>13909155</span></div></div><div className="col-4"><div className="inside_st_details"> DELIVERY % <span>55.55</span></div></div><div className="col-4"><div className="inside_st_details"> RSI <span>57.10</span></div></div></div></div>' },
  // { Order: 101, ShipName: 'Capte', instType: 'NIFTY2150614400CE', template: '<div className="d-inline-block wd-40">chart 2</div><div className="d-inline-block wd-60 adv_expanded_data"><div className="row"><div className="col-6 pl-0" style="margin-bottom: 20px"> <a href="stocks-analysis/3iinfotech" target="_blank" className="inside_stock_name" > 3IINFOTECH<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-box-arrow-up-right" viewBox="0 0 16 16" > <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" ></path> <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" ></path> </svg> </a></div><div className="col-6 text-right"><div className="stock-analysis-right"><ul style="padding-right: 14px !important" className="adv-stock-analysis-right" ><li className="stock-analysis-add-to-watchlist"> <a href="javascript:void(0);" ><span className="stock-analysis-box">Add to watchlist</span></a ></li><li style="cursor: pointer" className="stock-analysis-alert-btn" title="Save alert" > <a className="stock-analysis-box" href="#" data-toggle="modal" data-target="#create-alert-for-banking" >Create alert</a ></li></ul></div></div><div className="col-4"><div className="inside_st_details"> 52 Week Range <span>1.15 ~ 9.30</span></div></div><div className="col-4"><div className="inside_st_details"> 5 EMA <span className="percent-green">7.65</span></div></div><div className="col-4"><div className="inside_st_details"> 20 EMA <span className="percent-green">7.45</span></div></div><div className="col-4"><div className="inside_st_details"> 50 SMA <span className="percent-green">7.35</span></div></div><div className="col-4"><div className="inside_st_details"> 200 SMA <span className="percent-green">5.00</span></div></div><div className="col-4"><div className="inside_st_details"> AVG VOL <span>13909155</span></div></div><div className="col-4"><div className="inside_st_details"> DELIVERY % <span>55.55</span></div></div><div className="col-4"><div className="inside_st_details"> RSI <span>57.10</span></div></div></div></div>' },
  // { Order: 102, ShipName: 'Marlon', instType: 'NIFTY2150614450CE', template: '<div className="d-inline-block wd-40">chart 3</div><div className="d-inline-block wd-60 adv_expanded_data"><div className="row"><div className="col-6 pl-0" style="margin-bottom: 20px"> <a href="stocks-analysis/3iinfotech" target="_blank" className="inside_stock_name" > 3IINFOTECH<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-box-arrow-up-right" viewBox="0 0 16 16" > <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" ></path> <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" ></path> </svg> </a></div><div className="col-6 text-right"><div className="stock-analysis-right"><ul style="padding-right: 14px !important" className="adv-stock-analysis-right" ><li className="stock-analysis-add-to-watchlist"> <a href="javascript:void(0);" ><span className="stock-analysis-box">Add to watchlist</span></a ></li><li style="cursor: pointer" className="stock-analysis-alert-btn" title="Save alert" > <a className="stock-analysis-box" href="#" data-toggle="modal" data-target="#create-alert-for-banking" >Create alert</a ></li></ul></div></div><div className="col-4"><div className="inside_st_details"> 52 Week Range <span>1.15 ~ 9.30</span></div></div><div className="col-4"><div className="inside_st_details"> 5 EMA <span className="percent-green">7.65</span></div></div><div className="col-4"><div className="inside_st_details"> 20 EMA <span className="percent-green">7.45</span></div></div><div className="col-4"><div className="inside_st_details"> 50 SMA <span className="percent-green">7.35</span></div></div><div className="col-4"><div className="inside_st_details"> 200 SMA <span className="percent-green">5.00</span></div></div><div className="col-4"><div className="inside_st_details"> AVG VOL <span>13909155</span></div></div><div className="col-4"><div className="inside_st_details"> DELIVERY % <span>55.55</span></div></div><div className="col-4"><div className="inside_st_details"> RSI <span>57.10</span></div></div></div></div>' },
  // { Order: 103, ShipName: 'Black pearl', instType: 'NIFTY2150614500CE', template: '<div className="d-inline-block wd-40">chart 4</div><div className="d-inline-block wd-60 adv_expanded_data"><div className="row"><div className="col-6 pl-0" style="margin-bottom: 20px"> <a href="stocks-analysis/3iinfotech" target="_blank" className="inside_stock_name" > 3IINFOTECH<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-box-arrow-up-right" viewBox="0 0 16 16" > <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" ></path> <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" ></path> </svg> </a></div><div className="col-6 text-right"><div className="stock-analysis-right"><ul style="padding-right: 14px !important" className="adv-stock-analysis-right" ><li className="stock-analysis-add-to-watchlist"> <a href="javascript:void(0);" ><span className="stock-analysis-box">Add to watchlist</span></a ></li><li style="cursor: pointer" className="stock-analysis-alert-btn" title="Save alert" > <a className="stock-analysis-box" href="#" data-toggle="modal" data-target="#create-alert-for-banking" >Create alert</a ></li></ul></div></div><div className="col-4"><div className="inside_st_details"> 52 Week Range <span>1.15 ~ 9.30</span></div></div><div className="col-4"><div className="inside_st_details"> 5 EMA <span className="percent-green">7.65</span></div></div><div className="col-4"><div className="inside_st_details"> 20 EMA <span className="percent-green">7.45</span></div></div><div className="col-4"><div className="inside_st_details"> 50 SMA <span className="percent-green">7.35</span></div></div><div className="col-4"><div className="inside_st_details"> 200 SMA <span className="percent-green">5.00</span></div></div><div className="col-4"><div className="inside_st_details"> AVG VOL <span>13909155</span></div></div><div className="col-4"><div className="inside_st_details"> DELIVERY % <span>55.55</span></div></div><div className="col-4"><div className="inside_st_details"> RSI <span>57.10</span></div></div></div></div>' },
  // { Order: 104, ShipName: 'Pearl', instType: 6 },
  // { Order: 105, ShipName: 'Noth bay', instType: 7 },
  // { Order: 106, ShipName: 'baratna', instType: 8 },
  // { Order: 107, ShipName: 'Charge', instType: 9 }];

  // childGrid = {
  //   columns: [
  //     { template: '<div>${template}</div>', width: 120 }
  //   ],
  //   dataSource: this.dataManger,
  //   queryString: 'instType'
  // };

  filterOptions = {
    type: "Menu",
  };

  render() {
    const { tableColumns, tableRows } = this.props;

    return (
      <GridComponent
        dataSource={tableRows}
        allowPaging={true}
        pageSettings={this.pageSettings}
        className="table"
        toolbar={this.toolbarOptions}
        allowSorting={true}
        allowFiltering={true}
        queryCellInfo={this.customizeCell}
        // childGrid={this.childGrid}
        filterSettings={{
          type: "Menu",
        }}
      >
        <ColumnsDirective>
          {tableColumns && tableColumns.length > 0
            ? tableColumns.map((item, key) => {
              return (
                <ColumnDirective
                  key={key}
                  field={item["field"]}
                  headerText={item["headerText"]}
                  allowFiltering={key == 2 || key == 3 ? false : true}
                />
              );
            })
            : ""}
        </ColumnsDirective>

        <Inject
          services={[
            Page,
            Sort,
            Filter,
            Group,
            Search,
            Toolbar,
            Resize,
            DetailRow,
          ]}
        />
      </GridComponent>
    );
  }
}
