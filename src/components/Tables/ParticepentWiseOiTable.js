import Layout from "components/Layout";
import { Component } from "react";

class ParticipantWiseOiTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: [],
      diiData: [],
      fiiData: [],
      proData: [],
      total: [],
      clientDataChange: 0,
      diiDataChange: 0,
      fiiDataChange: 0,
      proDataChange: 0,
      totalChange: 0,
      future_index_long: 0,
      future_index_short: 0,
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    const { pwOiTableData } = this.props;
    const { clientData, diiData, fiiData, proData, total } = this.state;

    const clientAllData = pwOiTableData.map((data, index) => {
      if (data.client_type === "Client") {
        return data;
      }
    });

    const finalClinetData = clientAllData.filter(function (element) {
      return element !== undefined;
    });

    if (
      finalClinetData.length !== clientData.length ||
      pwOiTableData !== prevProps.pwOiTableData
    ) {
      this.setState({
        clientData: finalClinetData,
      });
    }

    const diiAllData = pwOiTableData.map((data, index) => {
      if (data.client_type === "DII") {
        return data;
      }
    });

    const finalDiiData = diiAllData.filter(function (element) {
      return element !== undefined;
    });

    if (
      finalDiiData.length !== diiData.length ||
      pwOiTableData !== prevProps.pwOiTableData
    ) {
      this.setState({
        diiData: finalDiiData,
      });
    }

    const fiiAllData = pwOiTableData.map((data, index) => {
      if (data.client_type === "FII") {
        return data;
      }
    });

    const finalFiiData = fiiAllData.filter(function (element) {
      return element !== undefined;
    });
    if (
      finalFiiData.length !== fiiData.length ||
      pwOiTableData !== prevProps.pwOiTableData
    ) {
      this.setState({
        fiiData: finalFiiData,
      });
    }

    const proAllData = pwOiTableData.map((data, index) => {
      if (data.client_type === "Pro") {
        return data;
      }
    });

    const finalProData = proAllData.filter(function (element) {
      return element !== undefined;
    });

    if (
      finalProData.length !== proData.length ||
      pwOiTableData !== prevProps.pwOiTableData
    ) {
      this.setState({
        proData: finalProData,
      });
    }

    const totalAllData = pwOiTableData.map((data, index) => {
      if (data.client_type === "TOTAL") {
        return data;
      }
    });

    const finalTotalData = totalAllData.filter(function (element) {
      return element !== undefined;
    });

    if (
      finalTotalData.length !== total.length ||
      pwOiTableData !== prevProps.pwOiTableData
    ) {
      this.setState({
        total: finalTotalData,
      });
    }
  }

  render() {
    const { clientData, diiData, fiiData, proData, total } = this.state;
    return (
      <table className="table part_wise_oi_table">
        <thead>
          <tr>
            <th className="text-left">Client Type</th>
            <th>Client</th>
            <th>Change</th>
            <th>DII</th>
            <th>Change</th>
            <th>FII</th>
            <th>Change</th>
            <th>Pro</th>
            <th>Change</th>
            <th>TOTAL</th>
            <th>Change</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="text-left">Future Index Long</td>
            <td>{clientData[0] ? clientData[0]["future_index_long"] : ""}</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_index_long"] -
                      clientData[0]["prev_future_index_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_index_long"] -
                  clientData[0]["prev_future_index_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["future_index_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_index_long"] -
                      diiData[0]["prev_future_index_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_index_long"] -
                  diiData[0]["prev_future_index_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["future_index_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_index_long"] -
                      fiiData[0]["prev_future_index_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_index_long"] -
                  fiiData[0]["prev_future_index_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["future_index_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_index_long"] -
                      proData[0]["prev_future_index_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_index_long"] -
                  proData[0]["prev_future_index_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["future_index_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["future_index_long"] -
                      total[0]["prev_future_index_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_index_long"] -
                  total[0]["prev_future_index_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Future Index Short</td>
            <td>{clientData[0] ? clientData[0]["future_index_short"] : ""}</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_index_short"] -
                      clientData[0]["prev_future_index_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_index_short"] -
                  clientData[0]["prev_future_index_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["future_index_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_index_short"] -
                      diiData[0]["prev_future_index_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_index_short"] -
                  diiData[0]["prev_future_index_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["future_index_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_index_short"] -
                      fiiData[0]["prev_future_index_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_index_short"] -
                  fiiData[0]["prev_future_index_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["future_index_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_index_short"] -
                      proData[0]["prev_future_index_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_index_short"] -
                  proData[0]["prev_future_index_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["future_index_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["future_index_short"] -
                      total[0]["prev_future_index_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_index_short"] -
                  total[0]["prev_future_index_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>

            <td
              className={
                clientData[0] ? clientData[0]["future_index_long"] - clientData[0]["future_index_short"] == 0
                    ? ""
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_index_long"] -
                  clientData[0]["future_index_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_index_long"] -
                      clientData[0]["prev_future_index_long"] -
                      (clientData[0]["future_index_short"] -
                        clientData[0]["prev_future_index_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_index_long"] -
                  clientData[0]["prev_future_index_long"] -
                  (clientData[0]["future_index_short"] -
                    clientData[0]["prev_future_index_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["future_index_long"] -
                      diiData[0]["future_index_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_index_long"] -
                  diiData[0]["future_index_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_index_long"] -
                      diiData[0]["prev_future_index_long"] -
                      (diiData[0]["future_index_short"] -
                        diiData[0]["prev_future_index_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_index_long"] -
                  diiData[0]["prev_future_index_long"] -
                  (diiData[0]["future_index_short"] -
                    diiData[0]["prev_future_index_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_index_long"] -
                      fiiData[0]["future_index_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_index_long"] -
                  fiiData[0]["future_index_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_index_long"] -
                      fiiData[0]["prev_future_index_long"] -
                      (fiiData[0]["future_index_short"] -
                        fiiData[0]["prev_future_index_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_index_long"] -
                  fiiData[0]["prev_future_index_long"] -
                  (fiiData[0]["future_index_short"] -
                    fiiData[0]["prev_future_index_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["future_index_long"] -
                      proData[0]["future_index_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_index_long"] -
                  proData[0]["future_index_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_index_long"] -
                      proData[0]["prev_future_index_long"] -
                      (proData[0]["future_index_short"] -
                        proData[0]["prev_future_index_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_index_long"] -
                  proData[0]["prev_future_index_long"] -
                  (proData[0]["future_index_short"] -
                    proData[0]["prev_future_index_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["future_index_long"] -
                      total[0]["future_index_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_index_long"] - total[0]["future_index_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["future_index_long"] -
                      total[0]["prev_future_index_long"] -
                      (total[0]["future_index_short"] -
                        total[0]["prev_future_index_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_index_long"] -
                  total[0]["prev_future_index_long"] -
                  (total[0]["future_index_short"] -
                    total[0]["prev_future_index_short"])
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Index Call Long</td>
            <td>
              {clientData[0] ? clientData[0]["option_index_call_long"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_call_long"] -
                      clientData[0]["prev_option_index_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_call_long"] -
                  clientData[0]["prev_option_index_call_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_index_call_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_call_long"] -
                      diiData[0]["prev_option_index_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_call_long"] -
                  diiData[0]["prev_option_index_call_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_index_call_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_call_long"] -
                      fiiData[0]["prev_option_index_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_call_long"] -
                  fiiData[0]["prev_option_index_call_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_index_call_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_call_long"] -
                      proData[0]["prev_option_index_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_call_long"] -
                  proData[0]["prev_option_index_call_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_index_call_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_call_long"] -
                      total[0]["prev_option_index_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_call_long"] -
                  total[0]["prev_option_index_call_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Index Call Short</td>
            <td>
              {clientData[0] ? clientData[0]["option_index_call_short"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_call_short"] -
                      clientData[0]["prev_option_index_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_call_short"] -
                  clientData[0]["prev_option_index_call_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_index_call_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_call_short"] -
                      diiData[0]["prev_option_index_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_call_short"] -
                  diiData[0]["prev_option_index_call_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_index_call_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_call_short"] -
                      fiiData[0]["prev_option_index_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_call_short"] -
                  fiiData[0]["prev_option_index_call_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_index_call_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_call_short"] -
                      proData[0]["prev_option_index_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_call_short"] -
                  proData[0]["prev_option_index_call_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_index_call_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_call_short"] -
                      total[0]["prev_option_index_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_call_short"] -
                  total[0]["prev_option_index_call_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_call_long"] -
                      clientData[0]["option_index_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_call_long"] -
                  clientData[0]["option_index_call_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_call_long"] -
                      clientData[0]["prev_option_index_call_long"] -
                      (clientData[0]["option_index_call_short"] -
                        clientData[0]["prev_option_index_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_call_long"] -
                  clientData[0]["prev_option_index_call_long"] -
                  (clientData[0]["option_index_call_short"] -
                    clientData[0]["prev_option_index_call_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_call_long"] -
                      diiData[0]["option_index_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_call_long"] -
                  diiData[0]["option_index_call_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_call_long"] -
                      diiData[0]["prev_option_index_call_long"] -
                      (diiData[0]["option_index_call_short"] -
                        diiData[0]["prev_option_index_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_call_long"] -
                  diiData[0]["prev_option_index_call_long"] -
                  (diiData[0]["option_index_call_short"] -
                    diiData[0]["prev_option_index_call_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_call_long"] -
                      fiiData[0]["option_index_call_short"] <
                    0
                    ? "percent-green"
                    : "percent-red"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_call_long"] -
                  fiiData[0]["option_index_call_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_call_long"] -
                      fiiData[0]["prev_option_index_call_long"] -
                      (fiiData[0]["option_index_call_short"] -
                        fiiData[0]["prev_option_index_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_call_long"] -
                  fiiData[0]["prev_option_index_call_long"] -
                  (fiiData[0]["option_index_call_short"] -
                    fiiData[0]["prev_option_index_call_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["option_index_call_long"] -
                      proData[0]["option_index_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_call_long"] -
                  proData[0]["option_index_call_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_call_long"] -
                      proData[0]["prev_option_index_call_long"] -
                      (proData[0]["option_index_call_short"] -
                        proData[0]["prev_option_index_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_call_long"] -
                  proData[0]["prev_option_index_call_long"] -
                  (proData[0]["option_index_call_short"] -
                    proData[0]["prev_option_index_call_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["option_index_call_long"] -
                      total[0]["option_index_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_call_long"] -
                  total[0]["option_index_call_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_call_long"] -
                      total[0]["prev_option_index_call_long"] -
                      (total[0]["option_index_call_short"] -
                        total[0]["prev_option_index_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_call_long"] -
                  total[0]["prev_option_index_call_long"] -
                  (total[0]["option_index_call_short"] -
                    total[0]["prev_option_index_call_short"])
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Index Put Long</td>
            <td>
              {clientData[0] ? clientData[0]["option_index_put_long"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_put_long"] -
                      clientData[0]["prev_option_index_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_put_long"] -
                  clientData[0]["prev_option_index_put_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_index_put_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_put_long"] -
                      diiData[0]["prev_option_index_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_put_long"] -
                  diiData[0]["prev_option_index_put_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_index_put_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_put_long"] -
                      fiiData[0]["prev_option_index_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_put_long"] -
                  fiiData[0]["prev_option_index_put_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_index_put_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_put_long"] -
                      proData[0]["prev_option_index_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_put_long"] -
                  proData[0]["prev_option_index_put_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_index_put_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_put_long"] -
                      total[0]["prev_option_index_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_put_long"] -
                  total[0]["prev_option_index_put_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Index Put Short</td>
            <td>
              {clientData[0] ? clientData[0]["option_index_put_short"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_put_short"] -
                      clientData[0]["prev_option_index_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_put_short"] -
                  clientData[0]["prev_option_index_put_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_index_put_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_put_short"] -
                      diiData[0]["prev_option_index_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_put_short"] -
                  diiData[0]["prev_option_index_put_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_index_put_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_put_short"] -
                      fiiData[0]["prev_option_index_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_put_short"] -
                  fiiData[0]["prev_option_index_put_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_index_put_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_put_short"] -
                      proData[0]["prev_option_index_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_put_short"] -
                  proData[0]["prev_option_index_put_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_index_put_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_put_short"] -
                      total[0]["prev_option_index_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_put_short"] -
                  total[0]["prev_option_index_put_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_put_long"] -
                      clientData[0]["option_index_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_put_long"] -
                  clientData[0]["option_index_put_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_index_put_long"] -
                      clientData[0]["prev_option_index_put_long"] -
                      (clientData[0]["option_index_put_short"] -
                        clientData[0]["prev_option_index_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_put_long"] -
                  clientData[0]["prev_option_index_put_long"] -
                  (clientData[0]["option_index_put_short"] -
                    clientData[0]["prev_option_index_put_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_put_long"] -
                      diiData[0]["option_index_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_put_long"] -
                  diiData[0]["option_index_put_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_put_long"] -
                      diiData[0]["prev_option_index_put_long"] -
                      (diiData[0]["option_index_put_short"] -
                        diiData[0]["prev_option_index_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_index_put_long"] -
                  diiData[0]["prev_option_index_put_long"] -
                  (diiData[0]["option_index_put_short"] -
                    diiData[0]["prev_option_index_put_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_put_long"] -
                      fiiData[0]["option_index_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_put_long"] -
                  fiiData[0]["option_index_put_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_index_put_long"] -
                      fiiData[0]["prev_option_index_put_long"] -
                      (fiiData[0]["option_index_put_short"] -
                        fiiData[0]["prev_option_index_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_index_put_long"] -
                  fiiData[0]["prev_option_index_put_long"] -
                  (fiiData[0]["option_index_put_short"] -
                    fiiData[0]["prev_option_index_put_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["option_index_put_long"] -
                      proData[0]["option_index_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_put_long"] -
                  proData[0]["option_index_put_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_index_put_long"] -
                      proData[0]["prev_option_index_put_long"] -
                      (proData[0]["option_index_put_short"] -
                        proData[0]["prev_option_index_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_index_put_long"] -
                  proData[0]["prev_option_index_put_long"] -
                  (proData[0]["option_index_put_short"] -
                    proData[0]["prev_option_index_put_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["option_index_put_long"] -
                      total[0]["option_index_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_put_long"] -
                  total[0]["option_index_put_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["option_index_put_long"] -
                      total[0]["prev_option_index_put_long"] -
                      (total[0]["option_index_put_short"] -
                        total[0]["prev_option_index_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_index_put_long"] -
                  total[0]["prev_option_index_put_long"] -
                  (total[0]["option_index_put_short"] -
                    total[0]["prev_option_index_put_short"])
                : ""}
            </td>
          </tr>

          <tr>
            <td className="text-left">Future Stock Long</td>
            <td>{clientData[0] ? clientData[0]["future_stock_long"] : ""}</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_stock_long"] -
                      clientData[0]["prev_future_stock_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_stock_long"] -
                  clientData[0]["prev_future_stock_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["future_stock_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_stock_long"] -
                      diiData[0]["prev_future_stock_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_stock_long"] -
                  diiData[0]["prev_future_stock_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["future_stock_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_stock_long"] -
                      fiiData[0]["prev_future_stock_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_stock_long"] -
                  fiiData[0]["prev_future_stock_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["future_stock_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_stock_long"] -
                      proData[0]["prev_future_stock_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_stock_long"] -
                  proData[0]["prev_future_stock_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["future_stock_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["future_stock_long"] -
                      total[0]["prev_future_stock_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_stock_long"] -
                  total[0]["prev_future_stock_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Future Stock Short</td>
            <td>{clientData[0] ? clientData[0]["future_stock_short"] : ""}</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_stock_short"] -
                      clientData[0]["prev_future_stock_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_stock_short"] -
                  clientData[0]["prev_future_stock_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["future_stock_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_stock_short"] -
                      diiData[0]["prev_future_stock_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_stock_short"] -
                  diiData[0]["prev_future_stock_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["future_stock_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_stock_short"] -
                      fiiData[0]["prev_future_stock_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_stock_short"] -
                  fiiData[0]["prev_future_stock_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["future_stock_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_stock_short"] -
                      proData[0]["prev_future_stock_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_stock_short"] -
                  proData[0]["prev_future_stock_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["future_stock_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["future_stock_short"] -
                      total[0]["prev_future_stock_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_stock_short"] -
                  total[0]["prev_future_stock_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_stock_long"] -
                      clientData[0]["future_stock_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["future_stock_long"] -
                  clientData[0]["future_stock_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["future_stock_long"] -
                      clientData[0]["prev_future_stock_long"] -
                      (clientData[0]["future_stock_short"] -
                        clientData[0]["prev_future_stock_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_index_put_long"] -
                  clientData[0]["prev_option_index_put_long"] -
                  (clientData[0]["future_stock_short"] -
                    clientData[0]["prev_future_stock_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["option_index_put_long"] -
                      diiData[0]["future_stock_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_stock_long"] -
                  diiData[0]["future_stock_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["future_stock_long"] -
                      diiData[0]["prev_future_stock_long"] -
                      (diiData[0]["future_stock_short"] -
                        diiData[0]["prev_future_stock_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["future_stock_long"] -
                  diiData[0]["prev_future_stock_long"] -
                  (diiData[0]["future_stock_short"] -
                    diiData[0]["prev_future_stock_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_stock_long"] -
                      fiiData[0]["future_stock_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_stock_long"] -
                  fiiData[0]["future_stock_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["future_stock_long"] -
                      fiiData[0]["prev_future_stock_long"] -
                      (fiiData[0]["future_stock_short"] -
                        fiiData[0]["prev_future_stock_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["future_stock_long"] -
                  fiiData[0]["prev_future_stock_long"] -
                  (fiiData[0]["future_stock_short"] -
                    fiiData[0]["prev_future_stock_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["future_stock_long"] -
                      proData[0]["future_stock_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_stock_long"] -
                  proData[0]["future_stock_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["future_stock_long"] -
                      proData[0]["prev_future_stock_long"] -
                      (proData[0]["future_stock_short"] -
                        proData[0]["prev_future_stock_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["future_stock_long"] -
                  proData[0]["prev_future_stock_long"] -
                  (proData[0]["future_stock_short"] -
                    proData[0]["prev_future_stock_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["future_stock_long"] -
                      total[0]["future_stock_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_stock_long"] - total[0]["future_stock_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["future_stock_long"] -
                      total[0]["prev_future_stock_long"] -
                      (total[0]["future_stock_short"] -
                        total[0]["prev_future_stock_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["future_stock_long"] -
                  total[0]["prev_future_stock_long"] -
                  (total[0]["future_stock_short"] -
                    total[0]["prev_future_stock_short"])
                : ""}
            </td>
          </tr>

          <tr>
            <td className="text-left">Option Stock Call Long</td>
            <td>
              {clientData[0] ? clientData[0]["option_stock_call_long"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_call_long"] -
                      clientData[0]["prev_option_stock_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_call_long"] -
                  clientData[0]["prev_option_stock_call_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_stock_call_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_call_long"] -
                      diiData[0]["prev_option_stock_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_call_long"] -
                  diiData[0]["prev_option_stock_call_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_stock_call_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_call_long"] -
                      fiiData[0]["prev_option_stock_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_call_long"] -
                  fiiData[0]["prev_option_stock_call_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_stock_call_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_call_long"] -
                      proData[0]["prev_option_stock_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_call_long"] -
                  proData[0]["prev_option_stock_call_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_stock_call_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_call_long"] -
                      total[0]["prev_option_stock_call_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_call_long"] -
                  total[0]["prev_option_stock_call_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Stock Call Short</td>
            <td>
              {clientData[0] ? clientData[0]["option_stock_call_short"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_call_short"] -
                      clientData[0]["prev_option_stock_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_call_short"] -
                  clientData[0]["prev_option_stock_call_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_stock_call_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_call_short"] -
                      diiData[0]["prev_option_stock_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_call_short"] -
                  diiData[0]["prev_option_stock_call_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_stock_call_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_call_short"] -
                      fiiData[0]["prev_option_stock_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_call_short"] -
                  fiiData[0]["prev_option_stock_call_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_stock_call_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_call_short"] -
                      proData[0]["prev_option_stock_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_call_short"] -
                  proData[0]["prev_option_stock_call_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_stock_call_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_call_short"] -
                      total[0]["prev_option_stock_call_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_call_short"] -
                  total[0]["prev_option_stock_call_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_call_long"] -
                      clientData[0]["option_stock_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_call_long"] -
                  clientData[0]["option_stock_call_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_call_long"] -
                      clientData[0]["prev_option_stock_call_long"] -
                      (clientData[0]["option_stock_call_short"] -
                        clientData[0]["prev_option_stock_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_call_long"] -
                  clientData[0]["prev_option_stock_call_long"] -
                  (clientData[0]["option_stock_call_short"] -
                    clientData[0]["prev_option_stock_call_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_call_long"] -
                      diiData[0]["option_stock_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_call_long"] -
                  diiData[0]["option_stock_call_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_call_long"] -
                      diiData[0]["prev_option_stock_call_long"] -
                      (diiData[0]["option_stock_call_short"] -
                        diiData[0]["prev_option_stock_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_call_long"] -
                  diiData[0]["prev_option_stock_call_long"] -
                  (diiData[0]["option_stock_call_short"] -
                    diiData[0]["prev_option_stock_call_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_call_long"] -
                      fiiData[0]["option_stock_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_call_long"] -
                  fiiData[0]["option_stock_call_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_call_long"] -
                      fiiData[0]["prev_option_stock_call_long"] -
                      (fiiData[0]["option_stock_call_short"] -
                        fiiData[0]["prev_option_stock_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_call_long"] -
                  fiiData[0]["prev_option_stock_call_long"] -
                  (fiiData[0]["option_stock_call_short"] -
                    fiiData[0]["prev_option_stock_call_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_call_long"] -
                      proData[0]["option_stock_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_call_long"] -
                  proData[0]["option_stock_call_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_call_long"] -
                      proData[0]["prev_option_stock_call_long"] -
                      (proData[0]["option_stock_call_short"] -
                        proData[0]["prev_option_stock_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_call_long"] -
                  proData[0]["prev_option_stock_call_long"] -
                  (proData[0]["option_stock_call_short"] -
                    proData[0]["prev_option_stock_call_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["option_stock_call_long"] -
                      total[0]["option_stock_call_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_call_long"] -
                  total[0]["option_stock_call_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_call_long"] -
                      total[0]["prev_option_stock_call_long"] -
                      (total[0]["option_stock_call_short"] -
                        total[0]["prev_option_stock_call_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_call_long"] -
                  total[0]["prev_option_stock_call_long"] -
                  (total[0]["option_stock_call_short"] -
                    total[0]["prev_option_stock_call_short"])
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Stock Put Long</td>
            <td>
              {clientData[0] ? clientData[0]["option_stock_put_long"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_put_long"] -
                      clientData[0]["prev_option_stock_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_put_long"] -
                  clientData[0]["prev_option_stock_put_long"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_stock_put_long"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_put_long"] -
                      diiData[0]["prev_option_stock_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_put_long"] -
                  diiData[0]["prev_option_stock_put_long"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_stock_put_long"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_put_long"] -
                      fiiData[0]["prev_option_stock_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_put_long"] -
                  fiiData[0]["prev_option_stock_put_long"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_stock_put_long"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_put_long"] -
                      proData[0]["prev_option_stock_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_put_long"] -
                  proData[0]["prev_option_stock_put_long"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_stock_put_long"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_put_long"] -
                      total[0]["prev_option_stock_put_long"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_put_long"] -
                  total[0]["prev_option_stock_put_long"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Option Stock Put Short</td>
            <td>
              {clientData[0] ? clientData[0]["option_stock_put_short"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_put_short"] -
                      clientData[0]["prev_option_stock_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_put_short"] -
                  clientData[0]["prev_option_stock_put_short"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["option_stock_put_short"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_put_short"] -
                      diiData[0]["prev_option_stock_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_put_short"] -
                  diiData[0]["prev_option_stock_put_short"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["option_stock_put_short"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_put_short"] -
                      fiiData[0]["prev_option_stock_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_put_short"] -
                  fiiData[0]["prev_option_stock_put_short"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["option_stock_put_short"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_put_short"] -
                      proData[0]["prev_option_stock_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_put_short"] -
                  proData[0]["prev_option_stock_put_short"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["option_stock_put_short"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_put_short"] -
                      total[0]["prev_option_stock_put_short"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_put_short"] -
                  total[0]["prev_option_stock_put_short"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_put_long"] -
                      clientData[0]["option_stock_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_put_long"] -
                  clientData[0]["option_stock_put_short"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["option_stock_put_long"] -
                      clientData[0]["prev_option_stock_put_long"] -
                      (clientData[0]["option_stock_put_short"] -
                        clientData[0]["prev_option_stock_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["option_stock_put_long"] -
                  clientData[0]["prev_option_stock_put_long"] -
                  (clientData[0]["option_stock_put_short"] -
                    clientData[0]["prev_option_stock_put_short"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_put_long"] -
                      diiData[0]["option_stock_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_put_long"] -
                  diiData[0]["option_stock_put_short"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["option_stock_put_long"] -
                      diiData[0]["prev_option_stock_put_long"] -
                      (diiData[0]["option_stock_put_short"] -
                        diiData[0]["prev_option_stock_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["option_stock_put_long"] -
                  diiData[0]["prev_option_stock_put_long"] -
                  (diiData[0]["option_stock_put_short"] -
                    diiData[0]["prev_option_stock_put_short"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_put_long"] -
                      fiiData[0]["option_stock_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_put_long"] -
                  fiiData[0]["option_stock_put_short"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["option_stock_put_long"] -
                      fiiData[0]["prev_option_stock_put_long"] -
                      (fiiData[0]["option_stock_put_short"] -
                        fiiData[0]["prev_option_stock_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["option_stock_put_long"] -
                  fiiData[0]["prev_option_stock_put_long"] -
                  (fiiData[0]["option_stock_put_short"] -
                    fiiData[0]["prev_option_stock_put_short"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_put_long"] -
                      proData[0]["option_stock_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_put_long"] -
                  proData[0]["option_stock_put_short"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["option_stock_put_long"] -
                      proData[0]["prev_option_stock_put_long"] -
                      (proData[0]["option_stock_put_short"] -
                        proData[0]["prev_option_stock_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["option_stock_put_long"] -
                  proData[0]["prev_option_stock_put_long"] -
                  (proData[0]["option_stock_put_short"] -
                    proData[0]["prev_option_stock_put_short"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["option_stock_put_long"] -
                      total[0]["option_stock_put_short"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_put_long"] -
                  total[0]["option_stock_put_short"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["option_stock_put_long"] -
                      total[0]["prev_option_stock_put_long"] -
                      (total[0]["option_stock_put_short"] -
                        total[0]["prev_option_stock_put_short"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["option_stock_put_long"] -
                  total[0]["prev_option_stock_put_long"] -
                  (total[0]["option_stock_put_short"] -
                    total[0]["prev_option_stock_put_short"])
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Total Long Contracts</td>
            <td>
              {clientData[0] ? clientData[0]["total_long_contracts"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["total_long_contracts"] -
                      clientData[0]["prev_total_long_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["total_long_contracts"] -
                  clientData[0]["prev_total_long_contracts"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["total_long_contracts"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["total_long_contracts"] -
                      diiData[0]["prev_total_long_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["total_long_contracts"] -
                  diiData[0]["prev_total_long_contracts"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["total_long_contracts"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["total_long_contracts"] -
                      fiiData[0]["prev_total_long_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["total_long_contracts"] -
                  fiiData[0]["prev_total_long_contracts"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["total_long_contracts"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["total_long_contracts"] -
                      proData[0]["prev_total_long_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["total_long_contracts"] -
                  proData[0]["prev_total_long_contracts"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["total_long_contracts"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["total_long_contracts"] -
                      total[0]["prev_total_long_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["total_long_contracts"] -
                  total[0]["prev_total_long_contracts"]
                : ""}
            </td>
          </tr>
          <tr>
            <td className="text-left">Total Short Contracts</td>
            <td>
              {clientData[0] ? clientData[0]["total_short_contracts"] : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["total_short_contracts"] -
                      clientData[0]["prev_total_short_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["total_short_contracts"] -
                  clientData[0]["prev_total_short_contracts"]
                : ""}
            </td>

            <td>{diiData[0] ? diiData[0]["total_short_contracts"] : ""}</td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["total_short_contracts"] -
                      diiData[0]["prev_total_short_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["total_short_contracts"] -
                  diiData[0]["prev_total_short_contracts"]
                : ""}
            </td>

            <td>{fiiData[0] ? fiiData[0]["total_short_contracts"] : ""}</td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["total_short_contracts"] -
                      fiiData[0]["prev_total_short_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["total_short_contracts"] -
                  fiiData[0]["prev_total_short_contracts"]
                : ""}
            </td>

            <td>{proData[0] ? proData[0]["total_short_contracts"] : ""}</td>
            <td
              className={
                proData[0]
                  ? proData[0]["total_short_contracts"] -
                      proData[0]["prev_total_short_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["total_short_contracts"] -
                  proData[0]["prev_total_short_contracts"]
                : ""}
            </td>

            <td>{total[0] ? total[0]["total_short_contracts"] : ""}</td>
            <td
              className={
                total[0]
                  ? total[0]["total_short_contracts"] -
                      total[0]["prev_total_short_contracts"] <
                    0
                    ? "part_wise_low"
                    : "part_wise_high"
                  : ""
              }
            >
              {total[0]
                ? total[0]["total_short_contracts"] -
                  total[0]["prev_total_short_contracts"]
                : ""}
            </td>
          </tr>
          <tr className="part_wise_net">
            <td className="text-left">NET</td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["total_long_contracts"] -
                      clientData[0]["total_short_contracts"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["total_long_contracts"] -
                  clientData[0]["total_short_contracts"]
                : ""}
            </td>
            <td
              className={
                clientData[0]
                  ? clientData[0]["total_long_contracts"] -
                      clientData[0]["prev_total_long_contracts"] -
                      (clientData[0]["total_short_contracts"] -
                        clientData[0]["prev_total_short_contracts"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {clientData[0]
                ? clientData[0]["total_long_contracts"] -
                  clientData[0]["prev_total_long_contracts"] -
                  (clientData[0]["total_short_contracts"] -
                    clientData[0]["prev_total_short_contracts"])
                : ""}
            </td>

            <td
              className={
                diiData[0]
                  ? diiData[0]["total_long_contracts"] -
                      diiData[0]["total_short_contracts"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["total_long_contracts"] -
                  diiData[0]["total_short_contracts"]
                : ""}
            </td>
            <td
              className={
                diiData[0]
                  ? diiData[0]["total_long_contracts"] -
                      diiData[0]["prev_total_long_contracts"] -
                      (diiData[0]["total_short_contracts"] -
                        diiData[0]["prev_total_short_contracts"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {diiData[0]
                ? diiData[0]["total_long_contracts"] -
                  diiData[0]["prev_total_long_contracts"] -
                  (diiData[0]["total_short_contracts"] -
                    diiData[0]["prev_total_short_contracts"])
                : ""}
            </td>

            <td
              className={
                fiiData[0]
                  ? fiiData[0]["total_long_contracts"] -
                      fiiData[0]["total_short_contracts"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["total_long_contracts"] -
                  fiiData[0]["total_short_contracts"]
                : ""}
            </td>
            <td
              className={
                fiiData[0]
                  ? fiiData[0]["total_long_contracts"] -
                      fiiData[0]["prev_total_long_contracts"] -
                      (fiiData[0]["total_short_contracts"] -
                        fiiData[0]["prev_total_short_contracts"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {fiiData[0]
                ? fiiData[0]["total_long_contracts"] -
                  fiiData[0]["prev_total_long_contracts"] -
                  (fiiData[0]["total_short_contracts"] -
                    fiiData[0]["prev_total_short_contracts"])
                : ""}
            </td>

            <td
              className={
                proData[0]
                  ? proData[0]["total_long_contracts"] -
                      proData[0]["total_short_contracts"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["total_long_contracts"] -
                  proData[0]["total_short_contracts"]
                : ""}
            </td>
            <td
              className={
                proData[0]
                  ? proData[0]["total_long_contracts"] -
                      proData[0]["prev_total_long_contracts"] -
                      (proData[0]["total_short_contracts"] -
                        proData[0]["prev_total_short_contracts"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {proData[0]
                ? proData[0]["total_long_contracts"] -
                  proData[0]["prev_total_long_contracts"] -
                  (proData[0]["total_short_contracts"] -
                    proData[0]["prev_total_short_contracts"])
                : ""}
            </td>

            <td
              className={
                total[0]
                  ? total[0]["total_long_contracts"] -
                      total[0]["total_short_contracts"] <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["total_long_contracts"] -
                  total[0]["total_short_contracts"]
                : ""}
            </td>
            <td
              className={
                total[0]
                  ? total[0]["total_long_contracts"] -
                      total[0]["prev_total_long_contracts"] -
                      (total[0]["total_short_contracts"] -
                        total[0]["prev_total_short_contracts"]) <
                    0
                    ? "percent-red"
                    : "percent-green"
                  : ""
              }
            >
              {total[0]
                ? total[0]["total_long_contracts"] -
                  total[0]["prev_total_long_contracts"] -
                  (total[0]["total_short_contracts"] -
                    total[0]["prev_total_short_contracts"])
                : ""}
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default ParticipantWiseOiTable;
