import React, { Component } from "react";
import { connect } from "react-redux";
import { encrypt } from "_helper/EncrDecrypt";
import {
  ErrorMessages,
  isNameValid,
  isEmailValid,
  isMobileNumberValid,
} from "_helper/MyValidations";
import { CALL_API } from "_services/CALL_API";
import Cookies from "js-cookie";
import SocialButton from "components/SocialButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF, faGoogle } from "@fortawesome/free-brands-svg-icons";
import { ToastContainer, toast } from "react-toastify";
import { AuthenticationService } from "_services/AuthenticationService";
import { faStar, faUser } from "@fortawesome/free-regular-svg-icons";
import { faPowerOff, faUserClock } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import Router from "next/router";
import { userData, userLoggedIn } from "_redux/actions";

const mapStateToProps = (state) => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  setUserData: (data) => dispatch(userData(data)),
  setUserLoggedIn: (data) => dispatch(userLoggedIn(data)),
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popupType: "login",
      mailSendingStatus: false,
      formData: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: true,
      },
      error: false,
      errorMessage: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: "",
      },
    };
    this.nodes = {};
    this.toastConfig = {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    };
  }

  componentDidMount() {}

  openLoginPopup = () => {
    this.setState({
      formData: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: true,
      },
      error: false,
      errorMessage: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: "",
      },
      popupType: "login",
    });
    $("#signuppopup").modal("hide");
    $("#loginpopup").modal("show");
  };

  openRegisterPopup = () => {
    this.setState({
      formData: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: true,
      },
      error: false,
      errorMessage: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: "",
      },
      popupType: "register",
    });
    $("#loginpopup").modal("hide");
    $("#signuppopup").modal("show");
  };

  openForgotPasswordPopup = () => {
    this.setState({
      formData: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: true,
      },
      error: false,
      errorMessage: {
        name: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        terms: "",
      },
      popupType: "forgotpassword",
    });
    $("#loginpopup").modal("hide");
    $("#signuppopup").modal("hide");
    $("#forgotpasswordpopup").modal("show");
  };

  handleFormInput =
    (name) =>
    ({ target: { value, checked } }) => {
      if (name == "terms") {
        this.setState({
          formData: {
            ...this.state.formData,
            [name]: checked,
          },
        });
      } else {
        this.setState({
          formData: {
            ...this.state.formData,
            [name]: value,
          },
        });
      }
    };

  registerFormValidation = () => {
    const { formData, popupType } = this.state;
    var msgName = "";
    var msgEmail = "";
    var msgMobile = "";
    var msgPassword = "";
    var msgConfirmPassword = "";
    var msgCheckTerms = "";
    var isValid = false;
    if (popupType == "register") {
      if (formData.name) {
        if (!isNameValid(formData.name)) {
          msgName = ErrorMessages.VALIDATION_ERROR_NAME;
        }
      } else {
        msgName = "Name is required";
      }

      // last name validation
      if (formData.email) {
        if (!isEmailValid(formData.email)) {
          msgEmail = ErrorMessages.VALIDATION_ERROR_EMAIL;
        }
      } else {
        msgEmail = "Email is required";
      }

      // mobile number. validation
      if (formData.phoneNumber) {
        if (!isMobileNumberValid(formData.phoneNumber)) {
          msgMobile = ErrorMessages.VALIDATION_ERROR_MOBILE;
        }
      }

      if (!formData["password"]) {
        msgPassword = "Password is required";
      }

      if (!formData["confirmPassword"]) {
        msgConfirmPassword = "Confirm Password is required";
      }

      if (formData["password"] && formData["confirmPassword"]) {
        if (formData["password"] != formData["confirmPassword"]) {
          msgConfirmPassword = "Password & Confirm password not matched";
        }
      }

      if (!formData["terms"]) {
        msgCheckTerms = "Please accept terms & conditions policy";
      }

      if (
        !msgName &&
        !msgEmail &&
        !msgMobile &&
        !msgPassword &&
        !msgConfirmPassword &&
        !msgCheckTerms
      ) {
        isValid = true;
      }
    } else if (popupType == "login") {
      // last name validation
      if (formData.email) {
        if (!isEmailValid(formData.email)) {
          msgEmail = ErrorMessages.VALIDATION_ERROR_EMAIL;
        }
      } else {
        msgEmail = "Email is required";
      }

      if (!formData["password"]) {
        msgPassword = "Password is required";
      }

      if (!msgEmail && !msgPassword) {
        isValid = true;
      }
    } else {
      if (formData.email) {
        if (!isEmailValid(formData.email)) {
          msgEmail = ErrorMessages.VALIDATION_ERROR_EMAIL;
        }
      } else {
        msgEmail = "Email is required";
      }

      if (!msgEmail) {
        isValid = true;
      }
    }

    if (isValid) {
      this.setState({
        error: true,
        errorMessage: {
          name: "",
          email: "",
          phoneNumber: "",
          password: "",
          confirmPassword: "",
          terms: "",
        },
      });
      return true;
    } else {
      this.setState({
        error: true,
        errorMessage: {
          name: msgName,
          email: msgEmail,
          phoneNumber: msgMobile,
          password: msgPassword,
          confirmPassword: msgConfirmPassword,
          terms: msgCheckTerms,
        },
      });
      return false;
    }
  };

  registerUser = () => {
    if (this.registerFormValidation()) {
      const { formData } = this.state;
      var params = {
        name: formData["name"],
        email: formData["email"],
        phone_no: formData["phoneNumber"],
        password1: formData["password"],
        password2: formData["password"],
        user_social_flag: 0,
        user_fb_id: null,
      };
      CALL_API("POST", process.env.REGISTER_USER_DATA, params, (res) => {
        if (res.status) {
          var accessToken = res["data"]["accessToken"];
          Cookies.set("_accessToken", accessToken, { expires: 30, path: "/" });
          // Cookies.set('_currentUser', encrypt(res['data'], { expires: 30, path: '/' }));
          $("#signuppopup").modal("hide");
          // AuthenticationService.setCurrentUserSubject(res['data']);

          this.toastConfig["type"] = "success";
          toast("User registration successfully.", this.toastConfig);

          // set user data
          this.props.setUserData(res["data"]);
          this.props.setUserLoggedIn(true);
        } else {
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      });
    }
  };

  loginUser = () => {
    if (this.registerFormValidation()) {
      const { formData } = this.state;
      var params = {
        email: formData["email"],
        password: formData["password"],
      };
      CALL_API("POST", process.env.LOGIN_USER_DATA, params, (res) => {
        if (res.status) {
          var accessToken = res["data"]["token"];
          Cookies.set("_accessToken", accessToken, { expires: 30, path: "/" });
          // Cookies.set('_currentUser', encrypt(res['data'], { expires: 30, path: '/' }));
          $("#loginpopup").modal("hide");
          // AuthenticationService.setCurrentUserSubject(res['data']);

          this.toastConfig["type"] = "success";
          toast("User logged in successfully.", this.toastConfig);

          // set user data
          this.props.setUserData(res["data"]);
          this.props.setUserLoggedIn(true);
        } else {
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      });
    }
  };

  sendMail = () => {
    if (this.registerFormValidation()) {
      this.setState({
        mailSendingStatus: true,
      });
      const { formData } = this.state;
      var params = {
        email: formData["email"],
      };
      CALL_API("GET", process.env.FORGOT_PASSWORD_USER_DATA, params, (res) => {
        if (res.status) {
          $("#forgotpasswordpopup").modal("hide");
          this.setState({
            mailSendingStatus: false,
          });
          this.toastConfig["type"] = "success";
          toast(res["message"], this.toastConfig);
        } else {
          this.setState({
            mailSendingStatus: false,
          });
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      });
    }
  };

  handleSocialLogin = (user) => {
    this.setState({
      clickSocial: user._provider,
    });
    var login_flag = "";
    if (user._provider == "facebook") {
      login_flag = 2;
    } else if (user._provider == "google") {
      login_flag = 1;
    }

    var params = {
      name: "",
      user_name: "",
      email: "",
      user_social_flag: login_flag,
      user_fb_id: null,
      password: "",
    };
    if (user._profile) {
      if (user._profile["firstName"] || user._profile["lastName"]) {
        params["name"] =
          user._profile["firstName"] + " " + user._profile["lastName"];
        params["user_name"] =
          user._profile["firstName"] + " " + user._profile["lastName"];
      }
      // if (user._profile['lastName']) {
      //   params['last_name'] = user._profile['lastName'];
      // }
      if (user._profile["email"]) {
        params["email"] = user._profile["email"];
      }
      if (user._profile["mobile"]) {
        params["phone_no"] = user._profile["mobile"];
      }

      if (user._provider == "facebook") {
        params["user_fb_id"] = user._token["accessToken"];
      }

      CALL_API("POST", process.env.LOGIN_USER_DATA, params, (res) => {
        if (res.status) {
          var accessToken = res["data"]["token"];
          Cookies.set("_accessToken", accessToken);
          // Cookies.set('_currentUser', encrypt(res['data']));
          $("#loginpopup").modal("hide");
          // AuthenticationService.setCurrentUserSubject(res['data']);

          this.toastConfig["type"] = "success";
          toast("User logged in successfully.", this.toastConfig);
          // set user data
          this.props.setUserData(res["data"]);
          this.props.setUserLoggedIn(true);
        } else {
          this.toastConfig["type"] = "error";
          toast(res["message"], this.toastConfig);
        }
      });
    }
  };

  handleSocialLoginFailure = (err) => {
    // if (this.state.clickSocial != '') {
    //   this.setState({
    //     alert: {
    //       toast: true,
    //       toastMessage: 'Something went wrong.',
    //       severity: 'error'
    //     },
    //   });
    // }
  };

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node;
    }
  }

  logout = () => {
    AuthenticationService.logout();

    this.props.setUserData({});
    this.props.setUserLoggedIn(false);
  };

  redirectToWatchlist = () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      Router.push("/watchlist");
    } else {
      $("#loginpopup").modal("show");
    }
  };

  render() {
    const { formData, mailSendingStatus, error, errorMessage } = this.state;
    const { currentUser, isLoggedIn } = this.props;
    var name = currentUser["name"] ? currentUser["name"].split(" ") : "";
    var email = currentUser["email"] ? currentUser["email"].split("@") : "";
    var userName = "";
    if (name) {
      var firstLater = name[0] ? name[0].charAt(0) : "";
      var secondLater = name[1] ? name[1].charAt(0) : "";
      userName = firstLater + secondLater;
    } else {
      var firstLater = email[0] ? email[0].charAt(0) : "";
      var secondLater = email[0] ? email[0].charAt(1) : "";
      userName = firstLater + secondLater;
    }
    return (
      <React.Fragment>
        {/* <ToastContainer /> */}
        <ul className="h_right_menubar get_app_menu" style={{ right: "145px" }}>
          <li className="responsive-d-none user-profile-nifty">
            <a title="Get App">Get App</a>
            <ul className="grid_dropdown get_app_dropdown">
              <li>
                <div className="right_dropwon_grid">
                  <section className="h_download_our_app">
                    <div className="container-fluid">
                      <div className="row">
                        <div className="col-lg-6 text-right h_app_right">
                          <div className="app-img-box">
                            <img
                              src="/images/dwnload-app-mobile-header.png"
                              className="img-fluid"
                              alt="App-img"
                            />
                          </div>
                        </div>
                        <div className="col-lg-6 h_app_left">
                          <div className="dwnload_app_content">
                            <span>Download Now !</span>
                            <h5>NiftyTrader App</h5>
                            <p>
                              Stock Screener, NSE BSE Market Pulse: NiftyTrader
                            </p>
                            <div className="qr-box">
                              <img
                                src="/images/nifty-app-qr.png"
                                alt="QR Code"
                              />
                            </div>
                            <a
                              href="https://play.google.com/store/apps/details?id=in.niftytrader&hl=en_IN"
                              target="_blank"
                              className="googl-play-btn"
                            >
                              <img
                                src="/images/google-play-app.png"
                                alt="Google play Button"
                              />
                            </a>
                            <h6>Over 200k+ Downloads in India</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
                <div className="clearfix"></div>
              </li>
            </ul>
          </li>
        </ul>
        <ul
          className="h_right_menubar"
          style={{
            right: Object.keys(currentUser).length > 0 ? "70px" : "70px",
            width: "70px",
          }}
        >
          <li className="responsive-d-none user-profile-nifty">
            {/* <Link href="/"> */}
            <a title="IN" className="dropdown-country">
              <img
                src="/images/in-flag.svg"
                width="27"
                height="27"
                style={{ margin: "-4px 4px 0px 0px !important" }}
              />{" "}
              IN
            </a>
            {/* </Link> */}
            <ul className="grid_dropdown grid_dropdown-country">
              <li>
                <a href="https://www.stockathon.com/" target="_blank">
                  {/* <div className="flag_grid_dropdown_icon_left">
                  </div> */}
                  <div className="right_dropwon_grid">
                    <span>
                      <img
                        src="/images/us-flag.svg"
                        className="country-flag"
                        alt=""
                        width="24"
                        height="24"
                        style={{ marginRight: "7px !important" }}
                      />
                      US
                    </span>
                  </div>
                  <div className="clearfix"></div>
                </a>
              </li>
            </ul>
          </li>
          <li
            className="menu_pipes responsive-d-none"
            style={{ marginLeft: "-24px" }}
          ></li>
          {/* <li aria-haspopup="true" className="responsive-d-none">
                        <Link href="/watchlist">
                          <a title="Watchlist" className="pr-lg-0">
                            <FontAwesomeIcon icon={faStar} width="20" height="18" />
                          </a>
                        </Link>
                      </li> */}
        </ul>
        {!isLoggedIn ? (
          <ul className="h_right_menubar">
            <li
              aria-haspopup="true"
              className="user-profile-nifty responsive-d-none"
            >
              <a
                onClick={this.openLoginPopup}
                className="active-register-btn-responsive active-register-btn"
                title="Login"
              >
                Login
              </a>
            </li>
            <li
              className="menu_pipes responsive-d-none"
              style={{ marginLeft: "-24px" }}
            ></li>
          </ul>
        ) : (
          <React.Fragment>
            {currentUser["name"] ? (
              <ul className="h_right_menubar">
                <li className="responsive-d-none user-profile-nifty">
                  {/* <Link href="/"> */}
                  <a
                    title={currentUser["name"]}
                    className="active-register-btn text-center text-uppercase"
                  >
                    {userName}
                  </a>
                  {/* </Link> */}
                  <ul className="grid_dropdown">
                    <li>
                      <div className="grid_dropdown_icon_left grid_dropdown_icon_none">
                        <img
                          src="/images/loginuser.png"
                          className=""
                          alt=""
                          width="24"
                          height="24"
                        />
                      </div>
                      <div className="right_dropwon_grid">
                        <span className="text-capitalize">
                          {currentUser["name"]}
                          {currentUser["membership_flag"] == 1 ? (
                            <img
                              src="/images/premium-cap.svg"
                              alt="icon"
                              className="nif_prime_member"
                              title="Prime member"
                            />
                          ) : (
                            ""
                          )}
                        </span>
                        <address>{currentUser["email"]}</address>
                      </div>
                      <div className="clearfix"></div>
                    </li>
                    <hr />
                    {/* <li>
                            <Link href="/my-profile">
                              <a title="My Profile" className="active">
                                <FontAwesomeIcon icon={faUser} width="18" height="18" /> My Profile
                          </a>
                            </Link>
                          </li> */}
                    <li>
                      <Link href="/watchlist">
                        <a title="My Watchlist" className="d-block">
                          <FontAwesomeIcon
                            icon={faStar}
                            width="18"
                            height="18"
                          />{" "}
                          My Watchlist
                        </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/stockalerts">
                        <a title="Stock Alerts" className="d-block">
                          <FontAwesomeIcon
                            icon={faUserClock}
                            width="18"
                            height="18"
                          />{" "}
                          Stock Alerts
                        </a>
                      </Link>
                    </li>
                    <li onClick={this.logout}>
                      <a title="Logout">
                        <FontAwesomeIcon
                          icon={faPowerOff}
                          width="18"
                          height="18"
                        />{" "}
                        Logout
                      </a>
                    </li>
                  </ul>
                </li>
                <li
                  className="menu_pipes responsive-d-none"
                  style={{ marginLeft: "-24px" }}
                ></li>
                {/* <li aria-haspopup="true" className="responsive-d-none">
                        <Link href="/watchlist">
                          <a title="Watchlist" className="pr-lg-0">
                            <FontAwesomeIcon icon={faStar} width="20" height="18" />
                          </a>
                        </Link>
                      </li> */}
              </ul>
            ) : (
              ""
            )}
          </React.Fragment>
        )}

        <div
          className="modal login-modal"
          id="loginpopup"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="loginpopup"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <button
                  type="button"
                  className="close close-modal-btn"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
                <div className="row ml-0 mr-0">
                  {!isLoggedIn ? (
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                      <form
                        id="loginform"
                        className="pl-3 pr-3 pb-4 text-center"
                      >
                        <h5 className="modal-title mt-3">LOGIN</h5>
                        <p className="mb-3 text-left p-0">
                          Welcome! Log into your account
                        </p>
                        <div className="form-group save-searchbar">
                          <input
                            className="form-control"
                            type="text"
                            placeholder="Enter your email id"
                            name="email"
                            autoFocus="autofocus"
                            value={formData["email"]}
                            onChange={this.handleFormInput("email")}
                          />
                          {error && errorMessage["email"] ? (
                            <div className="validation-message">
                              {errorMessage["email"]}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group save-searchbar">
                          <input
                            className="form-control"
                            type="password"
                            placeholder="Enter your password"
                            name="password"
                            autoFocus="autofocus"
                            value={formData["password"]}
                            onChange={this.handleFormInput("password")}
                          />
                          {error && errorMessage["password"] ? (
                            <div className="validation-message">
                              {errorMessage["password"]}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          <button
                            type="button"
                            className="btn modal-save-btn modal-login-btn w-100"
                            title="Login"
                            onClick={this.loginUser}
                          >
                            {" "}
                            Login{" "}
                          </button>
                        </div>
                        <p className="mt-3 mb-0 text-capitalize">
                          <a
                            id="forgotpassword1"
                            onClick={this.openForgotPasswordPopup}
                          >
                            {" "}
                            Forgot your password? Get help
                          </a>
                        </p>
                        <p className="text-capitalize">
                          <a id="Reg" onClick={this.openRegisterPopup}>
                            {" "}
                            Register Now
                          </a>
                        </p>
                        <h6 className="pt-0">Login with Social Network</h6>
                        {/* <a className="social-btn" href="Login?SocialLogin=Google&amp;returnUrl=" title="Google" target="_top"><i
                        className="fab fa-google"></i> Google</a>
                      <a className="social-btn social-f-btn" href="Login?SocialLogin=Facebook&amp;returnUrl=" title="Facebook"
                        target="_top"><i className="fab fa-facebook"></i> Facebook</a> */}
                        <a title="Google" className="social-btn">
                          <SocialButton
                            provider="google"
                            appId={process.env.GOOGLE_LOGIN_CLIENT_ID}
                            onLoginSuccess={this.handleSocialLogin}
                            onLoginFailure={this.handleSocialLoginFailure}
                            getInstance={this.setNodeRef.bind(this, "google")}
                            key={"google"}
                          >
                            <FontAwesomeIcon
                              icon={faGoogle}
                              width="13"
                              height="14"
                            />{" "}
                            Google
                          </SocialButton>
                        </a>
                        <a title="Facebook">
                          <SocialButton
                            provider="facebook"
                            appId={process.env.FACEBOOK_LOGIN_CLIENT_ID}
                            onLoginSuccess={this.handleSocialLogin}
                            onLoginFailure={this.handleSocialLoginFailure}
                            className="social-btn social-f-btn"
                            // clicksocial={this.state.clickSocial}
                          >
                            <FontAwesomeIcon
                              icon={faFacebookF}
                              width="13"
                              height="14"
                            />{" "}
                            Facebook
                          </SocialButton>
                        </a>
                      </form>
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="col-xl-6 col-lg-6 col-md-6 login-img col-sm-6 d-none d-sm-block">
                    <img
                      src="/images/modal-login.png"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal login-modal"
          id="signuppopup"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="signuppopup"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <button
                  type="button"
                  className="close close-modal-btn"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
                <div className="row ml-0 mr-0">
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <form
                      id="registerform"
                      className="pl-3 pr-3 pb-3 text-center"
                    >
                      <h5 className="modal-title mt-3">Register</h5>
                      <div className="form-group save-searchbar mb-3">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Name"
                          name="name"
                          autoFocus="autofocus"
                          value={formData["name"]}
                          onChange={this.handleFormInput("name")}
                        />
                        {error && errorMessage["name"] ? (
                          <div className="validation-message">
                            {errorMessage["name"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="form-group save-searchbar mb-3">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="E-mail"
                          name="email"
                          autoFocus="autofocus"
                          value={formData["email"]}
                          onChange={this.handleFormInput("email")}
                        />
                        {error && errorMessage["email"] ? (
                          <div className="validation-message">
                            {errorMessage["email"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="form-group save-searchbar mb-3">
                        <input
                          className="form-control"
                          type="text"
                          maxLength="10"
                          placeholder="Mobile Number"
                          name="phoneNumber"
                          autoFocus="autofocus"
                          value={formData["phoneNumber"]}
                          onChange={this.handleFormInput("phoneNumber")}
                        />
                        {error && errorMessage["phoneNumber"] ? (
                          <div className="validation-message">
                            {errorMessage["phoneNumber"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="form-group save-searchbar mb-3">
                        <input
                          className="form-control"
                          type="password"
                          placeholder="Password"
                          name="password"
                          autoFocus="autofocus"
                          value={formData["password"]}
                          onChange={this.handleFormInput("password")}
                        />
                        {error && errorMessage["password"] ? (
                          <div className="validation-message">
                            {errorMessage["password"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="form-group save-searchbar mb-3">
                        <input
                          className="form-control"
                          type="password"
                          placeholder="Confirm Password"
                          name="confirmPassword"
                          autoFocus="autofocus"
                          value={formData["confirmPassword"]}
                          onChange={this.handleFormInput("confirmPassword")}
                        />
                        {error && errorMessage["confirmPassword"] ? (
                          <div className="validation-message">
                            {errorMessage["confirmPassword"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customCheck19"
                          name="terms"
                          checked={formData["terms"]}
                          onChange={this.handleFormInput("terms")}
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customCheck19"
                        >
                          I agree to all{" "}
                          <a
                            href="/terms-and-conditions"
                            target="_blank"
                            title="Terms and Conditions"
                          >
                            Terms and Conditions
                          </a>{" "}
                          of NiftyTrader
                        </label>
                        {error && errorMessage["terms"] ? (
                          <div className="validation-message">
                            {errorMessage["terms"]}
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="custom-control custom-checkbox mb-1 modal-checkbox">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customCheck20"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customCheck20"
                        >
                          I agree to receive TA e-mails from NiftyTrader
                        </label>
                      </div>
                      <div className="form-group ">
                        <button
                          type="button"
                          className="btn modal-save-btn modal-login-btn w-100"
                          title="Register"
                          disabled={!formData["terms"]}
                          onClick={this.registerUser}
                        >
                          Register
                        </button>
                      </div>
                      <p className="mt-1 mb-1 text-capitalize">
                        <a
                          id="loginpopup2"
                          title="Login"
                          onClick={this.openLoginPopup}
                        >
                          Login
                        </a>
                      </p>
                    </form>
                  </div>
                  <div className="col-xl-6 col-lg-6 col-md-6 login-img col-sm-6 d-none d-sm-block">
                    <img
                      src="/images/modal-login.png"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal login-modal"
          id="forgotpasswordpopup"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="loginpopup"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <button
                  type="button"
                  className="close close-modal-btn"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
                <div className="row ml-0 mr-0">
                  {!isLoggedIn ? (
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                      <form
                        id="loginform"
                        className="pl-3 pr-3 pb-4 text-center"
                      >
                        <h5 className="modal-title mt-3">FORGOT PASSWORD</h5>
                        <p className="mb-3 text-left p-0">
                          A Link Will Be Sent To Your Email Id To Set Your
                          Password.
                        </p>
                        <div className="form-group save-searchbar">
                          <input
                            className="form-control"
                            type="text"
                            placeholder="Enter your email id"
                            name="email"
                            autoFocus="autofocus"
                            value={formData["email"]}
                            onChange={this.handleFormInput("email")}
                          />
                          {error && errorMessage["email"] ? (
                            <div className="validation-message">
                              {errorMessage["email"]}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="form-group">
                          {!mailSendingStatus ? (
                            <button
                              type="button"
                              className="btn modal-save-btn modal-login-btn w-100"
                              title="Forgot Password"
                              onClick={this.sendMail}
                              disabled={!formData["email"]}
                            >
                              {" "}
                              {mailSendingStatus
                                ? "Mail Sending..."
                                : "Forgot Password"}{" "}
                            </button>
                          ) : (
                            <button
                              type="button"
                              className="btn modal-save-btn modal-login-btn w-100"
                              title="Forgot Password"
                              onClick={this.sendMail}
                              disabled={true}
                            >
                              {" "}
                              {mailSendingStatus
                                ? "Mail Sending..."
                                : "Forgot Password"}{" "}
                            </button>
                          )}
                        </div>
                        <p className="text-capitalize">
                          <a onClick={this.openLoginPopup}> Login</a>
                        </p>
                      </form>
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="col-xl-6 col-lg-6 col-md-6 login-img col-sm-6 d-none d-sm-block">
                    <img
                      src="/images/modal-login.png"
                      alt=""
                      className="img-fluid"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// export default Login;
export default connect(mapStateToProps, mapDispatchToProps)(Login);
