import React, { useState, useEffect, Component } from "react";

class StockUpdates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      niftyStocksData: [],
    };
  }

  render() {
    const { nifty50Data } = this.props;
    var stockData = '';
    if (nifty50Data && nifty50Data.length > 0) {
      var stockData = nifty50Data.map((item, key) => {
        return (
          <a
            key={key}
            href={process.env.OLD_SITE_URL + 'stocks-analysis/' + item["symbol_name"].toLowerCase()}
            title={item["symbol_name"]}>
            <div className="ticker_stock">
              {item["symbol_name"]}
              <span>{item["last_trade_price"]}</span>
            </div>
            {item["change_per"] > 0 ? (
              <div className="ticker_stock_up">
                {item["change_per"]}%{" "}
                <img src="/images/up-icon.png" alt="icon" />
              </div>
            ) : (
              <div className="ticker_stock_down">
                {item["change_per"]}%{" "}
                <img src="/images/down-icon.png" alt="icon" />
              </div>
            )}
          </a>
        );
      })
    }

    return (
      <div className="tcontainer main_ticker">
        <div className="ticker-wrap">
          <div className="ticker-move">
            {stockData && stockData != ''
              ? stockData
              : ""}
          </div>
        </div>
      </div>
    );
  }
}

export default StockUpdates;
