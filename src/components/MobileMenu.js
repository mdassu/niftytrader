import Link from "next/link";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faChartBar,
  faChartLine,
  faStar,
  faTabletAlt,
  faDesktop,
  faRupeeSign,
  faCalculator,
  faNewspaper,
  faSignInAlt,
  faChartArea,
  faPowerOff,
  faUserClock,
} from "@fortawesome/free-solid-svg-icons";
import { Component } from "react";
import StockSearch from "./StockSearch";
import { AuthenticationService } from "_services/AuthenticationService";
import Router from "next/router";

const mapStateToProps = state => ({
  currentUser: state.userData.userData,
  isLoggedIn: state.userData.isLoggedIn
});

const mapDispatchToProps = (dispatch) => ({
  setUserData: data => dispatch(userData(data)),
  setUserLoggedIn: data => dispatch(userLoggedIn(data))
});

class MobileMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLogged: false
    }
  }

  componentDidMount() {
  }

  openLoginPopup = () => {
    $('#loginpopup').modal('show');
  }

  openWatchlistPopup = () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      Router.push('/watchlist');
    } else {
      this.openLoginPopup();
    }
  }

  openStockAlertPopup = () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      Router.push('/stockalerts');
    } else {
      this.openLoginPopup();
    }
  }

  logout = () => {
    AuthenticationService.logout();
    this.props.setUserData({})
    this.props.setUserLoggedIn(false)
  }

  render() {
    const { isLoggedIn } = this.props;
    return (
      <ul className="wsmenu-list" id="mobile-menu" >
        <li className="nifty-logo pl-3 pr-3">
          <div className="dropDownMen mobile-menu-search">
            {/* <!-- <label htmlFor="" className="select-cmpy mr-3">Select Stock </label> --> */}
            {/* <input type="text" className="typeahead stocks_query" autoComplete="off" spellCheck="true"
              placeholder="Search Stocks to Analyze" />
            <i className="fa fa-search" aria-hidden="true"></i> */}
            <StockSearch placeHolder="Search Stocks" />
          </div>
          {/* <!-- <a href="homepage-3.php" title="NiftyTrader">
          <img src="img/logo-white.svg" alt="" className="inner-page-logo" />
        </a> --> */}
        </li>
        <hr className="h_destop_hidden" />
        <span className="mobile-menu-label">Analytics</span>
        <li>
          <a href={process.env.OLD_SITE_URL + "stocks-analysis"} className="navtext">
            <FontAwesomeIcon
              icon={faChartBar}
              width="14"
              height="14"
              className="hideIcon"
            />Stocks Analytics</a>
        </li>
        <li>
          <Link href="/live-analytics">
            <a className="navtext" title="SGX Nifty">
              <FontAwesomeIcon
                icon={faChartLine}
                width="14"
                height="14"
                className="hideIcon"
              />Nifty50 Analytics</a>
          </Link>
        </li>
        <li>
          <Link href="/banknifty-live-analysis">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faChartArea}
                width="14"
                height="14"
                className="hideIcon"
              />Bank Nifty Analytics </a>
          </Link>
        </li>
        <hr className="h_destop_hidden" />
        <span className="mobile-menu-label"> Stock Selection</span>
        <li onClick={this.openWatchlistPopup}>
          {/* <Link href="/watchlist"> */}
          <a className="navtext">
            <FontAwesomeIcon
              icon={faStar}
              width="14"
              height="14"
              className="hideIcon"
            />Watchlist </a>
          {/* </Link> */}
        </li>
        <li>
          <Link href="/advance-stock-screener">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faTabletAlt}
                width="14"
                height="14"
                className="hideIcon"
              />Advance Stock Screener </a>
          </Link>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "advanced-stock-screener"} className="navtext">
            <FontAwesomeIcon
              icon={faDesktop}
              width="14"
              height="14"
              className="hideIcon"
            />Advanced Stock Screener </a>
        </li>
        <hr className="h_destop_hidden" />
        <span className="mobile-menu-label">Option Chain</span>
        <li>
          <Link href="/live-nifty-open-interest">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faChartBar}
                width="14"
                height="14"
                className="hideIcon"
              />Nifty Open Interest Tracker </a>
          </Link>
        </li>
        <li>
          <Link href="/banknifty-live-oi-tracker">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faChartLine}
                width="14"
                height="14"
                className="hideIcon"
              />Bank Nifty Open Interest Live Chart </a>
          </Link>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "options-trading"} className="navtext">
            <FontAwesomeIcon
              icon={faChartArea}
              width="14"
              height="14"
              className="hideIcon"
            /> Stock Option Chain </a>
        </li>
        <li>
          <Link href="/usd-inr-options-live">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faRupeeSign}
                width="14"
                height="14"
                className="hideIcon"
              /> USD INR Options Live </a>
          </Link>
        </li>
        <li>
          <Link href="/live-finnifty-open-interest">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faChartBar}
                width="14"
                height="14"
                className="hideIcon"
              /> Fin Nifty Open Interest Tracker </a>
          </Link>
        </li>
        <hr className="h_destop_hidden" />
        <span className="mobile-menu-label">Calculators </span>
        <li>
          <a href={process.env.OLD_SITE_URL + "option-pricing-calculator"} className="navtext">
            <FontAwesomeIcon
              icon={faCalculator}
              width="14"
              height="14"
              className="hideIcon"
            /> Option Pricing Calculator </a>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "fibonacci-calculator-2"} className="navtext">
            <FontAwesomeIcon
              icon={faCalculator}
              width="14"
              height="14"
              className="hideIcon"
            /> Fibonacci Calculator </a>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "pivot-calculator"} className="navtext">
            <FontAwesomeIcon
              icon={faCalculator}
              width="14"
              height="14"
              className="hideIcon"
            /> Pivot Calculator </a>
        </li>
        <hr className="h_destop_hidden" />
        <span className="mobile-menu-label">Other Links </span>
        <li>
          <Link href="/sgx-nifty">
            <a className="navtext">
              <FontAwesomeIcon
                icon={faChartArea}
                width="14"
                height="14"
                className="hideIcon"
              /> SGX Nifty </a>
          </Link>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "news"} className="navtext">
            <FontAwesomeIcon
              icon={faNewspaper}
              width="14"
              height="14"
              className="hideIcon"
            /> News </a>
        </li>
        <li>
          <a href={process.env.OLD_SITE_URL + "money/ipo/"} className="navtext">
            <FontAwesomeIcon
              icon={faRupeeSign}
              width="14"
              height="14"
              className="hideIcon"
            /> Money </a>
        </li>
        <li onClick={this.openStockAlertPopup}>
          {/* <Link href="/stockalerts"> */}
          <a title="Stock Alerts" className="navtext">
            <FontAwesomeIcon icon={faUserClock} width="14" height="14" className="hideIcon" /> Stock Alerts
                          </a>
          {/* </Link> */}
        </li>
        <li>
          {
            isLoggedIn ? (
              <a onClick={this.logout} className="navtext">
                <FontAwesomeIcon
                  icon={faPowerOff}
                  width="14"
                  height="14"
                  className="hideIcon"
                /> Logout </a>
            ) : (
              <a onClick={this.openLoginPopup} className="navtext">
                <FontAwesomeIcon
                  icon={faSignInAlt}
                  width="14"
                  height="14"
                  className="hideIcon"
                /> Login/Sign Up </a>
            )
          }
        </li>
        {/* <ul className="header-social mobile-show">
        <a href="https://twitter.com/niftytraderin" className="twitter-icons" title="Linkedin" target="_blank">
          <i className="fab fa-linkedin-in"></i>
        </a>
        <a href="https://www.facebook.com/niftytraderin/" className="facebook-icons" title="Facebook" target="_blank">
          <i className="fab fa-facebook-f"></i>
        </a>
        <a href="https://www.youtube.com/channel/UC6JvXGzemVCW9FMUjkLBLrA?sub_confirmation=1" className="youtube-icons"
          title="Youtube" target="_blank">
          <i className="fab fa-youtube"></i>
        </a>
        <a href="https://twitter.com/niftytraderin" className="twitter-icons" title="Twitter" target="_blank">
          <i className="fab fa-twitter"></i>
        </a>
        <a href="https://www.instagram.com/niftytraderin/" className="instagram-icons" title="Instagram" target="_blank">
          <i className="fab fa-instagram"></i>
        </a>

        <a href="https://play.google.com/store/apps/details?id=in.niftytrader&hl=en_IN" className="facebook-icons"
          title=" Download NiftyTrader App" target="_blank">
          <img
            src="https://icons-for-free.com/iconfiles/png/512/apps+game+googleplay+market+store+icon-1320168256573652215.png"
            alt="" />
        </a>
      </ul> */}
      </ul >
    )
  }
}

// export default MobileMenu;
export default connect(mapStateToProps, mapDispatchToProps)(MobileMenu);