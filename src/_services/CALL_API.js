import { httpGet, httpGetSignalR, httpPostTemp } from '_helper/ApiBase';

export const CALL_API = (method, url, params, cb) => {
  if (method == 'GET') {
    httpGet(url, {
      params: params, paramsSerializer: (params) => {
        // Sample implementation of query string building
        let result = '';
        Object.keys(params).forEach(key => {
          result += `${key}=${encodeURIComponent(params[key])}&`;
        });
        return result.substr(0, result.length - 1);
      }
    }, (response) => {
      try {
        if (response.result == 1) {
          var res = {
            status: true,
            data: response['resultData'],
            message: response['resultMessage']
          };
        } else {
          var res = {
            status: false,
            data: {},
            message: response['resultMessage']
          };
        }
        cb(res);
      } catch (err) {
        throw err;
        // cb(err)
      }
    });
  } else {
    httpPostTemp(url, params, (response) => {
      try {
        if (response.result == 1) {
          var res = {
            status: true,
            data: response['resultData'],
            message: response['resultMessage']
          };
        } else {
          var res = {
            status: false,
            data: {},
            message: response['resultMessage']
          };
        }
        cb(res);
      } catch (err) {
        throw err;
        // cb(err)
      }
    });
  }
}

export const CALL_SIGNALR_API = (method, url, params, cb) => {
  if (method == 'GET') {
    httpGetSignalR(url, {
      params: params, paramsSerializer: (params) => {
        // Sample implementation of query string building
        let result = '';
        Object.keys(params).forEach(key => {
          result += `${key}=${encodeURIComponent(params[key])}&`;
        });
        return result.substr(0, result.length - 1);
      }
    }, (response) => {
      try {
        if (response.result == 1) {
          var res = {
            status: true,
            data: response['resultData']
          };
        } else {
          var res = {
            status: false,
            data: {}
          };
        }
        cb(res);
      } catch (err) {
        throw err;
        // cb(err)
      }
    });
  } else {
    httpPostTemp(url, params, (response) => {
      try {
        if (response.result == 1) {
          var res = {
            status: true,
            data: response['resultData']
          };
        } else {
          var res = {
            status: false,
            data: {}
          };
        }
        cb(res);
      } catch (err) {
        throw err;
        // cb(err)
      }
    });
  }
}
