import Cookies from 'js-cookie';
import { BehaviorSubject } from 'rxjs';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import Router from 'next/router';

var data = null;

const currentUserSubject = new BehaviorSubject(data);

export const AuthenticationService = {
  setCurrentUserSubject,
  logout,
  numberWithCommas,
  currentUserSubject,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() { return currentUserSubject.value },
  // get isLogged() {
  //   if (this.currentUserValue && this.currentUserValue != null) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
};

function setCurrentUserSubject(userData) {
  currentUserSubject.next(userData);
}

function logout() {
  // remove user from local storage to log user out
  Cookies.remove('_currentUser');
  Cookies.remove('_accessToken');
  currentUserSubject.next(null);
  // Router.push('/');
}

function numberWithCommas(val) {
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}