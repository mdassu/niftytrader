import { BehaviorSubject } from 'rxjs';
const latestNewsSubject = new BehaviorSubject(null);

export const NewsService = {
  setLatestNews,
  getLatestNews: latestNewsSubject.asObservable(),
};

function setLatestNews(newsData) {
  latestNewsSubject.next(newsData);
}