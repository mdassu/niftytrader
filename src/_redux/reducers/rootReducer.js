// import reducer from './index';
// import { combineReducers } from 'redux';

// const rootReducer = combineReducers({
//   userData: reducer
// });

// export default rootReducer;

import { combineReducers } from 'redux'
import userData from './index'

export default combineReducers({
  userData,
})