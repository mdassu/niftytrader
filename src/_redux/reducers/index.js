import { LOGGED_IN, USER_DATA } from '../constants';

const userData = (state = { userData: {}, isLoggedIn: false }, action) => {
  switch (action.type) {
    case USER_DATA:
      return { ...state, userData: action['data'] };
    case LOGGED_IN:
      return { ...state, isLoggedIn: action['data'] };
    default:
      return state;
  }
};

export default userData;