import { LOGGED_IN, USER_DATA } from '../constants'

//Action Creator
export const userData = (data) => ({
  type: USER_DATA,
  data: data
});

export const userLoggedIn = (data) => ({
  type: LOGGED_IN,
  data: data
});