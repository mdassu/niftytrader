import Layout from 'components/Layout';
import Analytics from 'components/Analytics/LiveAnalytics/Analytics';

export default function OptionsScreener(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <Analytics title="NIFTY 50 Live Analytics" reqType="liveanalysis" symbol="NIFTY 50" sarType="JNSAR" sar10Type="J10SAR" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=live-analytics&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
