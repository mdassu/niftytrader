import Layout from 'components/Layout';
import OtherSiteLinks from 'components/OtherSiteLinks';
import MarketUpdates from 'components/MarketUpdates';
import NiftyPCR from 'components/Options/NiftyOI/NiftyPCR';

export default function FinNiftyPutCallRatio(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <NiftyPCR title="Fin Nifty Put Call Ratio | Fin Nifty Option Chain" requestType="finniftypcr" symbol="NIFTY FIN SERVICE" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=finnifty-put-call-ratio&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
