import Layout from 'components/Layout';
import OtherSiteLinks from 'components/OtherSiteLinks';
import MarketUpdates from 'components/MarketUpdates';
import NiftyChangeInOI from 'components/Options/NiftyOI/NiftyChangeInOI';

export default function FinNiftyLiveChangeInOI(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <NiftyChangeInOI title="Fin Nifty OI Live Chart : Fin Nifty Option Chain" requestType="finniftyoichange" symbol="NIFTY FIN SERVICE" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=finnifty-live-change-in-oi&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
