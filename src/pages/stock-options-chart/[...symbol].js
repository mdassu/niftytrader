import React, { Component } from 'react';
import Router from 'next/router';
import { CALL_API, CALL_SIGNALR_API } from '_services/CALL_API';
import Layout from 'components/Layout';
import ColumnLineMixedChart from 'components/Charts/ColumnLineMixedChart';
import { QuickLinks } from 'components/Options/NiftyOI/QuickLinks';
import Loader from 'components/Loader';
import Link from 'next/link';
import RightSection from 'components/RightSection';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faCaretUp
} from "@fortawesome/free-solid-svg-icons";
import * as signalR from "@microsoft/signalr";

class NiftyStock extends Component {

  constructor(props) {
    super(props);
    this.state = {
      symbol: '',
      chartData: [],
      stockList: [],
      symbolSpotData: {},
      isChartLoaded: false,
      isLoaded: false
    }
    this.connection = "";
  }

  componentDidMount() {
    this.getSignalRData();
    this.getSymbolList();
    this.setState({
      symbol: Router.query.symbol[0]
    }, () => {
      this.getOptionStockData(this.state.symbol);
    });
  }

  componentDidUpdate() {
    var symbol = Router.query.symbol[0];
    if (symbol != this.state.symbol) {
      this.getSymbolList();
      this.setState({
        symbol: symbol
      }, () => {
        this.getOptionStockData(this.state.symbol);
        if (this.connection != '') {
          this.connection.stop();
        }
        this.getSignalRData();
      });
    }
  }

  getSignalRData = () => {
    if (Router.query.symbol[0]) {
      this.connection = new signalR.HubConnectionBuilder()
        .withUrl("https://signalr.niftytrader.in/NiftySignalRTest/niftySignalRHub")
        .build();

      this.connection.on("sendTickDataToAndroidAtSA", (InstrumentToken, SymbolName, TickData) => {
        var spotData = {
          high: TickData['high'],
          low: TickData['low'],
          nifty_value: TickData['lastPrice'],
          open: TickData['open'],
          previous_close: TickData['close']
        };
        this.setState({
          symbolSpotData: spotData
        });
      });

      this.connection.start()
        .then(() => {
          this.addSymbolForSignalR(this.connection.connectionId);
        });

      this.getSymbolSpotData();
    }
  }

  getSymbolSpotData() {
    var symbol = '';
    if (Router.query.symbol[0].toUpperCase() == 'NIFTY') {
      symbol = Router.query.symbol[0].toUpperCase() + ' 50';
    } else {
      symbol = Router.query.symbol[0].toUpperCase();
    }
    CALL_API('POST', process.env.SYMBOL_SPOT_DATA, { symbol: symbol }, res => {
      this.setState({
        symbolSpotData: res['data']
      });
    });
  }

  addSymbolForSignalR = (connectionId) => {
    var symbol = '';
    if (Router.query.symbol[0].toUpperCase() == 'NIFTY') {
      symbol = Router.query.symbol[0].toUpperCase() + ' 50';
    } else {
      symbol = Router.query.symbol[0].toUpperCase();
    }
    CALL_SIGNALR_API('GET', process.env.ADD_SYMBOL_FOR_SIGNALR, { Symbol: symbol, ConnectionId: connectionId }, res => {

    });
  }

  getOptionStockData(symbol) {
    CALL_API('POST', process.env.OPTION_STOCK_CHART_DATA, { symbol: symbol }, res => {
      if (res.status) {
        this.setState({
          isChartLoaded: true,
          chartData: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          chartData: [],
          isLoaded: true
        });
      }
    });
  }

  getSymbolList() {
    CALL_API('GET', process.env.SYMBOL_LIST_DATA, {}, res => {
      if (res.status) {
        this.setState({
          stockList: res['data']
        });
      } else {
        this.setState({
          stockList: []
        });
      }
    });
  }

  // changeChartData(symbol) {
  //   this.setState({
  //     isChartLoaded: false,
  //     chartData: [],
  //     isLoaded: false
  //   }, () => {
  //     this.getOptionStockData(symbol);
  //   })
  // }

  componentWillUnmount = () => {
    // if (Router.query) {
    if (this.connection != '') {
      this.connection.stop();
    }
    // }
  }

  render() {
    const { metaData } = this.props;
    const { symbol, chartData, stockList, symbolSpotData, isLoaded, isChartLoaded } = this.state;
    var indexClose;
    if (chartData && chartData.length > 0) {
      indexClose = chartData[0]['index_close'];
    }
    var columnName = ['PUT OI', 'CALL OI', 'PUT Price', 'CALL Price'];
    var columnKey = ['puts_oi', 'calls_oi', 'puts_ltp', 'calls_ltp'];
    var chartTitle = `Live Option Interest Chart: ${symbol.toUpperCase()}`;
    var columnColor = ['#f96c92', '#2196f3', '#5ea4a7', '#d085b9'];
    var xName = 'strike_price';
    var allSymbolList = [];
    if (stockList && stockList.length > 0) {
      allSymbolList = stockList.map((item, key) => {
        var changePer = Number(((item['today_close'] - item['prev_close']) / item['today_close']) * 100).toFixed(2);
        return (
          <Link href={`/stock-options-chart/${item['symbol_name'].toLowerCase()}`}>
            <div key={key} className="col-lg-4 col-12 col-sm-6 col-md-6 padding_right_remove_0">
              <a title={item['symbol_name']}
              ><div className="technical_company">
                  <div className="float-left">
                    <h6 className="text-uppercase">{item['symbol_name']}</h6>
                    <span className="max-plan-value">Max Pain: {item['max_pain']}</span>
                  </div>
                  <div className="right_value float-right">
                    {item['today_close']}<span className={Math.sign(changePer) == -1 ? 'low_values' : 'high_values'}>{changePer}%</span>
                  </div>
                  <div className="clearfix"></div></div
                ></a>
            </div>
          </Link>
        )
      });
    }

    if (symbolSpotData) {
      var niftyValue = symbolSpotData['nifty_value'];
      var niftyValueDiff = Number(symbolSpotData['nifty_value'] - symbolSpotData['previous_close']).toFixed(2);
      var changePer = Number((niftyValueDiff / symbolSpotData['nifty_value']) * 100).toFixed(2);
    }

    return (
      <Layout metaData={metaData}>
        {
          isLoaded ? "" : <Loader />
        }
        <div className="row">
          <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 p-0 pr-4">
            <div className="sidebar-search searchbar-responsive">
              <div className="form-group">
                <form action="">
                  <div className="ui-widget">
                    <input
                      placeholder="Search"
                      className="stocksearching autocomplete-ui ui-autocomplete-input"
                      autocomplete="off"
                    />
                    <span className="fa fa-search"></span>
                  </div>

                  <a
                    id="HideStockNavigator"
                    style={{ display: 'none' }}
                    href="stocks-analysis/acc"
                    className="d-none"
                  ></a>
                </form>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-9 col-xl-9 col-12">
                <h1 className="main-page-heading">{symbol.toUpperCase()} : Stock Options Chart</h1>
              </div>

              <div className="col-12 col-lg-3 col-xl-3">
                {
                  symbolSpotData && Object.keys(symbolSpotData).length > 0 ? (
                    <div className="text-right">
                      <span
                        id="StockLTP"
                        style={{ fontSize: '15px' }}
                        className={Math.sign(niftyValueDiff) == -1 ? 'newstockLTPDown' : 'newstockLTPUP'}
                      >{niftyValue}</span>{" "}
                      <FontAwesomeIcon icon={Math.sign(niftyValueDiff) == -1 ? faCaretUp : faCaretUp} className={Math.sign(niftyValueDiff) == -1 ? 'percent-red' : 'percent-green'} width="10" height="16" />{" "}
                      <b className={Math.sign(niftyValueDiff) == -1 ? 'color-text-red' : 'color-text'} id="StockPriceChange"
                      >{" "}{niftyValueDiff} ({changePer}%)</b>
                    </div>
                  ) : ''
                }
              </div>
            </div>

            <div className="nifty-chart-panel">
              <div className="row m-0">
                <div className="col-md-12 col-sm-12 col-xl-12 col-lg-12 col-12 p-0">
                  <div className="select-dropdown nifty-chart-dropdown">
                    <div className="form-group">
                      <a
                        href="/options-trading"
                        className="nifty-table-icon float-right"
                      ><span className="table-tooltip-box">Table View</span></a
                      >
                    </div>
                  </div>
                </div>
              </div>

              <div style={{ height: '400px' }} id="container">
                {
                  isChartLoaded ? (<ColumnLineMixedChart chartData={chartData} columnName={columnName} columnKey={columnKey} chartTitle={chartTitle} columnColor={columnColor} xName={xName} />) : ''
                }
              </div>
            </div>
            <div className="max_plain_div">
              <div className="clearfix"></div>
              <div className="row stock_listings">{allSymbolList}</div>
            </div>

            <div className="max_plain_content">
              <div className="max_plain_block">
                <h2>What is “Stock Options Chart for derivative stocks”?</h2>

                <p>
                  The NSE Option Chain chart for stocks above shows open interest data
                  for stock options that are trading on NSE India. The total open
                  interest of Puts and Calls is visible for each strike price for that
                  particular stock.
</p>

                <h3>How to use these charts?</h3>

                <p>
                  The charts can be used to identify the support and resistance levels
                  based on Open Interest data. Generally speaking, the strike price
                  with highest OI of Calls is considered a resistance. Similarly, the
                  strike price with highest Open Interest of Puts of considered a
                  resistance level for that stock
                  </p>

                <p>
                  If a particular stock has a market cap of INR 1 Trillion and OI is
                  just ten thousand then the Open Interest data should not be used to
                  identify levels for that particular stock.
                  </p>

                <p>
                  Also, if the stock is in sharp uptrend or the momentum is very
                  strong, the trader should not rely on support and resistance from
                  these charts. The major supports and resistances get broken during
                  strong up or down moves.
</p>

                <h3>
                  Are these charts reliable in identifying support and resistance
                  levels?
</h3>

                <p>
                  The charts should not be relied upon for the stocks where the open
                  interest is very low. The higher the open interest of a particular
                  stock, the better.
</p>
              </div>
            </div>
            {
              metaData && metaData['page_Content'] != '' ? (
                <div className="max_plain_content">
                  <div className="max_plain_block">
                    <p className="mb-0" dangerouslySetInnerHTML={{ __html: metaData['page_Content'] }}>
                    </p>
                  </div>
                </div>
              ) : ''
            }
          </div>

          <RightSection />
        </div>
      </Layout>
    )
  }

}

export default NiftyStock;

export async function getServerSideProps(context) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=stock-options-chart/{Stocks}&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  var symbol = context.query['symbol'][0].toUpperCase();
  metaData['page_title'] = metaData['page_title'].replace(/ShareSymbolName/g, symbol);
  metaData['page_keyword'] = metaData['page_keyword'].replace(/ShareSymbolName/g, symbol);
  metaData['page_description'] = metaData['page_description'].replace(/ShareSymbolName/g, symbol);
  metaData['page_canonical'] = metaData['page_canonical'].replace(/ShareSymbolName/g, symbol);
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}