import Layout from 'components/Layout';
import OtherSiteLinks from 'components/OtherSiteLinks';
import MarketUpdates from 'components/MarketUpdates';
import NiftyOptionTracker from 'components/Options/NiftyOI/NiftyOptionTracker';

export default function BankNiftyLiveOITracker(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <NiftyOptionTracker title="Bank Nifty Open Interest Live Chart: Bank Nifty Option Chain" requestType="bankniftyoi" pcrRequestType="bankniftypcr" symbol="NIFTY BANK" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=banknifty-live-oi-tracker&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
