import Layout from "components/Layout";
import Home from "components/Home";
import { Component } from "react";

class DefaultPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if ("serviceWorker" in navigator) {
      navigator.serviceWorker
        .register("/serviceWorker.js")
        .then((registration) => {
          console.log("service worker registration successful");
        })
        .catch((err) => {
          console.warn("service worker registration failed", err.message);
        });
    }
  }

  render() {
    const { metaData } = this.props;
    return (
      // <Layout metaData={metaData}>
      <Home />
      // </Layout>
    );
  }
}

export default DefaultPage;

export async function getServerSideProps(props) {
  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=index&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: "GET", // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
    },
  });

  var data = await response.json();
  metaData = data["resultData"];
  if (metaData) {
    return {
      props: {
        metaData: metaData,
      },
    };
  } else {
    return {
      props: {}, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;
}

// export default function DefaultPage() {
//   return (
//     <Layout>
//       <Home />
//     </Layout>
//   )
// }
