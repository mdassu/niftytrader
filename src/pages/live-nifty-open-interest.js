// import Layout from "components/Layout";
import OtherSiteLinks from "components/OtherSiteLinks";
import MarketUpdates from "components/MarketUpdates";
import NiftyOptionTracker from "components/Options/NiftyOI/NiftyOptionTracker";
import NiftyLayout from "_layout/NiftyLayout";

export default function LiveNiftyOpenInterest(props) {
  const { metaData } = props;
  return (
    // <NiftyLayout metaData={metaData}>
    <NiftyOptionTracker
      title="Nifty Open Interest Live Chart : Nifty Option Chain"
      requestType="niftyoilist"
      pcrRequestType="niftypcr"
      symbol="NIFTY 50"
      pageContent={metaData}
    />
    // </NiftyLayout>
  );
}

export async function getServerSideProps(props) {
  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=live-nifty-open-interest&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: "GET", // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
    },
  });

  var data = await response.json();
  metaData = data["resultData"];
  if (metaData) {
    return {
      props: {
        metaData: metaData,
      },
    };
  } else {
    return {
      props: {}, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;
}
