import Layout from 'components/Layout';
import NiftyChangeInOI from 'components/Options/NiftyOI/NiftyChangeInOI';

export default function BankNiftyLiveOIChange(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <NiftyChangeInOI title="Bank Nifty Change in Open Interest: Bank Nifty Option Chain | NiftyTrader" requestType="bankniftyoichange" symbol="NIFTY BANK" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=bank-nifty-live-oi-change&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
