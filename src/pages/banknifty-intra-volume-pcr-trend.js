import Layout from 'components/Layout';
import OtherSiteLinks from 'components/OtherSiteLinks';
import MarketUpdates from 'components/MarketUpdates';
import NiftyPCRVolume from 'components/Options/NiftyOI/NiftyPCRVolume';

export default function BankNiftyIntraVolumnPCRTrend(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <NiftyPCRVolume title="Bank Nifty Volume Put Call Ratio | Bank Nifty Option Chain" requestType="bankniftyvolumepcr" symbol="NIFTY BANK" pageContent={metaData} />
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=banknifty-intra-volume-pcr-trend&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
