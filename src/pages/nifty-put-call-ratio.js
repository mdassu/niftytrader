import Layout from "components/Layout";
import NiftyPCR from "components/Options/NiftyOI/NiftyPCR";

export default function NiftyPutCallRatio(props) {
  const { metaData } = props;
  return (
    // <Layout metaData={metaData}>
    <NiftyPCR
      title="Nifty Put Call Ratio | Nifty Option Chain"
      requestType="niftypcr"
      symbol="NIFTY 50"
      pageContent={metaData}
    />
    //</Layout>
  );
}

export async function getServerSideProps(props) {
  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=nifty-put-call-ratio&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: "GET", // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
    },
  });

  var data = await response.json();
  metaData = data["resultData"];
  if (metaData) {
    return {
      props: {
        metaData: metaData,
      },
    };
  } else {
    return {
      props: {}, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;
}
