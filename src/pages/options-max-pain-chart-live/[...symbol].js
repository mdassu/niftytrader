import React, { Component } from "react";
import Router from "next/router";
import { CALL_API } from "_services/CALL_API";
import Layout from "components/Layout";
import GroupColumnsChart from "components/Charts/GroupColumnsChart";
import { QuickLinks } from "components/Options/NiftyOI/QuickLinks";
import Loader from "components/Loader";
import Link from "next/link";
import RightSection from "components/RightSection";
import * as moment from 'moment';
import AdComponent from "components/AdComponent";

class NiftyStock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      symbol: "",
      chartData: [],
      stockList: [],
      lastUpdatedStock: '',
      isChartLoaded: false,
      isLoaded: false,
    };
  }

  componentDidMount() {
    this.getSymbolList();
    this.setState(
      {
        symbol: Router.query.symbol[0],
      },
      () => {
        this.getOptionStockData(this.state.symbol);
      }
    );
    var lastUpdated;
    var currDateTime = moment();
    var currDate = moment().format('MM/DD/YYYY');
    var currDay = moment().format('dddd');
    if (currDay == 'Saturday' || currDay == 'Sunday') {
      lastUpdated = moment().startOf('week').add(5, 'days').format('dddd DD MMM YYYY ') + ' 04:00:00 PM';
    } else if (currDateTime >= moment(currDate + ' 09:00') && currDateTime <= moment(currDate + ' 16:00')) {
      lastUpdated = currDateTime.subtract(4, 'minutes').format('dddd DD MMM YYYY hh:mm:ss A');
    } else {
      lastUpdated = moment(currDate + ' 16:00').format('dddd DD MMM YYYY hh:mm:ss A');
    }
    this.setState({
      lastUpdatedStock: lastUpdated
    });
  }

  componentDidUpdate() {
    var symbol = Router.query.symbol[0];
    if (symbol != this.state.symbol) {
      this.getSymbolList();
      this.setState(
        {
          symbol: symbol,
        },
        () => {
          this.getOptionStockData(this.state.symbol);
        }
      );
    }
  }

  getOptionStockData(symbol) {
    CALL_API(
      "POST",
      process.env.LIVE_MAX_PAIN_DATA,
      { symbol: symbol },
      (res) => {
        if (res.status) {
          this.setState({
            isChartLoaded: true,
            chartData: res["data"],
            isLoaded: true,
          });
        } else {
          this.setState({
            chartData: [],
            isLoaded: true,
          });
        }
      }
    );
  }

  getSymbolList() {
    CALL_API("GET", process.env.SYMBOL_LIST_DATA, {}, (res) => {
      if (res.status) {
        this.setState({
          stockList: res["data"],
        });
      } else {
        this.setState({
          stockList: [],
        });
      }
    });
  }

  // changeChartData(symbol) {
  //   this.setState({
  //     isChartLoaded: false,
  //     chartData: [],
  //     isLoaded: false
  //   }, () => {
  //     this.getOptionStockData(symbol);
  //   })
  // }

  render() {
    const { metaData } = this.props;
    const {
      symbol,
      chartData,
      stockList,
      lastUpdatedStock,
      isLoaded,
      isChartLoaded,
    } = this.state;
    var maxPainValue;
    if (stockList && stockList.length > 0) {
      maxPainValue = stockList.filter(item => {
        return item['symbol_name'] == symbol.toUpperCase();
      })[0]['max_pain'];
    }
    var columnName = ["PP", "CP"];
    var columnKey = ["pp", "cp"];
    var chartTitle = `Option Max Pain Live Chart: ${symbol.toUpperCase()}.  Max Pain Level currently at: ${maxPainValue}`;
    var columnColor = ["#f96c92", "#2196f3"];
    var xName = "strike_price";
    var allSymbolList = [];
    if (stockList && stockList.length > 0) {
      allSymbolList = stockList.map((item, key) => {
        var changePer = Number(
          ((item["today_close"] - item["prev_close"]) / item["today_close"]) *
          100
        ).toFixed(2);
        return (
          <Link
            href={`/options-max-pain-chart-live/${item[
              "symbol_name"
            ].toLowerCase()}`}
          >
            <div
              key={key}
              className="col-lg-4 col-12 col-sm-6 col-md-6 padding_right_remove_0"
            >
              <a title={item["symbol_name"]}>
                <div className="technical_company">
                  <div className="float-left">
                    <h6 className="text-uppercase">{item["symbol_name"]}</h6>
                    <span className="max-plan-value">
                      Max Pain: {item["max_pain"]}
                    </span>
                  </div>
                  <div className="right_value float-right">
                    {item["today_close"]}
                    <span
                      className={
                        Math.sign(changePer) == -1
                          ? "low_values"
                          : "high_values"
                      }
                    >
                      {changePer}%
                    </span>
                  </div>
                  <div className="clearfix"></div>
                </div>
              </a>
            </div>
          </Link>
        );
      });
    }

    return (
      <Layout metaData={metaData}>
        {
          isLoaded ? "" : <Loader />
        }
        <div className="row">
          <div className="col-md-8 col-lg-9 col-xl-9 col-sm-12 col-12 pl-lg-0 pr-4">
            <div className="sidebar-search searchbar-responsive">
              <div className="form-group">
                <form action="">
                  <div className="ui-widget">
                    <input
                      placeholder="Search"
                      className="stocksearching autoComplete-ui ui-autoComplete-input"
                      autoComplete="off"
                    />
                    <span className="fa fa-search"></span>
                  </div>

                  <a
                    id="HideStockNavigator"
                    style={{ display: "none" }}
                    href="stocks-analysis/acc"
                    className="d-none"
                  ></a>
                </form>
              </div>
            </div>
            <h1 className="main-page-heading">
              {symbol.toUpperCase()} : Max Pain Live Chart For Derivative
                Stocks | NiftyTrader
              </h1>

            <div className="nifty-chart-panel">
              <p className="mb-3">
                Last Data Refresh: {lastUpdatedStock} GMT+05:30 (India
                  Standard Time)
                </p>
              <div className="row m-0">
                <QuickLinks
                  page={
                    symbol == "nifty"
                      ? "niftyOI"
                      : symbol == "finnifty"
                        ? "finNiftyOI"
                        : "bankNiftyOI"
                  }
                  index={0}
                />
              </div>
              <div style={{ height: "400px" }} id="container">
                {/* <img src="/images/chart.jpg" className="img-fluid" alt="" /> */}
                {isChartLoaded ? (
                  <GroupColumnsChart
                    chartData={chartData}
                    columnName={columnName}
                    columnKey={columnKey}
                    chartTitle={chartTitle}
                    columnColor={columnColor}
                    xName={xName}
                  />
                ) : (
                  ""
                )}
              </div>
            </div>

            <div className="max_plain_div">
              <p className="float-left max_text">
                Click on the stock symbol below to see Open Interest Max Pain
                Chart Live.
                </p>
              <div className="clearfix"></div>
              <div className="row stock_listings">{allSymbolList}</div>
            </div>

            <div className="max_plain_content">
              <div className="max_plain_block">
                <h5>What is Option Pain?</h5>
                <AdComponent adType="CM_NIFTYTRADER_WC1_RSPV" />
                <p>
                  Before understanding “Options Max Pain Theory”, it is
                  important to understand the meaning of “Options Pain”. It is
                  seen that 90%+ options expire out of the money. Since most
                  options buyers lose money in options trading, the price of
                  the underlying stock somehow must be adjusted / manipulated
                  to close in a way that benefits option writers at the time
                  of options expiry.
                  </p>
                <p>
                  The loss incurred by options buyers is also termed as
                  “Options Pain” for our discussion.
                  </p>
              </div>
              <div className="max_plain_block">
                <h5>What is Options Max Pain Theory?</h5>
                <p>
                  Options Max Pain Theory suggests, “On option expiration day,
                  the underlying stock price often moves toward a point that
                  brings maximum loss to option buyers.”
                  </p>
              </div>
              <div className="max_plain_block">
                <h5>How to calculate Options Max Pain Strike Price?</h5>
                <p>
                  Options Max Pain Theory suggests, “On option expiration day,
                  the underlying stock price often moves toward a point that
                  brings maximum loss to option buyers.”
                  </p>
              </div>
              <div className="max_plain_block">
                <h5>How can a trader benefit from “Max Pain Theory”?</h5>
                <p className="pb-0 mb-1">
                  Traders can utilize this concept to their advantage. Option
                  writing can be done near expiry based on this theory,
                  provided other technical indicators also favor the trade.
                  </p>
                <p className="pt-0">
                  No trade should be taken without extensive study (of
                  technicals or fundamentals of the underlying).
                  </p>
                <p></p>
              </div>
              <div className="max_plain_block">
                <h5>Interpretation of the above chart</h5>
                <AdComponent adType="CM_NIFTYTRADER_WC2_RSPV" />
                <p>
                  With the help of above charts you can identify the strike
                  price corresponding to max pain point in options. The
                  “Options Max Pain” charts are available for all derivative
                  stocks in the “Options Max Pain Chart” above.
                  </p>
              </div>
            </div>

            {
              metaData && metaData['page_Content'] != '' ? (
                <div className="max_plain_content">
                  <div className="max_plain_block">
                    <p className="mb-0" dangerouslySetInnerHTML={{ __html: metaData['page_Content'] }}>
                    </p>
                  </div>
                </div>
              ) : ''
            }
          </div>
          <RightSection />
        </div>
      </Layout>
    );
  }
}

export default NiftyStock;

export async function getServerSideProps(context) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=options-max-pain-chart-live/{Stocks}&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  var symbol = context.query['symbol'][0].toUpperCase();
  metaData['page_title'] = metaData['page_title'].replace(/ShareSymbolName/g, symbol);
  metaData['page_keyword'] = metaData['page_keyword'].replace(/ShareSymbolName/g, symbol);
  metaData['page_description'] = metaData['page_description'].replace(/ShareSymbolName/g, symbol);
  metaData['page_canonical'] = metaData['page_canonical'].replace(/ShareSymbolName/g, symbol);

  metaData['page_title'] = metaData['page_title'].replace(/ShareNameDetails/g, symbol);
  metaData['page_keyword'] = metaData['page_keyword'].replace(/ShareNameDetails/g, symbol);
  metaData['page_description'] = metaData['page_description'].replace(/ShareNameDetails/g, symbol);
  metaData['page_canonical'] = metaData['page_canonical'].replace(/ShareNameDetails/g, symbol);
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
