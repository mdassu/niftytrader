import Layout from 'components/Layout';
import Link from 'next/link';
import ThankYouPurchase from 'components/ThankYouPurchase';

export default function ThankYou(props) {
  const { metaData } = props;
  return (
    <Layout metaData={metaData}>
      <ThankYouPurchase pageContent={metaData}/>
    </Layout>
  )
}

export async function getServerSideProps(props) {

  var url = `${process.env.GET_API_URL}${process.env.META_DATA}?pagename=thank-you&reqType=Newpageseo`;

  var metaData = {};
  var response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    }
  });

  var data = await response.json();
  metaData = data['resultData'];
  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}
