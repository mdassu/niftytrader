const express = require("express");
const next = require("next");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const path = require("path");

const port = process.env.PORT || 3000;

const dev = process.env.NODE_ENV === "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const actualPage = "https://www1.niftytrader.in";
app
  .prepare()
  .then(() => {
    const server = express();

    server.use(express.static(path.join(__dirname, 'public')));

    server.use(
      bodyParser.urlencoded({
        extended: false
      })
    );

    server.use(bodyParser.json());
    server.use(express.json());
    server.use(express.urlencoded({ extended: false }));

    server.use(cookieParser());

    // server.get("/stock/:symbol", (req, res) => {
    //   const actualPage = "/stock";
    //   const queryParams = { symbol: req.params.symbol };
    //   app.render(req, res, actualPage, queryParams);
    // });

    function redirectTo302(url) {
      var finalUrl = url;
      if (url == '/eod-screener') {
        finalUrl = '/advanced-stock-screener'
      }
      server.get(url, (req, res) => {
        res.redirect(302, actualPage + finalUrl);
      });
    }

    redirectTo302('/stocks-analysis');
    redirectTo302('/free-course');
    redirectTo302('/money/ipo/');
    redirectTo302('/5paisa-2020-brokerage-charges-trading-platforms');
    redirectTo302('/broker-directory');
    redirectTo302('/options-trading');
    redirectTo302('/option-strategy');
    redirectTo302('/eod-screener');
    redirectTo302('/advanced-stock-screener');
    redirectTo302('/nse-stocks-volume');
    redirectTo302('/nse-stocks-price');
    redirectTo302('/gap-ups-gap-downs');
    redirectTo302('/opening-price-clues');
    redirectTo302('/bulk-deals-data');
    redirectTo302('/ban-list');
    redirectTo302('/investing-books');
    redirectTo302('/candlestick-patterns');
    redirectTo302('/select-best-broker');
    redirectTo302('/stock-brokers-in-india');
    redirectTo302('/fii-stats');
    redirectTo302('/fii-dii-activity');
    redirectTo302('/nse-fo-lot-size');
    redirectTo302('/bse-nse-trading-holidays');
    redirectTo302('/commodity-trading-holidays');
    redirectTo302('/option-pricing-calculator');
    redirectTo302('/fibonacci-calculator-2');
    redirectTo302('/pivot-calculator');
    redirectTo302('/developing-pivots');
    redirectTo302('/news');
    redirectTo302('/options-trading');
    redirectTo302('/about-us');
    redirectTo302('/terms-and-conditions');
    redirectTo302('/privacy-policy');
    redirectTo302('/disclaimers');
    redirectTo302('/limitation-of-liability');
    redirectTo302('/refund-and-cancellation-policy');
    redirectTo302('/contact-us');
    redirectTo302('/opening-price-clues');
    redirectTo302('/advanced-fibonacci-calculator');
    redirectTo302('/upstox-2019-brokerage-charges-trading-platforms');
    redirectTo302('/icici-direct-stock-trading-demat-account-brokerage');
    redirectTo302('/angel-broking-2020-brokerage-charges-trading-platforms');
    redirectTo302('/zerodha-2019-brokerage-charges-trading-platforms');
    redirectTo302('/money/ipo/blog/top-5-upcoming-ipos-in-2020.php');
    redirectTo302('/nifty50-contributors');
    redirectTo302('/terms');

    server.get("/options-max-pain-chart-live/stocks-analysis/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(301, '/options-max-pain-chart-live/' + symbol);
    });

    server.get("/stock-options-chart/STOCKS-ANALYSIS", (req, res) => {
      res.redirect(301, '/stock-options-chart/nifty');
    });

    server.get("/stock-options-chart/stocks-analysis/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(301, '/stock-options-chart/' + symbol);
    });

    server.get("/options-trading/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(302, actualPage + '/options-trading/' + symbol);
    });
    server.get("/terms/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(302, actualPage + '/terms/' + symbol);
    });
    server.get("/stocks-analysis/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(302, actualPage + '/stocks-analysis/' + symbol);
    });
    server.get("/terms/:key/:symbol", (req, res) => {
      const key = req.params.key;
      const symbol = req.params.symbol;
      res.redirect(302, actualPage + '/terms/' + key + '/' + symbol);
    });
    server.get("/brokerage/:symbol", (req, res) => {
      const symbol = req.params.symbol;
      res.redirect(302, actualPage + '/brokerage/' + symbol);
    });

    server.use(function (req, res, next) {
      if (req.host.indexOf("www.") !== 0) {
        res.redirect(301, req.protocol + "://www." + req.host + req.originalUrl);
      } else {
        next();
      }
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });
    server.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
